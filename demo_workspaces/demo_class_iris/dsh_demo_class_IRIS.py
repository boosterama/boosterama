# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 12:40:37 2015

@author: Data Primos
"""

import sys
if not "../../boosterama" in sys.path:
    sys.path.append("../../boosterama")
from my_Dashboard_Reiterators import *
from my_Dashboard_Utils import *

'''
Hay  480  combinaciones de dashboards
dict_find_diff - iteraremos sobre  5  variables:  
    ['PRP_FLOAT_QUANTILE', 'PRP_MONOTONIC_INT', 'PRP_MONOTONIC_STR', 'PRP_ONE_HOT_ENCODE', 'PRP_ONE_HOT_ENCODE_THRESHOLD']
'''


def CUSTOM_KERAS_MODEL(COMP_EVALUATION_METRIC, OPTIMIZADOR, INIT, DENSE, DROPOUT, XTRAIN_SHAPE):   #d['KERASR']['mym__model']()
    from keras.models import Sequential
    from keras.layers import Dense, Dropout, Activation
    from keras.layers.advanced_activations import PReLU
    from keras.layers.normalization import BatchNormalization
    # 
    model = Sequential()
    
    # LayerGroup 1
    model.add(Dense(DENSE, input_dim= XTRAIN_SHAPE, init= INIT))   #'he_normal'
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(DROPOUT))
    
    ## LayerGroup 2    
    model.add(Dense(int(DENSE), init= INIT))   #'he_normal'
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout((DROPOUT)))
    
    ## LayerGroup 2    
    model.add(Dense(int(DENSE), init= INIT))   #'he_normal'
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout((DROPOUT)))
    

    ## LayerGroup 2    
    model.add(Dense(int(DENSE), init= INIT))   #'he_normal'
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout((DROPOUT)))
    


    
    
    # LayerGroup 3
    model.add(Dense(1, init= INIT))  #'he_normal'
    
    model.compile(loss= COMP_EVALUATION_METRIC, optimizer= OPTIMIZADOR)  #(loss = 'mae', optimizer = 'adadelta')
    return(model)



############
# Algoritmos
############

algoritmos = {
              
"Ada":  {'mym__n_estimators': [10, 20, 50, 70]},
"DT":   {'mym__max_depth': [5, 30]},
"ELN":  {'mym__alpha': [0.001, 0.0001, 0.00001, 0.000001],
         'mym__l1_ratio':[0,0.5,1]},  
"ETC":  {
        "mym__n_estimators"          : [700],
        "mym__max_features"          : [30], 
        "mym__criterion"             : ['entropy'],
        "mym__min_samples_split"     : [5],
        "mym__max_depth"             : [50],
        "mym__min_samples_leaf"      : [3]},   
"KNN":   {'n_neighbors'            :[5],
         'n_jobs'                  :[4]},
"LR":   {'mym__fit_intercept':[True],
         'mym__normalize':[False]},
"RF":   {'mym__max_depth': [20],
         'mym__n_estimators': [10]},

"RIDG": {'mym__alpha': [0.01, 0.001]},
"SGD":  {'mym__loss': ['log', 'hinge'],
         'mym__penalty': ['l1', 'l2', 'elasticnet'],
         'mym__alpha': [0.001, 0.0001, 0.00001, 0.000001]},
"SVM":  {'mym__C': [2],
         'mym__gamma': [0.5]},

"XGBC_B":  {          #requiere 'ML_GRIDSEARCHCV_OR_ALGORITHM*' : ['Algorithm']
            "mym__objective"             : ['binary:logistic'],
            #reg:logistic requiere valores de clase entre 0 y 1
              'verbose_eval'               : [200],
              'mym__early_stopping_rounds' : [20],
              'mym__num_rounds'            : [50013],
              'mym__eta'                   : [0.01],#0.05],
              #'mym__eval_metric'           : ['rmse'], #
              'mym__max_depth'             : [6],
              'mym__min_child_weight'      : [2],
              #'mym__scale_pos_weight'      : [1],
              'mym__subsample'             : [0.8],
              'mym__colsample_bytree'      : [0.6],
              #'mym__alpha'                 : [0.4],  # def=0
              #'mym__gamma'                 : [0],  # def=0
              #'mym__base_score'            : [0.5],
              #'mym__silent'                : [1],
              #'mym__learning_rate'         : [0.05],
              #'mym__max_delta_step'        : [2],
              #'mym__n_estimators'          : [200],
          },
}




dashboard={

################################################################################################
#  MACHINE LEARNING
######################
'LEVEL' : '1_ML', # [ITER] {string} ('1_ML', '1_EXTERNAL_SCRIPTS', '1_EXTERNAL_PREDICTIONS', '2_ML', '2_ENSEMBLE', '2_CARUANA', '3_CARUANA', '3_BASIC_ENSEMBLE', '0_DELETE', '0_FEAT_IMP')

'ITERATOR' : 'grid', # 'grid', 'fastloop'

'ML_ALGO' : ["XGBC_B"], # NO ITERABLE {list of strings} Elegir Algoritmos (["KERASR"],["LR"],["XGBC_B"],["GBR"],["RFR"],["ELN"],["SGDR"],["RIDG"])
# CLASSIFICATION: Ada=Adaboost  DT=DecissionTree   RF=RandomForest  SGD=Stochastic Gradient Descent  SVM=Support Vector Machine    
# REGRESSION:     XGBR =Xgboost   AdaR=AdaBoostRegressor  ELN=ElasticNet  GBR=GradientBoostingRegressor  LR=LinearRegression   RFR=RandomForestRegressor  RIDG=Ridge  SGDR=SGDRegressor

'RANDOM_SEED' : 123,


################################################################################################
# CrossValidation
###################
'SPLIT_CV_TYPE'              : 'Random', #'Random','Stratified', 'TimeSeries'

'SPLIT_CV_KFOLDS'            : 3, # 0  ===>  Train = 100%, Val = 0% \\\  1  ===> ejecución normal con Val \\\   >= 2  ===> CrossValidation
'SPLIT_CV_KFOLDS_EARLY_STOP' : 1,  # {int}  0  ===>  Does all folds normally \\\  1  ===> stops on first fold if score is not the best. otherwise continues with rest of folds.

'SPLIT_VALIDATION_SET_PERC'  : 0,      #  {float16} (between 0.00 and 1.00)  # Only when ('SPLIT_CV_KFOLDS' = 1) AND ('SPLIT_CV_TYPE' = 'Random' or 'Stratified')


# CrossValidation for TimeSeries    
# # # # # # # # # # # # # # # # #    (section ignored unless 'SPLIT_CV_TYPE' = 'TimeSeries') 
'SPLIT_TS_TYPE*'               : ['TS_CV_TRAIN-ORIGIN_VAL-FIX'],  #  {string}   'TS_CV_ACADEMIC', 'TS_CV_TRAIN-ORIGIN_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
# Num_folds taken from    'SPLIT_CV_KFOLDS'  value
'SPLIT_TS_train_FIX_NUM*'      : [1000],                 #  {int} number of observations (starting from last one) of the train set
'SPLIT_TS_VALIDATION_FIX_NUM*' : [240],                 #  {int} number of observations (starting from last one) of the validation set
# 'SPLIT_VALIDATION_SET_PERC' is ignored => we use 'SPLIT_TS_VALIDATION_FIX_NUM'


################################################################################################
#Transformaciones X
####################
'PRP_DATE_ENCODE*'          : [()],  # [ITER]  [([list],[list],int,int,int)]  ([()]=Don't do,['year','month','yearmonth','dayofweek','dayofyear','day','weekofyear','quarter','hour','isweekend','owen_Day_-1','owen_Month_-1', 'owen_dayofweek', 'isofficehours','daysfromfirst']) datetime64 features converted to these Integers
        #([date_cols],['year','month'], do_Owen, DROP_New_after)    # Siempre borramos la variable original
'PRP_OWEN_2_DATE_2_FEATURE*'  : [()],  #[([list],str,[list of lists])],   # ([()]=Don't do)  ejemplo1:  [(['Car_Type'],'Date_',[[-1,-1],[-2,-2],[-7,-7],[-1,-7]])],  ejemplo2:  [(['y_str_orig'],'Date_',[[-7,-7],[-1,-7]])],  Both dates included. The order for period end/start does NOT matter.
                   # [([scope], 'date column', [[start_period, end_period]])]

'PRP_FLOAT_ENCODE*'         : [()],      # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)] convierte a categórica floats y luego hace Owen y Dropea Orig/Encoded
'PRP_INTEGER_ENCODE*'       : [()],      # [ITER] [([list],int,int,int)]    [()]=Don't do  ejemplo: [(['y_int'],1,0,1)] convierte a categórica integers y luego hace Owen y Dropea Orig/Encoded
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)
'PRP_LABEL_ENCODE_LEX*'     : [(['y_str_orig'], 0, 0, 0)],      # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_Obj'],1,0,1)]  Convierte categóricas a integers usando lógica lexicográfica. Importante. Si NO se selecciona, el ML no funcionará si hay categoricas.  
       # ([scope], do_owen, DROP_orig_after, DROP_New_after)

'PRP_FLOAT_QUANTILE*'       : [(),(['y_float'],1,0,1),(['y_float'],0,0,0)],  # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)]   convierte a cuantiles y hace owen.  ejemplos: [['y_float_LMen17']]   Siempre empieza por y_  o por o_
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)

'PRP_MONOTONIC_INT*'        : [[],['y_int_Top7'],['y_int_Top3'],['y_int_Top1']],     # [ITER] {[[string]]} ([[]]=Don't do)  convierte a counts los campos integers (mantiene features originales) ejemplos: [['y_int_LMen17']]  Siempre empieza por y_  o por o_
'PRP_MONOTONIC_STR*'        : [[],['y_str_top3'],['y_str_top5'],['y_str_top2']],     # [ITER] {[[string]]} ([[]]=Don't do)  convierte a counts los campos strings (mantiene features originales) ejemplo:  [['y_str_LMas17_LMen50']]   Siempre empieza por y_  o por o_


'PRP_NORMALIZE_NUMERIC*'    : [()],# [ITER] [(list,string)]  [()]=Don't do   ("Rescaling", "Standarizing", "Other")  Ejemplo:  [(['y_float'],'Standarizing')]
   # ([scope], method)
'PRP_X_TRANSFORM*'          : [()],      # [ITER] [(list,string)]  [()]=Don't do ('boxcox0.25', 'log','square','exp','sqrt')
    # ([scope], method)



'PRP_ONE_HOT_ENCODE*'        : [(),(['y_Obj'],1,0,0),(['y_Obj_top3'],0,0,0),(['y_Obj_top1'],0,0,0),(['y_Obj_top3'],1,0,0)],     # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_Obj'],1,0,1)] 
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)
'PRP_ONE_HOT_ENCODE_THRESHOLD*': [4,40],     # [ITER] [int]   []=Don't do  ejemplo: [4]  Minimum frequency of a value to apply OneHotEncode (below it, it will be grouped under the value 'other')


'PRP_OWEN_TRICK*'            : [()],        # {[[string]]} ([()]=Don't do)  ejemplos: [(['y_num_LMen17'], None, 0)] [(['y_obj_LMen17'],['mean','std'], 1)]  [(['y_str_LMas17_LMen50'],['mean','std'], 0)]  [(['o_obj_LMen17'],['mean','std'], 1)]   Siempre empieza por y_  o por o_
                          # ([scope], [metrics], DROP_orig_after)
'PRP_OWEN_TRICK_METRICS*'    : [['default']],  #default metric: 'default','mean','mode','std','max','min','sum'.  
                                    # using 'default' as metric, boosterama uses:  - 'mode' for multiclass problems  - 'mean' otherwise
'PRP_OWEN_TRANSFORM*'        : ['No'],  # 'No', 'log' to apply log after the agg/metric
'PRP_OWEN_RANDOM_RANGE*'     : [0.05],  #  random factor to multiply owen values on train to avoid overfitting. Range will be  VALUE * random(-RANDOM_RANGE , +RANDOM_RANGE)


'PRP_OWEN_2_FEATURE*'        : [()],   # ([()]=Don't do)  ejemplos:  [(['y_all'],['mean','std'],'Species',0)]
                    # [([scope], [metrics], 'variable_name_2_reference',DROP_orig_after)]


'PRP_REPLACER*'              : [[]], #  [()]=Don't do,   Ejemplo: [[(['y_num'],1,111),(['y_str'],'Blue','MILL')]]
                  # [[([SCOPE], REPLACE_THIS, PUT_THIS)]]
'PRP_REPLACE_RARE_STRINGS*'  : [()], # [ITER]   [(list, int, int, uint16)] [()]=Don't do,   Ejemplo: [(['y_string'],1,0,39)]   (0=No, 1 to 65535 = Threshold) con MENOS de threshold 
                  # ([SCOPE], DO_OWEN, DROP_Orig_after, LMen Threshold)

'PRP_WINSORING*'             : [()],# [ITER] [(list,float,float)] #   [()]=Don't do,     float ratios percentile from 0.0 to 1.0  ejemplo: [(['y_float_int'],0.03,0.97)]
        #([scope], lower_limit, upper_limit)

'PRP_NA_BINARIZE*'           : [[]],         # [ITER] {[[string]]} ([[]]=Don't do)  ejemplo: [['y_int_LMen17']] 
'PRP_NA_FILL*'               : [([['y_num'],-1],[['y_str_obj'],'aftfdgddgGgffdghfgd'])],   # [ITER] {string} ([()]=Don't do, '0'= rellenar a '0', 'mean','median','mode' any integer value IN QUOTES: '0'  '-9999' ...)
               # [([['y_num'],-1],[['y_str_obj'],'aftfdgddgGgffdghfgd'])]


# NLP # # #
'PRP_NLP_TFIDF_SVM*'        : [()],  # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['keyword','title_text'],1,400,10)]  
               # ([scope], DROP_orig_after, TruncatedSVD_Components, SVC_C_param)



#Transformaciones Y
#####################
'ML_PRP_Y_TRANSFORM*'       : ['No'],      # [ITER] {string} ('No'=Don't do,'log','square','exp','sqrt')
'PRP_Y_WINSORING_REMOVE*'   : [(0,0)],# [ITER] {(float,float)} # ((0,0)=Don't do, (lower_limit,upper_limit)) float ratios percentile from 0.0 to 1.0 #Remove Y outlier rows


#Transformaciones Y_test_preds
###############################
'ML_PRP_Y_APPLY_RESTRICTIONS*' : [()],#[ITER] {string}    [()]=Don't do, Ejemplo: [('NegativeToZero','Round','ToInt')]

                                  
#############################################################################################################
##          FEATURE ENGINEERING            
#############################################

# Join/Diff/Multiply these columns in Test and Train
'PRP_COLS_DIFF*'     : [()],   #  Don't do = [()] 
           # [([["CoverageField1B","PropertyField21B"],["GeographicField6A","GeographicField8A"]],0,0,'One')],   #  Don't do = [()] 
'PRP_COLS_JOIN*'     : [()],   #  Don't do = [()]       
'PRP_COLS_MULTIPLY*' : [()],   #  Don't do = [()] 
       # [([(COMBINACION_COLS)], do_Owen, DROP_New_after, One/Many_2/Many_3 )]
       # Ejemplo1:  [([['y_all']],1,0,'One')]        Ejemplo_2:   [([['SepalLength','SepalWidth'],['PetalLength','PetalWidth']],0,0,'One')] 

'PRP_COUNT_BY_ROWS' : (), # {([values1], [values2] ...)}  ([None, 0, "np.nan", -1, "-1", pd.NaT], [0], [-1]) For each list in values tupel, add an extra column counting the ocurrences of values on each row


#############################################
##           FEATURE SELECTION             ##
#############################################

# DROP
'PRP_DROP_NEAR_ZERO_VARIANCE*'               : ['No'],          # (['No']=Don't do)  ejemplo: [0.1]  drops features with variance LOWER than 0.1.  Works better when normalized/standarized features.
'PRP_DROP_CONSTANTS*'                        : [0],     # {uint8} ([0]=No, [1]=Yes)
'PRP_DROP_FEATURES_ALL_NA_IN_TRAIN_OR_TEST*' : [0],     # {uint8} ([0]=No, [1]=Yes) Aparentemente mejor resultado 0

#Drop these columns from Test and Train
'PRP_DROP_THESE_COLUMNS*'       : [[]], #[['v22','v91']],# [ITER]         (  [[]]=Don't do)   Ejemplo: [['y_alt1-2'],['y_alt2-2']]
'PRP_DROP_THESE_COLUMNS_b4_ML*' : [[]],# [ITER]   (  [[]]=Don't do)


'PRP_DROP_ALL_BUT_THESE*'       : [[]],     # [ITER] (  [[]]=Don't do)   Ejemplo: [['PetalWidth'],['SepalLength'],['IsPretty'],['SepalWidth'],['Color'],['PetalLength']]


#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 


#############################################
##               ML LEVEL 2                ##
#############################################
'L2_y_test_preds_PADRE_o_HIJO'  : 'hijo',  # 'padre' o 'hijo'

'L2_QUERY_WHERE'                : "score_validation<0.492858467 and score_train>0.06", #ID_EXECUTION=1612282146476 or ID_EXECUTION=1612281741184   #"score_validation<0.362858467 and score_train>0.06"
'L2_QUERY_ORDER_BY'             : ('score_validation','DESC'),

'L2_CV_OR_DATASET'              : 'CV',  # 'CV' o  'DATASET'
# 'CV' will use all Level1 & Level2 with CV (ignoring the field: d[L2_LEVEL1_DATASETS_TRAIN]).  
# 'DATASET' will take into account d['L2_LEVEL1_DATASETS_TRAIN' and TEST]

'L2_LEVEL1_DATASETS_TRAIN'      : 'level1_Train100%_WR(0,0)_TrainVal70%30%', #level1_Train100%_WR(0,0)_TrainVal70%30%     or  level1_Train100%_WR(0,0)_CV   ... # Ignored when 'L2_CV_OR_DATASET'=='CV'
'L2_LEVEL1_DATASETS_TEST'       : 'level1_test100%',  # usually level1_test100% even for validation  # Ignored when 'L2_CV_OR_DATASET'=='CV'


# # # # # # # # # # # # # # # # # # # # # # #
##               2_ENSEMBLE                ##
# # # # # # # # # # # # # # # # # # # # # # #

# Tomamos en cuenta config de ML Level 2 
# Ignoramos folds, algoritmo, etc...
'L2_ENSEMBLE_NUM_ROUNDS'           : 2,  


# # # # # # # # # # # # # # # # # # # # # # #
##                 CARUANA                 ##
# # # # # # # # # # # # # # # # # # # # # # #

'L2_CARUANA_BY_BLOCKS'                      : 0,                        # {uint8} (0=No, 1=Yes)
'L2_CARUANA_BY_BLOCKS_COLUMN_ID'            : 'VAR_0005',     # {string}
'L2_CARUANA_MAX_ROUNDS'                     : 200,                     # {uint16}
'L2_CARUANA_MAX_ROUNDS_WITHOUT_IMPROVEMENTS': 10, # {uint16}

#############################################
##               ML LEVEL 3                ##
#############################################

'L3_DO_CONVERT_2_RANKING' : 1,



#############################################
##         EXTERNAL_PREDICTIONS            ##
#############################################
'EXTERNAL_PREDICTIONS_IMPORT*' : [()], # [ITER] {lista de tuplas} [()]=Don't do  [(cvPredsFile1, testPredsFile1), (cvPredsFile2, testPredsFile2), ..., (cvPredsFileN, testPredsFileN)]

                                  
#############################################
##             EXTERNAL_SCRIPTS            ##
#############################################
#'EXTERNAL_SCRIPTS_IMPORT' : '1117_XGB_xgboostsingelmodel_12345_aliajouz.py', # [ITER] {string} ([]=Don't do, 'puntuacion_algoritmo_tituloScript_idScript_autor_NNN')
'EXTERNAL_SCRIPTS_IMPORT*' : ['0777_xgb_peazoscript_12344_mesala.py'], # [ITER] {string} ([]=Don't do, 'puntuacion_algoritmo_tituloScript_idScript_autor_NNN')


#############################################
##           DELETE / OVERFIT              ##
#############################################
# Requires 'LEVEL' == '0_DELETE'
'DELETE_OR_OVERFIT'        : "Delete",   # [ITER] {string} ('Delete' / 'Overfit')
# 'Delete' will totally delete execution from DDBB and everywhere
# 'Overfit' will mark the execution as 'overfit' so it won't be used in Level2, Ensemble...   It still will be there, so it won't run again when tried to be run with same dashboard.
'DELETE_OR_OVERFIT_QUERY_WHERE'  : "ID_EXECUTION != 1612282146476",#score_validation<0.492858467 and score_train>0.06", #ID_EXECUTION=1612282146476 or ID_EXECUTION=1612281741184   #"score_validation<0.362858467 and score_train>0.06"


#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 


#############################
#      PRINTS & CHARTS
#############################

# prints
'PRINT_ANALYSIS_ON_TRAIN_TEST' : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_CUSTOM_IMPORT'          : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_CUSTOM_IMPORT_DIFFS'    : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_DASHBOARD'              : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_DROP_INFO'              : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_EVAL_INFO'              : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_NA_INFO'                : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_NA_INFO_SHOW_FIELDS'    : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_QUERY_COLS'             : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_TIME'                   : 0,    # {uint8} (0=No, 1=Yes)  
'PRINT_X_INFO'                 : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_Y_INFO'                 : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_TRANSFORM_Y_INFO'       : 0,    # {uint8} (0=No, 1=Yes)

# charts
'CHART_CONFUSION_MATRIX'       : 0,    # {uint8} (0=No, 1=Yes) 
'CHART_CORRELATIONS'           : 0,    # {uint8} (0=No, 1=Yes)  
'CHART_CORRELATIONS_LONG'      : 0,    # {uint8} (0=No, 1=Yes)  This chart takes a looooong time to finish.
'CHART_HISTOGRAMS'             : 0,    # {uint8} (0=No, 1=Yes)
'CHART_BOXPLOT'                : 0,    # {uint8} (0=No, 1=Yes)



#############################################
##            HDF5,SQLITE SAVE             ##
#############################################
'SAVE_HDF5_EXECUTIONS'          : 1,          # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_CORR_MATRIX'         : 0,         # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_FEATURES_IMPORTANCE' : 0, # {uint8} (0=No, 1=Yes)

'SAVE_SQLITE_EXECUTIONS'        : 1,          # {uint8} (0=No, 1=Yes)

'SAVE_CSV_PREPROCESSED_DATASETS'  : 0,

#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  


#############################################
##                 ITERABLE                ##
#############################################

'ITER_DO_DOUBLE_EXECUTION'                   : 0, #  Caruana... : If dashboard is trained with a subset, ITERATOR_DO_DOUBLE_EXECUTION performs one extra execution trained with the complete trainset predicting the complete test set

'REITERATE'                                  : False,
'REITERATE_MAX_LEVELS'                       : 5,  # 0 - Infinite 
'REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD'  : -1, # -1 to ignore this threshold
#'REITERATE_FEATURE_MIN_ENHACEMENT_THRESHOLD' : 0.001,

'REITERATE_LEVEL'           : 0, # Internal - USUALLY defined as 0.  Indicates in wich level of reiteration this dashboard has been executed

'ITER_ITERABLE_DASHBOARD'   : True, # USUALLY True.  True - iterable variables are list of values, # False - iterable variables contains values


##########################################
##       NA Imputation based on ML      ##
##########################################
'NA_ML_IMPUTATION'            : False,

'NA_ML_FIELDS'                : 'auto',
'NA_ML_EXTRA_COLUMNS_TO_DROP' :  [],



#################################################
##  Parámetros configuración de las funciones  ##
#################################################

#import_data
'IMP_TRAIN_SUBSET_TYPE'  : 'allSamples',   #Para todo el dataset poner 'allSamples'. Otras opciones: 'pPercentageStratifiedRandomSamples','pPercentageRandomSamples', 'nConsecutiveSamples', 'nRandomSamples'
'IMP_TRAIN_PERCENTAGE'   : 7,               #Sólo se usa con TRAIN_SUBSET_TYPE:  'pPercentageRandomSamples'
'IMP_TRAIN_NUM_SAMPLES'  : 1000,            #Sólo se usa con TRAIN_SUBSET_TYPE:  'nConsecutiveSamples' o TRAIN_SUBSET_TYPE:  'nRandomSamples'
'IMP_TRAIN_FIRST_ROW'    : 1,               #Sólo se usa con TRAIN_SUBSET_TYPE:  'nConsecutiveSamples'

'IMP_TEST_SUBSET_TYPE'   : 'allSamples',    #Para todo el dataset poner 'allSamples'. Otras opciones: 'pPercentageStratifiedRandomSamples','pPercentageRandomSamples', 'nConsecutiveSamples', 'nRandomSamples'
'IMP_TEST_PERCENTAGE'    : 10,               #Sólo se usa con TEST_SUBSET_TYPE:  'pPercetageRandomSamples'
'IMP_TEST_NUM_SAMPLES'   : 1000,             #Sólo se usa con TEST_SUBSET_TYPE:  'nConsecutiveSamples' o TEST_SUBSET_TYPE:  'nRandomSamples'
'IMP_TEST_FIRST_ROW'     : 1,                #Sólo se usa con TEST_SUBSET_TYPE:  'nConsecutiveSamples'


#split_train_test_components
'SPLIT_VALIDATION_RANDOM_STATE' : 0,
'ML_ALGO_PARAMETROS*'           : [],  ### NO RELLENAR ### [ITER] {list of dictionaries} ### NO RELLENAR ###, se rellena automáticamente en función de la configuración de algoritmos





#########################################
##                 STEPS               ##
#########################################

'SYS_DO_CUSTOM_IMPORT'         : 0,          # requiere definir el CUSTOM_IMPORT() más abajo
'SYS_DO_CUSTOM_ML'             : 0,          # requiere definir el CUSTOM_ML() más abajo
'SYS_DO_CUSTOM_SUBMISSION_PRE' : 0,          # requiere definir el CUSTOM_SUBMISSION_PRE() más abajo
'SYS_DO_CUSTOM_SUBMISSION_CSV' : 0,          # requiere definir el CUSTOM_SUBMISSION_CSV() más abajo

'ML_FEATURE_IMPORTANCE'        : 1,          # {uint8} (0=No, 1=Yes) 
'SYS_SEND_EMAIL'               : 0,          # {uint8} (0=No, 1=Yes)



#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  



#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  




#############################
#  COMPETITION CONFIGURATION
#############################

#Special Column Names (useful for submissions)
'COMP_NAME'                         : "demo_class_iris",     # {string}
'COMP_PROBLEM_TYPE'                 : 'Class_B',       # {string} ('Regr', 'Class_B', 'Class_M')

'COMP_COLUMN_ID'                    : "Id",                 # {string}
'COMP_COLUMN_CLASS'                 : "Species",          # {string}
'COMP_COLUMN_CLASS_4_SUBMISSION'    : "SepalLength",         # {string}

'COMP_TRAIN_FILES'                  : ['train.csv'],      # {string} list of strings
'COMP_TEST_FILES'                   : ['test.csv'],        # {string} list of strings
'COMP_Y_TEST_FILE'                  : 'y_test.csv',     # {string} list of strings

  #CLASSIFICATION     #Acc - Accuracy Score    #F1 - F1 Score    #LogLoss  #ROC_AUC
  #REGRESSION         #Gini - Gini Score       #MSE  - Mean Squared Error
'COMP_EVALUATION_METRIC'                   : "ROC_AUC",       # {string} #'LogLoss'    'AUC'
'COMP_EVALUATION_METRIC_GREATER_IS_BETTER' : 1, # {uint8} (0=No, 1=Yes)


'ML_PREDICT_PROBA'           : 1,
'ML_CLASS_PROBA'             : 1,


#Convert Column to DATE in Train & Test
'PRP_COLUMN_2_DATE'          : [],        # {[string]} 

#Change Column Names in Train & Test
'PRP_CHANGE_COL_NAMES_OLD'   : [],        # {[string]}
'PRP_CHANGE_COL_NAMES_NEW'   : [],        # {[string]}


}




#############################################
##               CUSTOM IMPORT             ##
#############################################


def CUSTOM_IMPORTER(d,DIR_INPUT_CUSTOM,DIR_TRAIN_PATH,DIR_TEST_PATH,DO_PRINT,DIR_TRAIN_TEST_DIRECTORY,COLUMN_CLASS):
    
    # load training and test datasets
    TRAIN = pd.read_csv(DIR_INPUT_CUSTOM + 'train_set.txt',sep='\t')
    test = pd.read_csv(DIR_INPUT_CUSTOM + 'test_set.txt',sep='\t')
    
    print('sdfadsaasdfasdfasdf')
    print('sdfadsaasdfasdfasdf')
    print('sdfadsaasdfasdfasdf')
    print('sdfadsaasdfasdfasdf')
    print('sdfadsaasdfasdfasdf')
    print('sdfadsaasdfasdfasdf')

    
    if DO_PRINT:
        print("TRAIN columns")
        print(TRAIN.columns)
        print("test columns")
        print(test.columns)
        
        print(specs_data[2:3])
    


 
    if DO_PRINT:
        print("new train columns")
        print(TRAIN.columns)
        print(TRAIN[1:10])
        print(TRAIN.columns.to_series().groupby(TRAIN.dtypes).groups)
    


    
    
    ##########################################################################
    ##   Dejar todo lo que viene a partir de ahí en TODOS los CUSTOM IMPORT
    ##########################################################################

    indice_max_test = max(test[d['COMP_COLUMN_ID']].values)
    TRAIN[d['COMP_COLUMN_ID']] = np.arange(indice_max_test+1,indice_max_test+1+len(TRAIN))
    

    ###############################################################
    # Calculamos dtypes de train y test                           #
    ###############################################################
    
    train_test_dtypes = pd.Series()
        
    X_TRAIN__pd = pd.concat([TRAIN,test],join='inner',axis=0)
    X_cols_names = list(X_TRAIN__pd.columns.values)
    for col in X_cols_names:
        tipo = str(X_TRAIN__pd[col].values.dtype)
        if tipo == 'object' or tipo == 'datetime64[ns]':
            train_test_dtypes[col]='str' 
        else:
            train_test_dtypes[col]=tipo        
    
    y_dtype = str(TRAIN[COLUMN_CLASS].values.dtype)
    if y_dtype == 'object':
        train_test_dtypes[COLUMN_CLASS]='str'
    else:
        train_test_dtypes[COLUMN_CLASS] = y_dtype
        

    ###################################################################################
    # Almacenamos dtypes de train y test en train_test_dtypes.csv en directorio input #
    ###################################################################################
        
    train_test_dtypes.to_csv(DIR_TRAIN_TEST_DIRECTORY+'train_test_dtypes.csv', index=True, header=False)


    ###############################################################
    # Almacenamos train.csv y test.csv en directorio input        #
    ###############################################################
    
    ### Importante: generamos train.csv y test.csv en directorio input
    TRAIN.to_csv(DIR_TRAIN_PATH, index=False, header=True, engine='c')
    test.to_csv(DIR_TEST_PATH, index=False, header=True, engine='c')
    
    return



def CUSTOM_ML(d,idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test):
    #return y_train_preds, y_val_preds, y_test_preds
    pass




def CUSTOM_SUBMISSION_PRE(d, submission, idx_test,y_test_preds,dir_out_path,submission_name,COLUMN_ID,COLUMN_CLASS):
    #return 
    pass


def CUSTOM_SUBMISSION_CSV(d, submission, idx_test,y_test_preds,dir_out_path,submission_name,COLUMN_ID,COLUMN_CLASS):
    #return 
    pass





############################################
###      NO TOCAR A PARTIR DE AQUÍ       ###
############################################


if __name__ == '__main__':
    from my_Dashboard_Iterator import *
    
    runDashboard(dashboard, algoritmos, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
