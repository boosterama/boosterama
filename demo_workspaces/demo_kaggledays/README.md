
# Kaggledays Competition.  Warsaw, May 2018 #

Note:  It's not part of our 2nd place solution teaming up with Paweł Godula and Wojtek Rosinski.
I just hadn't time enough to use boosterama during the competition, and I wanted to give it a try.

Follow the next stept to execute this solution:

* Clone this repository
git clone git@bitbucket.org:boosterama/boosterama.git

* Follow the instructions at [boosterama.io](http://boosterama.io) to install the dependencies

* Download competition [dataset](https://www.kaggle.com/c/kaggledays/data) into demo_workspaces/demo_kaggledays/input

* Go to demo_workspaces/demo_kaggledays/ and execute preprocessing.py
This script creates a new preprocessed_train.csv/preprocessed_test.csv with features that haven't been automated (yet) in boosterama.

* Execute dashboard_kaggledays.py
It's going to create a lot of artifacts (files and folders) that you can explore if you like.
The predictions will be created in demo_workspaces/demo_kaggledays/submissions_Level_1

* Edit and execute submission_patch.py
You'll need to configure your .csv output filename 


Thanks a lot to the speakers, the Kaggledays organizers and Kaggle for this event.
The workshops, the conferences, the competition, the healthy food, the asmotphere, the tour, the party ... was impressive

## DESCRIPTION ##

### Preprocessing and feature engineering ###

* 10 TF-IDF features per question and per answer text

* number of zeroes counting for TF-IDF features

* Capital letters: max_capital_per_word, capital_per_word, period_per_capital

* group_by question: position ordered by time, 0-1 normalized position, and total number of answers

* thank-you counting and sum (group by question): thanks in the following message by time, and sum of thanks in the following messages

* counting the number of references in answer text to some domains: youtube, google, wikipedia, twitter, pcpartpicker...

* subsetting: inter-percentile range (0.00 to 0.90)

### Model ###

* LightGBM

* No hyperparameter tunning

* 5 fold crossvalidation

* log(y) transformation

### posprocessing ###

* clip:  max(1, preds)

* dataleak patch


### final notes ###

There is an improvement room for this model: hyperparameter tunning, bagging... ensembles

I just use this features by intuition and sent it to Kaggle, but I didn't invest in know which ones were performing better or if there was any feature misleading the model.

There are lot of possible enhancements, like converting the dates into weekdays, etcetera.













