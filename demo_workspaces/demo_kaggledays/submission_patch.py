#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 25 17:26:47 2018

@author: virilo
"""

import pandas as pd

filename=# configure here your submssion output, sth like 'NA_0,466412_0,442_CV=5_LGBMR_log_RMSLE_Ver-Unknown_L1_ML_1805281952179(new-starter-3,virilo-features.csv'
filename='submissions_Level_1/'+filename

import sys
#filename= sys.argv[1]

submission=pd.read_csv(filename)
leaks = pd.read_csv("leaked_records.csv").rename(columns={"answer_score": "leak"})

submission = pd.merge(submission, leaks, on="id", how="left")
submission.loc[~submission["leak"].isnull(), "answer_score"] = submission.loc[~submission["leak"].isnull(), "leak"]
del(submission["leak"])
submission.to_csv(filename+'.PATCHED.csv', index=False, header=True, sep=',')

'''
print("compressing...")
import subprocess
command=['7z', 'a', '\"{}.PATCHED.csv.7z\"'.format(filename), '\"{}"'.format(filename+'.PATCHED.csv')]
print(command)
subprocess.call(" ".join(command))
print("done")
'''
