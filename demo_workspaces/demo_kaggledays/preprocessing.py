#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 19 09:20:59 2018

"""

MAX_TFIDF_FEATURES_QUESTION=10
MAX_TFIDF_FEATURES_ANSWER=10

import numpy as np
import pandas as pd
from sklearn.pipeline import make_pipeline, make_union
from sklearn.model_selection import train_test_split

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDRegressor
from sklearn.base import BaseEstimator, TransformerMixin

import time
import re

t_inicio=time.clock()


train_df=pd.read_csv('input/train.csv', sep='\t')
test_df=pd.read_csv('input/test.csv',  sep='\t')

train_ids=train_df[['id']]
test_ids=test_df[['id']]


def rmsle(y, y0):
    assert len(y) == len(y0)
    return np.sqrt(
        np.mean((np.log1p(y) - np.log1p(y0)) ** 2)
    )

''' LEAKAGE ¿¿¿ ???
zz=train_df.loc[train_df["answer_text"]==train_df["question_text"]]
leak=pd.read_csv('leaked_records.csv',  sep=',')
leak.rename(columns={'answer_score':'answer_score_L'}, inplace=True)
z=leak.merge(train_df, on='id', how='inner')
'''





'''

                           CAPITAL LETTERS
                           ===============

'''


def max_capital_per_word(text):
    words=text.split()
    max_cap=0
    for word in words:
        cap=sum(1 for c in word if c.isupper())
        max_cap=max(max_cap, cap)
    return max_cap

def capital_per_word(text):
    num_words=max(1, len(text.split()))
    cap=sum(1 for c in text if c.isupper())
    
    cpw=cap/num_words
    return cpw

def period_per_capital(text):
    periods=text.count('.')
    cap=sum(1 for c in text if c.isupper())
    pvsc=periods/max(1, cap)
    return pvsc

train_df["max_capital_per_word"]=train_df["answer_text"].apply(max_capital_per_word)
test_df["max_capital_per_word"]=test_df["answer_text"].apply(max_capital_per_word)
train_df["capital_per_word"]=train_df["answer_text"].apply(capital_per_word)
test_df["capital_per_word"]=test_df["answer_text"].apply(capital_per_word)
train_df["period_per_capital"]=train_df["answer_text"].apply(period_per_capital)
test_df["period_per_capital"]=test_df["answer_text"].apply(period_per_capital)



'''

                           RANKING TIME ASC
                           ================

'''

train_df=train_df.sort_values(['question_id', 'answer_utc'], ascending=[True, True])
test_df=test_df.sort_values(['question_id', 'answer_utc'], ascending=[True, True])

train_df['question_sec']=train_df.groupby('question_id').cumcount()
test_df['question_sec']=test_df.groupby('question_id').cumcount()

train_df['question_sec_norm']=train_df.groupby('question_id')['question_sec'].apply(lambda x: (x-min(x))/(max(x)-min(x)))
test_df['question_sec_norm']=test_df.groupby('question_id')['question_sec'].apply(lambda x: (x-min(x))/(max(x)-min(x)))
train_df['question_sec_norm']=train_df['question_sec_norm'].fillna(value=0)
test_df['question_sec_norm']=test_df['question_sec_norm'].fillna(value=0)





train_msg_count=train_df.groupby('question_id')['question_sec'].agg(['count'])
test_msg_count=test_df.groupby('question_id')['question_sec'].agg(['count'])
train_msg_count.reset_index(level=0, inplace=True)
test_msg_count.reset_index(level=0, inplace=True)
train_msg_count.rename(columns={'count':'question_num_msg'}, inplace=True)
test_msg_count.rename(columns={'count':'question_num_msg'}, inplace=True)
train_df=train_df.merge(train_msg_count, on='question_id', how='left')
test_df=test_df.merge(test_msg_count, on='question_id', how='left')




'''

                           THANK-YOU
                           =========

'''

train_df=train_df.sort_values(['question_id', 'answer_utc'], ascending=[True, False])
test_df=test_df.sort_values(['question_id', 'answer_utc'], ascending=[True, False])

def count_thanks(text):
    return len(re.findall(r"thank|thx|appreciate|thanku|thanx|graci|obrigado", text, re.IGNORECASE))
train_df["count_thanks"]=train_df["answer_text"].apply(count_thanks)
train_df['count_thanks']=train_df.groupby('question_id')['count_thanks'].shift(1).fillna(0)
test_df["count_thanks"]=test_df["answer_text"].apply(count_thanks)
test_df['count_thanks']=test_df.groupby('question_id')['count_thanks'].shift(1).fillna(0)



train_df['csum_thanks']=train_df.groupby('question_id')['count_thanks'].cumsum()
test_df['csum_thanks']=test_df.groupby('question_id')['count_thanks'].cumsum()





train_df=train_df.sort_values(['question_id', 'answer_utc'], ascending=[True, True])
test_df=test_df.sort_values(['question_id', 'answer_utc'], ascending=[True, True])


'''

                           SORT & SAVE
                           ===========

'''

train_df=train_ids.merge(train_df, on='id', how='left')
test_df=test_ids.merge(test_df, on='id', how='left')


    

train_df.to_csv('./input/preprocessed_train.csv', index=False, header=True, sep='\t')
test_df.to_csv('./input/preprocessed_test.csv', index=False, header=True, sep='\t')


