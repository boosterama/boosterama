# -*- coding: utf-8 -*-
"""

@author: DataCousins
"""

import sys
if not "../../boosterama" in sys.path:
    sys.path.append("../../boosterama")
from my_Dashboard_Reiterators import *
from my_Dashboard_Utils import *
from my_Evaluation_Metrics import RMSLE






'''

┌─┐┬  ┌─┐┌─┐┬─┐┬┌┬┐┬ ┬┌┬┐  ┬ ┬┬ ┬┌─┐┌─┐┬─┐┌─┐┌─┐┬─┐┌─┐┌┬┐┌─┐┌┬┐┌─┐┬─┐┌─┐
├─┤│  │ ┬│ │├┬┘│ │ ├─┤│││  ├─┤└┬┘├─┘├┤ ├┬┘├─┘├─┤├┬┘├─┤│││├┤  │ ├┤ ├┬┘└─┐
┴ ┴┴─┘└─┘└─┘┴└─┴ ┴ ┴ ┴┴ ┴  ┴ ┴ ┴ ┴  └─┘┴└─┴  ┴ ┴┴└─┴ ┴┴ ┴└─┘ ┴ └─┘┴└─└─┘

- See demo_workspaces/demo_reg_iris/dsh_demo_reg_IRIS.py to use other algorithms examples: xgboost, knn, etc.

- You can iterate and try other hyperparameter values to the list:
    
    'mym__learning_rate': [0.1, 0.05, 0.01], 

'''


algorithm_hyperparameters = {


    "LGBMR":  {           #requiere 'ML_GRIDSEARCHCV_OR_ALGORITHM*' : ['Algorithm']
                'mym__bagging_fraction': [0.9], 
                'mym__feature_fraction': [0.4], 
                'mym__num_leaves': [1023], 
                'mym__max_bin': [255], 
                'mym__bagging_freq': [6],
                'mym__algorithm': ['lightgbm'], 
                'mym__objective': ['regression'], 
                'mym__boosting_type': ['gbdt'],  
                'mym__verbose': [0],  
                'mym__task': ['train'], 
                #'mym__metric': [{'l2'}], 
                'mym__feval':[RMSLE],
                'mym__learning_rate': [0.05], 
                'mym__num_rounds': [260], 
                'mym__early_stopping_rounds': [20],
                'data_random_seed':[1234],
                'feature_fraction_seed':[1234],
                'bagging_seed':[1234],
                'drop_seed':[1234],
              }

}


'''

┌┬┐┌─┐┌─┐┬ ┬┌┐ ┌─┐┌─┐┬─┐┌┬┐
 ││├─┤└─┐├─┤├┴┐│ │├─┤├┬┘ ││
─┴┘┴ ┴└─┘┴ ┴└─┘└─┘┴ ┴┴└──┴┘


- See more configurations (input, preprocessing, feature engineering...) at boosterama/my_Dsh_DEFAULT.py

- You can iterate over configurations, adding an ASTERISK, and using a list of the expected value:
    
    without iteration:
        
        'ML_PRP_Y_TRANSFORM'       : 'No',
    
    using boosterama iteration:
        
        'ML_PRP_Y_TRANSFORM*'       : ['No', 'log'],
        
- If you like to use boosterama cache, you've to set your PYTHONHASHSEED:
  Ubuntu users can do it in this way:
      
    echo >>~/.profile
    echo export PYTHONHASHSEED=1234 >>~/.profile
    

'''

dashboard={
        
#'ML_ALGO_PARAMETROS*' : [],  ### NO RELLENAR ### [ITER] {list of dictionaries} ### NO RELLENAR ###, se rellena automáticamente en función de la configuración de algorithm_hyperparameters

'FRIENDLY_ID':'new-starter', 
'HASHTAGS':['virilo-features'],

'AUTHOR':'virilo',

'IMP_TRAIN_SUBSET_TYPE'         : 'interPercentileRange',  #'allSamples', 
'IMP_TRAIN_INTER_PERCENTILE_RANGE': (0.0, 0.90), 

######################
#  MACHINE LEARNING
######################
'LEVEL'       : '1_ML', # [ITER] {string} ('1_ML', '1_EXTERNAL_SCRIPTS', '1_EXTERNAL_PREDICTIONS', '2_ML', '2_ENSEMBLE', '2_CARUANA', '3_CARUANA', '3_BASIC_ENSEMBLE', '0_DELETE', '0_FEAT_IMP')

'ITERATOR'     : 'fastloop', # 'grid', 'fastloop'

'ML_ALGO'      : ["LGBMR"], # NO ITERABLE {list of strings} Elegir Algoritmos (["KERASR"],["LR"],["XGBC_B"],["GBR"],["RFR"],["ELN"],["SGDR"],["RIDG"])
'RANDOM_SEED'  : 1234,


################################################################################################
# CrossValidation
###################
'SPLIT_CV_TYPE'               : 'Index', #'Random','Stratified', 'TimeSeries'
'SPLIT_IMPORT_INDEXES'        : './kfold_Wojtek.csv',
'SPLIT_CV_KFOLDS'             :  5,
'ITER_DO_DOUBLE_EXECUTION'    :  0,



################################################################################################
#Transformaciones X
####################

'PRP_TF_IDF'                : [
                                ('question_text', {'max_features':10, 'token_pattern':"\w+", 'prefix':'q_'}), 
                                ('answer_text', {'max_features':10, 'token_pattern':"\w+", 'prefix':'a_'}),
                              ],

'PRP_REGEX_COUNT'           : [  {   'field':'answer_text',
                                     'regex_list':[
                                         'youtube\\.com|youtu\\.be',
                                         'reddit\\.com',
                                         'wikipedia\\.org',
                                         'imgur\\.com',
                                         'pcpartpicker\\.com',
                                         'github\\.com',
                                         'wikia\\.com',
                                         'twitter\\.com',
                                         'co\\.uk',
                                         'google\\.ca',
                                         'google\\.com',
                                         'com\\.au',
                                         'arstechnica\\.com',
                                         'salon\\.com',
                                         'oxforddictionaries\\.com'
                                     ]
                                 }
                             ],

'PRP_LABEL_ENCODE'        : 0, # Deactivate (it was activated by default)

#Transformaciones Y
#####################
'ML_PRP_Y_TRANSFORM'       : 'log',      # [ITER] {string} ('No'=Don't do,'log','square','exp','sqrt')
#'ML_Y_POSPROCESSING'          : ['preds*0.8', 'preds.clip(0, np.inf)'],
'ML_Y_POSPROCESSING'          : ['preds.clip(1, np.inf)'],


#############################################################################################################
##          FEATURE ENGINEERING            
#############################################

'PRP_COUNT_BY_ROWS'    : [
                            {'values':[0], 'columns':'q_.*'},
                            {'values':[0], 'columns':'a_.*'}
                         ], # {[[values1], [values2] ...]}  [[None, 0, "np.nan", -1, "-1", pd.NaT], [0], [-1]] For each list in values tupel, add an extra column counting the ocurrences of values on each row


'PRP_COLS_DIFF'     : ([["question_utc","answer_utc"]],0,0,'One'),


#############################################
##           FEATURE IMPORTANCE            ##
#############################################

'ML_FEATURE_IMPORTANCE'    : 1,
'PRINT_FEATURE_IMPORTANCE' : 1,
           
#############################################
##           FEATURE SELECTION             ##
#############################################
'PRP_DROP_THESE_COLUMNS_b4_ML'                    : ['question_id', 'subreddit', 'question_utc', 'question_text', 'answer_utc', 'answer_text'],  #[['v22','v91']],# [ITER]         (  [[]]=Don't do)   Ejemplo: [['y_alt1-2'],['y_alt2-2']]


#############################
#      PRINTS & CHARTS
#############################


# charts
'CHART_CONFUSION_MATRIX'       : 0,    # {uint8} (0=No, 1=Yes) 
'CHART_CORRELATIONS'           : 0,    # {uint8} (0=No, 1=Yes)  
'CHART_CORRELATIONS_LONG'      : 0,    # {uint8} (0=No, 1=Yes)  This chart takes a looooong time to finish.
'CHART_HISTOGRAMS'             : 0,    # {uint8} (0=No, 1=Yes)
'CHART_BOXPLOT'                : 0,    # {uint8} (0=No, 1=Yes)



#############################################
##            HDF5,SQLITE SAVE             ##
#############################################
'SAVE_HDF5_CORR_MATRIX'         : 0,         # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_FEATURES_IMPORTANCE' : 0, # {uint8} (0=No, 1=Yes)
'SAVE_CSV_PREPROCESSED_DATASETS': 1,


#############################
#  COMPETITION CONFIGURATION
#############################

'COMP_NAME'                                : "demo_kaggledays",     # {string}
'COMP_PROBLEM_TYPE'                        : 'Regr',       # {string} ('Regr', 'Class_B', 'Class_M')

'COMP_COLUMN_ID'                           : "id",                 # {string}
'COMP_COLUMN_CLASS'                        : "answer_score",          # {string}
'COMP_COLUMN_CLASS_4_SUBMISSION'           : "answer_score",         # {string}

'COMP_TRAIN_FILES'                         : ['preprocessed_train.csv'],      # {string} list of strings
'IMP_TRAIN_DELIMITER'                      :'\t',
'COMP_TEST_FILES'                          : ['preprocessed_test.csv'],        # {string} list of strings
'IMP_TEST_DELIMITER'                      :'\t',
'COMP_EVALUATION_METRIC'                   : "RMSLE", #"QWKappa",       # {string}
'COMP_EVALUATION_METRIC_GREATER_IS_BETTER' : 0, # {uint8} (0=No, 1=Yes)


'ML_PREDICT_PROBA' : 0,
'ML_CLASS_PROBA': 0,


}





############################################
###      NO TOCAR A PARTIR DE AQUÍ       ###
############################################

if __name__ == '__main__':
    from my_Dashboard_Iterator import *
    
    runDashboard(dashboard, algorithm_hyperparameters)


