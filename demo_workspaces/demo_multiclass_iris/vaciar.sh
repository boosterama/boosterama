#!/bin/bash

PERMITIR_BORRADO=1

if [ "$PERMITIR_BORRADO" = 1 ]; then
	#rm -R cache
	rm -R correlations
	rm -R data
	rm -R demo_reg_iris_head.xlsx
	rm -R external_predictions
	rm -R external_scripts
	rm -R external_submissions
	rm -R feature_importance
	#rm -R input
	rm -R keras_checkpoints
	rm -R kfold
	rm -R pickles
	rm -R pickles-dashboards
	rm -R preprocessed_datasets
	rm -R submissions_Level_1
	rm -R submissions_Level_1_validation
	rm -R temp.pkl
	rm -R catboost_info
	rm -R submissions_Level_2
	rm -R submissions_Level_2_validation
else
	echo "$(tput setaf 1)$(tput setab 7) "
	echo "                              ESTAMOS COMPITIENDO ..."
	echo
	echo  "                     --- NO SE PUEDE VACIAR LA CARPETA ---"
	echo
	echo  "$(tput sgr 0)"
	notify-send "No se puede borrar ... ESTAMOS COMPITIENDO"
	#sleep 5s
fi



