rmdir cache /S /Q
rmdir correlations /S /Q
rmdir data /S /Q
rmdir external_predictions /S /Q
rmdir external_scripts /S /Q
rmdir external_submissions /S /Q
rmdir feature_importance /S /Q
rmdir files /S /Q
rmdir keras_checkpoints /S /Q
rmdir pickles /S /Q
rmdir preprocessed_datasets /S /Q
rmdir submissions_Level_1 /S /Q
rmdir submissions_Level_1_validation /S /Q
rmdir pickles-dashboards /S /Q
#del temp.pkl /S /Q

