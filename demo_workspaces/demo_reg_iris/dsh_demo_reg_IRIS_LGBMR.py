# -*- coding: utf-8 -*-
"""
Created on Fri Aug 14 12:40:37 2015

@author: DataCousins
"""

import sys
if not "../../boosterama" in sys.path:
    sys.path.append("../../boosterama")
from my_Dashboard_Reiterators import *
from my_Dashboard_Utils import *





#############
# Algoritmos
#############

algoritmos = {


    "LGBMR":  {           #requiere 'ML_GRIDSEARCHCV_OR_ALGORITHM*' : ['Algorithm']
                'mym__bagging_fraction': [0.9], 
                'mym__feature_fraction': [0.4], 
                'mym__num_leaves': [1023], 
                'mym__max_bin': [255], 
                'mym__bagging_freq': [6],
                'mym__algorithm': ['lightgbm'], 
                'mym__objective': ['regression'], 
                'mym__boosting_type': ['gbdt'],  
                'mym__verbose': [0],  
                'mym__task': ['train'], 
                'mym__metric': [{'l2'}], 
                'mym__learning_rate': [0.05], 
                'mym__num_rounds': [260], 
                'mym__early_stopping_rounds': [20],
                'mym__data_random_seed':[1234],
                'mym__feature_fraction_seed':[1234],
                'mym__bagging_seed':[1234],
                'mym__drop_seed':[1234],        

              }

}


dashboard={
        
'FRIENDLY_ID':'iris-reg-demo', 
'HASHTAGS':['testing', 'demo'],

'AUTHOR':'datacousins',

######################
#  MACHINE LEARNING
######################
'LEVEL'       : '1_ML', # [ITER] {string} ('1_ML', '1_EXTERNAL_SCRIPTS', '1_EXTERNAL_PREDICTIONS', '2_ML', '2_ENSEMBLE', '2_CARUANA', '3_CARUANA', '3_BASIC_ENSEMBLE', '0_DELETE', '0_FEAT_IMP')

'ITERATOR'     : 'fastloop', # 'grid', 'fastloop'

'ML_ALGO'      : ["LGBMR"], 
'ML_CATEGORICAL_FEATURES': ['Color','IsPretty','Species'], # accepts: list of feature names, LightGBM accepts 'auto'.  future, accept: expression "y_str"



'RANDOM_SEED'  : 123,


################################################################################################
# CrossValidation
###################
'SPLIT_CV_TYPE'               : 'Random', #'Random','Stratified', 'TimeSeries'

'SPLIT_CV_KFOLDS'             :  4,  # 0  ===>  Train = 100%, Val = 0% \\\  1  ===> ejecución normal con Val \\\   >= 2  ===> CrossValidation
'SPLIT_CV_KFOLDS_EARLY_STOP'  :  0,  # {int}  0  ===>  Does all folds normally \\\  1  ===> stops on first fold if score is not the best. otherwise continues with rest of folds.


'SPLIT_VALIDATION_SET_PERC'   :  0,      #  {float16} (between 0.00 and 1.00)  # Only when ('SPLIT_CV_KFOLDS' = 1) AND ('SPLIT_CV_TYPE' = 'Random' or 'Stratified')


# CrossValidation for TimeSeries    
# # # # # # # # # # # # # # # # #    (section ignored unless 'SPLIT_CV_TYPE' = 'TimeSeries') 
'SPLIT_TS_TYPE*'               : ['TS_CV_TRAIN-ORIGIN_VAL-FIX'],  #  {string}   'TS_CV_ACADEMIC', 'TS_CV_TRAIN-ORIGIN_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
# Num_folds taken from    'SPLIT_CV_KFOLDS'  value
'SPLIT_TS_train_FIX_NUM*'      : [1000],                 #  {int} number of observations (starting from last one) of the train set
'SPLIT_TS_VALIDATION_FIX_NUM*' : [240],                 #  {int} number of observations (starting from last one) of the validation set
# 'SPLIT_VALIDATION_SET_PERC' is ignored => we use 'SPLIT_TS_VALIDATION_FIX_NUM'



################################################################################################
#Transformaciones X
####################
'PRP_DATE_ENCODE*'            : [()],  # [ITER]  [([list],[list],int,int,int)]  ([()]=Don't do,['year','month','yearmonth','dayofweek','dayofyear','day','weekofyear','quarter','hour','isweekend','owen_Day_-1','owen_Month_-1', 'owen_dayofweek', 'isofficehours','daysfromfirst']) datetime64 features converted to these Integers
        #([date_cols],['year','month'], do_Owen, DROP_New_after)    # Siempre borramos la variable original
'PRP_OWEN_2_DATE_2_FEATURE*'  : [()],    #[([list],str,[list of lists])],   # ([()]=Don't do)  ejemplo1:  [(['Car_Type'],'Date_',[[-1,-1],[-2,-2],[-7,-7],[-1,-7]])],  ejemplo2:  [(['y_str_orig'],'Date_',[[-7,-7],[-1,-7]])],  Both dates included. The order for period end/start does NOT matter.
                   # [([scope], 'date column', [[start_period, end_period]])]

'PRP_FLOAT_ENCODE*'         : [()],      # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)] convierte a categórica floats y luego hace Owen y Dropea Orig/Encoded
'PRP_INTEGER_ENCODE*'       : [(['y_int_orig'],0,0,0)],      # [ITER] [([list],int,int,int)]    [()]=Don't do  ejemplo: [(['y_int'],1,0,1)] convierte a categórica integers y luego hace Owen y Dropea Orig/Encoded
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)
'PRP_LABEL_ENCODE_LEX*'     : [()],      # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_Obj'],1,0,1)]  Convierte categóricas a integers usando lógica lexicográfica. Importante. Si NO se selecciona, el ML no funcionará si hay categoricas.  
       # ([scope], do_owen, DROP_orig_after, DROP_New_after)

'PRP_FLOAT_QUANTILE*'       : [(['y_float'],0,0,0)],  # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)]   convierte a cuantiles y hace owen.  ejemplos: [['y_float_LMen17']]   Siempre empieza por y_  o por o_
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)

'PRP_MONOTONIC_INT*'        : [[]],     # [ITER] {[[string]]} ([[]]=Don't do)  convierte a counts los campos integers (mantiene features originales) ejemplos: [['y_int_LMen17']]  Siempre empieza por y_  o por o_
'PRP_MONOTONIC_STR*'        : [['Color', 'IsPretty', 'Species']],     # [ITER] {[[string]]} ([[]]=Don't do)  convierte a counts los campos strings (mantiene features originales) ejemplo:  [['y_str_LMas17_LMen50']]   Siempre empieza por y_  o por o_


'PRP_NORMALIZE_NUMERIC*'    : [(['y_float_orig'],'Standarizing')],  #[(['y_float'],'Standarizing')],# [ITER] [(list,string)]  [()]=Don't do   ("Rescaling", "Standarizing", "Other")  Ejemplo:  [(['y_float'],'Standarizing')]
   # ([scope], method)
'PRP_X_TRANSFORM*'          : [()],      # [ITER] [(list,string)]  [()]=Don't do ('boxcox0.25', 'log','square','exp','sqrt')
    # ([scope], method)



'PRP_ONE_HOT_ENCODE*'          : [()] ,     # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_Obj'],1,0,0)] 
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)
'PRP_ONE_HOT_ENCODE_THRESHOLD*': [2],     # [ITER] [int]   []=Don't do  ejemplo: [4]  Minimum frequency of a value to apply OneHotEncode (below it, it will be grouped under the value 'other')



'PRP_OWEN_TRICK*'            : [(['Species'], None, 0)],#['y_str_LMen500'], None, 0)],        # {[[string]]} ([()]=Don't do)  ejemplos: [(['y_str_LMen17'], None, 0)] [(['y_obj_LMen17'],['mean','std'], 1)]  [(['y_str_LMas17_LMen50'],['mean','std'], 0)]  [(['o_obj_LMen17'],['mean','std'], 1)]   Siempre empieza por y_  o por o_
                          # ([scope], [metrics], DROP_orig_after)
'PRP_OWEN_TRICK_METRICS*'    : [['default']],  #default metric: 'default','mean','mode','std','max','min','sum'.  
                                    # using 'default' as metric, boosterama uses:  - 'mode' for multiclass problems  - 'mean' otherwise
'PRP_OWEN_TRANSFORM*'        : ['No'],  # 'No', 'log' to apply log after the agg/metric
'PRP_OWEN_RANDOM_RANGE*'     : [0.05],  #  random factor to multiply owen values on train to avoid overfitting. Range will be  VALUE * random(-RANDOM_RANGE , +RANDOM_RANGE)


'PRP_OWEN_2_FEATURE*'        : [()],   # ([()]=Don't do)  ejemplos:  [(['y_all'],['mean','std'],'Species',0)]
                    # [([scope], [metrics], 'variable_name_2_reference',DROP_orig_after)]


'PRP_REPLACER*'              : [[]], #  [()]=Don't do,   Ejemplo: [[(['y_num'],1,111),(['y_str'],'Blue','MILL')]]
                  # [[([SCOPE], REPLACE_THIS, PUT_THIS)]]
'PRP_REPLACE_RARE_STRINGS*'  : [(['y_str'],0,0,5)], # [ITER]   [(list, int, int, uint16)] [()]=Don't do,   Ejemplo: [(['y_str'],1,0,39)]   (0=No, 1 to 65535 = Threshold) con MENOS de threshold 
                  # ([SCOPE], DO_OWEN, DROP_Orig_after, LMen Threshold)

'PRP_WINSORING*'             : [(['y_float_int'],0.01,0.99)],# [ITER] [(list,float,float)] #   [()]=Don't do,     float ratios percentile from 0.0 to 1.0  ejemplo: [(['y_float_int'],0.03,0.97)]
        #([scope], lower_limit, upper_limit)

'PRP_NA_BINARIZE*'           : [[]],         # [ITER] {[[string]]} ([[]]=Don't do)  ejemplo: [['y_int_LMen17']] 
'PRP_NA_FILL*'               : [([['y_num'],-1],[['y_obj'],'aftfdgdddfkggffGdfhggdgffkgffddghhfgd'])],   # [ITER] {string} ([()]=Don't do, '0'= rellenar a '0', 'mean','median','mode' any integer value IN QUOTES: '0'  '-9999' ...)
               # [([['y_num'],-1],[['y_obj'],'aftfdgddgGgffdghfgd'])]


# NLP # # #
'PRP_NLP_TFIDF_SVM*'        : [()],  # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['keyword','title_text'],1,400,10)]  
               # ([scope], DROP_orig_after, TruncatedSVD_Components, SVC_C_param)



#Transformaciones Y
#####################
'ML_PRP_Y_TRANSFORM*'       : ['No'],      # [ITER] {string} ('No'=Don't do,'log','square','exp','sqrt')
#'ML_PRP_Y_TRANSFORM*'       : ['res:@iris-reg-demo'], #['log'],      # [ITER] {string} ('No'=Don't do,'log','square','exp','sqrt')
'PRP_Y_WINSORING_REMOVE*'   : [(0,0)],# [ITER] {(float,float)} # ((0,0)=Don't do, (lower_limit,upper_limit)) float ratios percentile from 0.0 to 1.0 #Remove Y outlier rows


#Transformaciones Y_test_preds
###############################
'ML_PRP_Y_APPLY_RESTRICTIONS*' : [()],#[ITER] {string}    [()]=Don't do, Ejemplo: [('NegativeToZero','Round','ToInt')]

                                  
#############################################################################################################
##          FEATURE ENGINEERING            
#############################################

# Join/Diff/Multiply these columns in Test and Train
'PRP_COLS_DIFF*'       : [()],   #  Don't do = [()] 
           # [([["CoverageField1B","PropertyField21B"],["GeographicField6A","GeographicField8A"]],0,0,'One')],   #  Don't do = [()] 
'PRP_COLS_JOIN*'       : [([['PetalLength|Quant|', 'PetalWidth|Quant|', 'Species']], 0, 0, 'One')], #  Don't do = [()]     
'PRP_COLS_MULTIPLY*'   : [([['PetalLength', 'PetalWidth', 'SepalWidth']], 0, 0, 'One')],   #  Don't do = [()] 
       # [([(COMBINACION_COLS)], do_Owen, DROP_New_after, One/Many_2/Many_3 )]
       # Ejemplo1:  [([['y_all']],1,0,'One')]        Ejemplo_2:   [([['SepalLength','SepalWidth'],['PetalLength','PetalWidth']],0,0,'One')]  

'PRP_COUNT_BY_ROWS'    : ([None, 0, "np.nan", -1, "-1", pd.NaT], [0], [-1]), # {[[values1], [values2] ...]}  [[None, 0, "np.nan", -1, "-1", pd.NaT], [0], [-1]] For each list in values tupel, add an extra column counting the ocurrences of values on each row


#############################################
##           FEATURE SELECTION             ##
#############################################

# DROP
'PRP_DROP_NEAR_ZERO_VARIANCE*'               : ['No'],  # (['No']=Don't do)  ejemplo: [0.1]  drops features with variance LOWER than 0.1.  Works better when normalized/standarized features.
'PRP_DROP_CONSTANTS*'                        : [0],     # {uint8} ([0]=No, [1]=Yes)
'PRP_DROP_FEATURES_ALL_NA_IN_TRAIN_OR_TEST*' : [0],     # {uint8} ([0]=No, [1]=Yes) Aparentemente mejor resultado 0

#Drop these columns from Test and Train
'PRP_DROP_THESE_COLUMNS*'                    : [[]],  #[['v22','v91']],# [ITER]         (  [[]]=Don't do)   Ejemplo: [['y_alt1-2'],['y_alt2-2']]
'PRP_DROP_THESE_COLUMNS_b4_ML*'              : [[]],  # [ITER]   (  [[]]=Don't do)


'PRP_DROP_ALL_BUT_THESE*'                    : [[]],  # [ITER] (  [[]]=Don't do)   Ejemplo: [['PetalWidth'],['SepalLength'],['IsPretty'],['SepalWidth'],['Color'],['PetalLength']]


#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 


#############################################
##               ML LEVEL 2                ##
#############################################
'L2_y_test_preds_PADRE_o_HIJO'  : 'hijo',  # 'padre' o 'hijo'

'L2_QUERY_WHERE'                : "score_validation<0.316 and score_train>0.06", #ID_EXECUTION=1612282146476 or ID_EXECUTION=1612281741184   #"score_validation<0.362858467 and score_train>0.06"
'L2_QUERY_ORDER_BY'             : ('score_validation','DESC'),

'L2_CV_OR_DATASET'              : 'CV',  # 'CV' o  'DATASET'
# 'CV' will use all Level1 & Level2 with CV (ignoring the field: d[L2_LEVEL1_DATASETS_TRAIN]).  
# 'DATASET' will take into account d['L2_LEVEL1_DATASETS_TRAIN' and TEST]

'L2_LEVEL1_DATASETS_TRAIN'      : 'level1_Train100%_WR(0,0)_TrainVal83%17%|EarlyStopF5of6', #level1_Train100%_WR(0,0)_TrainVal70%30%     or  level1_Train100%_WR(0,0)_CV   ... # Ignored when 'L2_CV_OR_DATASET'=='CV'
'L2_LEVEL1_DATASETS_TEST'       : 'level1_test100%',  # usually level1_test100% even for validation  # Ignored when 'L2_CV_OR_DATASET'=='CV'


# # # # # # # # # # # # # # # # # # # # # # #
##               2_ENSEMBLE                ##
# # # # # # # # # # # # # # # # # # # # # # #

# Tomamos en cuenta config de ML Level 2 
# Ignoramos folds, algoritmo, etc...
'L2_ENSEMBLE_NUM_ROUNDS'       : 103,  


# # # # # # # # # # # # # # # # # # # # # # #
##                 CARUANA                 ##
# # # # # # # # # # # # # # # # # # # # # # #
'L2_CARUANA_BY_BLOCKS'                       : 0,                        # {uint8} (0=No, 1=Yes)
'L2_CARUANA_BY_BLOCKS_COLUMN_ID'             : 'VAR_0005',     # {string}
'L2_CARUANA_MAX_ROUNDS'                      : 200,                     # {uint16}
'L2_CARUANA_MAX_ROUNDS_WITHOUT_IMPROVEMENTS' : 10, # {uint16}


#############################################
##               ML LEVEL 3                ##
#############################################
'L3_DO_CONVERT_2_RANKING'      : 1,



#############################################
##         EXTERNAL_PREDICTIONS            ##
#############################################
'EXTERNAL_PREDICTIONS_IMPORT*' : [()], # [ITER] {lista de tuplas} [()]=Don't do  [(cvPredsFile1, testPredsFile1), (cvPredsFile2, testPredsFile2), ..., (cvPredsFileN, testPredsFileN)]

                                  
#############################################
##             EXTERNAL_SCRIPTS            ##
#############################################
#'EXTERNAL_SCRIPTS_IMPORT'    : '1117_XGB_xgboostsingelmodel_12345_aliajouz.py', # [ITER] {string} ([]=Don't do, 'puntuacion_algoritmo_tituloScript_idScript_autor_NNN')
'EXTERNAL_SCRIPTS_IMPORT*'    : ['0777_xgb_peazoscript_12344_mesala.py'], # [ITER] {string} ([]=Don't do, 'puntuacion_algoritmo_tituloScript_idScript_autor_NNN')


#############################################
##           DELETE / OVERFIT              ##
#############################################
# Requires 'LEVEL' == '0_DELETE'
'DELETE_OR_OVERFIT'        : "Delete",   # [ITER] {string} ('Delete' / 'Overfit')
# 'Delete' will totally delete execution from DDBB and everywhere
# 'Overfit' will mark the execution as 'overfit' so it won't be used in Level2, Ensemble...   It still will be there, so it won't run again when tried to be run with same dashboard.
'DELETE_OR_OVERFIT_QUERY_WHERE'  : "ID_EXECUTION=1701171113005",#"score_validation>0.252858467 and score_train>0.06", #ID_EXECUTION=1612282146476 or ID_EXECUTION=1612281741184   #"score_validation<0.362858467 and score_train>0.06"


#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 


#############################
#      PRINTS & CHARTS
#############################

# prints
'PRINT_ANALYSIS_ON_TRAIN_TEST' : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_CUSTOM_IMPORT'          : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_CUSTOM_IMPORT_DIFFS'    : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_DASHBOARD'              : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_DROP_INFO'              : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_EVAL_INFO'              : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_NA_INFO'                : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_NA_INFO_SHOW_FIELDS'    : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_QUERY_COLS'             : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_TIME'                   : 0,    # {uint8} (0=No, 1=Yes)  
'PRINT_X_INFO'                 : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_Y_INFO'                 : 0,    # {uint8} (0=No, 1=Yes)
'PRINT_TRANSFORM_Y_INFO'       : 1,    # {uint8} (0=No, 1=Yes)

# charts
'CHART_CONFUSION_MATRIX'       : 0,    # {uint8} (0=No, 1=Yes) 
'CHART_CORRELATIONS'           : 0,    # {uint8} (0=No, 1=Yes)  
'CHART_CORRELATIONS_LONG'      : 0,    # {uint8} (0=No, 1=Yes)  This chart takes a looooong time to finish.
'CHART_HISTOGRAMS'             : 0,    # {uint8} (0=No, 1=Yes)
'CHART_BOXPLOT'                : 0,    # {uint8} (0=No, 1=Yes)



#############################################
##            HDF5,SQLITE SAVE             ##
#############################################
'SAVE_HDF5_EXECUTIONS'          : 1,          # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_CORR_MATRIX'         : 0,         # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_FEATURES_IMPORTANCE' : 0, # {uint8} (0=No, 1=Yes)

'SAVE_SQLITE_EXECUTIONS'        : 1,          # {uint8} (0=No, 1=Yes)

'SAVE_CSV_PREPROCESSED_DATASETS': 1,


#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  


#############################################
##                 ITERABLE                ##
#############################################

'ITER_DO_DOUBLE_EXECUTION' : 0, #  Caruana... : If dashboard is trained with a subset, ITERATOR_DO_DOUBLE_EXECUTION performs one extra execution trained with the complete trainset predicting the complete test set

'REITERATE' : False,
'REITERATE_MAX_LEVELS' : 5,  # 0 - Infinite 
'REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD'  : -1, # -1 to ignore this threshold
#'REITERATE_FEATURE_MIN_ENHACEMENT_THRESHOLD' : 0.001,

'REITERATE_LEVEL' : 0, # Internal - USUALLY defined as 0.  Indicates in wich level of reiteration this dashboard has been executed

'ITER_ITERABLE_DASHBOARD' : True, # USUALLY True.  True - iterable variables are list of values, # False - iterable variables contains values


##########################################
##       NA Imputation based on ML      ##
##########################################
'NA_ML_IMPUTATION' : False,

'NA_ML_FIELDS' : 'auto',
'NA_ML_EXTRA_COLUMNS_TO_DROP' :  [],



#################################################
##  Parámetros configuración de las funciones  ##
#################################################

#import_data
'IMP_TRAIN_SUBSET_TYPE': 'allSamples',   #Para todo el dataset poner 'allSamples'. Otras opciones: 'pPercentageStratifiedRandomSamples','pPercentageRandomSamples', 'nConsecutiveSamples', 'nRandomSamples'
'IMP_TRAIN_PERCENTAGE' : 7,               #Sólo se usa con TRAIN_SUBSET_TYPE:  'pPercentageRandomSamples'
'IMP_TRAIN_NUM_SAMPLES': 1000,            #Sólo se usa con TRAIN_SUBSET_TYPE:  'nConsecutiveSamples' o TRAIN_SUBSET_TYPE:  'nRandomSamples'
'IMP_TRAIN_FIRST_ROW'  : 1,               #Sólo se usa con TRAIN_SUBSET_TYPE:  'nConsecutiveSamples'

'IMP_TEST_SUBSET_TYPE' : 'allSamples',    #Para todo el dataset poner 'allSamples'. Otras opciones: 'pPercentageStratifiedRandomSamples','pPercentageRandomSamples', 'nConsecutiveSamples', 'nRandomSamples'
'IMP_TEST_PERCENTAGE'  : 10,               #Sólo se usa con TEST_SUBSET_TYPE:  'pPercetageRandomSamples'
'IMP_TEST_NUM_SAMPLES' : 1000,             #Sólo se usa con TEST_SUBSET_TYPE:  'nConsecutiveSamples' o TEST_SUBSET_TYPE:  'nRandomSamples'
'IMP_TEST_FIRST_ROW'   : 1,                #Sólo se usa con TEST_SUBSET_TYPE:  'nConsecutiveSamples'


#split_train_test_components
'SPLIT_VALIDATION_RANDOM_STATE' : 0,
'ML_ALGO_PARAMETROS*' : [],  ### NO RELLENAR ### [ITER] {list of dictionaries} ### NO RELLENAR ###, se rellena automáticamente en función de la configuración de algoritmos



#########################################
##                 STEPS               ##
#########################################
'SYS_DO_CUSTOM_IMPORT'         : 0,          # requiere definir el CUSTOM_IMPORT() más abajo
'SYS_DO_CUSTOM_ML'             : 0,          # requiere definir el CUSTOM_ML() más abajo
'SYS_DO_CUSTOM_SUBMISSION_PRE' : 0,          # requiere definir el CUSTOM_SUBMISSION_PRE() más abajo
'SYS_DO_CUSTOM_SUBMISSION_CSV' : 0,          # requiere definir el CUSTOM_SUBMISSION_CSV() más abajo

'ML_FEATURE_IMPORTANCE'    : 1,          # {uint8} (0=No, 1=Yes) 
'SYS_SEND_EMAIL'           : 0,          # {uint8} (0=No, 1=Yes)


#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  


#############################
#  COMPETITION CONFIGURATION
#############################

#Special Column Names (useful for submissions)
'COMP_NAME'                                : "demo_reg_iris",     # {string}
'COMP_PROBLEM_TYPE'                        : 'Regr',       # {string} ('Regr', 'Class_B', 'Class_M')

'COMP_COLUMN_ID'                           : "Id",                 # {string}
'COMP_COLUMN_CLASS'                        : "SepalLength",          # {string}
'COMP_COLUMN_CLASS_4_SUBMISSION'           : "SepalLength",         # {string}

'COMP_TRAIN_FILES'                         : ['train.csv'],      # {string} list of strings
'COMP_TEST_FILES'                          : ['test.csv'],        # {string} list of strings
'COMP_Y_TEST_FILE'                         : 'y_test.csv',     # {string} list of strings

  #CLASSIFICATION     #Acc - Accuracy Score    #F1 - F1 Score    #LogLoss  
  #REGRESSION         #Gini - Gini Score       #MSE  - Mean Squared Error  #RMSE
'COMP_EVALUATION_METRIC'                   : "MAE", #"QWKappa",       # {string}
'COMP_EVALUATION_METRIC_GREATER_IS_BETTER' : 0, # {uint8} (0=No, 1=Yes)



'ML_PREDICT_PROBA' : 0,
'ML_CLASS_PROBA': 0,

#Convert Column to DATE in Train & Test
'PRP_COLUMN_2_DATE'                        : [],        # {[string]} 

#Change Column Names in Train & Test
'PRP_CHANGE_COL_NAMES_OLD'                 : [],        # {[string]}
'PRP_CHANGE_COL_NAMES_NEW'                 : [],        # {[string]}



}

#dashboard['DIR_BASE']='../demo_workspaces/'+dashboard['COMP_NAME']

CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV=None, None, None, None


############################################
###      NO TOCAR A PARTIR DE AQUÍ       ###
############################################

if __name__ == '__main__':
    from my_Dashboard_Iterator import *
    
    runDashboard(dashboard, algoritmos)


