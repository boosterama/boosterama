# -*- coding: utf-8 -*-
"""

@author: DataCousins
"""

dashboard_filename='demo_reg_iris/dsh_demo_reg_IRIS.py'
dashboard_modifications={
    'ML_ALGO':["XGBR"],
}

from boosterama_test.test_util import run_test

run_test(dashboard_filename, dashboard_modifications)

