# -*- coding: utf-8 -*-
"""

@author: DataCousins
"""

dashboard_filename='demo_reg_iris/dsh_demo_reg_IRIS.py'
dashboard_modifications={
    'ML_ALGO':["LR"],
}

from boosterama_test.test_util import run_test

run_test(dashboard_filename, dashboard_modifications, 
         expected_results_filename='test_dsh_demo_reg_IRIS_LR_expected_output.csv', 
         expected_oof_filename='test_dsh_demo_reg_IRIS_LR_expected_validation_output.csv')

