# -*- coding: utf-8 -*-
"""

@author: DataCousins
"""



import os
init_work_directory=os.getcwd()
import sys

if not "../boosterama" in sys.path:
    sys.path.append("../boosterama")
    sys.path.append("../demo_workspaces")

from my_Dashboard_Reiterators import *
from my_Dashboard_Utils import *
from my_Dashboard_Iterator import *

import pandas as pd
import ntpath

from config.config_manager import boosterama_config


DEFAULT_ACEPTABLE_DIFF_PERCENTAGE=1
DEFAULT_ACEPTABLE_DIFF_WORST_CASE=0.01

'''
accepted_error means:
    multiclass problem (Y values are labels): differences percentage
        accepted values [0, 100]
        default: 1
        
    multiclass problem or binary classification (Y values are floats):  worst case  error
        accepted values [0, 1]
        default: 0.001
'''

def run_test(dashboard_filename, dashboard_modifications, accepted_error=None, expected_results_filename=None, expected_oof_filename=None, check_validation_oof_output=True):
    path_dir,test_id =ntpath.split(dashboard_filename)
    if test_id.endswith('.py'): test_id=test_id[:-3]
    
    if path_dir=="":
        module_name=test_id
    else:
        module_name=path_dir.replace("\\","/").replace("/",".") + "." + test_id
    
    import_sentence="from {} import dashboard, algoritmos, CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV".format(module_name)
#    print(import_sentence)
    exec(import_sentence, globals())
    
    '''
    #    algoritmos, CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV= None,None,None,None,None
    import_sentence="from {} import dashboard".format(module_name)
    exec(import_sentence, globals())
    
    for var_name in ['algoritmos', 'CUSTOM_IMPORTER','CUSTOM_ML','CUSTOM_SUBMISSION_PRE', 'CUSTOM_SUBMISSION_CSV']:
        try:
            import_sentence="from {} import ".format(module_name)
            exec(import_sentence, globals())
        except:
            exec("{}=None".format(var_name), globals())
    if algoritmos is None:
        import_sentence="from {} import algorithm_hyperparameters;algoritmos = algorithm_hyperparameters".format(module_name)
        exec(import_sentence, globals())
    '''

    
    
    
    
    
    #test_id='demo_reg_IRIS'
    
    
    
    
    
    
    
    if path_dir!="" and os.path.exists("../demo_workspaces/"+dashboard_filename):
        os.chdir("../demo_workspaces/"+path_dir)
    elif path_dir!="":
        os.chdir(path_dir)
#    print("os.getcwd(): {}".format(os.getcwd()))
    
    
    for k in dashboard_modifications.keys():
        dashboard[k]=dashboard_modifications[k]
    dashboard['META_TEST']=True
    #dashboard_copy=dashboard.copy()
    
    resultados__pd = runDashboard(dashboard, algoritmos, CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
    print(resultados__pd)
    
    def do_check(validation=False, expected_results_filename=None, accepted_error=None):
        level1_submission_filename=dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_1'+ ("_VALIDATION" if validation else "")] + resultados__pd['submissionFilename'].values[0]
        level1_submission__pd=pd.read_csv(level1_submission_filename)
        
        if expected_results_filename is None:
            expected_results_filename=os.path.join(init_work_directory,'test_' + test_id + '_expected_' +("validation_" if validation else "") + 'output.csv')
        else:
            expected_results_filename=os.path.join(init_work_directory,expected_results_filename)
        expected__pd=pd.read_csv(expected_results_filename)
        
        
        
        
        num_results=level1_submission__pd.shape[0]
        
        expected__pd.rename(columns={dashboard['COMP_COLUMN_CLASS']:'expected_value'}, inplace=True)
        
        
        result__pd=level1_submission__pd.merge(expected__pd, on=dashboard['COMP_COLUMN_ID'])
        
        if dashboard['COMP_PROBLEM_TYPE']=='Class_M':
            class_col_name=dashboard['COMP_COLUMN_CLASS']
            result__pd['difference'] =result__pd.apply(lambda row: int(row[class_col_name] != row['expected_value']) * 1/num_results,axis=1)
            accepted_error = accepted_error if accepted_error is not None else DEFAULT_ACEPTABLE_DIFF_PERCENTAGE
        else:
            result__pd['difference']=result__pd[dashboard['COMP_COLUMN_CLASS']]-result__pd['expected_value']
            accepted_error = accepted_error if accepted_error is not None else DEFAULT_ACEPTABLE_DIFF_WORST_CASE
        
        same_num_results=result__pd.shape[0]==num_results
        
        if not same_num_results:
            print("ERROR: different number of results ... !!!")
        
        
        perfect=same_num_results and (result__pd['difference']==0).all()
        
        if dashboard['COMP_PROBLEM_TYPE']=='Class_M':
            worst_error = result__pd['difference'].sum()*100
        else:
            worst_error = max(abs(result__pd['difference'].min()), result__pd['difference'].max())  
        acceptable = worst_error<=accepted_error
        
        
        
        
        output_txt=""
        
        if perfect:
            output_txt="Ok, perfect!"  
        elif acceptable:
            output_txt="Ok, ({}: {})".format("differences percentage" if dashboard['COMP_PROBLEM_TYPE']=='Class_M' else "worst case with error", worst_error)
        else:
            output_txt="ERROR\n=====\n"
            output_txt+=str(result__pd['difference'].describe())
            
            
        #    SKIP_ALREADY_EXECUTED_DASHBOARDS=True
        
        output_txt = ("VALIDATION: " if validation else "TEST: ") + output_txt
        
        if int(boosterama_config.rewrite_test_csv_files):
            print('updating... ', level1_submission_filename, ' ==> ', expected_results_filename)
            from shutil import copyfile
            copyfile(level1_submission_filename, expected_results_filename)
        
        
        return output_txt
    
    print ("\n\n\n\n")
    
    output_txt = do_check(expected_results_filename=expected_results_filename)
    print(output_txt)
    
    if check_validation_oof_output and dashboard['SPLIT_CV_KFOLDS']>0:
        output_txt_val = do_check(validation=True, expected_results_filename=expected_oof_filename)
        print(output_txt_val)
    
    
    os.chdir(init_work_directory)
    
    
