# -*- coding: utf-8 -*-
"""

@author: DataCousins
"""

dashboard_filename='demo_reg_iris/dsh_demo_reg_IRIS_LGBMR.py'
dashboard_modifications={
    'ML_ALGO':["LGBMR"],
}

from boosterama_test.test_util import run_test

run_test(dashboard_filename, dashboard_modifications)

