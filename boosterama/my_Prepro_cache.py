# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 12:57:41 2016

@author: DataCousins
"""

import time
from my_Cache import *
from my_Prepro import *


IMPORT_AND_PREPROCESSING_VARIABLES = [ 
    'PRP_.*',
    'IMP_.*',
    'COMP_.*'

]


def cached_import_and_preprocessing(d, dXT, CUSTOM_IMPORTER, TRAIN__pd = None, test__pd = None):        
    start = time.clock() 
    cache_key = get_cache_key(d, IMPORT_AND_PREPROCESSING_VARIABLES)
    print ( "cache_key is: " +str(cache_key))
    #memmory_before = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    #print ("Memoria antes de usar la cache: (kb)" + str(memmory_before))
    #cache = get_elm_cache(main_caches, d, "import_and_preprocessing")
    cache = get_elm_cache(None, d, "import_and_preprocessing")
    
    was_in_cache=False
    import_and_preprocessing_results = None
    if cache_key in cache:
        print("Extracting data from cache for cache_key: " +str(cache_key))
        import_and_preprocessing_results = cache[cache_key]
        was_in_cache=True
    else:
        print("It wasn't on cache. Let's run import_and_preprocessing")
        import_and_preprocessing_results = import_and_preprocessing(d, dXT, CUSTOM_IMPORTER, TRAIN__pd, test__pd)
        print("Saved in cache with cache_key: " +str(cache_key))
        cache[cache_key] = import_and_preprocessing_results
    
    #Asignación de múltiples valores devueltos por import_and_preprocessing
    try: # antes cacheabamos menos valores, por eso el try/except
        (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd, dXT) = import_and_preprocessing_results
        
        if was_in_cache:
            from framework.execution_context import ExecutionContext
            _, current_dXT, _ = ExecutionContext().get_current_context()
            current_dXT.clear()
            current_dXT.update(dXT)
            
        
    except:
        (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd) = import_and_preprocessing_results
        
    try:
        cache.close()
    except: pass
    
    elapsed = time.clock()
    elapsed = elapsed - start
    
    #memmory_after = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    #memmory_leftover = memmory_after - memmory_before
    #print ("Memoria después de usar la cache (kb):" + str(memmory_before))
    #print ("\n\n\n========================")
    print (Fore.YELLOW + Back.BLACK + Style.DIM+ "\nTiempo en llamar a import_and_preprocessing: " +str(elapsed)  +"\n" +  Style.RESET_ALL)
    #print ("Memoria por haber usado la cache (kb):" + str(memmory_leftover))
    #print ("========================\n\n\n")
    
    # the number of random numbers consumed by import_and_preprocessing varies depending on cache usage
    # so, we have to set the random seed again in order to have the same results in different executions
    set_random_seed(d)
    
    return (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd, dXT)
