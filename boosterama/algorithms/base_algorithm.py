
from framework.metaclasses import algorithm

import time

@algorithm("--default--")
class BaseAlgorithm():
    ''' AlgorithmFactory initilizes this object with the next attributes:
        
        self.algorithm_id  (i.e. XGBR)
        self.dashboard
        self.dXT
        self.results
    '''
    
    def make_up(self, dashboard, default_dashboard):
        pass    
    
    def convert_to_algorithm_input_format(self, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf):
        return X_train, X_val, X_test

    '''
    
    X_test parameter could seem unuseful for this task of model training.
    But there are some utilities like fastAI.DataLoader that requieres to be initialized with tran, val and test datasets 
    
        ¯\_(ツ)_/¯
    
    '''
    def fit(self, model, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf, param, *args, **kwargs):
        trained_model = model.fit(X_train, y_train_transf)
        return trained_model
    
    def param_extractor(self, param, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        return param, {}, {}
    
    def predict_proba(self, model, X_train, class_prob):
        y=model.predict_proba(X_train)[:,class_prob]
        return y
    
    def predict(self, model, X_train, dataset_id='unknown'):
        y=model.predict(X_train)
        return y
    
    def requires_label_encode(self):
        return False
    
    def set_earlystopping_best_model(self, best_model, fold):
        """ saves in "results" info related to best epoch for early stopping implementation
        """
        pass
    
    def set_col_names(self, X_TRAIN__pd):
        self.col_names=list(X_TRAIN__pd.columns.values)
        self.categorical_features=self._get_categorical_features(X_TRAIN__pd)
        print("self.categorical_features: ", self.categorical_features)
        self.categorical_features_idx=self.categorical_features
        
        if isinstance(self.categorical_features, list):
            self.categorical_features_size={}
            for c in self.categorical_features:
                self.categorical_features_size[c]= len(X_TRAIN__pd[c].unique())
            
            ind_dict = dict((k,i) for i,k in enumerate(self.col_names))
            inter = set(ind_dict).intersection(self.categorical_features)
            self.categorical_features_idx = [ ind_dict[x] for x in inter ]
            
#            for i in self.categorical_features_idx:
#                print(self.col_names[i], " is a categorical feature")
    def _get_categorical_features(self, X_TRAIN__pd):

        print("_get_categorical_features: DEFAULT")
        start_time=time.clock()
        cat_list=self.dashboard['ML_CATEGORICAL_FEATURES']
        if cat_list=='auto':
#            cat_list = [col for col in X_TRAIN__pd if X_TRAIN__pd[col].dtype.name != 'float64' and X_TRAIN__pd[col].dtype.name != 'float32' and len(X_TRAIN__pd[col].unique()) < self.dashboard['ML_CATEGORICAL_FEATURES_THRESHOLD']]
            cat_list = [col for col in X_TRAIN__pd if len(X_TRAIN__pd[col].unique()) < self.dashboard['ML_CATEGORICAL_FEATURES_THRESHOLD']]
            
            for col in [self.dashboard['COMP_COLUMN_ID'],self.dashboard['COMP_COLUMN_CLASS']]:
                if col in cat_list:
                    cat_list.remove(col)
        elapsed_time=time.clock()-start_time
        print("Categorical detected in {} seconds".format(elapsed_time))
        print("Categorical columns:", cat_list)
        return cat_list
    
    def uses_early_stopping(self):
        return False
    
    def implements_feature_importance(self):
        print("INFO: feature_importance(best_model) not implemented for this algorithm")
        return False
    
    def feature_importance(self, best_model):
        return None, None, None, None
    
    
    

    

