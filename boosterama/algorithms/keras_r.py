from framework.metaclasses import algorithm
from framework.execution_context import ExecutionContext
from algorithms.base_algorithm import BaseAlgorithm

import os
import lightgbm as lgb
import numpy as np
import pandas as pd

from colorama import Fore, Back, Style

from my_Evaluation_Metrics import my_score
from my_ML_fx import njobs

import traceback

import tensorflow as tf

from keras.models import load_model




## import libraries
import numpy as np
np.random.seed(123)

#import pandas as pd
#import subprocess
#from scipy.sparse import csr_matrix, hstack
#from sklearn.metrics import mean_absolute_error
#from sklearn.preprocessing import StandardScaler
#from sklearn.cross_validation import KFold
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.layers.normalization import BatchNormalization
from keras.layers.advanced_activations import PReLU


from keras.callbacks import ModelCheckpoint
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard

## Batch generators ##################################################################################################################################

def batch_generator(X, y, batch_size, shuffle):
    #chenglong code for fiting from generator (https://www.kaggle.com/c/talkingdata-mobile-user-demographics/forums/t/22567/neural-network-for-sparse-matrices)
    number_of_batches = np.ceil(X.shape[0]/batch_size)
    counter = 0
    sample_index = np.arange(X.shape[0])
    if shuffle:
        np.random.shuffle(sample_index)
    while True:
        batch_index = sample_index[batch_size*counter:batch_size*(counter+1)]
        X_batch = X[batch_index,:]
        y_batch = y[batch_index]
        counter += 1
        yield X_batch, y_batch
        if (counter == number_of_batches):
            if shuffle:
                np.random.shuffle(sample_index)
            counter = 0

def batch_generatorp(X, batch_size, shuffle):
    number_of_batches = X.shape[0] / np.ceil(X.shape[0]/batch_size)
    counter = 0
    sample_index = np.arange(X.shape[0])
    while True:
        batch_index = sample_index[batch_size * counter:batch_size * (counter + 1)]
        X_batch = X[batch_index, :].toarray()
        counter += 1
        yield X_batch
        if (counter == number_of_batches):
            counter = 0

''' keras 1


def nn_model(n_inputs):
    model = Sequential()
    
    model.add(Dense(400, input_dim = n_inputs, init = 'he_normal'))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
        
    model.add(Dense(200, init = 'he_normal'))
    model.add(PReLU())
    model.add(BatchNormalization())    
    model.add(Dropout(0.2))
    
    model.add(Dense(50, init = 'he_normal'))
    model.add(PReLU())
    model.add(BatchNormalization())    
    model.add(Dropout(0.2))
    
    model.add(Dense(1, init = 'he_normal'))
    model.compile(loss = 'mae', optimizer = 'adadelta')
    return(model)

'''





def nn_model(n_inputs):
    model = Sequential()
    
    model.add(Dense(400, input_dim = n_inputs, kernel_initializer = 'he_normal'))
    model.add(PReLU())
    model.add(BatchNormalization())
    model.add(Dropout(0.4))
        
    model.add(Dense(200, kernel_initializer = 'he_normal'))
    model.add(PReLU())
    model.add(BatchNormalization())    
    model.add(Dropout(0.2))
    
    model.add(Dense(50, kernel_initializer = 'he_normal'))
    model.add(PReLU())
    model.add(BatchNormalization())    
    model.add(Dropout(0.2))
    
    model.add(Dense(1, kernel_initializer = 'he_normal'))
    model.compile(loss = 'mae', optimizer = 'adadelta')
    return(model)













@algorithm("VIRI_KERAS_R")
class KerasWrapper(BaseAlgorithm):
    
    def __init__(self, **kwargs):
        self.batch_size_train=4
        self.batch_size_predict=10
        self.nepochs = 2
        self.use_checkpoints = None
        self.use_weights_only= False # bug in Keras 2.0  https://github.com/keras-team/keras/issues/4875
        super().__init__()
    
    def get_value_with_alias(self, d, alias, default=None, remove_all=False):
        v=default
        used_alias=[]
        for k in alias:
            if k in d:
                v=d[k]
                used_alias.append(v)
        if remove_all:
            for k in used_alias:
                del(d[k])
        if len(used_alias)>1:
            print("ERROR. hyperparameter defined more than once: {}".format(used_alias))
        return v
        
        
    def convert_to_algorithm_input_format(self, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf):
        
        '''
        las últimas versiones de LightGBM sólo usan el formato propio Dataset para el entrenamiento.
        Para predicción daría un error:
            TypeError: Cannot use Dataset instance for prediction, please use raw data instead
        
        Así que hacemos la conversión sólo en la función self.fit(...)
        
        '''
            
    
        return X_train, X_val, X_test
    
    def fit(self, model, val_exists, X_train, X_val,  X_test,y_train_transf, y_val_transf, params, *args, **kwargs):
        dashboard, dXT, _ = ExecutionContext().get_current_context()
        
        
        
        current_fold_str=str(dXT.get('CURRENT_FOLD', 'x'))
        
        checkpoints_dir=os.path.abspath(dashboard['DIR_BASE']+'/keras_checkpoints/' )
        os.makedirs(checkpoints_dir, exist_ok=True)
        
        tensorboard_dir=os.path.abspath(dashboard['DIR_BASE']+'/keras_tensorboard/' )
        os.makedirs(checkpoints_dir, exist_ok=True)
        
        if self.use_checkpoints is None:
            
            '''
            ntrain = X_train.shape[0]
            
            xtr = X_train
            ytr = y_train_transf
            xte = X_test
            yte = y_val_transf
            '''
            
    #        callbacks = None
            callbacks = [
                ModelCheckpoint(monitor='val_loss',
                     filepath= os.path.join(checkpoints_dir , \
                            current_fold_str + "_"+dashboard['FRIENDLY_ID']+'_epoch[{epoch:03d}]'+('_weights' if self.use_weights_only else '')+'.hdf5'), #+'_[{epoch:03d}]_{val_loss:.7f}.hdf5'),
                     save_best_only=False,
                     save_weights_only=self.use_weights_only,
                     mode='max'),
                 TensorBoard(log_dir='{}/{}'.format(tensorboard_dir, current_fold_str + "_"+ dashboard['FRIENDLY_ID']))]
            
            
            model= nn_model(X_train.shape[1])
            
            with tf.device('/gpu:0'):
            
                history= model.fit(
                    x=X_train,
                    y=y_train_transf,
                    batch_size=self.batch_size_train,
                    epochs=self.nepochs,
                    verbose=1,
                    callbacks=callbacks,
                    validation_data=(X_val, y_val_transf),
                    shuffle=True,
                    class_weight=None,
                    sample_weight=None,
                    initial_epoch=0,
                    steps_per_epoch=None,
                    validation_steps=None,
                    **kwargs)
            '''
            history = trained_model.fit(generator = batch_generator(xtr, ytr, batch_size=self.batch_size_train, shuffle=True),
                                          nb_epoch = self.nepochs,
                                          samples_per_epoch = xtr.shape[0],
                                          verbose = 0)
            '''
        else:
            model=None
            fold_checkpoint=''
            for x in self.use_checkpoints:
                if x.startswith(current_fold_str+"_"):
                    fold_checkpoint=x
            
            print("loading ", fold_checkpoint)
            
            checkpoint_path=os.path.join(checkpoints_dir, fold_checkpoint)
            
            if self.use_weights_only:
                model= nn_model(X_train.shape[1])
                model.load_weights(checkpoint_path)
            else:
                model = load_model(checkpoint_path)
        
        return model    
    
    def param_extractor(self, params, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        
        reduced_params={}
        for p in params:
            if hasattr(self, p):
                setattr(self, p, params[p]);
            else:
                reduced_params[p]=params[p]
                

        return reduced_params, {}, {}

    def predict_proba(self, model, X_train, class_prob):
        y=self.predict(model, X_train, class_prob)
        return y
    
    def predict(self, trained_model, X, dataset_id='unknown'):
        
#        print("model.predict_generator: ", trained_model.predict_generator)
        
        #y = model.predict_generator(generator = batch_generatorp(X_train, batch_size=self.batch_size_predict, shuffle=False))
        
        y=trained_model.predict(X,
                batch_size=self.batch_size_predict,
                verbose=0,
                steps=None)
        
        y=y[:,0]  # the same as y.reshape(-1)
        
        return y
    
    def requires_label_encode(self):
        return self.algorithm_id in ['LGBMC', 'LGBMC_M']
        


    
    def uses_early_stopping(self):
        return False
    
    def implements_feature_importance(self):
        return False

    
