from framework.metaclasses import algorithm
from framework.execution_context import ExecutionContext
from algorithms.base_algorithm import BaseAlgorithm

import os
import lightgbm as lgb
import numpy as np
import pandas as pd

from colorama import Fore, Back, Style

from my_Evaluation_Metrics import my_score
from my_ML_fx import njobs

import traceback


NUM_ROUNDS_ALIAS = ['n_estimators', 'num_iterations', 'num_iteration', 'num_tree', 'num_trees', 'num_round', 'num_rounds', 'num_boost_round', 'n_estimators']           
NUM_TRHEADS_ALIAS= ['num_threads', 'alias=num_thread', 'nthread', 'nthreads']
BOOSTER_PARAMS_LIST=['early_stopping_rounds'] + NUM_ROUNDS_ALIAS
OTHER_PARAMS_LIST=['algorithm']
RANDOM_SEED_PARAMS = ['data_random_seed', 'feature_fraction_seed', 'bagging_seed', 'drop_seed']

'''
boosterama to lightgbm metric, see: https://github.com/Microsoft/LightGBM/blob/master/docs/Parameters.rst

#'Gini':
'LogLoss': 'binary_logloss', #'multi_logloss'-->softmax
'MAE': 'l1',
#'QWKappa':
#'R2':
'MSE': 'l2',
'RMSE': 'l2_root',
#'RMSLE':
#'RMSPE':
'ROC_AUC':'auc',
'Acc': 'mape'
'''


class LightGbmMetricAdaptor():
    def __init__(self, func):
        self.adapted_func=func
        d, _, _ = ExecutionContext().get_current_context()
        self.problem_type=d['COMP_PROBLEM_TYPE']
        self.is_bigger_better=bool(d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'])
        self.eval_name='custom_{}'.format(func.__name__)
    
    def __call__(self, preds, ground_truth):
        ground_truth=ground_truth.get_label()
#        if self.problem_type.startswith('Regr'):
#            ground_truth=ground_truth.astype(preds.dtype)
        return (self.eval_name, self.adapted_func(ground_truth, preds), self.is_bigger_better)



@algorithm("LGBMC|LGBMC_B|LGBMC_M|LGBMR")
class LightGbmWrapper(BaseAlgorithm):
    
    def get_value_with_alias(self, d, alias, default=None, remove_all=False):
        v=default
        used_alias=[]
        for k in alias:
            if k in d:
                v=d[k]
                used_alias.append(v)
        if remove_all:
            for k in used_alias:
                del(d[k])
        if len(used_alias)>1:
            print("ERROR. hyperparameter defined more than once: {}".format(used_alias))
        return v
        
        
    def convert_to_algorithm_input_format(self, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf):
        
        '''
        las últimas versiones de LightGBM sólo usan el formato propio Dataset para el entrenamiento.
        Para predicción daría un error:
            TypeError: Cannot use Dataset instance for prediction, please use raw data instead
        
        Así que hacemos la conversión sólo en la función self.fit(...)
        
        '''
            
    
        return X_train, X_val, X_test
    
    def fit(self, model, val_exists, X_train, X_val,  X_test,y_train_transf, y_val_transf, params, *args, **kwargs):
        feval=None
        if 'feval' in params:
            feval=LightGbmMetricAdaptor(params['feval'])
            del(params['feval'])
            
        hyperparams={} #lightgbm modifies the params dictionary
        booster_params={}
        for param_name in params:
            
            if param_name in BOOSTER_PARAMS_LIST:
                booster_params[param_name]=params[param_name]
            elif param_name not in OTHER_PARAMS_LIST:
                hyperparams[param_name]=params[param_name]
        
        
        dashboard, _, _ = ExecutionContext().get_current_context()
        for param_name in RANDOM_SEED_PARAMS:
            if param_name not in hyperparams:
                hyperparams[param_name]=dashboard['RANDOM_SEED']
                
        
        scores=[]
        best_round=[]
        feature_importances_by_split={}
        feature_importances_by_gain={}
        
        lgb_train = lgb.Dataset(X_train, y_train_transf)
        
        early_stopping_rounds=None
        if val_exists:
            lgb_eval = lgb.Dataset(X_val, y_val_transf, reference=lgb_train)
            early_stopping_rounds=booster_params.get('early_stopping_rounds', None)
            if early_stopping_rounds==-1:
                early_stopping_rounds=None
        else:
            lgb_eval=None
        print("early_stopping_rounds: ", early_stopping_rounds)
        print("val_exists: ", val_exists)
        
        
        n_threads=self.get_value_with_alias(hyperparams,NUM_TRHEADS_ALIAS, -2, True)
        if n_threads==-2:
            import multiprocessing
            num_cpu_threads=multiprocessing.cpu_count()
            n_threads = int(num_cpu_threads / 2) # using a single thread, per cpu (should be pinned!!!)
        hyperparams['num_threads']=n_threads
        
        print("self.categorical_features_idx: ", self.categorical_features_idx)
        
        trained_model = lgb.train(hyperparams,
                lgb_train,
                num_boost_round=self.get_value_with_alias(booster_params,NUM_ROUNDS_ALIAS),
                valid_sets=lgb_eval,
                feval=feval,
                categorical_feature=self.categorical_features_idx,
                early_stopping_rounds=early_stopping_rounds)
        
        return trained_model    
    
    def param_extractor(self, param, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        # Parametros: 
        ##############
    
#        if 'seed' in param:
#            print("mym__seed is: " + str(param['seed']))
    
        # Añadimos 'num_class' a parametros
        if self.algorithm_id.upper() == "LGBMC_B":
            param['num_class'] = 1
        elif self.algorithm_id.upper()[:-2] == "LGBMC":
            if val_exists:
                param['num_class'] = len(set(np.concatenate([y_train_transf,y_val_transf],axis=0)))   
            else:
                param['num_class'] = len(set(y_train_transf))
                
        """
        # args_parametros: Parametros posicionales, sin 'keyword'. Deben ir en una posición exacta al llamar a xgb.train, tras X_train.
        ##################
        
        args=()
        #args = args + (param.pop('num_rounds'),)  # Movemos 'num_rounds' de parametros al primer args_parametro
    
    
        # kwargs_parametros: Parametros 'keyworded' que se pasan a xgb.train tras los args_parametros
        ####################
    
        kwargs={}
        if 'early_stopping_rounds' in param:
            kwargs['early_stopping_rounds'] = param.pop('early_stopping_rounds') # Movemos 'early_stopping_rounds' de parametros a kwargs_parametros
        #if 'verbose_eval' in param:
        #    kwargs['verbose_eval'] = param.pop('verbose_eval') # Movemos 'verbose_eval' de parametros a kwargs_parametros 
        #if xgb_maximize_can_do():
        #    kwargs['maximize'] = xgb_maximize(d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'])                            
     
        #if '@' in param['objective']:
        #    kwargs['obj'] = param.pop('objective') #eval(param['objective'].replace('@',''))
        #    kwargs['obj'] = eval(kwargs['obj'].replace('@',''))
            #del param['objective']
            
        if val_exists:
            kwargs['eval_set'] = [(X_train, 'train'), (X_val, 'eval')]
        else:
            kwargs['eval_set'] = [(X_train, 'train')]  
            
        kwargs['eval_metric'] = d['COMP_EVALUATION_METRIC']
    
        return param, args, kwargs
        """
        return param, {}, {}

    def predict_proba(self, model, X_train, class_prob):
        y=self.predict(model, X_train, class_prob)
        return y
    
    def predict(self, model, X_train, dataset_id='unknown'):
        
        num_iteration=-1 
        if 'best_iteration' in model.__dict__:
            num_iteration=model.best_iteration
            print("Predicting with num_iteration=",num_iteration)
        
        y=model.predict(X_train, num_iteration=num_iteration)
        
        return y
    
    def requires_label_encode(self):
        return self.algorithm_id in ['LGBMC', 'LGBMC_M']
    
    def set_earlystopping_best_model(self, best_model, fold):
        if 'alg_param' in self.results and 'early_stopping_rounds' in self.results['alg_param'] and self.results['alg_param']['early_stopping_rounds']>0:
            #self.results['score_validation_by_fold'][fold]        = best_model.best_score
            
            
            self.results['best_iteration'][fold]              = best_model.best_iteration
            if 'best_score' in best_model.__dict__:
                print('best_model.best_score',best_model.best_score)
                print('best_model.best_iteration',best_model.best_iteration)
            else:
                print("DEBUG: lightgbm not using early stopping")



    def _get_categorical_features(self, X_TRAIN__pd):
        '''
        does nothing because lightgbm handles 'auto' by itself
        '''
        cat_list=self.dashboard['ML_CATEGORICAL_FEATURES']
        print("_get_categorical_features returns: ", cat_list)
        return cat_list
    
    def uses_early_stopping(self):
        return True
    
    def implements_feature_importance(self):
        return True
    
    def feature_importance(self, best_model):
        #GET FEATURE IMPORTANCE
        #fscore, should be equivalent to xgboost 'weight' - the number of times a feature is used to split the data across all trees. is it?
        importance_fscore_array=best_model.feature_importance(importance_type='split')
        importance_gain_array=best_model.feature_importance(importance_type='gain')
        
        importance_fscore={}
        for i,x in enumerate(importance_fscore_array):
            importance_fscore[self.col_names[i]]=x
        
        importance_gain={}
        for i,x in enumerate(importance_gain_array):
            importance_gain[self.col_names[i]]=x
        
        
        
        import operator
        
        importance_array = sorted(importance_gain.items(), key=operator.itemgetter(1))

        importance_array= importance_array.copy()
    
        df_feat_imp = pd.DataFrame(importance_array, columns=['feature', 'feature_score'])
            
        #Creamos tabla con Feature, Dtype, Feat_Imp
#        df_feat_imp['feature_score'] = df_feat_imp['feature_score']# / df_feat_imp['feature_score'].sum()
        return importance_fscore, importance_gain, importance_array, df_feat_imp
    
