from framework.metaclasses import algorithm
from algorithms.base_algorithm import BaseAlgorithm

import os
import pandas as pd

import xgboost as xgb
import numpy as np

from colorama import Fore, Style

from my_Evaluation_Metrics import my_score
from my_ML_fx import njobs


@algorithm("MEANR")
class ColumnsMean(BaseAlgorithm):
    
    def __init__(self):
        self.accept_nan_values=True
        super().__init__()
    
    def param_extractor(self, param, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        self.accept_nan_values=param.get('accept_nan_values', True)
        
        return super().param_extractor(param, val_exists, X_train, X_val, y_train_transf, y_val_transf)
    
    def fit(self, model, val_exists, X_train, X_val, y_train_transf, X_test, y_val_transf, param, *args, **kwargs):
        return self
    
    
    def predict(self, model, X_train, dataset_id='unknown'):
        
        if self.accept_nan_values:
            cols_mean=np.nanmean(X_train, axis=1)
        else:
            cols_mean=np.mean(X_train, axis=1)
        
        return cols_mean

    
    

