from framework.metaclasses import algorithm
from algorithms.base_algorithm import BaseAlgorithm
from framework.execution_context import ExecutionContext

import os
import pandas as pd

import numpy as np

from colorama import Fore, Style

from my_Evaluation_Metrics import my_score
from my_ML_fx import njobs
from my_Prepro_fx import fit_transform_PCA
from sklearn import neighbors

RANDOM_SEED_PARAMS = ['random_state']
KNNR_PARAMS=['n_neighbors', 'weights', 'algorithm', 'leaf_size', 'p', 'metric', 'metric_params', 'n_jobs']



    

@algorithm("PCA_KNNR")
class ColumnsMean(BaseAlgorithm):
    
    def __init__(self):
        self.accept_nan_values=True
        super().__init__()
    
    def make_up(self, dashboard, default_dashboard):
        '''
        print("params_combinations"*50, params_combinations)
        
        for params in params_combinations:
            params['xyz']=[123]
        '''
        pass
    
    def param_extractor(self, params, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        self.standarize=params.get('standarize', True)
        
        if not 'n_jobs' in params:
            params['n_jobs']=-1
        
        return super().param_extractor(params, val_exists, X_train, X_val, y_train_transf, y_val_transf)
    
    def fit(self, model, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf, params, *args, **kwargs):
        
        dashboard, _, _ = ExecutionContext().get_current_context()
        for param_name in RANDOM_SEED_PARAMS:
            if param_name not in params:
                params[param_name]=dashboard['RANDOM_SEED']
                
        svd_solver=params.get('svd_solver', 'auto')
        whiten=params.get('whiten', False)
        tol=params.get('tol', 0.0)
        
        iterated_power=params.get('iterated_power', 'auto')
        
        
        self.pca, self.standard_scaler, X_train=fit_transform_PCA(X_train, 
                        self.standarize,
                        n_components = params['n_components'],
                        random_state=params['random_state'],
                        svd_solver=svd_solver,
                        tol=tol,
                        iterated_power=iterated_power,
                        whiten=whiten,
                        predict=True
                        )
        
        knn_kwargs={}
        for param_name in KNNR_PARAMS:
            if param_name in params:
                knn_kwargs[param_name]=params[param_name]
        self.knn = neighbors.KNeighborsRegressor(**knn_kwargs)
        self.knn.fit(X_train, y_train_transf)
        
        

        
        return self
    
    
    def predict(self, model, X_test, dataset_id='unknown'):
        
        if self.standarize:
            from sklearn.preprocessing import StandardScaler
            X_test= self.standard_scaler.transform(X_test)
        
        X_test = self.pca.transform(X_test)
        
        y = self.knn.predict(X_test)
        
        
        return y

