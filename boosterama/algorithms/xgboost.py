from framework.metaclasses import algorithm
from algorithms.base_algorithm import BaseAlgorithm

import os
import pandas as pd

import xgboost as xgb
import numpy as np

from colorama import Fore, Style

from my_Evaluation_Metrics import my_score
from my_ML_fx import njobs

class XGboostMetricConverter:
    def __init__(self, dashboard,score_name,name_y_true,name_y_predicted,can_do_xgbmaximize):
        self.dashboard = dashboard
        self.score_name = score_name
        self.name_y_true = name_y_true
        self.name_y_predicted = name_y_predicted
        self.can_do_xgbmaximize = can_do_xgbmaximize
        
    
    def my_score(self, y_preds__np, y__Dmatrix):
        
        evaluation_metric=self.dashboard['COMP_EVALUATION_METRIC']
        competition_name=self.dashboard['COMP_NAME']
        score_name=self.score_name
        name_y_true=self.name_y_true
        name_y_predicted=self.name_y_predicted
        do_print=self.dashboard['PRINT_EVAL_INFO']
        ML_APPLY_RESTRICTIONS=self.dashboard['ML_PRP_Y_APPLY_RESTRICTIONS']
        can_do_xgbmaximize = self.can_do_xgbmaximize
        evaluation_metric_greater_is_better = self.dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']
    
        #metric-fucntion(y_true, y_predicted,EVALUATION_METRIC,COMPETITION_NAME,score_name,name_y_true,name_y_predicted,doPrint,ML_APPLY_RESTRICTIONS=None)):
        
        y__np = y__Dmatrix.get_label()
        
        float_result = my_score(y__np, y_preds__np, evaluation_metric,competition_name,score_name,name_y_true,name_y_predicted,do_print,ML_APPLY_RESTRICTIONS)
        
        # RETURN
        if can_do_xgbmaximize:
            return 'error', float_result
        else:
            if evaluation_metric_greater_is_better:
                return 'error', -float_result  #Tengo que negativizar para que early stopping funcione como esperado
            else:
                return 'error', float_result

@algorithm("XGBC|XGBC_M|XGBR|XGBC_B")
class XgboostWrapper(BaseAlgorithm):
        
    def convert_to_algorithm_input_format(self, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf):
        
        X_train = xgb.DMatrix(X_train, missing=float('nan'), label=y_train_transf)   
        
        if val_exists:
            X_val = xgb.DMatrix(X_val, missing=float('nan'), label=y_val_transf)   
        
        X_test = xgb.DMatrix(X_test, missing=float('nan')) 
        
    
        return X_train, X_val, X_test
    
    def fit(self, model, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf, param, *args, **kwargs):
        trained_model = xgb.train(list(param.items()), X_train, *args, **kwargs)
        return trained_model
        
    
    ''' ALWAYS TRUE!!! '''
    def xgb_maximize_can_do(self):
        import platform
        sistema_operativo = platform.system()
        if ('Darwin' in sistema_operativo):
            return True
        else:
            return True # Solia ser False antes de versión 0.6 de xgb. Ahora no estoy seguro. 
    
    
    def param_extractor(self, param, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        # Parametros: 
        #############
    
        if 'seed' in param:
            print("mym__seed is: " + str(param['seed']))
    
        # Añadimos 'num_class' a parametros
        if self.algorithm_id.upper() == "XGBC_B":
            param['num_class'] = 1
        elif self.algorithm_id.upper()[:4] == "XGBC":
            if val_exists:
                param['num_class'] = len(set(np.concatenate([y_train_transf,y_val_transf],axis=0)))   
            else:
                param['num_class'] = len(set(y_train_transf))
    
    
        if 'n_jobs' in param:
            num_jobs=param['n_jobs']
        else:
            num_jobs=-2
    
        num_jobs=njobs(num_jobs)            
    
        # args_parametros: Parametros posicionales, sin 'keyword'. Deben ir en una posición exacta al llamar a xgb.train, tras X_train.
        ##################
        
        args=()
        args = args + (param.pop('num_rounds'),)  # Movemos 'num_rounds' de parametros al primer args_parametro
    
    
        # kwargs_parametros: Parametros 'keyworded' que se pasan a xgb.train tras los args_parametros
        ####################
    
        kwargs={}
        if 'early_stopping_rounds' in param:
            kwargs['early_stopping_rounds'] = param.pop('early_stopping_rounds') # Movemos 'early_stopping_rounds' de parametros a kwargs_parametros
        if 'verbose_eval' in param:
            kwargs['verbose_eval'] = param.pop('verbose_eval') # Movemos 'verbose_eval' de parametros a kwargs_parametros 

        if self.xgb_maximize_can_do(): #''' ALWAYS TRUE!!! '''
            kwargs['maximize'] = bool(self.dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'])                            
     
        if '@' in param['objective']:
            kwargs['obj'] = param.pop('objective') #eval(param['objective'].replace('@',''))
            kwargs['obj'] = eval(kwargs['obj'].replace('@',''))
            #del param['objective']
            
        if val_exists:
            kwargs['evals'] = [(X_train, 'train'), (X_val, 'eval')]
        else:
            kwargs['evals'] = [(X_train, 'train')]  
            
        kwargs['feval'] = XGboostMetricConverter(self.dashboard, "Score Train XGB","y_train_transf","y_train_transf_preds",self.xgb_maximize_can_do()).my_score                           
    
        param['nthread']=num_jobs
    
        return param, args, kwargs
    
    def predict_proba(self, model, X_train, class_prob):
        y=self.predict(model, X_train, class_prob)
        return y
    
    def predict(self, model, X_train, dataset_id='unknown'):
        
        if self.results['best_ntree_limit']!=[]:
            ntree_limit=model.best_ntree_limit
        else:
            ntree_limit=0 #defaults to 0 (use all trees)
        
        y=model.predict(X_train, ntree_limit=ntree_limit)
        
        return y
    
    
    def requires_label_encode(self):
        return self.algorithm_id in ['XGBC', 'XGBC_M']
    
    def set_earlystopping_best_model(self, best_model, fold):
        # En el caso XGB comprobamos si existe el parámetro early_stopping_rounds

        if 'alg_param' in self.results and 'early_stopping_rounds' in self.results['alg_param'] and self.results['alg_param']['early_stopping_rounds']>0:
            #self.results['score_validation_by_fold'][fold]        = best_model.best_score
            self.results['best_iteration'][fold]              = best_model.best_iteration
            print('best_model.best_score',best_model.best_score)
            print('best_model.best_iteration',best_model.best_iteration)
            try:
                self.results['best_ntree_limit'][fold]        = best_model.best_ntree_limit
                print('best_model.best_ntree_limit',best_model.best_ntree_limit)
            except:
                print(Fore.RED + 'Your xgboost version is old. best_model does not return the parameter best_ntree_limit, so prediction was NOT done in the optimal iteration, but early_stopping_rounds iterations later.' + Style.RESET_ALL)

    def uses_early_stopping(self):
        return True
    
    def implements_feature_importance(self):
        return True

    def feature_importance(self, best_model):
        import operator
        
        
        importance_fscore = best_model.get_fscore()
        importance_gain = best_model.get_score(importance_type='gain')
        
        
        mapper = {'f{0}'.format(i): v for i, v in enumerate(self.col_names)}
        importance_fscore = {mapper[k]: v for k, v in importance_fscore.items()}
        importance_gain = {mapper[k]: v for k, v in importance_gain.items()}

        importance_array = sorted(importance_gain.items(), key=operator.itemgetter(1))

        importance_array= importance_array.copy()

        #get_score(fmap='', importance_type='gain')
        #Get feature importance of each feature. Importance type can be defined as:
            #‘weight’ - the number of times a feature is used to split the data across all trees. 
            #‘gain’ - the average gain of the feature when it is used in trees 
            #‘cover’ - the average coverage of the feature when it is used in trees
        #Parameters:	fmap (str (optional)) – The name of feature map file
    
        df_feat_imp = pd.DataFrame(importance_array, columns=['feature', 'feature_score'])
            
        #Creamos tabla con Feature, Dtype, Feat_Imp
#        df_feat_imp['feature_score'] = df_feat_imp['feature_score']# / df_feat_imp['feature_score'].sum()
        return importance_fscore, importance_gain, importance_array, df_feat_imp

