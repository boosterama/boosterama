from framework.metaclasses import algorithm
from framework.execution_context import ExecutionContext
from algorithms.base_algorithm import BaseAlgorithm

import os
import lightgbm as lgb
import numpy as np
import pandas as pd

from colorama import Fore, Back, Style

from my_Evaluation_Metrics import my_score
from my_ML_fx import njobs

import traceback

from catboost import CatBoostRegressor

from inspect import isfunction


'''

https://github.com/catboost/tutorials/blob/master/catboost_python_tutorial.ipynb

  Synonyms:
      
    - 'learning_rate', 'eta'
    - 'border_count', 'max_bin'
    - 'depth', 'max_depth'
    - 'rsm', 'colsample_bylevel'
    - 'random_seed', 'random_state'
    - 'l2_leaf_reg', 'reg_lambda'
    - 'iterations', 'n_estimators', 'num_boost_round', 'num_trees'
    - 'od_wait', 'early_stopping_rounds'
    
'''

import pickle

PICKLE_PROTOCOL=pickle.HIGHEST_PROTOCOL

class CatBoostMetricAdaptor():
    def __init__(self, func):
        self.adapted_func=func
        d, _, _ = ExecutionContext().get_current_context()
        self.problem_type=d['COMP_PROBLEM_TYPE']
        self.is_bigger_better=bool(d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'])
        self.eval_name='custom_{}'.format(func.__name__)
    
    def get_final_error(self, error, weight):
        return error / (weight + 1e-38)

    def is_max_optimal(self):
        return self.is_bigger_better
    
    def _CatBoostArrayWrapper_to_numpy(self, daw):
        output=np.empty(len(daw))
        for i in range(len(daw)):
            output[i]=daw[i]
        return output
            
    

    def evaluate(self, approxes, target, weight):
        # approxes is a list of indexed containers
        # (containers with only __len__ and __getitem__ defined),
        # one container per approx dimension.
        # Each container contains floats.
        # weight is a one dimensional indexed container.
        # target is float.
        
        # weight parameter can be None.
        # Returns pair (error, weights sum)
#        print("AAAAAAAAAA"*1000)
        
        assert len(approxes) == 1
        assert len(target) == len(approxes[0])

        approx = approxes[0]
        preds=self._CatBoostArrayWrapper_to_numpy(approx)
        
        weight_sum = 0.0
        for i in range(len(preds)):
            w = 1.0 if weight is None else weight[i]
            weight_sum += w
        
        ground_truth=self._CatBoostArrayWrapper_to_numpy(target)
        
        error_sum = self.adapted_func(ground_truth, preds)
        
        pack=(error_sum, weight_sum)
        with open('eval_output.pickle', 'wb') as handle:
            pickle.dump(pack, handle, protocol=PICKLE_PROTOCOL)

        return error_sum, weight_sum
    


class LoglossMetric(object):
    def get_final_error(self, error, weight):
        return error / (weight + 1e-38)

    def is_max_optimal(self):
        return False

    def evaluate(self, approxes, target, weight):
        # approxes is a list of indexed containers
        # (containers with only __len__ and __getitem__ defined),
        # one container per approx dimension.
        # Each container contains floats.
        # weight is a one dimensional indexed container.
        # target is float.
        
        # weight parameter can be None.
        # Returns pair (error, weights sum)
        
        assert len(approxes) == 1
        assert len(target) == len(approxes[0])

        approx = approxes[0]

        error_sum = 0.0
        weight_sum = 0.0

        for i in range(len(approx)):
            w = 1.0 if weight is None else weight[i]
            weight_sum += w
            error_sum += -w * (target[i] * approx[i] - np.log(1 + np.exp(approx[i])))

        return error_sum, weight_sum


    




@algorithm("CATBOOSTR")
class CatBoostWrapper(BaseAlgorithm):
    
            
    def convert_to_algorithm_input_format(self, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf):
        
        '''
        las últimas versiones de LightGBM sólo usan el formato propio Dataset para el entrenamiento.
        Para predicción daría un error:
            TypeError: Cannot use Dataset instance for prediction, please use raw data instead
        
        Así que hacemos la conversión sólo en la función self.fit(...)
        
        '''
            
    
        return X_train, X_val, X_test
    
    def fit(self, model, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf, params, *args, **kwargs):
        
        
        if 'eval_metric' in params:
            eval_metric=params['eval_metric']
        else:
            d, _, _ = ExecutionContext().get_current_context()
            eval_metric=d['COMP_EVALUATION_METRIC']
            print("INFO. eval_metric not defined for CatBoost: using dashboard['COMP_EVALUATION_METRIC']: ", d['COMP_EVALUATION_METRIC'])
        
        if isfunction(eval_metric):
            eval_metric=CatBoostMetricAdaptor(eval_metric) #LoglossMetric() #
        params['eval_metric']=eval_metric
        # specify the training parameters via map
        
        categorical_features=self.categorical_features_idx
        if categorical_features=='auto':
            print("WARNING. CatBoost cat_features parameter doesn't accept 'auto'.  Using None")
            categorical_features=None
        
        trained_model = CatBoostRegressor(**params)
        #train the model
        trained_model.fit(
            X_train, y_train_transf,
            cat_features=categorical_features,
            eval_set=(X_val, y_val_transf),
        #     logging_level='Verbose',  # you can uncomment this for text output
            plot=True
        );
        

        
        return trained_model    
    
    def param_extractor(self, param, val_exists, X_train, X_val, y_train_transf, y_val_transf):
        if not 'use_best_model' in param:
            param['use_best_model']=True
            print("INFO. use_best_model wasn't configured.  Set to True")
        return param, {}, {}

    def predict_proba(self, model, X_train, class_prob):
        y=self.predict(model, X_train, class_prob)
        return y
    
    def predict(self, model, X_train, dataset_id='unknown'):
        
        # make the prediction using the resulting model
        #preds_class = model.predict(test_pool, prediction_type='Class')
        y = model.predict(X_train)
        
        
        return y
    
    def requires_label_encode(self):
        return self.algorithm_id in ['CATBOOSTC', 'CATBOOSTC_M']
    
    
    
    def implements_feature_importance(self):
        return True
    
    def feature_importance(self, best_model):
        #GET FEATURE IMPORTANCE
        #fscore, should be equivalent to xgboost 'weight' - the number of times a feature is used to split the data across all trees. is it?
        importance=best_model.get_feature_importance()
        
        ''' 
            WTF!!! :
        '''
        importance_fscore_array,importance_gain_array=importance,importance
        
        importance_fscore={}
        for i,x in enumerate(importance_fscore_array):
            importance_fscore[self.col_names[i]]=x
        
        importance_gain={}
        for i,x in enumerate(importance_gain_array):
            importance_gain[self.col_names[i]]=x
        
        
        
        import operator
        
        importance_array = sorted(importance_gain.items(), key=operator.itemgetter(1))

        importance_array= importance_array.copy()
    
        df_feat_imp = pd.DataFrame(importance_array, columns=['feature', 'feature_score'])
            
        #Creamos tabla con Feature, Dtype, Feat_Imp
#        df_feat_imp['feature_score'] = df_feat_imp['feature_score']# / df_feat_imp['feature_score'].sum()
        return importance_fscore, importance_gain, importance_array, df_feat_imp

    