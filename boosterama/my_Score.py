# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 22:15:11 2016

@author: DataCousins
"""

import matplotlib.pyplot as plt
import numpy as np
import sys

from colorama import Fore, Back, Style
from sklearn.metrics import confusion_matrix


from my_Feature_Importance import *
from my_Input_Output import *




    

def scores_did_improve(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best_overall, score_previous_best_fold=[]):   
         
    ##  Did score improve previous record ?
    #######################################
    did_improve_overall = "unknown"
    did_improve_fold    = "unknown"
    
    if val_exists:
        
        # FOLD
        if isinstance(score_previous_best_fold, float) == True:
            best_past_score_fold = float("{0:.10f}".format(score_previous_best_fold))
        else:
            best_past_score_fold = "No"
        
        
        if isinstance(results['model_score_val'], float) == True:
            current_score_fold = float("{0:.10f}".format(results['model_score_val']))
        else:
            current_score_fold = "No"

        if results['model_score_val'] is not None:                            
            if isinstance(best_past_score_fold, float) == True and  isinstance(current_score_fold, float) == True:
                if (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1 and current_score_fold > best_past_score_fold) or (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==0 and current_score_fold < best_past_score_fold): 
                    if best_past_score_fold != 0 and best_past_score_fold != 999999999999999999:
                        did_improve_fold = "yes"
                elif current_score_fold == best_past_score_fold:
                    if best_past_score_fold != 0 and best_past_score_fold != 999999999999999999:
                        did_improve_fold = "same"
                else:
                    if best_past_score_fold != 0 and best_past_score_fold != 999999999999999999:
                        did_improve_fold = "no"

                       
        # OVERALL
        if isinstance(score_previous_best_overall, float) == True:
            best_past_score_overall = float("{0:.10f}".format(score_previous_best_overall))
        else:
            best_past_score_overall = "No"
            

        if isinstance(results['model_score_cv'], float) == True:
            current_score_overall = float("{0:.10f}".format(results['model_score_cv']))
        else:
            current_score_overall = "No"

        if results['model_score_cv'] is not None:                            
            if isinstance(best_past_score_overall, float) == True and isinstance(current_score_overall, float) == True:
                if (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1 and current_score_overall > best_past_score_overall) or (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==0 and current_score_overall < best_past_score_overall): 
                    if best_past_score_overall != 0 and best_past_score_overall != 999999999999999999:
                        did_improve_overall = "yes"
                elif current_score_overall == best_past_score_overall:
                    if best_past_score_overall != 0 and best_past_score_overall != 999999999999999999:
                        did_improve_overall = "same"
                else:
                    if best_past_score_overall != 0 and best_past_score_overall != 999999999999999999:
                        did_improve_overall = "no"

         


    return did_improve_overall, did_improve_fold
    

    
    
    
    
    
    
    
def scores_get_previous_best(d, num_folds):
    
    # Create DEFAULTs
    ##################
    if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1:
        score_previous_best_folds = [0] * num_folds
        score_previous_best = 0
    else:
        score_previous_best_folds = [999999999999999999] * num_folds
        score_previous_best = 999999999999999999
        
    id_execution_best = 0
        
    
    
        
    try:  # If fails, it's usually because it's the first overall submission

        # GET and subset the executions from SQlite 
        ##############################################
        db_path = d['DIR_DATA'] +'executions_info.db'
        conn = sqlite3.connect(db_path) 
    
        
        query_similar = "SELECT LEVEL, ML_ALGO, SPLIT_CV_TYPE, SPLIT_CV_KFOLDS, SPLIT_VALIDATION_SET_PERC, score_validation, score_validation_by_fold, overfit, ID_EXECUTION, SPLIT_TS_TYPE, SPLIT_TS_train_FIX_NUM, SPLIT_TS_VALIDATION_FIX_NUM " \
                         "FROM executions_info " \
                         "WHERE LEVEL=\"" + str(d['LEVEL']) + "\" and SPLIT_CV_TYPE=\"" + str(d['SPLIT_CV_TYPE']) + "\" and SPLIT_CV_KFOLDS=" + str(d['SPLIT_CV_KFOLDS']) + " and ML_ALGO=\"" + str(d['ML_ALGO']) + "\" and overfit !=1"
    
        executions_similar_pd = pd.read_sql(query_similar, conn)
        
        conn.close()
    
        
        # CLEAN UP
        ##############################
        
        # CLEAN UP when NUM_FOLDS = 1
        if (d['SPLIT_CV_TYPE']=='Random' or d['SPLIT_CV_TYPE']=='Stratified') and d['SPLIT_CV_KFOLDS']==1 and len(executions_similar_pd)>0:
            executions_similar_pd = executions_similar_pd[executions_similar_pd['SPLIT_VALIDATION_SET_PERC']==d['SPLIT_VALIDATION_SET_PERC']]
        
        # CLEAN UP TIMESERIES
        elif d['SPLIT_CV_TYPE']=='TimeSeries' and len(executions_similar_pd)>0:
            
            executions_similar_pd = executions_similar_pd[executions_similar_pd['SPLIT_TS_TYPE']==d['SPLIT_TS_TYPE']]
            
            if "VAL-FIX" in d['SPLIT_TS_TYPE'] and len(executions_similar_pd)>0:
                executions_similar_pd = executions_similar_pd[executions_similar_pd['SPLIT_TS_VALIDATION_FIX_NUM']==d['SPLIT_TS_VALIDATION_FIX_NUM']]
                
            if "TRAIN-FIX" in d['SPLIT_TS_TYPE'] and len(executions_similar_pd)>0:
                executions_similar_pd = executions_similar_pd[executions_similar_pd['SPLIT_TS_train_FIX_NUM']==d['SPLIT_TS_train_FIX_NUM']]
    
                
    
                
        # CALCULATE 
        #############
        if len(executions_similar_pd)>0:
    
            # score_previous_best 
            #############################
            
            if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1: 
                score_previous_best = executions_similar_pd['score_validation'].max()
            else:
                score_previous_best = executions_similar_pd['score_validation'].min()
                
                
            # id_execution_best
            #############################
            id_execution_best = executions_similar_pd[executions_similar_pd['score_validation']==score_previous_best]['ID_EXECUTION'].tolist()[0]
            
            
            
            # score_previous_best_FOLDS
            #############################
            
            # Split score column into MULTIPLE COLUMNS
            columnas_scores_num = list(range(0,num_folds))
            columnas_scores = list(map(str, columnas_scores_num))
            executions_similar_pd = pd.DataFrame(executions_similar_pd.score_validation_by_fold.str.replace("[","").str.replace("]","").str.replace(" ","").str.split(',',num_folds).tolist(), columns = columnas_scores)
            
    
            # Converting strings to float
            #executions_similar_pd = executions_similar_pd.convert_objects(convert_numeric=True)
            for column in executions_similar_pd:
                executions_similar_pd[column] = pd.to_numeric(executions_similar_pd[column])
    
    
            # Best for each fold
            if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1: 
                for col_num in columnas_scores_num:
                    score_previous_best_folds[col_num] = executions_similar_pd[str(col_num)].max()
            else:
                for col_num in columnas_scores_num:
                    score_previous_best_folds[col_num] = executions_similar_pd[str(col_num)].min()

                   
        else: # NO similar previous execution.
            print("\nNo similar previous execution to calculate 'best score'\n")
            # we keep the default score
    

    except:
            print("\nNo similar previous execution to calculate 'best score'\n")

            
    #########################
    ###  BEST DASHBOARD   ###
    #########################
    if id_execution_best > 0:
        dashboard_best = pickle_load_one_dashboard(id_execution_best,d['DIR_BASE'])
    else:
        dashboard_best = "No"
        
    

    #########################
    ###   BEST FEATURES   ###
    #########################  
    is_feature_file, features_best_historic =  feature_importance_extract(d)           

            
    return score_previous_best, score_previous_best_folds, dashboard_best, features_best_historic, is_feature_file

    

    
    

def scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best_overall, score_previous_best_fold=[]):   
    if results['model_score_cv'] is None:  
        print("")                
        print (Fore.YELLOW + Back.BLACK + Style.DIM+ "    SCORE FOLD    " +  Style.RESET_ALL)
        print("")
    
    print(d['ML_GRIDSEARCHCV_OR_ALGORITHM'],alg,"best parameters:")
    print(best_params)
    print("") 
    
                    
    ##  Did score improve previous record ?
    #######################################
    did_improve_overall = "unknown"
    did_improve_fold    = "unknown"
    
    if val_exists:
        
        # FOLD
        if isinstance(score_previous_best_fold, float) == True:
            best_past_score_fold = float("{0:.10f}".format(score_previous_best_fold))
        else:
            best_past_score_fold = "No"
        
        
        if isinstance(results['model_score_val'], float) == True:
            current_score_fold = float("{0:.10f}".format(results['model_score_val']))
        else:
            current_score_fold = "No"

        if results['model_score_val'] is not None:                            
            if isinstance(best_past_score_fold, float) == True and  isinstance(current_score_fold, float) == True:
                if (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1 and current_score_fold > best_past_score_fold) or (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==0 and current_score_fold < best_past_score_fold): 
                    if best_past_score_fold != 0 and best_past_score_fold != 999999999999999999:
                        did_improve_fold = "yes"
                elif current_score_fold == best_past_score_fold:
                    if best_past_score_fold != 0 and best_past_score_fold != 999999999999999999:
                        did_improve_fold = "same"
                else:
                    if best_past_score_fold != 0 and best_past_score_fold != 999999999999999999:
                        did_improve_fold = "no"

                       
        # OVERALL
        if isinstance(score_previous_best_overall, float) == True:
            best_past_score_overall = float("{0:.10f}".format(score_previous_best_overall))
        else:
            best_past_score_overall = "No"
            

        if isinstance(results['model_score_cv'], float) == True:
            current_score_overall = float("{0:.10f}".format(results['model_score_cv']))
        else:
            current_score_overall = "No"

        if results['model_score_cv'] is not None:                            
            if isinstance(best_past_score_overall, float) == True and isinstance(current_score_overall, float) == True:
                if (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==1 and current_score_overall > best_past_score_overall) or (d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']==0 and current_score_overall < best_past_score_overall): 
                    if best_past_score_overall != 0 and best_past_score_overall != 999999999999999999:
                        did_improve_overall = "yes"
                elif current_score_overall == best_past_score_overall:
                    if best_past_score_overall != 0 and best_past_score_overall != 999999999999999999:
                        did_improve_overall = "same"
                else:
                    if best_past_score_overall != 0 and best_past_score_overall != 999999999999999999:
                        did_improve_overall = "no"

                        
                        
    if results['model_score_cv'] is not None: print("{t:20s} {v: 15.13f} {t2:20s}".format(t='TrainCV Score:',v=results['model_score_cv'], t2='(y values transformed)'))
    
    ################
    #    Train    ##
    ################
    if d['ML_PRP_Y_TRANSFORM'] != 'No':
        if results['model_score_train_transf'] is not None: print("{t:20s} {v: 15.13f} {t2:20s}".format(t='Train Scr Trf:',v=results['model_score_train_transf'], t2='(y_train_transf, y_train_transf_preds)'))
    if results['model_score_train'] is not None: print("{t:20s} {v: 15.13f} {t2:20s}".format(t='Train Score:',v=results['model_score_train'], t2='(y_train, y_train_preds)'))
    
    ###############
    # Validation ##
    ###############
    if val_exists:
        if d['ML_PRP_Y_TRANSFORM'] != 'No':
            if results['model_score_val_transf'] is not None: print("{t:20s} {v: 15.13f} {t2:20s}".format(t='ValidationSet Scr Trf:',v=results['model_score_val_transf'], t2='(y_val_transf_preds, y_val_transf)'))
        if results['model_score_val'] is not None: 
            print(Fore.GREEN + "{t:20s} {v: 15.13f} {t2:20s}".format(t='ValidationSet Score:',v=results['model_score_val'], t2='(y_val_preds, y_val)')  +  Style.RESET_ALL)

            if isinstance(score_previous_best_fold, float) == True:
                if did_improve_fold == "yes": 
                    print(Fore.BLUE + "\t\t\t  (" + str(score_previous_best_fold) + " was previous best score for similar executions. New Record!!)" +  Style.RESET_ALL)
                elif did_improve_fold == "same":
                    print(Fore.BLACK  + "\t\t\t  (your score was identical to previous best score for similar executions)" + Style.RESET_ALL)                
                elif did_improve_fold == "no":
                    if score_previous_best_fold != 0 and score_previous_best_fold != 999999999999999999:
                        print(Fore.RED  + "\t\t\t  (" + str(score_previous_best_fold) +  " is the historical best score for similar executions)" + Style.RESET_ALL)
    


    ################
    #   CV Score  ##
    ################
    
    if d['SPLIT_CV_KFOLDS']>=2 and results['model_score_cv'] is not None:

        if isinstance(score_previous_best_overall, float) == True:
            if did_improve_overall == "yes": 
                print(Fore.BLUE + "\t\t\t  (" + str(score_previous_best_overall) + " was previous best score for similar executions. New Record!!)" +  Style.RESET_ALL)
            elif did_improve_overall == "same":
                print(Fore.BLACK  + "\t\t\t  (your score was identical to previous best score for similar executions)" + Style.RESET_ALL)                
            elif did_improve_overall == "no":
                if score_previous_best_overall != 0 and score_previous_best_overall != 999999999999999999:
                    print(Fore.RED  + "\t\t\t  (" + str(score_previous_best_overall) +  " is the historical best score for similar executions)" + Style.RESET_ALL)

        
        
        if results['model_score_cv'] is not None: print("{t:20s} {v: 15.13f} {t2:20s}".format(t='CV Score Average:',v=results['model_score_cv'], t2='(y_val_preds, y_val)'))    

    ################
    #     Test    ##
    ################
    if y_test_exists:
        if results['model_score_test'] is not None: print(Fore.MAGENTA + "{t:20s} {v: 15.13f} {t2:20s}".format(t='TestSet Score:',v=results['model_score_test'], t2='(y_test_preds, y_test)')  +  Style.RESET_ALL)
    


    #####################   
    # CV Std. Deviation #
    #####################
    if d['SPLIT_CV_KFOLDS']>=2 and results['model_score_cv'] is not None:
        if results['model_score_cv_sd'] is not None: print("{t:20s} {v: 15.13f} {t2:20s}".format(t='CV Stdandar Dev.:',v=results['model_score_cv_sd'], t2='(y_val_preds, y_val)'))

    
    print()
    greaterbetter = 'Yes' if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'] == 1   else  'No'
    print('Evaluation Metric= ' + d['COMP_EVALUATION_METRIC'] + '.         Greater is Better= ' + greaterbetter + '.\n')
    
    return did_improve_overall, did_improve_fold
       
    


    
def confusion_matrix_compute_and_print(d, y_val, y_val_preds):
    if not d['CHART_CONFUSION_MATRIX']:
        #print("confussion matrix - skipped")
        return

    if(str(y_val.dtype)[0:3] != str(y_val_preds.dtype)[0:3]):
        
        if(str(y_val.dtype)[0:3]=='int' and str(y_val_preds.dtype)[0:3]=='flo'):
            y_true = y_val
            y_predicted = np.round(y_val_preds,0).astype('int')
        else:
            print('y_val.dtype',y_val.dtype)
            print('y_val_preds.dtype',y_val_preds.dtype)
            sys.exit('Error: dtypes de y_val o y_val_preds incompatibles con un problema de clasificación')
    else:
        y_true = y_val.copy()
        y_predicted = y_val_preds.copy()
    

    cm = confusion_matrix(y_true, y_predicted)
    cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    y_unique_labels = np.unique(y_true)
    
    print("Confusion Matrix\n")
    print(" Eje y: True Label. Eje x: Predicted Label.\n","Labels (en este orden):",y_unique_labels,"\n")       
    print(' Confusion Matrix Not Normalized\n',cm,"\n")       
    print(' Confusion Matrix Normalized\n',cm_normalized,"\n")
    

    if d['CHART_CONFUSION_MATRIX']:
        # Plot confusion matrix  (not normalized)
        np.set_printoptions(precision=2)
        plt.figure()
        confusion_matrix_plot(cm, y_unique_labels, title='Confusion matrix')

        
        # Plot confusion matrix normalized
        plt.figure()
        confusion_matrix_plot(cm_normalized, y_unique_labels, title='Normalized confusion matrix')
        plt.show()
        
    
        
def confusion_matrix_plot(cm, y_unique_labels, title='Confusion matrix', cmap=plt.cm.Blues):
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(y_unique_labels))
    plt.xticks(tick_marks, y_unique_labels, rotation=90)
    plt.yticks(tick_marks, y_unique_labels)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


    