# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 12:22:19 2016

@author: DataCousins
"""

import numpy as np
import pandas as pd
import sys


#################################
##  Import external submissions #
################################# 

def import_external_submissions(filePath,id_col_name,y_col_name):

    data = pd.read_csv(filePath, encoding='utf-8', sep=',', engine='c')
    
    
    # Comprobamos si los nombres de las columnas del dataframe son los esperados
    # Si hay un problema de mayusculas/minusculas lo corregimos, si no, abortamos
    cols_names = list(data.columns)
    if len(data.columns)!=2:
        print('Error: Two columns submission was expected, and found ', len(data.columns))
        sys.exit('End')
    else:
        if cols_names == [id_col_name,y_col_name] or cols_names == [y_col_name,id_col_name]:
            pass
        else:
            cols_names_lower = [col_name.lower() for col_name in cols_names]
            if cols_names_lower == [id_col_name.lower(),y_col_name.lower()]:
                data=data.rename(columns = {data.columns[0]:id_col_name})
                data=data.rename(columns = {data.columns[1]:y_col_name})
            elif cols_names_lower == [y_col_name.lower(),id_col_name.lower()]:
                data=data.rename(columns = {data.columns[0]:y_col_name})
                data=data.rename(columns = {data.columns[1]:id_col_name})
            else:
                print('Error: Columns names submission [',data.columns[0],',',data.columns[1],'] are differnts of expected ones: [',id_col_name,',',y_col_name,']')
                sys.exit('End')            
                                
                
    data = data.sort_values(id_col_name, axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    idx = np.array(data.pop(id_col_name))
    y = np.array(data).reshape(data.shape[0],)
    
    return idx, y
    
    
########################
##   CSV subsetting   ##
######################## 

def analisis_train_test(d,input_path):
    
    train = pd.read_csv(input_path + d['COMP_TRAIN_FILES'][0],nrows=20)
    test = pd.read_csv(input_path + d['COMP_TEST_FILES'][0],nrows=20)
    
    campos_train_ordenados = list(train.columns)
    campos_test_ordenados = list(test.columns)

    campos_comunes = list(set(train.columns) & set(test.columns))
    campos_train_no_test = list(set(train.columns) & (set(train.columns) ^ set(test.columns)))
    campos_test_no_train = list(set(test.columns) & (set(train.columns) ^ set(test.columns)))
           
    print("campos_comunes:\n",campos_comunes)
    print("campos_train_no_test:\n",campos_train_no_test)
    print("campos_test_no_train:\n",campos_test_no_train)

    return campos_comunes, campos_train_no_test, campos_test_no_train, campos_train_ordenados, campos_test_ordenados
    
    

def csv_subsetting_train(in_csv_file, out_csv_file, id_col_name, id_col_values, id_col_selected_values, y_col):
# Función que lee el fichero csv in_csv_file en un pd.DataFrame
# Comprueba que existe el campo id_col_name, y si no existiera lo crea con los valores id_col_values
# Hace un subset seleccionando las filas cuyo id_col_value esté en [id_col_selected_values]
# Guarda el subset en out_csv_file (si id_col_value no existía inicialmente, no guarda dicho campo)
# Devuelve el campo y_col como un np.array

    df = pd.read_csv(in_csv_file)
    
    #print('TRAIN.shape',df.shape)
    #print('id_col_values.shape',len(id_col_values))
    
    if id_col_name not in df.columns:
        df[id_col_name] = id_col_values    
        df = df.loc[(df[id_col_name].isin(id_col_selected_values))]
        df.drop(id_col_name, axis=1, inplace=True)
    else:
        df = df.loc[(df[id_col_name].isin(id_col_selected_values))]

    #print('train.shape',df.shape)
        
    df.to_csv(out_csv_file, sep=',', na_rep='', float_format=None, columns=None, header=True, index=False, index_label=None, mode='w', encoding=None, quoting=None, quotechar='"', line_terminator='\n', chunksize=None, tupleize_cols=False, date_format=None, doublequote=True, escapechar=None, decimal='.')
    
    return np.array(df[y_col])


def csv_subsetting_val_as_test(in_csv_file, out_csv_file, id_col_name, id_col_values, id_col_selected_values, y_col, campos_train_no_test, campos_test_no_train, campos_test_ordenados):
# Función que lee el fichero csv in_csv_file en un pd.DataFrame
# Comprueba que existe el campo id_col_name, y si no existiera lo crea con los valores id_col_values
# Hace un subset seleccionando las filas cuyo id_col_value esté en [id_col_selected_values]
# Guarda el subset en out_csv_file (si id_col_value no existía inicialmente, no guarda dicho campo)
# Elimina el campo "y" si se indica
  
    df = pd.read_csv(in_csv_file) 

    #print('TRAIN.shape',df.shape)
    #print('id_col_values.shape',len(id_col_values))
    
    if id_col_name not in df.columns:
        df[id_col_name] = id_col_values    

    df = df.loc[(df[id_col_name].isin(id_col_selected_values))]

    y_val = np.array(df[y_col])
    
    df.drop(campos_train_no_test, axis=1, inplace=True)
        
    
    campos_test_no_train_sin_contar_id = [x for x in campos_test_no_train if x != id_col_name]
    if len(campos_test_no_train_sin_contar_id) >= 1:
        print("El fichero Test original tiene features que no existen en Train (y que no es la feature 'Id'):\n",campos_test_no_train_sin_contar_id)

    #print('val_test.shape',df.shape)
    
    df = df[campos_test_ordenados]
          
    df.to_csv(out_csv_file, sep=',', na_rep='', float_format=None, columns=None, header=True, index=False, index_label=None, mode='w', encoding=None, quoting=None, quotechar='"', line_terminator='\n', chunksize=None, tupleize_cols=False, date_format=None, doublequote=True, escapechar=None, decimal='.')
    
    return y_val
