# -*- coding: utf-8 -*-
"""
Created on Sat Jan  9 09:57:36 2016

@author: DataCousins
"""

from collections import Counter

from colorama import Fore, Back, Style

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import re
import sqlite3
import sys

from sklearn import metrics
from sklearn import preprocessing
from sklearn.ensemble import *
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import ElasticNet,LinearRegression,LogisticRegression, Ridge, SGDClassifier, SGDRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.neighbors import KNeighborsClassifier, KNeighborsRegressor
from sklearn.pipeline import Pipeline
from sklearn.svm import SVC


from my_Evaluation_Metrics import *
from my_Input_Output       import *

from framework.algorithm.factory import AlgorithmFactory
from framework.execution_context import ExecutionContext


def rank_predictions(preds):
    print("rank_predictions", "="*50)
    print(preds.__class__)
    print(preds)
    ranked = preds.argsort().argsort()
    ranked=ranked.reshape((ranked.shape[0],1))
    min_max_scaler = preprocessing.MinMaxScaler()
    ranked_scaled = min_max_scaler.fit_transform(ranked)
    ranked_scaled=ranked_scaled.reshape(-1)
    print(ranked_scaled)
    return ranked_scaled




def keras_sparse_from_numpy(val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf):

    from scipy.sparse import csr_matrix, hstack
    from sklearn.metrics import mean_absolute_error
    from sklearn.preprocessing import StandardScaler
    from keras.models import Sequential
    from keras.layers import Dense, Dropout, Activation
    from keras.layers.advanced_activations import PReLU
    
    #import os
    #os.environ['THEANO_FLAGS'] = "device=cuda"
    #import theano
    #theano.config.device = 'gpu'
    #print(theano.config.device)
    #theano.config.device
    

    X_train = hstack([csr_matrix(X_train)], format = 'csr')
    
    if val_exists:
        X_val = hstack([csr_matrix(X_val)], format = 'csr')


    X_test = hstack([csr_matrix(X_test)], format = 'csr')



    return X_train, X_val, X_test






'''

X_test could seem to be unuseful for this task of model training.
But there are some utilities like fastAI.DataLoader that requieres to be initialized with tran, val and test datasets 

    ¯\_(ツ)_/¯

'''
def ml_algorithm(d, alg, val_exists, X_train, X_TRAIN__pd, y_train_transf, X_val, y_val_transf, X_test):
    
    #######################################################
    #        Parámetros que pasaremos a Algorithm         #
    #######################################################
    
    algorithm_factory=AlgorithmFactory()
    algorithm=algorithm_factory.get(d)
    algorithm.set_col_names(X_TRAIN__pd)
#    print("algorithm: ", algorithm)

    param_GridSearchCV = d['ML_ALGO_PARAMETROS'][alg]
    param={}
    for param_name in sorted(param_GridSearchCV.keys()):
        param[str(param_name.replace("mym__",""))] = param_GridSearchCV[param_name][0]
    
    param_original = param.copy()

    
    if 'n_jobs' in param:
        num_jobs=param['n_jobs']
    else:
        num_jobs=-2
    num_jobs=njobs(num_jobs)
 
    

    ########################################################################################
    #   En XGB y LGBM diferenciamos entre parametros, parametros posicionales y kwargs     #
    ########################################################################################
   
    param, args, kwargs = algorithm.param_extractor(param, val_exists, X_train, X_val, y_train_transf, y_val_transf)
    

    ###########################################################################
    #  Generamos y ajustamos el modelo según algoritmo y parámetros elegidos  #
    ###########################################################################
    instance=None
    # CLASSIFICATION models                    
    if alg == "Ada": # No permite paralelización. Solo datasets pequeños (decenas de miles de observaciones)
        instance = AdaBoostClassifier(**param)
    elif alg == "DT": # No permite paralelización
        instance = DecisionTreeClassifier(**param)
    elif alg == "ETC": # Permite paralelizar con n_jobs=-1
        instance = ExtraTreesClassifier(**param)
    elif alg == "KNN": # Permite paralelizar con n_jobs=-1
        instance = KNeighborsClassifier(**param)
    elif alg == "RF": # Permite paralelizar con n_jobs=-1
        instance = RandomForestClassifier(n_jobs=num_jobs,**param)
    elif alg == "SGD": 
        instance = SGDClassifier(**param)
    elif alg == "SVM": # No permite paralelización. Solo datasets pequeños (decenas de miles de observaciones)
        instance = SVC(**param)


    # REGRESSION models
    elif alg == "AdaR": # No permite paralelización
        instance = AdaBoostRegressor(**param)
    elif alg == "ELN": # No permite paralelización
        instance = ElasticNet(**param)
    elif alg == "ETR": # Permite paralelizar con n_jobs=-1
        instance = ExtraTreesRegressor(**param)
    elif alg == "GBR": # No permite paralelización
        instance = GradientBoostingRegressor(**param)        
    elif alg == "KNNR": # Permite paralelizar con n_jobs=-1
        instance = KNeighborsRegressor(n_jobs=num_jobs,**param)
    elif alg == "LR": # Permite paralelizar con n_jobs=-1
        instance = LinearRegression(n_jobs=num_jobs,**param)
    elif alg == "RFR": # Permite paralelizar con n_jobs=-1
        instance = RandomForestRegressor(n_jobs=num_jobs,**param)
    elif alg=="SGDR":
        instance = SGDRegressor(**param)

    



    ###########################################################################
    #     FIT                                                                 #
    ###########################################################################
    #print(alg)
    print("instance: ", instance)
    best_model=algorithm.fit(instance, val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf, param, *args, **kwargs)
    


    #######################################################
    #Almacenamos los parámetros con los formatos adecuados#
    #######################################################

    # Save best parametres for printing
    best_parameters=""
    best_params = []
    
    for param_name in sorted(param.keys()):
        best_params.append( str(param_name) + '=' + str(param[param_name]) )
        best_parameters += str(param_name)[:6]+str(param[param_name])+"-"
        
    best_parameters = re.sub(r'[^a-zA-Z0-9\-]','', best_parameters[:-1])


    return best_model, best_params, best_parameters, param_original








def ml_algo_keras(d, alg, val_exists, X_train, y_train_transf, X_val, y_val_transf, X_test, num_fold):

    from keras.models import Sequential
    from keras.layers import Dense, Dropout, Activation
    from keras.layers.advanced_activations import PReLU
    from keras.callbacks import EarlyStopping, History, ModelCheckpoint 
    

    
    




    def keras_batch_generator(X, y, batch_size, shuffle):
        #chenglong code for fiting from generator (https://www.kaggle.com/c/talkingdata-mobile-user-demographics/forums/t/22567/neural-network-for-sparse-matrices)
        number_of_batches = np.ceil(X.shape[0]/batch_size)
        counter = 0
        sample_index = np.arange(X.shape[0])
        if shuffle:
            np.random.shuffle(sample_index)
        while True:
            batch_index = sample_index[batch_size*counter:batch_size*(counter+1)]
            X_batch = X[batch_index,:].toarray()
            y_batch = y[batch_index]
            counter += 1
            yield X_batch, y_batch
            if (counter == number_of_batches):
                if shuffle:
                    np.random.shuffle(sample_index)
                counter = 0
    
    
    
    def keras_batch_generator_p(X, batch_size, shuffle):
        number_of_batches = X.shape[0] / np.ceil(X.shape[0]/batch_size)
        counter = 0
        sample_index = np.arange(X.shape[0])
        while True:
            batch_index = sample_index[batch_size * counter:batch_size * (counter + 1)]
            X_batch = X[batch_index, :].toarray()
            counter += 1
            yield X_batch
            if (counter == number_of_batches):
                counter = 0

   
    #######################################################
    #        Parámetros que pasaremos a Algorithm         #
    #######################################################

    param_GridSearchCV = d['ML_ALGO_PARAMETROS'][alg]
    param={}
    for param_name in sorted(param_GridSearchCV.keys()):
        param[str(param_name.replace("mym__",""))] = param_GridSearchCV[param_name][0]
    
    param_original = param.copy()

  
    ###########################################################################
    #    KERAS                                                                #
    ###########################################################################

    
    
    best_model = None
    
    if 'nbags' in param:
        NUM_BAGS = param['nbags']
    else:
        NUM_BAGS = 1
    
    y_train_transf_preds = np.zeros(X_train.shape[0])
    y_val_transf_preds   = np.zeros(X_val.shape[0])
    y_test_transf_preds  = np.zeros(X_test.shape[0])
    
    history = History()
    

    for num_bag in range(NUM_BAGS):
        best_model = param['model'](param['loss'], param['optimizer'], param['init'], param['dense'], param['dropout'], X_train.shape[1])  #ECUSTOM_KERAS_MODEL()
        
        
        # CALLBACKS
        early_stopping = EarlyStopping(monitor='val_loss', patience=param['early_stopping_rounds'], verbose=1, mode='auto')
        
        filepath = d['DIR_BASE'] + "/keras_checkpoints/" + str(d['ID']) + "_fold_" + str(num_fold) + "_batch_" + str(num_bag) + ".hdf5"
        checkpoint = ModelCheckpoint(filepath=filepath, monitor='val_loss', verbose=0, save_best_only=True, mode='auto')

        #callbacks_list = [checkpoint]
        
        fiteado = best_model.fit_generator(generator    = keras_batch_generator(X_train, y_train_transf, param['batch_generator_size'], True),
                                  nb_epoch          = param['nepochs'],
                                  samples_per_epoch = X_train.shape[0],
                                  #batch_size        = param['batch_generator_size'],
                                  #validation_split  = 0.2,
                                  validation_data   = [X_val.todense(),y_val_transf],
                                  callbacks         = [early_stopping, checkpoint, history],
                                  verbose           = 0)
        
        
        print()
        print('Best Epoch: ',str(early_stopping.best))
        #print(history.history.keys())
        
        '''
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title(d['COMP_NAME'] + ' model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train','val'], loc='upper left')
        plt.show()
        '''        

        
        best_model.load_weights(filepath)
        
        y_train_transf_preds +=  best_model.predict_generator(generator= keras_batch_generator_p(X_train,800, False), val_samples = X_train.shape[0])[:,0]
        y_val_transf_preds   +=  best_model.predict_generator(generator= keras_batch_generator_p(X_val,  800, False), val_samples = X_val.shape[0])[:,0]
        y_test_transf_preds  +=  best_model.predict_generator(generator= keras_batch_generator_p(X_test, 800, False), val_samples = X_test.shape[0])[:,0]
        
    #best_model.summary()    

    y_train_transf_preds = y_train_transf_preds / NUM_BAGS
    y_val_transf_preds   = y_val_transf_preds   / NUM_BAGS
    y_test_transf_preds  = y_test_transf_preds  / NUM_BAGS

    

    #######################################################
    #Almacenamos los parámetros con los formatos adecuados#
    #######################################################

    # Save best parametres for printing
    best_parameters=""
    best_params = []
    
    for param_name in sorted(param.keys()):
        best_params.append( str(param_name) + '=' + str(param[param_name]) )
        best_parameters += str(param_name)[:6]+str(param[param_name])+"-"
        
    best_parameters = re.sub(r'[^a-zA-Z0-9\-]','', best_parameters[:-1])


    return best_model, best_params, best_parameters, param_original, y_train_transf_preds, y_val_transf_preds, y_test_transf_preds
    
    # Big return de la función padre. Intentar devolver la mayoría de estos:
    #    best_params
    #    best_parameters
    #
    #    y_train
    #    y_train_preds
    #    y_train_transf
    #    y_train_transf_preds
    #
    #    y_val
    #    y_val_preds
    #    y_val_transf
    #    y_val_transf_preds
    #
    #    y_test_transf_preds










                            
def ml_gridSearchCV(d, results, alg, X_train, X_TRAIN__pd, y_train_transf, num_folds_CV):
    
    
    # XGB no lo hacemos con GridsearchCV sino con la implementación directa, mucho más potente.
    if d['ML_ALGO'][0].lower()[:3] == 'xgb':
        print(Fore.RED + " XgBoost NO se puede utilizar con DO_GRIDSEARCHCV_OR_ALGORITHM == 'gridsearchcv'.\n\n Selecciona  DO_GRIDSEARCHCV_OR_ALGORITHM == 'Algorithm' \n\n" + Style.RESET_ALL)
        sys.exit()
        

    #######################################################
    #               Parámetros GridSearchCV               #
    #######################################################
    param_GridSearchCV = d['ML_ALGO_PARAMETROS'][alg]

    
    my_model = initialize_model(alg) # Initialize Models
    clf = Pipeline([('mym', my_model)])  # Create the pipeline 
    
    custom_scorer = metrics.make_scorer(my_score,EVALUATION_METRIC=d['COMP_EVALUATION_METRIC'],COMPETITION_NAME=d['COMP_NAME'],score_name="Score TrainCV",name_y_true="y_train_transf_CVfoldN",name_y_predicted="y_train_transf_preds_CVfoldN",doPrint=d['PRINT_EVAL_INFO'],ML_APPLY_RESTRICTIONS=d['ML_PRP_Y_APPLY_RESTRICTIONS'])

    model = GridSearchCV(clf, param_GridSearchCV, n_jobs=num_jobs, scoring=custom_scorer, cv=num_folds_CV)          
  
    
    # Fit Grid Search Model
    model.fit(X_train, y_train_transf)
    
    ## Get best model
    best_model=model.best_estimator_

    results['model_score_cv'] = model.best_score_


    # Save best parametres for printing
    best_parameters=""
    best_params = []
    for param_name in sorted(param_GridSearchCV.keys()):
        best_params.append( str(param_name.replace("mym__","")) + '=' + str(model.best_params_[param_name]) )
        best_parameters += str(param_name.replace("mym__",""))[:6]+str(model.best_params_[param_name])+"-"
    best_parameters = re.sub(r'[^a-zA-Z0-9\-]','', best_parameters[:-1])
    

    param_original={}
    for param_name in sorted(param_GridSearchCV.keys()):
        param_original[str(param_name.replace("mym__",""))] = param_GridSearchCV[param_name][0]
        
    return best_model, best_params, best_parameters, param_original




def moda(my_list):
    counter = Counter(my_list)
    max_count = max(counter.values())
    mode = [k for k,v in counter.items() if v == max_count]
    return mode



def njobs(n_jobs):

    if n_jobs==-2:
        sistema_operativo = platform.system()
        if ('Windows' in sistema_operativo):
            n_jobs= 1
        else:
            n_jobs= -1
    
    return n_jobs


def obj_fair_100(y_true, y_predicted):
    FAIR_CONSTANT = 100
    #labels = y_true
    y_predicted = y_predicted.get_label()
    #y_true = -y_true

    x = y_true - y_predicted
    den = abs(x) + FAIR_CONSTANT
    grad = FAIR_CONSTANT * x / den
    hess = FAIR_CONSTANT * FAIR_CONSTANT / (den * den)
    return grad, hess
    
    
def obj_logreg_2(y_true,y_predicted):
    FAIR_CONSTANT = 2
    #labels = y_true
    y_predicted = y_predicted.get_label()
    #y_true = -y_true

    x = y_true - y_predicted
    den = abs(x) + FAIR_CONSTANT
    grad = FAIR_CONSTANT * x / den
    hess = FAIR_CONSTANT * FAIR_CONSTANT / (den * den)
    return grad, hess
    

def order_by_idx(d, idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd):
    # Ordenamos observaciones del TRAIN según los valores de d['COMP_COLUMN_ID'] (idx_TRAIN__pd)
    TRAIN__pd = pd.concat([idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd], axis=1)
    TRAIN__pd.sort_values(by=[d['COMP_COLUMN_ID']], ascending=[True], inplace = True)
    TRAIN__pd.reset_index(drop=True, inplace=True)
    idx_TRAIN__pd = TRAIN__pd.pop(d['COMP_COLUMN_ID'])
    y_TRAIN__pd = TRAIN__pd.pop(d['COMP_COLUMN_CLASS'])
    X_TRAIN__pd = TRAIN__pd
    
    # Ordenamos observaciones del test según los valores de d['COMP_COLUMN_ID'] (idx_test__pd)
    test__pd = pd.concat([idx_test__pd, X_test__pd], axis=1)
    test__pd.sort_values(by=[d['COMP_COLUMN_ID']], ascending=[True], inplace = True)
    test__pd.reset_index(drop=True, inplace=True)
    idx_test__pd = test__pd.pop(d['COMP_COLUMN_ID'])
    X_test__pd = test__pd

    return (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd)


def order_datasets_by_idx(d, idx__pd, y__pd, X__pd=None):
    # Ordenamos observaciones del TRAIN según los valores de d['COMP_COLUMN_ID'] (idx__pd)
    if type(X__pd).__name__ == 'NoneType':
        T__pd = pd.concat([idx__pd, y__pd], axis=1)
        T__pd.sort_values(by=[d['COMP_COLUMN_ID']], ascending=[True], inplace = True)
        T__pd.reset_index(drop=True, inplace=True)
        idx__pd = T__pd.pop(d['COMP_COLUMN_ID'])
        y__pd = T__pd.pop(d['COMP_COLUMN_CLASS'])        
    else:
        T__pd = pd.concat([idx__pd, X__pd, y__pd], axis=1)
        T__pd.sort_values(by=[d['COMP_COLUMN_ID']], ascending=[True], inplace = True)
        T__pd.reset_index(drop=True, inplace=True)
        idx__pd = T__pd.pop(d['COMP_COLUMN_ID'])
        y__pd = T__pd.pop(d['COMP_COLUMN_CLASS'])
        X__pd = T__pd
    return (idx__pd, y__pd, X__pd)

  


def predict(best_model, X_train, X_val, X_test, N_train, N_val, N_test, val_exists, results, alg, predict_prob, class_prob):
    #if alg.upper()[:3] == 'XGB':
    #    import xgboost as xgb
    
    d, _, _ = ExecutionContext().get_current_context()
    algorithm_factory=AlgorithmFactory()
    algorithm=algorithm_factory.get(d)
    
    y_val_transf_preds = None
    
    y_train_transf_preds = np.zeros(N_train,dtype=float)
    y_test_transf_preds = np.zeros(N_test,dtype=float)
    
    if predict_prob:
        y_train_transf_preds = algorithm.predict_proba(best_model, X_train, class_prob)
        y_test_transf_preds  = algorithm.predict_proba(best_model, X_test,  class_prob)
    else:
        y_train_transf_preds = algorithm.predict(best_model, X_train, dataset_id='train')
        y_test_transf_preds  = algorithm.predict(best_model, X_test, dataset_id='test')

    if val_exists:
        y_val_transf_preds = np.zeros(N_val,dtype=float)
        
        
        if predict_prob:
            y_val_transf_preds = algorithm.predict_proba(best_model, X_val, class_prob)
        else:
            y_val_transf_preds = algorithm.predict(best_model, X_val, dataset_id='val')       

    
    
    # Activar para chequear predict_proba  
    #print('y_train_transf_preds.shape',y_train_transf_preds.shape,y_train_transf_preds[0:10])
    #if val_exists:print('y_val_transf_preds.shape',y_val_transf_preds.shape,y_val_transf_preds[0:10])
    #print('y_test_transf_preds.shape',y_test_transf_preds.shape,y_test_transf_preds[0:10])
    
    return y_train_transf_preds, y_val_transf_preds, y_test_transf_preds



    










def y_label_decode(lab_enc, val_exists, y_train_transf, y_train_transf_preds, y_val_transf, y_val_transf_preds, y_test_transf_preds):
    
    ''' TRANSFORM PROBS TO SINGLE CLASS PREDICTION '''
    def probs_to_single_class_prediction(probs):
        if not(len(probs.shape)==1 or probs.shape[1]==1):
            # we have probs for each class.  probably softmax "probs"
            probs=np.argmax(probs, axis=1) 
        return probs
    
    y_train_transf = lab_enc.inverse_transform([int(i) for i in y_train_transf])
    
    
    y_train_transf_preds=probs_to_single_class_prediction(y_train_transf_preds) 
    y_test_transf_preds =probs_to_single_class_prediction(y_test_transf_preds) 
    
        
    y_train_transf_preds = lab_enc.inverse_transform([int(i) for i in y_train_transf_preds])
    y_test_transf_preds  = lab_enc.inverse_transform([int(i) for i in y_test_transf_preds])
    
    if val_exists:
        y_val_transf = lab_enc.inverse_transform([int(i) for i in y_val_transf])
        
        y_val_transf_preds =probs_to_single_class_prediction(y_val_transf_preds) 
        y_val_transf_preds = lab_enc.inverse_transform([int(i) for i in y_val_transf_preds])
        
    

    return y_train_transf, y_train_transf_preds, y_val_transf, y_val_transf_preds, y_test_transf_preds

      
      
def y_label_encode(val_exists, y_train_transf, y_val_transf):
    
    lab_enc = preprocessing.LabelEncoder()                                                         
    if val_exists:
        lab_enc.fit(np.concatenate([y_train_transf,y_val_transf],axis=0))    #print(lab_enc.classes_)  
        y_train_transf = lab_enc.transform(y_train_transf)     #    print(y_train_transf_enc)     #array([2, 2, 1]...)
        y_val_transf = lab_enc.transform(y_val_transf)     #    print(y_train_transf_enc)     #array([2, 2, 1]...)
    else:                             
        lab_enc.fit(y_train_transf)    #print(lab_enc.classes_)  
        y_train_transf = lab_enc.transform(y_train_transf)    
    return y_train_transf, y_val_transf, lab_enc







def y_transform(d, val_exists, idx_train, y_train, idx_val, y_val):
    
    y_train_original = None
    y_train_transf = None
    y_val_original = None
    y_val_transf = None
    
    if d['ML_PRP_Y_TRANSFORM'] != 'No':
        y_train_original = np.copy(y_train)
        y_train_transf = transform_y(d, y_train,d['ML_PRP_Y_TRANSFORM'],idx_train,inverse=False,v_original=None,v_name="y_train",do_print=d['PRINT_TRANSFORM_Y_INFO'])
        if val_exists:
            y_val_original = np.copy(y_val)
            y_val_transf = transform_y(d, y_val,d['ML_PRP_Y_TRANSFORM'],idx_val,inverse=False,v_original=None,v_name="y_val",do_print=d['PRINT_TRANSFORM_Y_INFO'])                        
    else:
        y_train_transf = y_train
        if val_exists:
            y_val_transf = y_val
    
    return y_train_original, y_train_transf, y_val_original, y_val_transf


 
def y_transform_inverse(d, val_exists, y_train_original, y_train_transf, y_train_transf_preds, y_val_original, y_val_transf, y_val_transf_preds, y_test_transf_preds, idx_train, idx_val, idx_test):
    
    y_val = None
    y_val_preds = None
    
    if d['ML_PRP_Y_TRANSFORM'] != 'No':
        
        y_train = transform_y(d, y_train_transf,d['ML_PRP_Y_TRANSFORM'],idx_train,inverse=True,v_original=y_train_original,v_name="y_train",do_print=d['PRINT_TRANSFORM_Y_INFO'])
        y_train_preds = transform_y(d, y_train_transf_preds,d['ML_PRP_Y_TRANSFORM'],idx_train,inverse=True,v_original=None,v_name="y_train_preds",do_print=d['PRINT_TRANSFORM_Y_INFO'])
    
        if val_exists:
            y_val_preds = transform_y(d, y_val_transf_preds,d['ML_PRP_Y_TRANSFORM'],idx_val,inverse=True,v_original=None,v_name="y_val_preds",do_print=d['PRINT_TRANSFORM_Y_INFO'])
            y_val = transform_y(d, y_val_transf,d['ML_PRP_Y_TRANSFORM'],idx_val,inverse=True,v_original=y_val_original,v_name="y_val",do_print=d['PRINT_TRANSFORM_Y_INFO'])
            #if d['COMP_PROBLEM_TYPE'] == 'Regr' or d['COMP_PROBLEM_TYPE'] == 'Class_B':                        
            #    y_val = y_val.astype(float)
        
        y_test_preds = transform_y(d, y_test_transf_preds,d['ML_PRP_Y_TRANSFORM'],idx_test,inverse=True,v_original=None,v_name="y_test_preds",do_print=d['PRINT_TRANSFORM_Y_INFO'])
    
    else:
        
        y_train = y_train_transf
        y_train_preds = y_train_transf_preds
        if val_exists:
            y_val_preds = y_val_transf_preds
            y_val = y_val_transf
        y_test_preds = y_test_transf_preds
    
    # POSPROCESSING
    if d['ML_Y_POSPROCESSING_RANK'].lower()=='yes':
        y_train_preds=rank_predictions(y_train_preds)
        if val_exists:
            y_val_preds=rank_predictions(y_val_preds)
        y_test_preds=rank_predictions(y_test_preds)

    if len(d['ML_Y_POSPROCESSING'])>0:
        '''
        y_cols_name=['y_train', 'y_train_preds', 'y_test_preds']
        if val_exists:
            y_cols_name.extend(['y_val', 'y_val_preds'])
        for col_name in y_cols_name:
            for func_src_code in d['ML_Y_POSPROCESSING']:
                python_transformation="preds = "+func_src_code
                python_sentence=python_transformation.replace('preds', col_name)
                print(python_sentence)
                exec(python_sentence, locals())
        '''
        for func_src_code in d['ML_Y_POSPROCESSING']:
            #print(y_train.__class__.__name__)
            #print(y_train_preds.__class__.__name__)
            #print(eval.__class__.__name__)
            #print(y_val_preds.__class__.__name__)
            #print(y_test_preds.__class__.__name__)
            y_train=eval(func_src_code.replace('preds', 'y_train'))
            y_train_preds=eval(func_src_code.replace('preds', 'y_train_preds'))
            if val_exists:
                y_val=eval(func_src_code.replace('preds', 'y_val'))
                y_val_preds=eval(func_src_code.replace('preds', 'y_val_preds'))
            y_test_preds=eval(func_src_code.replace('preds', 'y_test_preds'))

            #print("----------")
            #print(y_train.__class__.__name__)
            #print(y_train_preds.__class__.__name__)
            #print(eval.__class__.__name__)
            #print(y_val_preds.__class__.__name__)
            #print(y_test_preds.__class__.__name__)
            
        
    
    return y_train, y_train_preds, y_val, y_val_preds, y_test_preds






def transform_y(d, v,ML_TRANSFORM_Y,idx,inverse,v_original=None,v_name=None,do_print=0):

    if do_print: print("Antes de transformar ","inversa" if inverse else "",v_name,"np.min()",np.min(v),"np.max()",np.max(v))

    if ML_TRANSFORM_Y == 'log':
        if not inverse:
            v = np.log1p(v)
        else:
            v = np.expm1(v)  
    elif ML_TRANSFORM_Y.startswith('res:'):
        
        if '@' in ML_TRANSFORM_Y:
            reference_id=ML_TRANSFORM_Y.split('@')[1]
        
        use_oof_preds=v_name.startswith('y_train') or v_name.startswith('y_val')
            
        preds = csv_load_predictions(d, reference_id, new_y_col_name='reference_model_preds', load_oof=use_oof_preds, load_test=not use_oof_preds)
        
        idx_pd=pd.DataFrame({d['COMP_COLUMN_ID']:idx})
        ref_model_preds=idx_pd.merge(preds, how='left', on=d['COMP_COLUMN_ID'])
        if not inverse:
            v-=ref_model_preds.reference_model_preds.values
        else:
            v+=ref_model_preds.reference_model_preds.values
    

            
    elif ML_TRANSFORM_Y[:5] == 'log_x':  # log_x_+_200
    
        try:
            ELEMENTOS = ML_TRANSFORM_Y.split('_')
            SIMBOLO = ELEMENTOS[2]
            CONSTANTE = int(ELEMENTOS[3])
    
            if SIMBOLO == '+':    
                if not inverse:
                    v = np.log1p(v + CONSTANTE)
                else:
                    v = np.expm1(v) - CONSTANTE
    
            if SIMBOLO == '-':    
                if not inverse:
                    v = np.log1p(v - CONSTANTE)
                else:
                    v = np.expm1(v) + CONSTANTE
    
            if SIMBOLO == '*':    
                if not inverse:
                    v = np.log1p(v * CONSTANTE)
                else:
                    v = np.expm1(v) / CONSTANTE
    
            if SIMBOLO == '/':    
                if not inverse:
                    v = np.log1p(v / CONSTANTE)
                else:
                    v = np.expm1(v) * CONSTANTE
                    
        except:
            print()
            print('We were not able to parse',ML_TRANSFORM_Y)
            print('We will only apply log(x)')
            print()
            print()

            if not inverse:
                v = np.log1p(v)
            else:
                v = np.expm1(v)
                        

    elif ML_TRANSFORM_Y == 'square':
        if not inverse:
            v = v**2
        else:
            #v[v<0]=0 #Sustituimos valores incoherentes (podríamos hacer esto en todos los casos)
            v = np.sqrt(v)
    elif ML_TRANSFORM_Y == 'exp':
        if not inverse:
            v = np.expm1(v)
        else:
            v = np.log1p(v)
    elif ML_TRANSFORM_Y == 'sqrt':
        if not inverse:
            v = np.sqrt(v)
        else:
            v = v**2
    elif ML_TRANSFORM_Y == 'int':
        if not inverse:
            v = v.astype(int)
        else:
            v = v.astype(float)
    else:
        sys.exit("Error: function transform_y")
    
    if inverse==True and type(v_original).__name__ != 'NoneType':
        dif_arr = v - v_original
        mse = (dif_arr ** 2).mean(axis=None)
        max_value_error = np.max(dif_arr)
        min_value_error = np.min(dif_arr)
        num_distinct_values = np.sum(dif_arr!=0)
        num_equal_values = np.sum(dif_arr==0)
        if do_print: 
            print ("\tML_TRANSFORM_Y: We force iverse_transform(transform(",v_name,")) == ",v_name," to avoid decimals conversion errors")
            print ("\tML_TRANSFORM_Y: Max individual value error between iverse_transform(transform(",v_name,")) and ",v_name,":",max_value_error)
            print ("\tML_TRANSFORM_Y: Min individual value error between iverse_transform(transform(",v_name,")) and ",v_name,":",min_value_error)
            print ("\tML_TRANSFORM_Y: Mean Square Error between iverse_transform(transform(",v_name,")) and ",v_name,":",mse)
            print ("\tML_TRANSFORM_Y: Num equal values between iverse_transform(transform(",v_name,")) and ",v_name,":",num_equal_values)
            print ("\tML_TRANSFORM_Y: Num distinct values between iverse_transform(transform(",v_name,")) and ",v_name,":",num_distinct_values)        
        v = v_original
        
    if do_print:    
        print("Tras de transformar","inversa" if inverse else "",v_name,"np.min()",np.min(v),"np.max()",np.max(v))
        print("-------------------------------------------------------------------")
        
    return v                



