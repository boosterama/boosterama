# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 19:40:51 2016

@author: DataCousins
"""

import numpy as np
import pandas as pd

def debug_compare_pandas_fields_names_from_two_dataframes(A_name,A,B_name,B):

    campos_comunes = list(set(A.columns) & set(B.columns))
    campos_A_no_B = list(set(A.columns) & (set(A.columns) ^ set(B.columns)))
    campos_B_no_A = list(set(B.columns) & (set(A.columns) ^ set(B.columns)))
           
    print("Total campos comunes:",len(campos_comunes))
    print("Campos en",A_name,"y NO en",B_name,":",campos_A_no_B)
    print("Campos en",B_name,"y NO en",A_name,":",campos_B_no_A)

    return None
    


def debug_elements_shape(list_of_tuples):
# lista de tuplas: (nombre,elemento)

    for tupla in list_of_tuples:
        print(tupla[0],tupla[1].shape)

    return None
    
    
'''
Ejemplo de uso:

if DEBUG:
    print("\n\n------ INI DEBUG ------")
    debug_elements_shape([('X_TRAIN__pd',X_TRAIN__pd),('X_test__pd',X_test__pd)])
    debug_compare_pandas_fields_names_from_two_dataframes('X_TRAIN__pd',X_TRAIN__pd,'X_test__pd',X_test__pd)
    print("------ FIN DEBUG ------\n\n")
'''