# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 16:14:01 2015

@author: DataCousins
"""

import gc
from my_Dashboard_Utils import *
from my_MAIN import *
from my_Input_Output import *
import pandas as pd
import pickle
from random import randrange, uniform
#import resource
import os
import subprocess
import sys
import time

TEST_MODE=False # No ejecuta predicciones ni carga datos.  Sirve para probar el iterador (combinaciones)
DEBUG_MODE=True



def runDashboard(dashboard,algoritmos, CUSTOM_IMPORTER=None,CUSTOM_ML=None,CUSTOM_SUBMISSION_PRE=None, CUSTOM_SUBMISSION_CSV=None):
    
    # BAGGING IMPLEMENTATION
    
    # all FRIENDLY_ID from dashboards sent to run_iterateGo_single to be executed during this call to runDashboard.
    session_executions_history=[] 
    
    
    if 'BAGGING' in dashboard and dashboard['BAGGING']==1:
        '''
        
            1.  EXECUTE ALL DASHBOARDS TO DO BAGGING
            
                We will remove all the BAGGING configurations, since they're unnecesary and make it dirtier for the cache
        
        '''
        main_dashboard=dashboard.copy()
        main_dashboard['BAGGING']=0
        main_dashboard['BAGGING_ALGORITHM']=[]
        main_dashboard['BAGGING_ALGORITHM_PARAMS']={}
        
        # Remove features definend for bagging only
        for_bagging_only=[]
        for k in main_dashboard.keys():
            if k.startswith('BAGGING__'):
                for_bagging_only.append(k)
        for k in for_bagging_only:
            del main_dashboard[k]
        
        resultados__pd = runDashboard_algoritmos(main_dashboard, algoritmos, session_executions_history, CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
        print('session_executions_history: ', session_executions_history)
        
        
        '''
            2.  DO BAGGING
                We will make up dashboard in order to do a ML_2 with the previous executions
        '''
        
        # 1 - Cleansing: no preprocessing
        from my_Dsh_DEFAULT import default_dashboard
        for k in dashboard.keys():
            key=k
            if k.endswith('*'):
                key=k[:-1]
            if key.startswith('PRP_') or key in BAGGING_NOT_SUPPORTED:
                if key in default_dashboard:
                    print('/// {}: dashboard[{}]={}'.format(k, key, default_dashboard[key]))
                    del(dashboard[k])
                    dashboard[key]=default_dashboard[key]
                else:
                    print('WARNING.  ', key, " wasn't defined in default_dashboard")
        
        # 2 - Add PREPROCESSING defined explicitely for bagging with prefix  BAGGING__
        for k in for_bagging_only:
            dashboard[k.replace('BAGGING__','')]=dashboard[k]
            
            if k in dashboard:
                del dashboard[k]
            if k.endswith('*'):
                k=k[:-1]
            if k in dashboard:
                del dashboard[k]
        
        
        dashboard['ML_ALGO']=dashboard['BAGGING_ALGORITHM']
        algoritmos=dashboard['BAGGING_ALGORITHM_PARAMS']
        try:
            base_level=int(dashboard['LEVEL'][0])
            level="{}_ML".format(base_level + 1)
        except:
            level='2_ML'
        
        dashboard['LEVEL']=level
        print("bagging performed as ", level)
        #="or FRIENDLY_ID='".join(session_execution_friendly_ids)
        where_clause="FRIENDLY_ID in ('" + "', '".join(session_executions_history) + "')"
        dashboard['L2_QUERY_WHERE']=where_clause
        
        # Delete from dashboard all combinations
        for k in dashboard.keys():
            if k.endswith('*'):
                dashboard[k]=dashboard[k][0:1]
        for algorithm in algoritmos.keys():
            algorithm_parameters=algoritmos[algorithm]
            for parameter in algorithm_parameters.keys():
                if len(algorithm_parameters[parameter])>1:
                    algorithm_parameters[parameter]=algorithm_parameters[parameter][0:1]
        
        #dashboard['BAGGING_ALGORITHM']=[]
        #dashboard['BAGGING_ALGORITHM_PARAMS']={}
        dashboard['IMP_LOAD_GROUND_TRUTH_ONLY']=1
        
        
        
        
    
    resultados__pd = runDashboard_algoritmos(dashboard, algoritmos, session_executions_history, CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
    
    
    return resultados__pd        


def runDashboard_algoritmos(dashboard,algoritmos, session_executions_history, CUSTOM_IMPORTER=None,CUSTOM_ML=None,CUSTOM_SUBMISSION_PRE=None, CUSTOM_SUBMISSION_CSV=None):
    
    for i, ml_algo in enumerate(dashboard['ML_ALGO']):
        if i==len(dashboard['ML_ALGO'])-1:
            d=dashboard # last execution: write on input dashboard object (required by test framework)
        else:
            d = dashboard.copy()
        d['ML_ALGO'] = [ml_algo]
        make_up(d, algoritmos)
        resultados__pd = runDashboard_single(d, session_executions_history, CUSTOM_IMPORTER,CUSTOM_ML,CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
    return resultados__pd      

    
def runDashboard_single(dashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV):    
    resultados__pd = None

    if (len(sys.argv)>1):
        temp_pkl_path = str(sys.argv[2])       
        print("Lanzamos un único dashboard - run_iterateGo_single_safe:" + temp_pkl_path)
        run_iterateGo_single_safe(temp_pkl_path, None, None)
    elif dashboard['NA_ML_IMPUTATION']:
        imputationDashboards = NA_imputation_dsh_get(dashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
        imputationDashboards = dsh_combinations_get_all(imputationDashboards)
        
        #imputationDashboardsMap = groupDashboardByImportation(imputationDashboards)
        
        imputationDashboardsMap = NA_imputation_group_by_y(imputationDashboards)
        
        imputation_filenames_map = {}
        
        for yColumn in imputationDashboardsMap.keys():
            dashboardList = imputationDashboardsMap[yColumn]
            
            resultadosImputaciones__pd = None 
            for imputationDashboard in dashboardList:
                (test__pd,TRAIN__pd) = NA_imputation_datasets_gen(yColumn, imputationDashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
                
                #(test__pd,TRAIN__pd, fieldsWithNAs_TRAIN, fieldsWithNAs_test) = doImport(dashboard, CUSTOM_IMPORTER)
                #doImport(dashboard, CUSTOM_IMPORTER)
    
                resultado__pd = run_iterateGo(imputationDashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=test__pd, TRAIN__pd=TRAIN__pd)
                
                if resultadosImputaciones__pd is None:
                    resultadosImputaciones__pd = resultado__pd
                else:
                    resultadosImputaciones__pd = resultadosImputaciones__pd.append(resultado__pd, ignore_index=True)
            
            resultadosImputaciones__pd.sort_values(by=['model_score'], ascending=[not dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']], inplace=True)
            print (str(resultadosImputaciones__pd.shape[0]) + " posibles imputaciones para: " + yColumn)
            print ("==============================")
            print (resultadosImputaciones__pd)
            print ("And the winner is...")
            print (resultadosImputaciones__pd.iloc[0]["submissionFilename"])
            
            imputation_filenames_map[yColumn] = resultadosImputaciones__pd.iloc[0]["submissionFilename"]
        
        NA_imputation_do(imputation_filenames_map, dashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
            
            
    else:
        executions_history__pd = executions_history_get(dashboard)
        resultados__pd = run_iterateGo(dashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, executions_history__pd=executions_history__pd)
        
        #if executions_history__pd is not None:
        #    resultados__pd = resultados__pd.append(executions_history__pd, ignore_index=True)
            
        
        if DEBUG_MODE and not dashboard['REITERATE']:
            if resultados__pd.shape[0] > 1:
                print ("resultados__pd para " + str(resultados__pd.shape[0]) + " iteraciones")
                print (resultados__pd)
                print (resultados__pd.iloc[0]['submissionFilename'])
        
        results_sort(resultados__pd)
        best_score = resultados__pd.iloc[0]['model_score']
        
        do_reiteration = dashboard['REITERATE']
        while do_reiteration:
            if DEBUG_MODE:
                if resultados__pd.shape[0] > 1:
                    print ("resultados__pd para " + str(resultados__pd.shape[0]) + " iteraciones")
                    print (resultados__pd)
                    print (resultados__pd.iloc[0]['submissionFilename'])
            
            reiteration_dashboards = reiterator_get_dashboards(resultados__pd)
            
            if not reiteration_dashboards:
                do_reiteration = False # stops reiteration
            else:
                new_results__pd = run_iterateGo(reiteration_dashboards, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV) 
                resultados__pd = resultados__pd.append(new_results__pd, ignore_index=True)
                
                results_sort(resultados__pd)
                new_best_score = resultados__pd.iloc[0]['model_score']
                
                if dashboard['REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD']>0 and best_score + dashboard['REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD'] > new_best_score:
                    do_reiteration = False
                    if DEBUG_MODE:
                        print ("Re-iteration finished: insuficient enhacement.  Last iteration best score: " + str(best_score )+ ", REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD: " + str(dashboard['REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD']) + ", best score after this iteration: " + str(new_best_score))
                else:
                    best_score = new_best_score
                    
    return resultados__pd
                    



'''
Genera todas las combinaciones de dashboards, y las ejecuta siguiendo la estrategia de iteración indicada en los dashboard
'''
def run_iterateGo(dashboards, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=None,TRAIN__pd=None, cloneDataSets=False, executions_history__pd=None):
    resultados=[]
    
    if isinstance(dashboards, dict):
        dashboards = [dashboards]

    #dashboard=dashboardOriginal.copy()
    #iterableVariables=dashboard['ITER_ITERABLE_VARIABLES'] 
    #variablesToBeIterated = variables_to_be_iterated_get(iterableVariables, dashboard)
    #print ("Iteraremos sobre " , len(variablesToBeIterated), " variables: " , variablesToBeIterated)
    
    
    dashboardCombinations = dsh_combinations_get_all(dashboards)
    dashboards_orig = dashboardCombinations.copy()
    print ("Hay ", len(dashboardCombinations), " combinaciones de dashboards")
    
    variablesToBeIterated = dict_find_diff(dashboardCombinations)
    print ("dict_find_diff - iteraremos sobre " , len(variablesToBeIterated), " variables: " , variablesToBeIterated)
    
    
    dasboard_iterator = iterator_dsh_create_grid_fastloop(dashboards[0]['ITERATOR'], dashboardCombinations)#, dashboards_orig)
    
    num_dashboards=0
    submissionFilename, model_score, model_score_val, model_score_cv = None, None, None, None
    
    for dashboardCombination in dasboard_iterator:
        
        dashboardCombination_orig = dashboardCombination.copy()
        print ("\n\n\nITERAMOS (" + dasboard_iterator.name + ")\n========\n")
        
        for variableName in variablesToBeIterated:
            print (variableName + ": " + str(dashboardCombination[variableName]))
        print ("ejecutamos sobre columna y: " + dashboardCombination['COMP_COLUMN_CLASS'])
        
        if test__pd is not None and TRAIN__pd is not None:
            dashboardCombination['SYS_DO_IMPORT'] = False
                
        if TEST_MODE:
            submissionFilename, model_score_val, model_score_cv = str(num_dashboards)+".csv", uniform(0,1), uniform(0,1)
            model_score = score_select(dashboardCombination, model_score_cv, model_score_val)
        else:
            dashboardCombinationCopy = dashboardCombination.copy()
            submissionFilename, model_score, model_score_val, model_score_cv, model_score_test = run_iterateGo_double(dashboardCombination, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd,TRAIN__pd, executions_history__pd=executions_history__pd)
            if (get_dashboard_without_internals(dashboardCombination) != get_dashboard_without_internals(dashboardCombinationCopy)) and dashboards[0]['SPLIT_CV_KFOLDS_EARLY_STOP'] != 1:
                print ("ERROR: Dashboard has been modified on MAIN.py.  Only internals can be modified.  Forgot to register variable as internal?  "
                        + "changes on: "+ str(dict_find_diff([get_dashboard_without_internals(dashboardCombination), get_dashboard_without_internals(dashboardCombinationCopy)])))
        
        
            
        if model_score_test != 99999 and model_score_test != -99999 and model_score_test != None:    
            resultados.append((submissionFilename, model_score, model_score_val, model_score_cv, model_score_test, dashboardCombination))
        else:
            resultados.append((submissionFilename, model_score, model_score_val, model_score_cv, dashboardCombination))
        
        dasboard_iterator.prune(model_score_val, model_score_cv, dashboardCombinationCopy)
        
        num_dashboards += 1
    
    resultados__pd=pd.DataFrame(np.array(resultados))
    if model_score_test != 99999 and model_score_test != -99999 and model_score_test != None: 
        resultados__pd.columns=["submissionFilename", "model_score", "model_score_val","model_score_cv","model_score_test", "dashboard"]
    else:
        resultados__pd.columns=["submissionFilename", "model_score", "model_score_val","model_score_cv", "dashboard"]
    resultados__pd.sort_values(by=['model_score'], ascending=[not dashboards[0]['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']], inplace=True)
    return resultados__pd

    
    
    
    
def run_iterateGo_double (dashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=None,TRAIN__pd=None, executions_history__pd=None):
    ''' Wrapper to do the "double execution"
    '''

    do_double_execution = dashboard.get('ITER_DO_DOUBLE_EXECUTION', True)

    father_dashboard_id = dsh_father_get_id(dashboard)

    if do_double_execution and not dsh_father_is_father(dashboard) and father_dashboard_id < 0:
        father_dashboard = dashboard.copy()
        father_dashboard['ID_FAMILY'] = 0
        father_dashboard['SPLIT_CV_KFOLDS'] = 0 
        father_dashboard['SPLIT_VALIDATION_SET_PERC'] = 0.0
        father_dashboard['IMP_TRAIN_SUBSET_TYPE'] = 'allSamples'
        father_dashboard['IMP_TEST_SUBSET_TYPE'] = 'allSamples'

        print("Double execution: start running father dashboard")
        run_iterateGo_single (father_dashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=test__pd,TRAIN__pd=TRAIN__pd, executions_history__pd=executions_history__pd)
        father_dashboard_id = dsh_father_get_id(dashboard)
    
    
    dashboard['ID_FAMILY'] = father_dashboard_id


    return run_iterateGo_single (dashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=test__pd,TRAIN__pd=TRAIN__pd, executions_history__pd=executions_history__pd)





def run_iterateGo_single (dashboard, session_executions_history, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=None,TRAIN__pd=None, executions_history__pd=None):
    skip_already_executed_dashboards=False if not 'META_TEST' in dashboard else dashboard['META_TEST']
    results = None
    dashboard_id = None
    print("111111111111111")
    if skip_already_executed_dashboards:
        print("Check for previously executed dashboards is DISABLED")
    else:
        # CHECK IF DASHBOARD WAS PREVIOUSLY EXECUTED
        executions_history__pd = executions_history_get(dashboard)
        if executions_history__pd is not None:
            executed_dashboards = executions_history__pd['dashboard'].tolist()   
            for executed_dashboard in executed_dashboards:
                executed_dashboard_without_internals = get_dashboard_without_internals(executed_dashboard)
                dashboard_without_internals = get_dashboard_without_internals(dashboard)
                
                
                
                if dashboard_without_internals == executed_dashboard_without_internals:
                    dashboard_id = executed_dashboard['ID']
                    results = pickle_load_one_execution_results_dict(dashboard_id, dashboard['DIR_BASE'])
                    print (Fore.BLACK + Back.YELLOW + Style.DIM+ "Skipped: dashboard was executed with id " + str(dashboard_id)  +  Style.RESET_ALL)
                    session_executions_history.append(executed_dashboard['FRIENDLY_ID'])
                    break
            
    
        
    # ELSE: EXECUTE!  
    if results is None: # wasn't previously executed
        dashboard['FRIENDLY_ID']=generate_unused_friendly_id(pickle_load_all_friendly_id_already_used(dashboard['DIR_BASE']), dashboard['FRIENDLY_ID'])
        session_executions_history.append(dashboard['FRIENDLY_ID'])
        dashboard_id = id_execution_generator()
        dashboard['ID'] = dashboard_id
        
        
        
        pickle_save_temp_dashboard(dashboard)
        
        run_iterateGo_single_safe (None, dashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd,TRAIN__pd)
    
        #pickle_reload_temp_dashboard(dashboard)
    results = pickle_load_one_execution_results_dict(dashboard_id, dashboard['DIR_BASE'])
    
    model_score_cv=results.get('score_train_cv', None)
    model_score_val=results.get('score_validation', None)
    
    model_score_test=results.get('score_test', None)
    
    # ################  BIG ÑAPA
    if model_score_cv is None and model_score_val is None and 'score_train' in results:
        score_train = results['score_train']
        model_score_cv, model_score_val = score_train, score_train
    # ################  END OF BIG ÑAPA
        
    model_score = score_select(dashboard, model_score_cv, model_score_val)
    submission_filename = results.get('submission_name', None)
     
    
    return submission_filename, model_score, model_score_val, model_score_cv, model_score_test

    
    
    
def run_iterateGo_single_safe(pickle_parameters_filename,*args, **kwargs):
    #memmory_before = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    #print ("Memoria antes de llamar a go_Go_Go: (kb)" + str(memmory_before))

    #  Convert dashboard into a readonly dictionary <-- DESACTIVADO: Ver con Fernando pq escribimos ...
    args_list=list(args)
    dashboard=args[0]
#    dashboard=ReadOnlyDictionary(args[0]) # DEACTIVATED!!!
    args_list[0]=dashboard
    args=tuple(args_list)
    
    
    
    perform_execution_on_this_thread = True
    
    perform_execution_on_this_thread = pickle_parameters_filename is not None or not dsh_requires_isolated_execution(dashboard)
    
    if pickle_parameters_filename is not None:
        print("Loading file: " + pickle_parameters_filename)
        (args, kwargs) = pickle_load(pickle_parameters_filename) 
        os.remove(pickle_parameters_filename)
        print(pickle_parameters_filename + " loaded")
        
    if perform_execution_on_this_thread:
        go_Go_Go(*args, **kwargs)
    else:
        parameters = (args, kwargs)
        temp_filename = pickle_save_temp_parameters(dashboard,parameters)
        
        print (Fore.BLACK + Back.GREEN + Style.DIM+ "Starting a new python process (say bye to Spyder debug!)" +  Style.RESET_ALL)
        
        cmd = ["python", dashboard['SCRIPT_FILENAME'], "--args", temp_filename]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        pid = str(p.pid)
        print (Fore.BLACK + Back.GREEN + Style.DIM+ "isolated process " + pid + Style.RESET_ALL)
        sys.stdout.flush()
        
        subprocess_in_execution= True        
        while subprocess_in_execution:
            line = p.stdout.readline()
            if line:
                print (Fore.BLACK + Back.GREEN + Style.DIM+ "isolated process " + pid + ": " +  Style.RESET_ALL + str(line))
            else:
                subprocess_in_execution=False

        print (Fore.BLACK + Back.GREEN + Style.DIM+ "Isolated process is finished. Here we are!" +  Style.RESET_ALL )
    
    #memmory_after = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    #memmory_leftover = memmory_after - memmory_before
    #print ("Memoria después de llamar a go_Go_Go (kb):" + str(memmory_before))
    #print ("\n\n\n========================")
    #print ("Memoria no liberada tras llamar a go_Go_Go (kb):" + str(memmory_leftover))
    #print ("========================\n\n\n")



    
def dsh_father_get_id(dashboard):
    father_dashboard_id = -1
    #do_double_execution = dashboard.get('ITER_DO_DOUBLE_EXECUTION', True)

    if dsh_father_is_father(dashboard):
        father_dashboard_id = 0
    else:
        execution_results = pickle_load_all_execution_results_to_dict(dashboard['DIR_BASE'])
        
        extra_internals = SUBSETTING_VARIABLES
        
        for dashboard_id in execution_results.keys():
            # TO-DO: faltaría comprobar si la ejecucion había fallado
            executed_dashboard = pickle_load_one_dashboard(dashboard_id, dashboard['DIR_BASE'])

            if dsh_father_is_father(executed_dashboard):
                executed_dashboard_without_internals = get_dashboard_without_internals(executed_dashboard, extra_internals=extra_internals)
                dashboard_without_internals = get_dashboard_without_internals(dashboard, extra_internals=extra_internals)

                if dashboard_without_internals == executed_dashboard_without_internals:
                    father_dashboard_id = executed_dashboard['ID']
                    break        

    return father_dashboard_id
    

    
def dsh_father_is_father(dashboard):
    is_father = (
        (
            dashboard['SPLIT_CV_KFOLDS'] == 0
            or (dashboard['SPLIT_CV_KFOLDS'] == 1 and dashboard['SPLIT_VALIDATION_SET_PERC'] == 0.0)
        )
        and dashboard['IMP_TRAIN_SUBSET_TYPE'] == 'allSamples' and dashboard['IMP_TEST_SUBSET_TYPE'] == 'allSamples' 
    )

    return is_father

    
    

def dsh_requires_isolated_execution(dashboard):
    isolated_execution_required = False
    
    algortimos_parametros = dashboard['ML_ALGO_PARAMETROS']
    if 'XGB' in algortimos_parametros.keys():
        isolated_execution_required = True
        
    
    return isolated_execution_required




def dsh_combinations_get_all(dashboards):
    combinations = []
    if isinstance(dashboards, dict):
        dashboards = [dashboards]

    for dashboard in dashboards:    
        iterableVariables = dashboard['ITER_ITERABLE_VARIABLES']
        if dashboard['ITER_ITERABLE_DASHBOARD']:
            dsh_combinations_get(dashboard, iterableVariables, tempList=combinations)
        else:
            combinations.append(dashboard)
    
    for dashboard in combinations:
        dashboard['ITER_ITERABLE_VARIABLES']=[]
    
    return combinations



def dsh_combinations_get(dashboard, iterableVariables, tempList=[]):
    dashboard = dashboard.copy()
    dashboard['ITER_ITERABLE_DASHBOARD'] = False

    if len(iterableVariables)>0:
        

        variableName=iterableVariables[0]
        values = dashboard[variableName]
        
        # Aseguramos que es una lista, tupla ... algo iterable
        try:
            some_object_iterator = iter(values)
        except TypeError as te:
            #print ( "DEBUG: " + variableName + " no es iterable ... ¿olvidaste los corchetes?")
            values = [ values ]
        
        if len(values)==0:
            print ("ERROR: " + variableName + " is empty. forgot square brackets?")

        for value in values:            
            dashboard[variableName] = value

            dsh_combinations_get(dashboard, iterableVariables[1:], tempList=tempList)
    else:
        tempList.append(dashboard)
    
    return tempList

    
    
    

    
def variables_used_in_import_get():
    '''
    returns variable names used on doImport (very useful to cache doImport)
    '''
    return ['COMP_COLUMN_CLASS',
        'COMP_COLUMN_ID',
        'COMP_NAME', 
        'DIR_OUTPATH_SUBMISSIONS_LEVEL_1',
        'DIR_OUTPATH_SUBMISSIONS_LEVEL_2',
        'DIR_OUTPATH_SUBMISSIONS_VALIDATION',
        'DIR_TEST_PATH',
        'DIR_TRAIN_PATH',
        'DIR_TRAIN_TEST_DIRECTORY', 
        'SYS_DO_CUSTOM_IMPORT', 
        'DO_DROP_FEATURES_ALL_NA_TRAIN_OR_TEST',
        'LEVEL', 
        'DO_NA_INFO',
        'PRINT_CUSTOM_IMPORT', 
        'PRINT_DROP_INFO',
        'PRINT_NA_INFO',
        'PRINT_Y_INFO',
        'PRP_DROP_THESE_COLUMNS',
        'IMP_TEST_DELIMITER',
        'IMP_TEST_ENCODING_TYPE',
        'IMP_TEST_FIRST_ROW',
        'IMP_TEST_PERCENTAGE',
        'IMP_TEST_SUBSET_TYPE',
        'IMP_TRAIN_DELIMITER',
        'IMP_TRAIN_ENCODING_TYPE',
        'IMP_TRAIN_FIRST_ROW',
        'IMP_TRAIN_NUM_SAMPLES',
        'IMP_TRAIN_PERCENTAGE',
        'IMP_TRAIN_SUBSET_TYPE',
        'SPLIT_VALIDATION_SET_PERC']

def variables_to_be_iterated_get(iterableVariables, dashboard):
    '''
        Returns variables which more than one value to be iterated
    '''
    variablesToBeIterated = []

    for variableName in iterableVariables:
        try:
            values = dashboard[variableName]
            some_object_iterator = iter(values) # Aseguramos que es una lista, tupla ... algo iterable
            
            if len(values)>1:
                variablesToBeIterated.append(variableName)

        except TypeError as te:
            print ( "WARNING: " + variableName + " no es iterable ... ¿olvidaste los corchetes?")

    return variablesToBeIterated



def score_select(dashboard, model_score_cv, model_score_val):
    score = None
    
    if dashboard['SPLIT_CV_KFOLDS'] == 0: # TRAIN completo
        score = model_score_val
    else:
        score = model_score_cv
    
    return model_score_cv
    
    

def executions_history_get(dashboard):
    executions_history__pd = None
    execution_results = pickle_load_all_execution_results_to_dict(dashboard['DIR_BASE'])
    
    resultados=[]
    for dashboard_id in execution_results.keys():
        executed_dashboard = pickle_load_one_dashboard(dashboard_id, dashboard['DIR_BASE'])
        results = execution_results[dashboard_id]
        model_score_cv=results.get('score_train_cv', None)
        model_score_val=results.get('score_validation', None)
        model_score_val_transf = results.get('score_validation_transf', None)
        model_score = score_select(dashboard, model_score_cv, model_score_val)
        submission_filename = results.get('submission_name', None)
        
        resultados.append((submission_filename, model_score, model_score_val, model_score_cv, executed_dashboard))
    
    if len(resultados):
        executions_history__pd=pd.DataFrame(np.array(resultados))
        executions_history__pd.columns=["submissionFilename","model_score", "model_score_val","model_score_cv", "dashboard"]
        executions_history__pd.sort_values(by=['model_score_val', 'model_score_cv'], ascending=[not dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'], not dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']], inplace=True)
        
    return executions_history__pd
    

def dict_find_diff(dictionariesList):
    ''' Return a list of keys with different values in at least two dictionaries
    Useful to get deltas between dashboards
    '''
    if len(dictionariesList)==0:
        return []
        
    modifiedKeys=set()
    
    keys = set()
    for d in dictionariesList:
        for k in d.keys():
            keys.add(k)
    
        
    for k in keys:
        if k in dictionariesList[0]:
            value = dictionariesList[0][k]
            
            for d in dictionariesList[1:]:
                if k in d:
                    if d[k]!=value:
                        modifiedKeys.add(k)
                else:
                    modifiedKeys.add(k)
        else:
            modifiedKeys.add(k)
    
    return sorted(modifiedKeys)


    
def list_add_new_elements(a, b):
    '''
    return a list contaning all elements in a,
    followed by elements in b which aren't contained in a
    
    perserves elements order in lists    
    
        print (list_add_new_elements([1,2,3,4,5], [2, 4, 6 , 8]))
        [1, 2, 3, 4, 5, 6, 8]
        
    '''
    a = a.copy()
    for e in b:
        if not e in a:
            a += [e]
    
    return a

    
def list_remove_duplicates(l):
    new_list=[]
    for x in l:
        if x not in new_list:
            new_list.append(x)
    return new_list

    
# sort results pandas by 'model_score_val', 'model_score_cv'
# according to EVALUATION_METRIC_GREATER_IS_BETTER criteria
def results_sort(resultados__pd):
    greater_is_better = resultados__pd.iloc[0]['dashboard']['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']
    resultados__pd.sort_values(by=['model_score_val', 'model_score_cv'], ascending=[not greater_is_better, not greater_is_better], inplace=True)

    
    
    
###############################################
###               NA IMPUTATION             ###
###############################################


def NA_imputation_datasets_gen(yColumn, dashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV):
    
    
    (test__pd,TRAIN__pd, fieldsWithNAs_TRAIN, fieldsWithNAs_test) = doImport(dashboard, CUSTOM_IMPORTER)
    
    NA_imputation_ML_NA_index_add(test__pd,TRAIN__pd)
    
    #cols_names,test__pd, TRAIN__pd = drop_these_columns(d['PRP_DROP_THESE_COLUMNS'],test__pd, TRAIN__pd,'PRP_DROP_THESE_COLUMNS',d['PRINT_DROP_INFO'],"Motivo: Indicado en Dashboard")
    
    if not all(TRAIN__pd.columns == test__pd.columns): 
        print ("Las colmnas de test__pd y TRAIN__pd no son iguales, o no están en el mismo orden.\nTRAIN__pd.columns: " + str(TRAIN__pd.columns) + "\ntest__pd.columns: " + str(test__pd.columns))
        raise AssertionError("Las colmnas de test__pd y TRAIN__pd no son iguales")
    
    wholeData__pd = TRAIN__pd.append(test__pd, ignore_index=True)
    imputation_test__pd = wholeData__pd.loc[wholeData__pd[yColumn].isnull()]
    imputation_TRAIN__pd = wholeData__pd.loc[wholeData__pd[yColumn].notnull()]
    wholeData__pd = None
    gc.collect()
    
    print ("test_rows,"  + yColumn + "," + str(imputation_test__pd.shape[0]) + ",test_cols,"  + yColumn + "," + str(imputation_test__pd.shape[1]) )
    print ("TRAIN_rows," + yColumn + "," + str(imputation_TRAIN__pd.shape[0])+ ",TRAIN_cols," + yColumn + "," + str(imputation_TRAIN__pd.shape[1]))
    
    
       
    return (imputation_test__pd, imputation_TRAIN__pd)
    
                    
                    
def NA_imputation_do(imputation_files_map, dashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV):
    dashboard = dashboard.copy()

    dashboard['IMP_TRAIN_SUBSET_TYPE']='allSamples'
    dashboard['IMP_TEST_SUBSET_TYPE']='allSamples'
    dashboard['SYS_DO_IMPORT'] = True
    dashboard['LEVEL'] = '1_ML'
    #setPaths(dashboard)
    dashboard['PRP_DROP_THESE_COLUMNS'] = []
    dashboard['DO_NA_INFO']=True
    

    (test__pd,TRAIN__pd, fieldsWithNAs_TRAIN, fieldsWithNAs_test) = doImport(dashboard, CUSTOM_IMPORTER)
    
    imputation_TRAIN_filename=dashboard['DIR_BASE']+'/input/train-imputation.csv'
    imputation_test_filename =dashboard['DIR_BASE']+'/input/test-imputation.csv'
    
    for imputation_column_name in imputation_files_map:
        imputation_csv_filename = imputation_files_map[imputation_column_name]
        
        imputations__pd = pd.read_csv(imputation_csv_filename, encoding=dashboard['IMP_TRAIN_ENCODING_TYPE'],sep=dashboard['IMP_TRAIN_DELIMITER'], engine='c')
        
        # split mlna_field by underscore separator
        imputations__pd['destination'], imputations__pd['idx'] = zip(*imputations__pd['mlna_index'].apply(lambda x: x.split('_', 1)))
        imputations__pd["idx"] = imputations__pd["idx"].astype(int)
        
        # divide imputations in two groups:  for TRAIN / for test
        imputations_TRAIN__pd = imputations__pd.loc[imputations__pd['destination'] == "TRAIN"]
        imputations_test__pd  = imputations__pd.loc[imputations__pd['destination'] == "test"]
        
        print ("TRAIN. There are " + str(len(imputations_TRAIN__pd)) + " imputations to make in " + imputation_column_name)
        print ("TRAIN. Before imputing " + imputation_column_name)
        print ("TRAIN. Num. NAs " + imputation_column_name + " in TRAIN: " + str(TRAIN__pd.loc[TRAIN__pd[imputation_column_name].isnull()].shape[0]))
        print (TRAIN__pd[imputation_column_name].describe())
        print ("")
        
        print ("test. There are " + str(len(imputations_test__pd)) + " imputations to make in " + imputation_column_name)
        print ("test. Before imputing " + imputation_column_name)
        print ("test. Num. NAs " + imputation_column_name + " in test: " + str(test__pd.loc[test__pd[imputation_column_name].isnull()].shape[0]))
        print (test__pd[imputation_column_name].describe())
        print ("")
        
        print ("------------------------------------------ metrics only for imputations ------------------------------------------ ")
        print ("TRAIN imputations:")
        print (imputations_TRAIN__pd[imputation_column_name].describe())
        print (imputations_test__pd[imputation_column_name].describe())
        print ("------------------------------------------ -------------------------- ------------------------------------------ ")
        
        
        TRAIN__pd.ix[imputations_TRAIN__pd["idx"], imputation_column_name]=imputations_TRAIN__pd[imputation_column_name].values
        test__pd.ix [imputations_test__pd ["idx"], imputation_column_name]=imputations_test__pd [imputation_column_name].values
        
        print ("TRAIN. after imputing " + imputation_column_name)
        print ("TRAIN. Num. NAs " + imputation_column_name + " in TRAIN: " + str(TRAIN__pd.loc[TRAIN__pd[imputation_column_name].isnull()].shape[0]))
        print (TRAIN__pd[imputation_column_name].describe())
        print ("")
        
        print ("test. after imputing " + imputation_column_name)
        print ("test. Num. NAs " + imputation_column_name + " in test: " + str(test__pd.loc[test__pd[imputation_column_name].isnull()].shape[0]))
        print (test__pd[imputation_column_name].describe())
        print ("")
        
    
    
    print ("We save imputations:\n" + imputation_TRAIN_filename + "\n" + imputation_test_filename)
    #pd.save_csv(filePath, encoding=dashboard['IMP_TRAIN_ENCODING_TYPE'],sep=dashboard['IMP_TRAIN_DELIMITER'], engine='c')
    TRAIN__pd.to_csv(imputation_TRAIN_filename, encoding=dashboard['IMP_TRAIN_ENCODING_TYPE'],sep=dashboard['IMP_TRAIN_DELIMITER'], index=False,header=True, engine='python')
    test__pd.to_csv(imputation_test_filename, encoding=dashboard['IMP_TEST_ENCODING_TYPE'],sep=dashboard['IMP_TEST_DELIMITER'], index=False,header=True, engine='python')
    
      
        
    
def NA_imputation_dsh_get(originalDashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV):
    dashboardsForImputation = []
    
    fieldsToImpute = originalDashboard['NA_ML_FIELDS']
    if not isinstance(fieldsToImpute, list):
        print ("Detecting fields to impute")
        (fieldsWithNAs_TRAIN, fieldsWithNAs_test) = NA_imputation_get_fields_with_NA(originalDashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV)
    
        #print ("fieldsWithNAs_TRAIN")
        #print (fieldsWithNAs_TRAIN)
    
        #print ("")
    
        #print ("fieldsWithNAs_test")
        #print (fieldsWithNAs_test)
    
        fieldsToImpute = sorted(set(fieldsWithNAs_TRAIN + fieldsWithNAs_test))
    
        #print ("")
    
    print ("fieldsToImpute")
    print (fieldsToImpute)
    
    for field in fieldsToImpute:
        dashboard = originalDashboard.copy()

        
        
        # Ensures that:
        #    - the competition Y-column is dropped
        #    - id columns is dropped, due to it couldn't exists in TRAIN set
        #    - fields with NA are dropped (with the exception of the field to be imputated)
        originalDropColumns = dashboard['PRP_DROP_THESE_COLUMNS']
        newDropColumns = []
        for dropColumnsIterationN in originalDropColumns:
            extraColumnsToDrop=fieldsToImpute.copy()
            extraColumnsToDrop.remove(field)
            extraColumnsToDrop.append(dashboard['COMP_COLUMN_CLASS'])
            extraColumnsToDrop.append(dashboard['COMP_COLUMN_ID'])
            extraColumnsToDrop += dashboard['NA_ML_EXTRA_COLUMNS_TO_DROP']
            newDropColumns.append(list_add_new_elements(dropColumnsIterationN, extraColumnsToDrop))
        dashboard['PRP_DROP_THESE_COLUMNS'] = newDropColumns
        print ("newDropColumns: " + str(newDropColumns))
        
        # Use an ad-hoc id for ML_NA
        dashboard['COMP_COLUMN_ID']='mlna_index'
        # The field to imputate will be the Y column for this dashboard
        #competitionYColumnName = dashboard['COMP_COLUMN_CLASS']
        dashboard['COMP_COLUMN_CLASS'] = field

        # Ensures that all rows will be loaded
        dashboard['IMP_TRAIN_SUBSET_TYPE']='allSamples'
        dashboard['IMP_TEST_SUBSET_TYPE']='allSamples'
        
        # Set output path for ML
        dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_1']=dashboard['DIR_BASE']+'/imputation/' + field + '/'
        #setPaths(dashboard)
        
        dashboard['LEVEL'] = '1_ML'
        #dashboard['DO_FILL_NA_NUMERIC'] = [0]
        #dashboard['DO_FILL_NA_NONNUMERIC'] = [0]
        dashboard['SYS_DO_SUBMISSION'] = True
        dashboard['SYS_DO_IMPORT'] = False
        
        

        dashboardsForImputation.append(dashboard)

    return dashboardsForImputation
    
    

def NA_imputation_get_fields_with_NA(dashboard, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV):
    dashboard = dashboard.copy()

    dashboard['IMP_TRAIN_SUBSET_TYPE']='allSamples'
    dashboard['IMP_TEST_SUBSET_TYPE']='allSamples'
    dashboard['SYS_DO_IMPORT'] = True
    dashboard['LEVEL'] = '1_ML'
    #setPaths(dashboard)
    
    newDropColumns = []
    if('PRP_DROP_THESE_COLUMNS' in dashboard['ITER_ITERABLE_VARIABLES']):
        # we're going to iterate over DROP_THESE_COLUMNS
        # we need a single list of column names to do a single import (instead of a list of lists),
        # use the common factor (column names used in all iterations)
        originalDropColumns = dashboard['PRP_DROP_THESE_COLUMNS'][1:]
        newDropColumns = dashboard['PRP_DROP_THESE_COLUMNS'][0]
        for dropColumnsIterationN in originalDropColumns:
            newDropColumns = list(set(newDropColumns) & set(dropColumnsIterationN))
    newDropColumns.append("COLUMN_ID")
    newDropColumns.append("COLUMN_CLASS")
    newDropColumns += dashboard['NA_ML_EXTRA_COLUMNS_TO_DROP']
    newDropColumns=sorted(newDropColumns)
    dashboard['PRP_DROP_THESE_COLUMNS'] = newDropColumns
    print ("newDropColumns: " + str(newDropColumns))

    # Ensures that all rows will be loaded
    dashboard['IMP_TRAIN_SUBSET_TYPE']='allSamples'
    dashboard['IMP_TEST_SUBSET_TYPE']='allSamples'
    dashboard['PRINT_NA_INFO']=True
    dashboard['PRP_NA_FILL_NUMERIC'] = 0
    dashboard['PRP_NA_FILL_NON_NUMERIC'] = 0
    
    

    (test__pd,TRAIN__pd, fieldsWithNAs_TRAIN, fieldsWithNAs_test) = doImport(dashboard, CUSTOM_IMPORTER)
    test__pd,TRAIN__pd = None, None
    gc.collect()
    

    return (fieldsWithNAs_TRAIN, fieldsWithNAs_test)

    
    

def NA_imputation_group_by_y(dashboards):
    groupedDashboards={}
    
    for dashboard in dashboards:
        yColumnName = dashboard['COMP_COLUMN_CLASS']
        if not yColumnName in groupedDashboards:
            groupedDashboards[yColumnName]=[]
        groupedDashboards[yColumnName].append(dashboard)
    
    return groupedDashboards
        
    
    
def NA_imputation_ML_NA_index_add(test__pd,TRAIN__pd):
    TRAIN__pd['mlna_index'] = TRAIN__pd.index
    TRAIN__pd['mlna_index'] = TRAIN__pd['mlna_index'].apply(str)
    TRAIN__pd['mlna_index'] = "TRAIN_" + TRAIN__pd['mlna_index'] 
    
    
    test__pd['mlna_index'] = test__pd.index
    test__pd['mlna_index'] = test__pd['mlna_index'].apply(str)
    test__pd['mlna_index'] = "test_" + test__pd['mlna_index'] 

    
            


##############################################
###              ITERATORS                 ###
##############################################

    
def iterator_is_Iterable(x):
    
    isIterable=True
    
    try:
        iter(x)
    except TypeError:
        isIterable=False
    
    return isIterable
            



def iterator_dsh_create_grid_fastloop(iterator_name, dashboards):#, dashboards_orig):
    
    if iterator_name=='fastloop':
        return Iterator_FastLoop(dashboards)#, dashboards_orig)
    else:
        return Iterator_Grid(dashboards)#, dashboards_orig)

        

class Iterator_Grid:
    name = "grid"

    def __init__(self, dashboards):#, dashboards_orig):
        self.dashboardList = dashboards
        #self.dashboard_list_orig = dashboards_orig
    
    def __iter__(self):
        return self
        
    def __next__(self):
        if len(self.dashboardList)<1:
            raise StopIteration
        else:
            self.current_dashboard      = self.dashboardList[0]
            #self.current_dashboard_orig = self.dashboard_list_orig[0]
            self.dashboardList          = self.dashboardList[1:]
            #self.dashboard_list_orig    = self.dashboard_list_orig[1:]
            return self.current_dashboard#, self.current_dashboard_orig
            
    def prune(self, score_val, score_cv, dashboardCombination_orig):
        pass




    
class Iterator_FastLoop:
    name = "fastloop"
    debug = True
    

    def __init__(self, dashboards):#, dashboards_orig):
        self.dashboard_list      = dashboards
        #self.dashboard_list_orig = dashboards_orig
        
        self.greater_is_better = dashboards[0]['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']
        self.best_dashboard = None
        self.best_score = -inf if self.greater_is_better else inf
    
    def __iter__(self):
        return self
        
    def __next__(self):
        if len(self.dashboard_list)<1:
            raise StopIteration
        else:
            self.current_dashboard      = self.dashboard_list[0]
            #self.current_dashboard_orig = self.dashboard_list_orig[0]
            self.dashboard_list         = self.dashboard_list[1:]
            #self.dashboard_list_orig    = self.dashboard_list_orig[1:]
            return self.current_dashboard#, self.current_dashboard_orig
    
            
    def prune(self, score_val, score_cv, dashboardCombination_orig):
        looser_dashboard = None
        
        if self.best_dashboard is None:
            self.best_dashboard = dashboardCombination_orig
            self.best_score = (score_val, score_cv)
        else:
            # Select the current best dashboard
            if self.is_better_score(score_val, score_cv):
                looser_dashboard = self.best_dashboard
                self.best_dashboard = dashboardCombination_orig
                self.best_score = (score_val, score_cv)
            else:
                looser_dashboard = dashboardCombination_orig
            
            # Prune dashboards with the same characteristics that the worst dashboard
            best_dashboard_without_internals   = get_dashboard_without_internals(self.best_dashboard) 
            looser_dashboard_without_internals = get_dashboard_without_internals(looser_dashboard)
            
            for key in dict_find_diff([best_dashboard_without_internals, looser_dashboard_without_internals]):
                
                value_to_be_pruned = looser_dashboard[key]
                
                new_dashboard_list = []
                
                for dashboard in self.dashboard_list:
                    
                    #print(key,' ',dashboard[key],'  ',self.current_dashboard[key],'  ', dashboardCombination_orig[key])
                    
                    if dashboard[key] != value_to_be_pruned:
                        new_dashboard_list.append(dashboard)
                    else:
                        if Iterator_FastLoop.debug:
                            print ("Desechamos por tener " + str(key) + " = " + str(value_to_be_pruned))
                
                self.dashboard_list = new_dashboard_list
            
            # Sort the new dashboard list by similarity with current best dashboard
            self.dashboard_list = sorted(self.dashboard_list, key=lambda dashboard: len(dict_find_diff([self.best_dashboard, dashboard])))
            
    
    def is_better_score(self, score_val, score_cv):
        (best_score_val, best_score_cv) = self.best_score
        
        # Ensures score_val to have a value 
        best_score_val = best_score_cv if best_score_val is None else best_score_val
        score_val =      score_cv      if score_val      is None else score_val
        
        
        is_better = score_val > best_score_val
        if not self.greater_is_better:
            is_better = not is_better
        
        if Iterator_FastLoop.debug:
            print (str(self.best_score) + (" vs >>>" if is_better else "<<< vs ") + str((score_val, score_cv))
            + (" (greater is better)" if self.greater_is_better else " (greater is worst)"))
        
        
        return is_better
    
            
            
##############################################
###              RE-ITERATION              ###
##############################################
 

REITERABLE_MARK = '_REITERATOR'

    

def reiterator_get_dashboards(results__pd):
    reiterate_dashboards_list = False
    
    level = reiterator_get_max_level(results__pd) + 1
    results_sort(results__pd)
    best_dashboard = results__pd.iloc[0]['dashboard']
    
    
    
    max_levels = best_dashboard['REITERATE_MAX_LEVELS']
    greater_is_better = best_dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']
    
    print ("level: " + str(level))
    
    
    reiterate_dashboard = False
    if best_dashboard['REITERATE_LEVEL'] < reiterator_get_max_level(results__pd):
        if DEBUG_MODE:
            print ("Last iteration did not make any improvement: reiteration stops at level " + str(level-1))
    elif max_levels>0 and level > max_levels:
        if DEBUG_MODE:
            print ("Re-iteration finished (REITERATE_MAX_LEVELS = " + str ( max_levels) + ")")       
    elif not reiterator_variable_get_reiterable(best_dashboard):
        if DEBUG_MODE:
            print ("No reiterable variables declared")       
    else:
        reiterate_dashboard = best_dashboard.copy()
        reiterate_dashboard['REITERATE_LEVEL'] = level
        new_iterable_variables = []
        
        reiterable_variables = reiterator_variable_get_reiterable(best_dashboard)
        
        for variable in reiterable_variables:
            current_best_value = best_dashboard[variable]
            variable_best_scores = reiterator_variable_get_best_scores(variable, results__pd)
            new_values = best_dashboard[variable + REITERABLE_MARK](current_best_value, variable_best_scores, greater_is_better)
            if not current_best_value in new_values:
                new_values = [current_best_value] + new_values
            
            new_values = list_remove_duplicates(new_values)
            
            if len(new_values)>1:
                new_iterable_variables.append(variable)
                reiterate_dashboard[variable] = new_values
                if DEBUG_MODE:
                    print ("reiteration values for " + variable + ":" + str(new_values))
                
        
        reiterate_dashboard['ITER_ITERABLE_VARIABLES'] = new_iterable_variables
        reiterate_dashboard['ITER_ITERABLE_DASHBOARD'] = True
    
    if reiterate_dashboard:
        reiterate_dashboards_list = dsh_combinations_get_all(reiterate_dashboard)
    ''' ya no es necesario
    if reiterate_dashboard:
        reiterate_dashboards_list = []
        executed_dashboards = results__pd['dashboard'].tolist()
        
        executed_dashboards_without_internals = [ get_dashboard_without_internals(x) for x in executed_dashboards ]
        
        
        for dashboard in dsh_combinations_get_all(reiterate_dashboard):
            if get_dashboard_without_internals(dashboard) in executed_dashboards_without_internals:
                if DEBUG_MODE:
                    print ("Skipped. (combination has already been executed)")       
            else:
                reiterate_dashboards_list.append(dashboard)
    '''            
        
        
    return reiterate_dashboards_list
                

  
    
    
def reiterator_get_max_level(results__pd):
    
    level = 0
    for d in results__pd['dashboard'].tolist():
        level = max(level, d['REITERATE_LEVEL'])
    
    return level
            
def reiterator_variable_get_reiterable(dashboard):
    reiterable_variables = []
    
    for variable_name in dashboard.keys():
        reiterator_variable_name = variable_name + REITERABLE_MARK
        
        if reiterator_variable_name in dashboard:
            reiterable_variables.append(variable_name)
    
    return reiterable_variables
'''
def get_reiterator_marks(dashboard):
    reiterable_variables = reiterator_variable_get_reiterable(dashboard)
    
    return [x + REITERABLE_MARK for x in reiterable_variables]
'''

def reiterator_variable_get_best_scores(variable, results__pd):
    '''
    Return a dictionary with all values used on dashboards for this variable (as dictionary keys)
    and their respectives best scores
    '''
    variable_best_scores={}
    
    #for d in results__pd['dashboard'].tolist():
    for (pandas_id, submissionFilename, model_score, model_score_val, model_score_cv, dashboard) in results__pd.itertuples():
        value = dashboard[variable]
        
        '''
        if DEBUG_MODE:
            print ("reiterator_variable_get_best_scores - variable: " + variable)
            print ("reiterator_variable_get_best_scores - dashboard[REITERATE_LEVEL]:", d['REITERATE_LEVEL'])
            print ("reiterator_variable_get_best_scores - value: " + str(value))
            print ("reiterator_variable_get_best_scores - value.__class__.__name__: " + value.__class__.__name__)
        '''
        
        score = model_score
        
        if not value in variable_best_scores:
            variable_best_scores[value]=score
        else:
            if dashboard['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']:
                variable_best_scores[value]=max(score, variable_best_scores[value])
            else:
                variable_best_scores[value]=min(score, variable_best_scores[value])
    if DEBUG_MODE:
        print ("variable_best_scores: " + str(variable_best_scores))    
        
    return variable_best_scores


    
    
        
#run_iterateGo_single_safe("../liberty/temp/parameters-1511011101113.pkl", None, None)
#run_iterateGo_single_safe("../liberty/temp/parameters-1511011124021hlns.pkl", None, None)

