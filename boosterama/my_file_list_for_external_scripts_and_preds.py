# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 20:49:20 2015

@author: DataCousins
"""

import os




d={}
d['COMPETITION_NAME']='homesite'
execute = 'PO'   # 'PO' or 'SO'


#######################################################
#                   External_Scripts                    #
#######################################################

if execute == 'SO':
    inPath=d['DIR_BASE']+'/external_scripts/src/'
    files = os.listdir(inPath)
    py_files = [file for file in files if '.py' in file or '.R' in file]
    py_files = sorted(py_files,reverse=False) 
    
    print(py_files)




#######################################################
#                  External Predictions               #
#######################################################

elif execute == 'PO':
    inPath=d['DIR_BASE']+'/external_predictions/'
    files = os.listdir(inPath)
    
    # Prefijos cvPred y testPred
    cv_prefix = 'cvPreds_'
    test_prefix = 'testPreds_'
    
    # Ficheros cvPreds en el directorio external_predictions
    cvPreds_files = [fichero[8:] for fichero in files if cv_prefix in fichero]
    
    # Ficheros testPreds en el directorio external_predictions
    testPreds_files = [fichero[10:] for fichero in files if test_prefix in fichero]


    #######################################################
    # INICIO Modificación especial para los ficheros de Branden
    #######################################################

    # Indicamos ficheros a descartar
    cvPreds_descartados = [fichero for fichero in cvPreds_files if '17' in fichero or '18' in fichero or '20' in fichero]
    testPreds_descartados = [fichero for fichero in testPreds_files if '17' in fichero or '18' in fichero or '20' in fichero]
    
    # Quitamos los descartados
    cvPreds_files = [fichero for fichero in cvPreds_files if fichero not in cvPreds_descartados]
    testPreds_files = [fichero for fichero in testPreds_files if fichero not in testPreds_descartados]

    '''    
    # Sustituimos 'cv.csv' por '.csv' en los cvPreds_files
    cvPreds_files = [fichero[:-6]+'.csv' if 'cv.csv' in fichero else fichero for fichero in cvPreds_files]
    
    # Sustituimos 'full.csv' por '.csv' en los testPreds_files
    testPreds_files = [fichero[:-8]+'.csv' if 'full.csv' in fichero else fichero for fichero in testPreds_files]
    '''
    #######################################################
    # FIN Modificación especial para los ficheros de Branden
    #######################################################
    
    # AyB AnoB BnoA
    cvPred_NO_testPred = sorted(list(set(cvPreds_files) & (set(cvPreds_files) ^ set(testPreds_files))))
    testPred_NO_cvPred = sorted(list(set(testPreds_files) & (set(cvPreds_files) ^ set(testPreds_files))))
    cvPred_COMUNES_testPred = sorted(list(set(cvPreds_files) & set(testPreds_files)))
    
    # lista tuplas
    lista_tuplas = [(cv_prefix+f, test_prefix+f) for f in cvPred_COMUNES_testPred]
    
    print("Descartados cvPreds_files\n",cvPreds_descartados)
    print("Descartados testPreds_files\n",testPreds_descartados)
    print("Existe cvPred y no testPred\n",cvPred_NO_testPred)
    print("Existe testPred y no cvPred\n",testPred_NO_cvPred)
    print("Comunes cvPred y testPred\n",cvPred_COMUNES_testPred)
    print("Lista de tuplas\n",lista_tuplas)

