# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 23:15:30 2015

@author: DataCousins
"""

import collections
from colorama import Fore, Back, Style
from datetime import datetime, timedelta
import numpy as np
import os as os
import pytz
import re
import sys
import traceback

from framework.algorithm.factory import AlgorithmFactory


import re


from my_Dsh_DEFAULT import *



DSH_UTILS_DEBUG = False

DASHBOARD_INTERNAL_VARIABLES = [ 
	'ID', 					# Identification
    'ID_NAME',
	'ID_FAMILY', 
 
	'FRIENDLY_ID',
	'HASHTAGS',
	'AUTHOR',
    
    'META_.*', #annotations: META_TEST, ¿...?

	'PRINT_ANALYSIS_ON_TRAIN_TEST',	# Prints 
	'ML_FEATURE_IMPORTANCE', 
	'PRINT_.*', 
	'CHART_.*', 

	'HDF5_.*',					# Optional output 
	'SQLITE_.*', 
	'SAVE_.*',	

	'ITERATOR', 					# Iterator
	'ITER_ITERABLE_VARIABLES', 
	'ITER_ITERABLE_DASHBOARD', 
	'ITER_DO_DOUBLE_EXECUTION', 
	'REITERATE', 
	'.*_REITERATOR', 
	'REITERATE_.*', 

	'SYS_DO_SUBMISSION', 				# Optional behaviour
	'ML_MODEL_TO_PICKLE', 
	'SYS_SEND_EMAIL', 

	'DIR_.* ',						# Paths
     'SCRIPT_FILENAME',
     'SCRIPT_PATH',
 
     'ML_ALGO'
]


SUBSETTING_VARIABLES = [
	'SPLIT_CV_KFOLDS', 
	'SPLIT_VALIDATION_SET_PERC', 
	'IMP_TRAIN_SUBSET_TYPE', 
	'IMP_TRAIN_PERCENTAGE', 
	'IMP_TRAIN_NUM_SAMPLES', 
	'IMP_TRAIN_FIRST_ROW', 
	'IMP_TEST_SUBSET_TYPE', 
	'IMP_TEST_PERCENTAGE', 
	'IMP_TEST_NUM_SAMPLES', 
	'IMP_TEST_FIRST_ROW'
]

#BAGGING_META = {'BAGGING','BAGGING_ALGORITHM','BAGGING_ALGORITHM_PARAMS'}
BAGGING_NOT_SUPPORTED = {'ML_PRP_Y_TRANSFORM'}

class ReadOnlyDictionary(dict):
    def __write_protection__(self, *args, **kwargs):
        raise RuntimeError("Inmutable ReadOnlyDictionary: write operations aren't allowed!")
    
    __setitem__ = __write_protection__
    __delitem__ = __write_protection__
    
    clear = __write_protection__
    pop = __write_protection__
    popitem = __write_protection__
    setdefault = __write_protection__
    update = __write_protection__
    
    


def clean_up(dashboard):
    #############################################
    ##      EVITAMOS EJECUCIONES ABSURDAS      ##
    #############################################

    if dashboard['LEVEL'][0:2] != '2_':
        dashboard['L2_QUERY_WHERE'] = 'No'

    if dashboard['LEVEL'] != '1_EXTERNAL_SCRIPTS':
        dashboard['EXTERNAL_SCRIPTS_IMPORT'] = ['Ninguno']    

    if dashboard['LEVEL'] != '1_EXTERNAL_PREDICTIONS':
        dashboard['EXTERNAL_PREDICTIONS_IMPORT'] = ['Ninguno']

    if dashboard['LEVEL'] != '2_ENSEMBLE':
        dashboard['L2_ENSEMBLE_NUM_ROUNDS'] = [0]
        




      
        
def generate_unused_friendly_id(already_used_ids, friendly_id):
    friendly_id="".join([c for c in friendly_id if c.isalpha() or c.isdigit() or c in (' ', '_', '-')]).strip()
    
    if friendly_id in already_used_ids:
        i=2
        new_id_generated=False
        while not new_id_generated:
            new_id=friendly_id+"-"+str(i)
            if new_id in already_used_ids:
                i+=1
            else:
                friendly_id=new_id
                new_id_generated=True
    
    return friendly_id
        
    
    

def get_dashboard_without_internals(dashboard, extra_internals=None):
    '''
    returns a dashboard copy, without internal variables
    '''
    
    dasboard_internals = DASHBOARD_INTERNAL_VARIABLES

    if extra_internals is not None:
        dasboard_internals = extra_internals + dasboard_internals
        
    regex = "^(" + "|".join(dasboard_internals) + ")$"
    
    dashboard_without_internals = dashboard.copy()
    
    
    variables = list(dashboard_without_internals.keys())
    
    for variable in variables:
        if re.search(regex, variable):
            #print ("eliminamos variable interna: " + variable)
            del dashboard_without_internals[variable]

    
    return dashboard_without_internals



def get_iterable_variables(dashboard):
    iterable_variables = []
        
    for variable_name in dashboard.keys():
        if variable_name.endswith('*'):
            variable_name = variable_name[:-1]
            iterable_variables.append(variable_name)
    
    iterable_variables = sorted (iterable_variables)
    
    return iterable_variables
    

def get_marked_variables(dashboard, mark):
    '''
    Return the list of variable keys in dashboards that ends with mark string

    Returned values doesn't contain the ending mark
    '''
    marked_variables = []
    
    for variable_name in dashboard.keys():
        marked_variable_name = variable_name + mark
        
        if marked_variable_name in dashboard:
            marked_variables.append(variable_name)
    
    return marked_variables

def eml_dashboard_hash(dashboard):
    hashable_dashboard = hashable(dashboard)
    #print ("============== eml_dashboard_hash ================= " + str(hashable_dashboard.__hash__()))
    #print(hashable_dashboard)
    #print ("============== eml_dashboard_hash ================= " + str(hashable_dashboard.__hash__()))
    return str(str(hashable_dashboard).__hash__())

def hashable(obj):
    processed_mark="slwio902834_!!!"
    items_mark="|||-|||"
    if isinstance(obj, collections.Hashable) and not isinstance(obj,tuple):
        items = obj
    elif isinstance(obj, tuple) and np.size(obj)>0 and obj[0]==processed_mark:
        items =  obj
    
        '''
        elif isinstance(obj, frozenset) and (processed_mark,processed_mark) in obj:
        items =  obj
        elif isinstance(obj, collections.Mapping):
            dictx={processed_mark:processed_mark}
            for k,v in obj.items():
                dictx[k]=hashable(v)
            
            #tuples = (k, hashable(v)) for k, v in obj.items()
            #print (tuples)
            #print (dictx)
            items = frozenset(dictx.items())
        '''
    elif isinstance(obj, collections.Mapping):
        items = (processed_mark,) 
        keys=sorted(obj.keys())
        for k in keys:
            items += k,
            items += hashable(obj[k]),
            items += items_mark,
    elif isinstance(obj, collections.Iterable):
        items = (processed_mark,) + tuple(hashable(item) for item in obj)
    
    else:
        raise TypeError(type(obj))

    return items

    
    
    
def reduce_dashboard_to(dashboard, variables_white_list):
    '''
    returns a dashboard copy, with variables in white list only
    '''
    
    reduced_dashboard = dashboard.copy()
    
       
    regex = "^(" + "|".join(variables_white_list) + ")$"
    
    
    
    
    variables = list(reduced_dashboard.keys())
    
    for variable in variables:
        if not re.search(regex, variable):
            #print ("eliminamos variable: " + variable)
            del reduced_dashboard[variable]

    
    return reduced_dashboard


def set_paths_on(dashboard):
    dashboard['SCRIPT_FILENAME']=os.path.basename(sys.argv[0])
    dashboard['SCRIPT_PATH']=os.path.dirname(sys.argv[0])
    
    if not 'DIR_BASE' in dashboard:
        dashboard['DIR_BASE']='../'+dashboard ['COMP_NAME']
    
    dashboard['DIR_TRAIN_PATH'] =dashboard['DIR_BASE']+"/input/"+(dashboard['COMP_TRAIN_FILES'] if isinstance(dashboard['COMP_TRAIN_FILES'], str) else dashboard['COMP_TRAIN_FILES'][0])
    dashboard['DIR_TEST_PATH']  =dashboard['DIR_BASE']+"/input/"+(dashboard['COMP_TEST_FILES'] if isinstance(dashboard['COMP_TEST_FILES'], str) else dashboard['COMP_TEST_FILES'][0])
    
    dashboard['DIR_Y_TEST_PATH'] = "" if 'COMP_Y_TEST_FILE' not in dashboard else dashboard['DIR_BASE']+"/input/"+dashboard['COMP_Y_TEST_FILE']
    
    dashboard['DIR_TRAIN_EXTERNAL_SCRIPTS_PATH']=dashboard['DIR_BASE']+'/external_scripts/input/train.csv'
    dashboard['DIR_TEST_EXTERNAL_SCRIPTS_PATH']=dashboard['DIR_BASE']+'/external_scripts/input/test.csv'
    
    dashboard['DIR_COMP'] =dashboard['DIR_BASE']+'/'
    dashboard['DIR_DATA'] = dashboard['DIR_BASE']+'/data/'
    dashboard['DIR_EXTERNAL_SUBMISSIONS'] = dashboard['DIR_BASE']+'/external_submissions/'
    dashboard['DIR_EXTERNAL_SCRIPTS'] = dashboard['DIR_BASE']+'/external_scripts/'
    dashboard['DIR_TRAIN_TEST_DIRECTORY'] = dashboard['DIR_BASE']+'/input/'
    dashboard['DIR_INPUT_CUSTOM'] = dashboard['DIR_BASE']+'/input_custom/'
    
    dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_1_VALIDATION']=dashboard['DIR_BASE']+'/submissions_Level_1_validation/'
    dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_2_VALIDATION']=dashboard['DIR_BASE']+'/submissions_Level_2_validation/'
    #dashboard['DIR_OUTPATH_SUBMISSIONS_TRAIN']=dashboard['DIR_BASE']+'/submissions_train/'
    
    if not 'DIR_OUTPATH_SUBMISSIONS_LEVEL_1' in dashboard:
        dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_1']=dashboard['DIR_BASE']+'/submissions_Level_1/'
    dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_2']=dashboard['DIR_BASE']+'/submissions_Level_2/'
    dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_3']=dashboard['DIR_BASE']+'/submissions_Level_3/'
    
    dashboard['DIR_OUTPATH_CHARTS'] =dashboard['DIR_BASE']+'/'
    
    dashboard['DIR_OUTPATH_PICKLES'] = dashboard['DIR_BASE']+'/pickles/'
    
    dashboard['DIR_CACHE'] = dashboard['DIR_BASE']+'/cache/'
    dashboard['DIR_PREPROCESSED_DATASETS'] = dashboard['DIR_BASE']+'/preprocessed_datasets/'

def set_defaults(dashboard, default_dashboard):
    for key in default_dashboard:
        
        non_iterable_variable_name = str(key)
        if non_iterable_variable_name.endswith('*'):
            non_iterable_variable_name = non_iterable_variable_name[:-1]
        
        iterable_variable_name = non_iterable_variable_name + "*"
        
        
        if not non_iterable_variable_name in dashboard and not iterable_variable_name in dashboard:
            if DSH_UTILS_DEBUG:
                print ("Asignamos " + str(key) + ": " + str(default_dashboard[key]))
            dashboard[key] = default_dashboard[key]


def make_up(dashboard, algoritmos, default_dashboard=default_dashboard):
    from my_Input_Output import hdf5_dataset_name
    
    algorithm_factory=AlgorithmFactory()
    algorithm=algorithm_factory.get(dashboard)
    
    allowed_chars_regex=r'^[a-zA-Z0-9_]*$'
    for k in default_dashboard.keys():
        if k.endswith('*'):
            raise RuntimeError('found * in default dashboard:  default must contain just boosterama params; secuence of params is not allowed for default dashboard {}'.format(k))
        if not re.match(allowed_chars_regex, k):
            raise RuntimeError('{} contains not valid characters.  allowed chars:  alphanum and underscore'.format(k))
    
    set_defaults(dashboard, default_dashboard)
    
    dashboard['META_TRAIN_SPLIT_DESC'] = hdf5_dataset_name(dashboard,"y_val_preds",0,"HDF5")[:-len('_y_val_preds')]
    
    make_up_split_cv_external_predictions(dashboard)   
    make_up_split_level2(dashboard)  
    set_paths_on(dashboard)
    make_up_import_train_stratified(dashboard)
    make_up_algoritmos_parametros(dashboard, algoritmos)
    #params_dict_list=dashboard['ML_ALGO_PARAMETROS*'] if 'ML_ALGO_PARAMETROS*' in dashboard else [dashboard['ML_ALGO_PARAMETROS']]
    #params_combinations=[x[dashboard['ML_ALGO'][0]] for x in params_dict_list]
    #algorithm.make_up(dashboard, params_combinations, default_dashboard)
    algorithm.make_up(dashboard, default_dashboard) #not tested yet
    
    
    make_up_iterable_variables(dashboard)   
    clean_up(dashboard)
    
    if dashboard['PRINT_DASHBOARD']:
        print_dashboard(dashboard)


def make_up_split_cv_external_predictions(dashboard):
    if dashboard['LEVEL']=='1_EXTERNAL_PREDICTIONS':
        dashboard['SPLIT_CV_KFOLDS'] = 1111
        dashboard['SPLIT_VALIDATION_SET_PERC']=0.0
        dashboard['ITER_DO_DOUBLE_EXECUTION']=0



        
def make_up_split_level2(dashboard):

    if dashboard['LEVEL'][0:2]=='2_'  and dashboard['L2_CV_OR_DATASET'] == 'CV':
        from my_Input_Output import hdf5_dataset_name
        
        #dashboard['L2_LEVEL1_DATASETS_TRAIN'] = 'level1_Train100%_WR(0,0)_CV'
        dataset_name = hdf5_dataset_name(dashboard,"y_val_preds",0,"HDF5", level='level1')[:-len('_y_val_preds')]
        dashboard['L2_LEVEL1_DATASETS_TRAIN'] = dataset_name
        dashboard['L2_LEVEL1_DATASETS_TEST'] = 'level1_test100%'
        
        
    if dashboard['LEVEL']=='2_ENSEMBLE':
        dashboard['SPLIT_CV_KFOLDS'] = 1
        dashboard['SPLIT_VALIDATION_SET_PERC'] = 0.0
        dashboard['ITER_DO_DOUBLE_EXECUTION']=0


   
def make_up_import_train_stratified(dashboard):
    if dashboard['IMP_TRAIN_SUBSET_TYPE']=='pPercentageStratifiedRandomSamples':
        dashboard['IMP_TRAIN_STRATIFIED'] = 'SPLIT_CV_KFOLDS_' + str(dashboard['SPLIT_CV_KFOLDS*'])
    else:
        dashboard['IMP_TRAIN_STRATIFIED'] = 'NoAplica'



def make_up_iterable_variables(dashboard):
    dashboard['ITER_ITERABLE_VARIABLES'] = get_iterable_variables(dashboard)

    for iterable_variable in dashboard['ITER_ITERABLE_VARIABLES']:
        if DSH_UTILS_DEBUG:
            print (iterable_variable + " es Iterable")
        dashboard[iterable_variable] = dashboard.pop(iterable_variable + '*')



def make_up_algoritmos_parametros(dashboard, algoritmos):
    #############################################
    ### CUMPLIMENTACIÓN ALGORITMOS_PARAMETROS ###
    #############################################
    
    if algoritmos is None:
        dashboard['ML_ALGO_PARAMETROS*']=[{"None":{}}]
    else:
        algs_requested = dashboard['ML_ALGO']
        
        parametros_algotimos = []
        for alg in algs_requested:
            parametros_algotimos.append({alg : algoritmos[alg]})
        
        dashboard['ML_ALGO_PARAMETROS*']= parametros_algotimos
        
        
        def combinations(a):
            r=[[]]
            for x in a:
                t = []
                for y in x:
                    for i in r:
                        t.append(i+[y])
                r = t
            return r
            
        def split_combinations(ALGORITMOS_PARAMETROS):
            parametros_algotimos = []
            cont = 0
            for num_alg in range(len(ALGORITMOS_PARAMETROS)):
                for alg_key in ALGORITMOS_PARAMETROS[num_alg].keys():
                    param_names = []            
                    param_list = []
                    param_dict = ALGORITMOS_PARAMETROS[num_alg][alg_key]
                    if alg_key.upper()[:3] == "XGB" and 'mym__seed' not in param_dict:
                        param_dict['mym__seed'] = [dashboard['RANDOM_SEED']]
                    for parametro in param_dict:
                        param_names.append(parametro)
                        param_list.append(param_dict[parametro])
                    
                    param_combinations = combinations(param_list)
                    
                    for num_combination in range(len(param_combinations)):
                        parametros_algotimos.append({alg_key:{}})
                        for num_param in range(len(param_combinations[num_combination])):
                            parametros_algotimos[cont][alg_key][param_names[num_param]]=[param_combinations[num_combination][num_param]]
                        cont += 1
            return parametros_algotimos
                   
        dashboard['ML_ALGO_PARAMETROS*']= split_combinations(dashboard['ML_ALGO_PARAMETROS*'])
    
def print_dashboard(dashboard):
    
    print()
    print("DASHBOARD")
    print("---------")
    for key in dashboard.keys():
        print(key,":",dashboard[key])
    print()



            
#######################################################
###                                                 ### 
###           PRINT DASHBOARD DIFFERENCES           ###
###                (current vs best)                ### 
#######################################################


def dict_diff(first, second):
    """ Return a dict of keys that differ with another config object.  If a value is
        not found in one fo the configs, it will be represented by KEYNOTFOUND.
        @param first:   Fist dictionary to diff.
        @param second:  Second dicationary to diff.
        @return diff:   Dict of Key => (first.val, second.val)
    """
    KEYNOTFOUNDIN1 = '<KEYNOTFOUNDIN1>'       # KeyNotFound for dictDiff
    KEYNOTFOUNDIN2 = '<KEYNOTFOUNDIN2>'       # KeyNotFound for dictDiff

    diff = {}
    sd1 = set(first)
    sd2 = set(second)
    #Keys missing in the second dict
    for key in sd1.difference(sd2):
        diff[key] = KEYNOTFOUNDIN2
    #Keys missing in the first dict
    for key in sd2.difference(sd1):
        diff[key] = KEYNOTFOUNDIN1
    #Check for differences
    for key in sd1.intersection(sd2):
        if first[key] != second[key]:
            diff[key] = (first[key], second[key])    
    return diff

    
    


def print_dashboard_differences(d, dashboard_best, did_improve_overall):
    
    try:
    
        if dashboard_best != "No":
            
            # Clean Dictionaries
            d_PRP              = {k: v for k, v in d.items() if ('PRP' in k or k=='ID')}
            dashboard_best_PRP = {k: v for k, v in dashboard_best.items() if ('PRP' in k or k=='ID')}
            d_ML               = {k: v for k, v in d.items() if ('ML_ALGO_PARAMETROS' in k)}
            dashboard_best_ML  = {k: v for k, v in dashboard_best.items() if ('ML_ALGO_PARAMETROS' in k)}
                     
    
            
            dict_differences_PRP   = dict_diff(d_PRP, dashboard_best_PRP)
            dict_differences_ML = dict_diff(d_ML['ML_ALGO_PARAMETROS'][d['ML_ALGO'][0]], dashboard_best_ML['ML_ALGO_PARAMETROS'][d['ML_ALGO'][0]])
                   
    
            '''
            # Ignore
            ignore_these_keys = ['SPLIT_CV_KFOLDS_EARLY_STOP']
            
            for key in ignore_these_keys:
                try:
                    dict_differences_PRP.pop(key)
                except:
                    pass
            '''
            
            # Get IDs
            ID_current = d_PRP.pop('ID')
            ID_best    = dashboard_best_PRP.pop('ID')
            dict_differences_PRP.pop('ID')
            
            
            # Text before scores    
            if did_improve_overall == "yes":
                current_label = "Current Execution:"
                best_label    = "Previous Best:    "
            else:
                current_label = "Current:"
                best_label    = "Best:   "
    
            
            if 'ML_ALGO_PARAMETROS' in dict_differences_ML:
                diff_in_ML_ALGO_PARAMETROS = 1
            else:
                diff_in_ML_ALGO_PARAMETROS = 0
    
    
            print('\n---------------------------------------------------------------------------')
            print("ID{} {} ({})        ID{} {} ({})".format(
                    current_label, d['FRIENDLY_ID'], ID_current,
                    best_label, dashboard_best['FRIENDLY_ID'], ID_best
                ))
        
    
            
    
     
            # Preprocessing
            #################
            if len(dict_differences_PRP) > 1 or (len(dict_differences_PRP) > 0 and 'PRP_NA_FILL' not in dict_differences_PRP):
                print (Fore.BLACK + Back.GREEN + Style.DIM + "\n    DIFF DASHBOARDS PREPROCESSING    " +  Style.RESET_ALL)
                
                if 'PRP_NA_FILL' in dict_differences_PRP:
                    PRP_NA_FILL = dict_differences_PRP.pop('PRP_NA_FILL')
                    # If first element is equal in current and best we do not print (it's mostly noise)
    
                    if PRP_NA_FILL[0][0] != PRP_NA_FILL[1][0]:
                        
                        
                        if did_improve_overall == "yes":
                            print('\t ' + Fore.BLACK + Back.CYAN + Style.DIM +  ' PRP_NA_FILL ' +  Style.RESET_ALL)
                            print(current_label,Fore.BLUE + str(PRP_NA_FILL[0]) +  Style.RESET_ALL)
                            print(best_label,str(PRP_NA_FILL[1]))
                        elif did_improve_overall == "no":
                            print('\t ' + Fore.BLACK + Back.YELLOW + Style.DIM +  ' PRP_NA_FILL ' +  Style.RESET_ALL)
                            print(current_label,Fore.RED + str(PRP_NA_FILL[0]) +  Style.RESET_ALL)
                            print(best_label,str(PRP_NA_FILL[1]))
                        else:
                            print('\t ' + ' PRP_NA_FILL ')
                            print(current_label,str(PRP_NA_FILL[0]))
                            print(best_label,str(PRP_NA_FILL[1]))
                    
                for key, value in dict_differences_PRP.items():
                    
                    if did_improve_overall == "yes":
                        print('\t ' + Fore.BLACK + Back.CYAN + Style.DIM + ' ' +  key + ' ' +  Style.RESET_ALL)
                        print(current_label,Fore.BLUE + str(value[0]) +  Style.RESET_ALL)
                        print(best_label,str(value[1])) 
                    elif did_improve_overall == "no":
                        print('\t ' + Fore.BLACK + Back.YELLOW + Style.DIM + ' ' +  key + ' ' +  Style.RESET_ALL)
                        print(current_label,Fore.RED + str(value[0]) +  Style.RESET_ALL)
                        print(best_label,str(value[1])) 
                    else:
                        print('\t ' +  key + ' ')
                        print(current_label,str(value[0]))
                        print(best_label,str(value[1]))
                    
                    #print(current_label,value[0])
                    #print(best_label,value[1])
    
                
            # ML parameters
            ################
            if diff_in_ML_ALGO_PARAMETROS:          
                print (Fore.BLACK + Back.GREEN + Style.DIM + "\n        DIFF DASHBOARDS ML         " +  Style.RESET_ALL)
                
                for key, value in dict_differences_ML.items():
                    
                    if did_improve_overall == "yes":
                        print('\t' + Fore.BLACK + Back.CYAN + Style.DIM + ' ' +  key + ' ' +  Style.RESET_ALL)
                        print(current_label,Fore.BLUE + str(value[0]) +  Style.RESET_ALL)
                        print(best_label,str(value[1])) 
                    elif did_improve_overall == "no":
                        print('\t' + Fore.BLACK + Back.YELLOW + Style.DIM + ' ' +  key + ' ' + Style.RESET_ALL)
                        print(current_label,Fore.RED + str(value[0]) +  Style.RESET_ALL)
                        print(best_label,str(value[1])) 
                    else:
                        print('\t ' +  key + ' ')
                        print(current_label,str(value[0]))
                        print(best_label,str(value[1]))                
                    
    
            print('---------------------------------------------------------------------------\n')
            
    except Exception as e:
        print("ERROR: ",e.__class__.__name__," in print_dashboard_differences: \n", traceback.format_exception(None, e, e.__traceback__))
        
