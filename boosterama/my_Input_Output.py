# -*- coding: utf-8 -*-
"""
Created on Tue Sep 15 20:08:44 2015

@author: DataCousins
"""

from colorama import Fore, Back, Style
import csv
from datetime import datetime, timedelta
import gc
import glob
import h5py
import numpy as np
import os
import pandas as pd
import pickle
import pytz
import os
import re
import sqlite3
import sys
import xlsxwriter

from my_Cache import *
from my_Prepro_fx import *
from my_Submissions import *

from framework.execution_context import ExecutionContext

from my_Dashboard_Utils import ReadOnlyDictionary

PICKLE_PROTOCOL = pickle.HIGHEST_PROTOCOL
#PICKLE_PROTOCOL = 0


########################################################
#####                                              #####
#####                    DELETE                    #####
#####                                              #####
########################################################   
    
def delete_execution_data(d):
    #Delete from CSV, PKL and from HDF5
    
    submission_folders = ['submissions_Level_1','submissions_Level_1_validation','submissions_Level_2','submissions_Level_2_validation']
    
    ##  TRAIN:Selecting desired id_executions ##
    ############################################      
    select_list = ['ID_FAMILY', 'ID_EXECUTION'] + ['score_validation']
    query_where = d['DELETE_OR_OVERFIT_QUERY_WHERE']
    
    
    # Ejecutamos query para obtener los id_executions (en esta ejecución ordenamos por id_execution)
    family_ids_requested_list,ids_executions_requested_list,orderby_field_values = sqlite_query_select_id_executions(select_list,query_where,'ID_EXECUTION','ASC',d['DIR_DATA'])

    
    ##  Selecting common HDF5 id_executions in TRAIN and Test  ##
    #############################################################     
    TRAIN_ids_executions_found, TRAIN_ids_executions_not_found = hdf5_find_id_executions(d, ids_executions_requested_list,'Ignore', d['DIR_DATA'], 'CV')
    
    print('ids_executions_requested_list:',ids_executions_requested_list,'\n')
    
    if len(TRAIN_ids_executions_not_found)>0:
        print('\nTRAIN_ids_executions_not_found:',TRAIN_ids_executions_not_found,'\n')
    
    # Conectamos a Sqlite
    db_path = d['DIR_DATA'] +'executions_info.db'
    

    conn = sqlite3.connect(db_path)
    c = conn.cursor()


    # Set as OVERFIT
    #################
    if d['DELETE_OR_OVERFIT'] == "Overfit":
        
        for exec_id in TRAIN_ids_executions_found:
            
            # SQlite
            try:
                c.execute("UPDATE executions_info SET overfit=1 WHERE ID_EXECUTION='" + str(exec_id) + "';" )
                conn.commit()
                print(Fore.BLUE + "SQLITE: Execution " + str(exec_id) + " marked as 'Overfit'"+  Style.RESET_ALL)
            except Exception as e:
                print(Fore.RED + "SQLITE: " + str(e) +  Style.RESET_ALL)
                print(Fore.RED + "\tWe could not set execution " + str(exec_id) + " to overfit=1 in SQlite"+  Style.RESET_ALL)
    
            # Pickle
            try:
                # Load Pickle
                this_execution_results_dict = pickle_load_one_execution_results_dict(exec_id, d['DIR_BASE'])
                # Edit Dictionary
                this_execution_results_dict['overfit'] = 1
                # Save Pickle
                pickle_save_execution_results_dict(this_execution_results_dict, exec_id, d['DIR_BASE'])
                print(Fore.BLUE + "PICKLE: Execution " + str(exec_id) + " marked as 'Overfit'"+  Style.RESET_ALL)
            except:
                print(Fore.RED + "PICKLE: We could not set execution " + str(exec_id) + " to overfit=1 in the PICKLEd results"+  Style.RESET_ALL)
    

    # DELETE Totally
    #################           
    elif d['DELETE_OR_OVERFIT'] == "Delete":
        
        for exec_id in TRAIN_ids_executions_found:
            
            # Sqlite
            #c.execute("DELETE FROM executions_info WHERE ID_EXECUTION='" + str(exec_id) + "';" )
            #print(Fore.BLUE + "Execution " + str(exec_id) + " removed from SQlite"+  Style.RESET_ALL)
            try:
                c.execute("DELETE FROM executions_info WHERE ID_EXECUTION='" + str(exec_id) + "';" )
                conn.commit()
                print(Fore.BLUE + "SQLITE: Execution " + str(exec_id) + " removed from SQlite"+  Style.RESET_ALL)
            except:
                try:    
                    conn.close()
                    
                    conn = sqlite3.connect(db_path)
                    c = conn.cursor()
                    
                    c.execute("DELETE FROM executions_info WHERE ID_EXECUTION='" + str(exec_id) + "';" )
                    conn.commit()
                    print(Fore.BLUE + "SQLITE: Execution " + str(exec_id) + " removed from SQlite"+  Style.RESET_ALL)
                except Exception as e:
                    print(Fore.RED + "SQLITE: " + str(e) +  Style.RESET_ALL)
                    print(Fore.RED + "\tWe could not remove " + str(exec_id) + " in SQlite\n\tIf this execution was succesfully removed from pickle, this may create issues in the future.\n\tDatabase may be locked. Try restarting your computer and deleting the execution again.\n"+  Style.RESET_ALL)

            # Pickle
            pickle_delete(d, exec_id)

            # CSV
            for subm_folder in submission_folders:
                CSV_file_names_delete_all(d['DIR_COMP'] + subm_folder + "/", str(exec_id))

            # Hdf5
            # Still not deleting submissions in Hdf5, although this should not have any effect, since it has been deleted in  sqlite and pickle.

            
    # Closing SQlite connection                
    conn.close()

    if d['PRINT_TIME']:
        print(Fore.BLUE + '\nELAPSED TIME -  ML Level 3: ' + str(int(timeit.default_timer()-t0)) + " segundos." +  Style.RESET_ALL)
    


########################################################
#####                                              #####
#####                      Import                  #####
#####                                              #####
########################################################       


def doImport(d, CUSTOM_IMPORTER,doPrintNA,doPrintNAShowFields):
    
    if d['SYS_DO_CUSTOM_IMPORT'] == 1:
        
        #Si no existen train.csv y test.csv en input hacemos custom_import con los ficheros de input_custom
        if (d['LEVEL']=='1_ML' or d['LEVEL']=='2_ML' ) and not os.path.isfile(d['DIR_TRAIN_PATH']) and not os.path.isfile(d['DIR_TEST_PATH']):
            print("Proceed to custom import")
            CUSTOM_IMPORTER(d ,d['DIR_INPUT_CUSTOM'],d['DIR_TRAIN_PATH'],d['DIR_TEST_PATH'],d['PRINT_CUSTOM_IMPORT'],d['DIR_TRAIN_TEST_DIRECTORY'],d['COMP_COLUMN_CLASS'])


        if (d['LEVEL']=='1_ML' or d['LEVEL']=='2_ML' ) and os.path.isfile(d['DIR_TRAIN_PATH']) and os.path.isfile(d['DIR_TEST_PATH']): # Importamos Normalmente
            train_test_dtypes = pd.read_csv(d['DIR_TRAIN_TEST_DIRECTORY']+'train_test_dtypes.csv',index_col=0,header=None,dtype='object')
            train_test_dtypes_dict = train_test_dtypes.to_dict()[1]          
            (trainNumRowsOriginal,trainNumRowsAfterSampling, TRAIN__pd, duplicated_cols) = import_data(d, d['DIR_TRAIN_PATH'],encodingType=d['IMP_TRAIN_ENCODING_TYPE'],delimiter=d['IMP_TRAIN_DELIMITER'],subsetType=d['IMP_TRAIN_SUBSET_TYPE'],percentage=d['IMP_TRAIN_PERCENTAGE'],tSamples=d['IMP_TRAIN_NUM_SAMPLES'],firstRow=d['IMP_TRAIN_FIRST_ROW'],types=train_test_dtypes_dict,COLUMN_CLASS=d['COMP_COLUMN_CLASS'],PROBLEM_TYPE=d['COMP_PROBLEM_TYPE'],NUM_FOLDS=d['SPLIT_CV_KFOLDS'],VALIDATION_SET_PERC=d['SPLIT_VALIDATION_SET_PERC'], duplicated_cols=None, is_trainset=True)   
            (testNumRowsOriginal,testNumRowsAfterSampling, test__pd, _)                  = import_data(d, d['DIR_TEST_PATH'],encodingType=d['IMP_TEST_ENCODING_TYPE'],delimiter=d['IMP_TEST_DELIMITER'],subsetType=d['IMP_TEST_SUBSET_TYPE'],percentage=d['IMP_TEST_PERCENTAGE'],tSamples=d['IMP_TRAIN_NUM_SAMPLES'],firstRow=d['IMP_TEST_FIRST_ROW'],types=train_test_dtypes_dict,COLUMN_CLASS=None,PROBLEM_TYPE=d['COMP_PROBLEM_TYPE'],NUM_FOLDS=d['SPLIT_CV_KFOLDS'],VALIDATION_SET_PERC=d['SPLIT_VALIDATION_SET_PERC'], duplicated_cols=duplicated_cols, is_trainset=False)   
            dic_dtypes_train_original,dic_dtypes_test_original,dic_dtypes_train, dic_dtypes_test, differences = dictionary_dtypes_train_and_test(TRAIN__pd,test__pd)
            if len(differences)==0:
                if d['PRINT_CUSTOM_IMPORT_DIFFS']:
                    print("Importer: common-train&test-columns dtypes are identical\n")
            else:
                if d['PRINT_CUSTOM_IMPORT_DIFFS']:
                    print("Importer: There were dtypes in common-train&test-columns that were DIFFERENT:",differences,"\n")
                    print(dic_dtypes_train_original,"\n")
                    print(dic_dtypes_test_original,"\n")
                    print("Importer: Unifying dtypes from common-train&test-columns that were different\n")
                    print(dic_dtypes_train,"\n")
                    print(dic_dtypes_test,"\n")
                    print("Importer: Re-importing, with unified dtypes:\n")
                (trainNumRowsOriginal,trainNumRowsAfterSampling, TRAIN__pd, duplicated_cols) = import_data(d, d['DIR_TRAIN_PATH'],encodingType=d['IMP_TRAIN_ENCODING_TYPE'],delimiter=d['IMP_TRAIN_DELIMITER'],subsetType=d['IMP_TRAIN_SUBSET_TYPE'],percentage=d['IMP_TRAIN_PERCENTAGE'],tSamples=d['IMP_TRAIN_NUM_SAMPLES'],firstRow=d['IMP_TRAIN_FIRST_ROW'],types=dic_dtypes_train,COLUMN_CLASS=d['COMP_COLUMN_CLASS'],PROBLEM_TYPE=d['COMP_PROBLEM_TYPE'],NUM_FOLDS=d['SPLIT_CV_KFOLDS'],VALIDATION_SET_PERC=d['SPLIT_VALIDATION_SET_PERC'], duplicated_cols=None, is_trainset=True)   
                (testNumRowsOriginal,testNumRowsAfterSampling, test__pd, _)                  = import_data(d, d['DIR_TEST_PATH'],encodingType=d['IMP_TEST_ENCODING_TYPE'],delimiter=d['IMP_TEST_DELIMITER'],subsetType=d['IMP_TEST_SUBSET_TYPE'],percentage=d['IMP_TEST_PERCENTAGE'],tSamples=d['IMP_TRAIN_NUM_SAMPLES'],firstRow=d['IMP_TEST_FIRST_ROW'],types=dic_dtypes_test,COLUMN_CLASS=None,PROBLEM_TYPE=d['COMP_PROBLEM_TYPE'],NUM_FOLDS=d['SPLIT_CV_KFOLDS'],VALIDATION_SET_PERC=d['SPLIT_VALIDATION_SET_PERC'], duplicated_cols=duplicated_cols, is_trainset=False)   
                dic_dtypes_train_original,dic_dtypes_test_original,dic_dtypes_train, dic_dtypes_test, differences = dictionary_dtypes_train_and_test(TRAIN__pd,test__pd)                
                if len(differences)==0:
                    if d['PRINT_CUSTOM_IMPORT_DIFFS']:
                        print("Importer: dtypes from common-train&test-columns are now identical\n")
                else:
                    sys.exit("Importer: There are dtypes from common-train&test-columns that are DIFFERENT!!")
            print_import_info(trainNumRowsOriginal,trainNumRowsAfterSampling,testNumRowsOriginal,testNumRowsAfterSampling,d['IMP_TRAIN_SUBSET_TYPE'],d['IMP_TEST_SUBSET_TYPE'])           

        else:
            sys.exit("Import Error")        

    else: # Importamos Normalmente (no es un Custom Import)
        (trainNumRowsOriginal,trainNumRowsAfterSampling, TRAIN__pd, duplicated_cols) = import_data(d, d['DIR_TRAIN_PATH'],encodingType=d['IMP_TRAIN_ENCODING_TYPE'],delimiter=d['IMP_TRAIN_DELIMITER'],subsetType=d['IMP_TRAIN_SUBSET_TYPE'],percentage=d['IMP_TRAIN_PERCENTAGE'],tSamples=d['IMP_TRAIN_NUM_SAMPLES'],firstRow=d['IMP_TRAIN_FIRST_ROW'],types=None,COLUMN_CLASS=d['COMP_COLUMN_CLASS'],PROBLEM_TYPE=d['COMP_PROBLEM_TYPE'],NUM_FOLDS=d['SPLIT_CV_KFOLDS'],VALIDATION_SET_PERC=d['SPLIT_VALIDATION_SET_PERC'], duplicated_cols=None, is_trainset=True, only_ground_truth=bool(d['IMP_LOAD_GROUND_TRUTH_ONLY']))   
        (testNumRowsOriginal,testNumRowsAfterSampling, test__pd, _)                  = import_data(d, d['DIR_TEST_PATH'],encodingType=d['IMP_TEST_ENCODING_TYPE'],delimiter=d['IMP_TEST_DELIMITER'],subsetType=d['IMP_TEST_SUBSET_TYPE'],percentage=d['IMP_TEST_PERCENTAGE'],tSamples=d['IMP_TRAIN_NUM_SAMPLES'],firstRow=d['IMP_TEST_FIRST_ROW'],types=None,COLUMN_CLASS=None,PROBLEM_TYPE=d['COMP_PROBLEM_TYPE'],NUM_FOLDS=d['SPLIT_CV_KFOLDS'],VALIDATION_SET_PERC=d['SPLIT_VALIDATION_SET_PERC'], duplicated_cols=duplicated_cols, is_trainset=False, only_ground_truth=bool(d['IMP_LOAD_GROUND_TRUTH_ONLY']))   
        print_import_info(trainNumRowsOriginal,trainNumRowsAfterSampling,testNumRowsOriginal,testNumRowsAfterSampling,d['IMP_TRAIN_SUBSET_TYPE'],d['IMP_TEST_SUBSET_TYPE'])

    if doPrintNA: print_na_info(TRAIN__pd,"TRAIN__pd",test__pd,"test__pd",calculate=True,message="After import",showFields=doPrintNAShowFields)
    
    return (test__pd, TRAIN__pd)

    
    

def import_y_test(filePath,encodingType,delimiter,types=None):
    if 'xls' in filePath[-4:]:
        data = pd.read_excel(filePath) #,  dtype=types)
    else:
        data = pd.read_csv(filePath, encoding=encodingType, sep=delimiter, engine='c', dtype=types)

    return (data)

    

def import_data(d, filePath,encodingType,delimiter,subsetType,percentage,tSamples,firstRow,types,COLUMN_CLASS,PROBLEM_TYPE,NUM_FOLDS,VALIDATION_SET_PERC, duplicated_cols, is_trainset,only_ground_truth=False):
    
#    dashboard, dXT, results = ExecutionContext().get_by_id(dashboard_id)
#    d, _, _= ExecutionContext().get_current_context()
    
    if 'xls' in filePath[-4:]:
        data = pd.read_excel(filePath) #,  dtype=types)
    else:
        usecols = [d['COMP_COLUMN_ID'], d['COMP_COLUMN_CLASS']] if only_ground_truth else None
        if usecols is not None:
            data = pd.read_csv(filePath, encoding=encodingType, sep=delimiter, engine='c', dtype=types, nrows=1)
            usecols=[x for x in usecols if x in data.columns]
            
        data = pd.read_csv(filePath, encoding=encodingType, sep=delimiter, engine='c', dtype=types, usecols=usecols)
    '''
    if is_trainset: # check for column class. sometimes user mistakenly cross over COMP_TRAIN_FILES and COMP_TEST_FILES 
        if not d['COMP_COLUMN_CLASS'] in data.columns:
            raise ValueError("No '{}' column in {}".format(d['COMP_COLUMN_CLASS'], filePath))
    '''
    delete_duplicates=d['PRP_IMPORT_DELETE_DUPLICATED_COLUMNS'].lower()=='yes'
    if len(d['PRP_IMPORT_FEATURES'])>0:
        def add_path(filename):
            if not filename.startswith('.'):
                filename='features/'+filename
            return filename
        if d['PRP_IMPORT_DELETE_DUPLICATED_COLUMNS'].lower()=='auto':
            delete_duplicates=True
        
        for feature_filename in d['PRP_IMPORT_FEATURES']:
            feature_name=None
            if '{}' in feature_filename:
                feature_name=feature_filename.replace('-{}', '').replace('{}', '')
                feature_filename=(feature_filename.format('train'), feature_filename.format('test'))
            
            importing_test_test=COLUMN_CLASS is None
                            
                
            if isinstance(feature_filename, tuple): # Separated files for train/test
                feature_filename=feature_filename[int(importing_test_test)]
                feature_df = pd.read_csv(add_path(feature_filename), encoding=encodingType, sep=delimiter, engine='c', dtype=types)
            else: # stacked file for train/test
                feature_name=feature_filename
                if d['PRP_IMPORT_FEATURES_TRAIN_TEST_ROWS']==(0,0):
                    #assert d['PRP_IMPORT_FEATURES_USING_SAME_ROW_ORDER'], "FATAL. Cant import
                    feature_df = pd.read_csv(add_path(feature_filename), encoding=encodingType, sep=delimiter, engine='c', dtype=types)
                else:
                    if importing_test_test: # test_set
                        feature_df = pd.read_csv(add_path(feature_filename), skiprows=range(1,d['PRP_IMPORT_FEATURES_TRAIN_TEST_ROWS'][0] +1), encoding=encodingType, sep=delimiter, engine='c', dtype=types)
                    else:
                        feature_df = pd.read_csv(add_path(feature_filename), nrows=d['PRP_IMPORT_FEATURES_TRAIN_TEST_ROWS'][0], encoding=encodingType, sep=delimiter, engine='c', dtype=types)
            
            
            using_same_row_order=d['PRP_IMPORT_FEATURES_USING_SAME_ROW_ORDER']
            if not using_same_row_order and not d['COMP_COLUMN_ID'] in feature_df.columns:
                print('WARNING. {} not found in {}: forcing PRP_IMPORT_FEATURES_USING_SAME_ROW_ORDER=1'.format(d['COMP_COLUMN_ID'], feature_filename))
                using_same_row_order=1
            if using_same_row_order and d['COMP_COLUMN_ID'] in feature_df.columns:
                del feature_df.columns[d['COMP_COLUMN_ID']]
            
            i=0
            for col_name in feature_df.columns:
                renames={}
                if (col_name in data.columns or col_name==d['COMP_COLUMN_CLASS'] or col_name==d['COMP_COLUMN_CLASS_4_SUBMISSION']) \
                    and col_name!=d['COMP_COLUMN_ID']:
                    i+=1
                    
                    print('INFO. column name {} from {} already in use by another datasource. renamed'.format(col_name, feature_filename))
                    if feature_name is None:
                        new_col_name=col_name+'_imp_'+str(i)
                    else:
                        new_col_name=feature_name+"_"+col_name
                        if i>2:
                            new_col_name=new_col_name+"_"+str(i)
                    renames[col_name]=new_col_name
                if len(renames):
                    feature_df.rename(columns=renames, inplace=True)
                    
                    
            
            if using_same_row_order:
                data=pd.concat([data, feature_df], axis=1)
            else:
                data=data.merge(feature_df, on=d['COMP_COLUMN_ID'], how='left')

    if COLUMN_CLASS is None:
        if d['COMP_COLUMN_CLASS'] in data:
            print(Fore.RED +"WARNING.  Removing column Y from test dataset: {}  - Source filename: {}", d['COMP_COLUMN_CLASS'], filePath)
            del(data[d['COMP_COLUMN_CLASS']])
    
    if delete_duplicates and duplicated_cols is None:
        duplicated_cols=[]
        
        import hashlib
        
        '''
            quick an dirty implementation.
            there should be a more efficient way to get the hash
        '''
        hashes={}
        not_to_be_deleted=[d['COMP_COLUMN_CLASS'], d['COMP_COLUMN_ID']]
        for col_name in data.columns:
#            h=hash(tuple(data[col_name].values))
            h=hashlib.sha1(data[col_name].values).hexdigest()
            
            if h in hashes:
                col_to_delete = col_name 
                if col_name in not_to_be_deleted:
                    col_to_delete=hashes[h]
                    hashes[h]=col_name
#                del data[col_to_delete]
                    
                if(np.all(data[col_to_delete].values==data[hashes[h]].values)):
                    data.drop(columns=[col_to_delete], inplace=True)
                    duplicated_cols.append(col_to_delete)
                    print("PRP_IMPORT_DELETE_DUPLICATED_COLUMNS: {} is a duplicate of {}.  DELETED (hash={})".format(col_to_delete, hashes[h], h))
                else:
                    # too much hash collisions: I think it is using only the column beggining and ending to perform the hash ¿?
                    print("DEBUG. PRP_IMPORT_DELETE_DUPLICATED_COLUMNS: {} is NOT a duplicate of {}.  (hash={})".format(col_to_delete, hashes[h], h))
            else:
                hashes[h]=col_name
            gc.collect()
    elif delete_duplicates and duplicated_cols is not None:
        
        for col_name in data.columns:
           if col_name in duplicated_cols:
                data.drop(columns=[col_name], inplace=True)
                print("PRP_IMPORT_DELETE_DUPLICATED_COLUMNS: {} is a known duplicate.  DELETED".format(col_name))
                gc.collect()
    
            
    
    dataRowsOriginal = data.shape[0]

    if subsetType == 'allSamples':
        pass
    elif subsetType == 'booleanExpressions':
        for expression in d['IMP_TRAIN_SUBSET_EXPRESSION_LIST']:
            expression = expression.replace('df', 'data')
            expression = 'data[{}]'.format(expression)
            data=eval(expression)
            data.reset_index(drop=True, inplace=True)
    elif subsetType == 'interPercentileRange':
        left_q, right_q=d['IMP_TRAIN_INTER_PERCENTILE_RANGE']
        left_q_value, right_q_value = data[COLUMN_CLASS].quantile(left_q), data[COLUMN_CLASS].quantile(right_q)
        if left_q>0.0:
            data=data[data[COLUMN_CLASS] > left_q_value]
            data.reset_index(drop=True, inplace=True)
        if right_q<1.0:
            data=data[data[COLUMN_CLASS] < right_q_value]
            data.reset_index(drop=True, inplace=True)
    elif subsetType == 'nConsecutiveSamples':
        data = data.iloc[range(firstRow,firstRow+tSamples)]
    elif subsetType == 'nRandomSamples':
        randomSelection = np.random.choice(range(data.shape[0]), size=tSamples, replace=False, p=None) 
        data = data.iloc[randomSelection]
    elif subsetType == 'pPercentageRandomSamples':
        tSamples = int(round(data.shape[0] * percentage/100,0))
        randomSelection = np.random.choice(range(data.shape[0]), size=tSamples, replace=False, p=None) 
        data = data.iloc[randomSelection] 
    elif subsetType == 'pPercentageStratifiedRandomSamples' and COLUMN_CLASS!=None:
        
        if PROBLEM_TYPE[:5].lower() == 'class':
            percentage_solicitado = percentage
            if percentage > 50:
                percentage = 100 - percentage
                subset = "train"
            else:
                subset = "test"
              
            # Introduzco este caso particular pues utilizaremos SPLIT_CV_KFOLDS = 1 para indicar que no hacemos CV, sino ValidationSet, es decir (train, val)
            if NUM_FOLDS == 1: #Caso en el que no hay CrossValidation, sino ValidationSet
                NUM_FOLDS = 2 #Es como si tuvieramos dos folds: (train, val)
                
            num_clases_distintas = len(data.groupby([COLUMN_CLASS]))
            muestras_por_fold = np.max([np.ceil(data.shape[0]*percentage/100),num_clases_distintas*(NUM_FOLDS)])
            nFolds_minimo = np.max([round(data.shape[0] / muestras_por_fold,0), 2])
            nFolds = np.min([data.shape[0],nFolds_minimo])

            skf = StratifiedKFold(data[COLUMN_CLASS], n_folds=nFolds)        
            for train_index, test_index in skf:
                if (subset == "test"):
                    X_TRAIN_fold_test = data.iloc[test_index]
                else:
                    X_TRAIN_fold_test = data.iloc[train_index]
            num_clases_distintas_en_train_reducido = len(X_TRAIN_fold_test.groupby([COLUMN_CLASS]))
            num_elmtos_clase_menos_representada = np.min(X_TRAIN_fold_test.groupby(COLUMN_CLASS).Species.count())
            
            #print('nFolds',nFolds)
            #print('num_elmtos_clase_menos_representada',num_elmtos_clase_menos_representada)
   
            # La condiciÃ³n es que en cross validation, tiene que haber al menos un label de la clase menos representada para cada fold
            # No tomaremos todo el y_train, sino el mini_y_train de CrossValidation len(mini_y_train) = len(y_TRAIN) * (NUM_FOLDS-1)/NUM_FOLDS
            # Hay que garantizar que en la particion del mini_train en CrossValidation haya al menos NUM_FOLDS-1 elementos de la clase menos representada
            proporcion_mini_train = (NUM_FOLDS-1)/NUM_FOLDS
            
            while (num_clases_distintas_en_train_reducido < num_clases_distintas or int(num_elmtos_clase_menos_representada * proporcion_mini_train) < NUM_FOLDS-1) and nFolds>2:
                nFolds = nFolds - 1
                skf = StratifiedKFold(data[COLUMN_CLASS], n_folds=nFolds)        
                for train_index, test_index in skf:
                    if (subset == "test"):
                        X_TRAIN_fold_test = data.iloc[test_index]
                    else:
                        X_TRAIN_fold_test = data.iloc[train_index]
                num_clases_distintas_en_train_reducido = len(X_TRAIN_fold_test.groupby([COLUMN_CLASS]))
                num_elmtos_clase_menos_representada = np.min(X_TRAIN_fold_test.groupby(COLUMN_CLASS).Species.count())
                #print('nFolds',nFolds)
                #print('num_elmtos_clase_menos_representada',num_elmtos_clase_menos_representada)
                
            if data.shape[0]*percentage/100 != len(X_TRAIN_fold_test):
                real_percentage = round(len(X_TRAIN_fold_test)*100/data.shape[0],1)
                if real_percentage > percentage_solicitado + 5:
                    print("Importador: Se solicito un pPercentageStratifiedRandomSamples del",percentage_solicitado,'%, pero se ha adecuado a un',real_percentage,'% para que se cumpla la condicion de que en CrossValidation debe haber al menos 1 muestra de la clase menos representada en cada FOLD.')
                else:
                    print("Importador: Se solicito un pPercentageStratifiedRandomSamples del",percentage_solicitado,'%, pero se ha adecuado a un',real_percentage)
                    
            data = X_TRAIN_fold_test
        else:
            sys.exit("Error: It is not possible to import using pPercentageStratifiedRandomSamples since this is not a Classification problem") 
    else:
        sys.exit("Importer Error: 'subsetType' value is NOT valid") 

    dataRowsAfterSampling = data.shape[0]
    
    if d['COMP_COLUMN_CLASS'] in data and d['COMP_PROBLEM_TYPE'].startswith('Regr') and 'int' in str(data[d['COMP_COLUMN_CLASS']].dtype):
        data[d['COMP_COLUMN_CLASS']]=data[d['COMP_COLUMN_CLASS']].astype(np.float64)
        print("WARNING: {} had dtype=int for a regression problem.  Converted to float64".format(d['COMP_COLUMN_CLASS']))
    
    
    return (dataRowsOriginal, dataRowsAfterSampling, data, duplicated_cols)





    

########################################################
#####                                              #####
#####                      CSV                     #####
#####                                              #####
########################################################       

########################
###      CSV SAVE    ###
########################
    
def csv_load_predictions(dashboard, dashboard_id, is_user_friendly_id=True, new_y_col_name=None, load_oof=True, load_test=True):
    assert is_user_friendly_id, "not implemented"
    
    def find_submission_filename_by_user_friendly_id(filepath, dashboard_id):
        find_pattern=filepath.replace('Level_1', 'Level_?')
        find_pattern+="*({},*.csv".format(dashboard_id)
#        print(find_pattern)
        results=glob.glob(find_pattern)
        assert len(results)>0, "not found"
        assert len(results)==1, "ambiguous"
        return results[0]
    
    if load_oof:
        oof_filename=find_submission_filename_by_user_friendly_id(dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_1_VALIDATION'],dashboard_id)
        oof_preds=pd.read_csv(oof_filename, sep=",", engine='c')
    
    if load_test:
        test_filename=find_submission_filename_by_user_friendly_id(dashboard['DIR_OUTPATH_SUBMISSIONS_LEVEL_1'],dashboard_id)
        test_preds=pd.read_csv(test_filename, sep=",", engine='c')
    
    if new_y_col_name is not None:
        if load_oof:
            oof_preds.rename (columns={dashboard['COMP_COLUMN_CLASS_4_SUBMISSION']:new_y_col_name}, inplace=True)
        if load_test:
            test_preds.rename(columns={dashboard['COMP_COLUMN_CLASS_4_SUBMISSION']:new_y_col_name}, inplace=True)
    
    if load_oof and load_test:
        return oof_preds, test_preds
    elif load_oof:
        return oof_preds
    else:
        return test_preds
        
    

def csv_save_executions_info(executions_info_dict,DIR_DATA):
    executions_info_csv = DIR_DATA +'executions_info.csv'
    executions_simplified_excel = DIR_DATA +'Executions_Simplified.xlsx'

    # Nombre de las columnas del csv
    csv_columns=[]
    contador = 0
    for k in executions_info_dict.keys():
        contador += 1
        if contador == 1: # solo una vez
            sub_dict = executions_info_dict[k]
            for subk in sub_dict:
                csv_columns.append(subk)
    csv_columns = sorted(csv_columns)
                    
    with open(executions_info_csv, 'w') as executions_info_csv:
        writer = csv.writer(executions_info_csv)
        writer.writerow(csv_columns)
        for k in executions_info_dict.keys():
            sub_dict = executions_info_dict[k]
            ordered_row = [value for (key, value) in sorted(sub_dict.items())]
            writer.writerow(ordered_row)

    # Simplified Excel with selected columns
    simplified_df =  pd.DataFrame.from_dict(executions_info_dict, orient='index')
    filter_col = [col for col in list(simplified_df) if (col.startswith('PRP_')) or (col.startswith('submission_')) or (col.startswith('xgb')) or (col.startswith('score_')) or (col.startswith('ID_')) or (col.startswith('ML_ALGO')) or (col == 'LEVEL') or (col == 'version') or (col == 'overfit') or (col == 'ML_GRIDSEARCHCV_OR_ALGORITHM')]
    simplified_df = simplified_df[filter_col]
    simplified_df['ID_EXECUTION'] = simplified_df['ID_EXECUTION'].astype(str)
    simplified_df['ID_FAMILY'] = simplified_df['ID_FAMILY'].astype(str)
    simplified_df = simplified_df.reindex_axis(sorted(simplified_df.columns), axis=1)
    simplified_df.to_excel(executions_simplified_excel, index=False)

    
    
def csv_save_preprocessed_data(dashboard, X_TRAIN__pd, y_TRAIN__pd, idx_TRAIN__pd, idx_test__pd, X_test__pd):
    from my_Prepro_cache import IMPORT_AND_PREPROCESSING_VARIABLES
    
    print("IMPORT_AND_PREPROCESSING_VARIABLES: " + str(IMPORT_AND_PREPROCESSING_VARIABLES))
    cache_key = get_cache_key(dashboard, IMPORT_AND_PREPROCESSING_VARIABLES)
    
    if not os.path.exists(dashboard['DIR_PREPROCESSED_DATASETS']):
        os.makedirs(dashboard['DIR_PREPROCESSED_DATASETS'])
    
    train_output_filename = dashboard['DIR_PREPROCESSED_DATASETS'] + "train_" + cache_key + ".csv"
    test_output_filename  = dashboard['DIR_PREPROCESSED_DATASETS'] + "test_"   + cache_key + ".csv"
    
    TRAIN__pd = X_TRAIN__pd.copy()
    TRAIN__pd[dashboard['COMP_COLUMN_ID']]= idx_TRAIN__pd
    TRAIN__pd[dashboard['COMP_COLUMN_CLASS']] = y_TRAIN__pd
    len(TRAIN__pd)
    len(TRAIN__pd.columns)
    
    test__pd = X_test__pd.copy()
    test__pd[dashboard['COMP_COLUMN_ID']]= idx_test__pd
    
    if os.path.isfile(train_output_filename) and os.path.isfile(train_output_filename):
        print("skipped csv_save_preprocessed_data for cache_key {}".format(cache_key))
    else:
        print("saving preprocesed dataset:")
        print(train_output_filename)
        print(test_output_filename)
    
        TRAIN__pd.to_csv(train_output_filename, header=True, index=False, sep=dashboard['IMP_TRAIN_DELIMITER'], encoding=dashboard['IMP_TRAIN_ENCODING_TYPE'])
        test__pd.to_csv(test_output_filename, header=True, index=False, sep=dashboard['IMP_TEST_DELIMITER'], encoding=dashboard['IMP_TEST_ENCODING_TYPE'])
  

 
#########################
###     CSV DELETE    ###
#########################
 
def CSV_file_names_delete_all(directory, substring='.csv'):
    try:
        csv_files_pre = os.listdir(directory)
    except:
        csv_files_pre = []
    csv_files = [s for s in csv_files_pre if substring in s]
    csv_PathFiles = [directory + s for s in csv_files]
    if len(csv_PathFiles) > 0:
        for filepath in csv_PathFiles:
            try:
                os.remove(filepath)
                print(Fore.BLUE + "CSV: Removed " + filepath + Style.RESET_ALL) 
            except:
                print(Fore.RED + "CSV: We were not able to remove the CSV: " + filepath + Style.RESET_ALL)



  
   
    
    

########################################################
#####                                              #####
#####                PANDAS_DATAFRAME              #####
#####                                              #####
######################################################## 

########################
###    PD_DF Utils   ###
########################

def find_differences_df(df1,df2):
    ne_stacked = (df1 != df2).stack()
    changed = ne_stacked[ne_stacked]
    changed.index.names = ['row', 'column']
        
    difference_locations = np.where(df1 != df2)
    
    changed_from = df1.values[difference_locations]
    changed_to = df2.values[difference_locations]
    
    changed_from_dtypes = []
    for j in range(len(changed_from)):
        changed_from_dtypes.append(type(changed_from[j]))

    changed_to_dtypes = []
    for j in range(len(changed_to)):
        changed_to_dtypes.append(type(changed_to[j]))
    
    differences = pd.DataFrame({'from': changed_from,'to': changed_to, 'from_dtype': changed_from_dtypes, 'to_dtype': changed_to_dtypes}, index=changed.index)
    differences = differences[~pd.isnull(changed_from) | ~pd.isnull(changed_to)]
    print (differences)
    

def find_similitudes_df(df1,df2):
    ne_stacked = (df1 == df2).stack()
    changed = ne_stacked[ne_stacked]
    changed.index.names = ['row', 'column']
        
    similitude_locations = np.where(df1 == df2)
    
    changed_from = df1.values[similitude_locations]
    changed_to = df2.values[similitude_locations]
    
    changed_from_dtypes = []
    for j in range(len(changed_from)):
        changed_from_dtypes.append(type(changed_from[j]))

    changed_to_dtypes = []
    for j in range(len(changed_to)):
        changed_to_dtypes.append(type(changed_to[j]))
    
    similitudes = pd.DataFrame({'from': changed_from,'to': changed_to, 'from_dtype': changed_from_dtypes, 'to_dtype': changed_to_dtypes}, index=changed.index)
    similitudes = similitudes[~pd.isnull(changed_from) | ~pd.isnull(changed_to)]
    print (similitudes)
    
    



########################################################
#####                                              #####
#####                      HDF5                    #####
#####                                              #####
######################################################## 


########################
###     HDF5 Read    ###
########################

def hdf5_read_element(dataset_name,DIR_DATA,cols_names):
    
    executions_data_file = DIR_DATA+'executions_data.hdf5'
    file_hdf5 = h5py.File(executions_data_file, 'r')
    datasets = [x for x in file_hdf5.keys()]
    
    if dataset_name in datasets:
        
        element_dtype = file_hdf5[dataset_name].dtype
        if str(element_dtype).startswith('|S'):
            element = np.array(file_hdf5[dataset_name],dtype=element_dtype)
        else:
            element = np.array(file_hdf5[dataset_name][:,0],dtype=element_dtype)
        num_rows = len(element)
        
        # Caso strings (clasificacion)            
        if 'int' not in str(element_dtype) and 'float' not in str(element_dtype): 
            element_str = []           
            for f in range(0,num_rows):
                element_str.append(element[f].decode(encoding='UTF-8'))
            element_dtype = 'str'
            element = np.array(element_str, dtype=element_dtype)
        
        
        element_reshaped = element.reshape(num_rows,1)
        
        return pd.DataFrame(data=element_reshaped,columns=cols_names,dtype=element_dtype)
    else:
        print("Error: Requested element not found in executions_data.hdf5")
        return None

        
        
        
        
        
        
        
def hdf5_find_id_executions(d, ids_executions_requested_list,dataset,DIR_DATA, CV_OR_DATASET):

    executions_data_file = DIR_DATA+'executions_data.hdf5'
    file_hdf5 = h5py.File(executions_data_file, 'r')
    datasets = [x for x in file_hdf5.keys()]


    # Calculamos ids_executions_saved
    ####################################
    
    if CV_OR_DATASET == 'CV' and (d['LEVEL'][0:2] == '2_' or d['LEVEL'][0:2] == '0_'):
        datasets_train = []
        datasets_test = []
        
        for dts in datasets:
            #if 'Train' in dataset:
            try:
                if ('Train' in dts) and ('_CV_' in dts):
                    datasets_train.append(dts)
            except:
                print('NO HAY')
                    

        datasets = datasets_train
            
        ids_executions_saved = []
        
        
        for dataset in datasets:
            try:
                ids_executions_saved.append(list(file_hdf5[dataset].attrs['ID_EXECUTION']))
            except:
                pass
        
            
        ids_executions_saved = list(set(x for l in ids_executions_saved for x in l))
        
   
        
    else:  #  d['LEVEL'][0:2] != '2_'  or   CV_OR_DATASET != 'CV'
        if dataset in datasets:
            ids_executions_saved = list(file_hdf5[dataset].attrs['ID_EXECUTION'])
        else:
            print(Fore.RED + "\nError: Requested dataset doesn't exist:\n\t" + str(dataset) + "\n" +  Style.RESET_ALL)
            if ('_idx_val' in str(dataset)) or ('_y_val' in str(dataset)):
                print(Fore.RED + "\tMake sure to remove '_idx_val', '_y_val' or 'y_val_preds' from the last part of your dataset name\n" +  Style.RESET_ALL)
            ids_executions_saved = []            

            
            
    # Calculamos ids_executions_found   y   ids_executions_not_found
    ###################################################################
    
    ids_executions_found = sorted(list(set(ids_executions_requested_list) & set(ids_executions_saved)))
    ids_executions_not_found = sorted(list(set(ids_executions_requested_list) & (set(ids_executions_requested_list) ^ set(ids_executions_saved))))
    
    if len(ids_executions_not_found)>0:
        print("\nThere are",len(ids_executions_not_found),"id_execution NOT found in executions_data.hdf5:",ids_executions_not_found)       
    
    if len(ids_executions_found)>0:
        if d['LEVEL'][0:2] == '2_' and CV_OR_DATASET == 'CV':
            print("\nThere are",len(ids_executions_found),"id_execution found in executions_data.hdf5:\n",ids_executions_found)        
        else:
            print("\nThere are",len(ids_executions_found),"id_execution found in the dataset",dataset,"from executions_data.hdf5:\n",ids_executions_found)        

        
  
    file_hdf5.close()
    return ids_executions_found, ids_executions_not_found
    

    
    
    
def hdf5_read_execution_data(d, ids_executions_requested_list, dataset, DIR_DATA):
# Use always the function hdf5_find_id_executions before using this function


   
    executions_data_file = DIR_DATA+'executions_data.hdf5'
    file_hdf5 = h5py.File(executions_data_file, 'r')
    
    datasets = [x for x in file_hdf5.keys()]


    # Calculamos ids_executions_saved
    ####################################



    
    if d['LEVEL'][0:2] == '2_' and d['L2_CV_OR_DATASET'] == 'CV':
        

        datasets_train = []
        datasets_test = []
        
        for dts in datasets:
            if 'Train' in dataset:
                try:
                    if 'Train' in dts:
                        datasets_train.append(dts)
                except:
                    print('NO HAY')
                    
            else: # dataset es *test*
                try:
                    if 'test' in dts:
                        datasets_test.append(dts)
                except:
                    print('NO HAY')
            

        if 'Train' in dataset:
            datasets = datasets_train
        else:
            datasets = datasets_test
            
        ids_executions_saved = []
        


        for dt in datasets:

            try:

                executions_in_dataset = list(file_hdf5[dt].attrs['ID_EXECUTION'])
                indexes = [i for i, item in enumerate(executions_in_dataset) if item in set(ids_executions_requested_list)]
                           
                executions_in_dataset_2_use = [e for e in executions_in_dataset if e in set(ids_executions_requested_list)]
                                               

                
                if len(executions_in_dataset_2_use) > 0:
                    
                    if 'result_data' not in locals():
                        result_data = pd.DataFrame(file_hdf5[dt][:,indexes])
                        

                    else:

                        for el_index in indexes:
                            #print('executions_in_dataset_2_use',executions_in_dataset_2_use)
                            
                            try:
                                result_data.loc[:,len(result_data.columns) + 1] = pd.Series(file_hdf5[dt][:,el_index], index=result_data.index)
                            except Exception as errorrr:
                                print(errorrr, el_index)
                                            
                
                    ids_executions_saved.append(executions_in_dataset_2_use)
                    
            except:
                pass
        


        ids_executions_saved = list(set(x for l in ids_executions_saved for x in l))

        indexes = [i for i, item in enumerate(ids_executions_saved) if item in set(ids_executions_requested_list)]
       
                   
        #result_data = result_data.transpose()
        result_data = result_data.as_matrix()


   
        
    else:  #  d['LEVEL'][0:2] != '2_' or d['L2_CV_OR_DATASET'] != 'CV'
        if dataset in datasets:
            ids_executions_saved = list(file_hdf5[dataset].attrs['ID_EXECUTION'])
            indexes = [i for i, item in enumerate(ids_executions_saved) if item in set(ids_executions_requested_list)]
            if len(indexes)>0:
                result_data = file_hdf5[dataset][:,indexes]
            
        else:
            print(Fore.RED + "\nError: Requested dataset doesn't exist:\n\t" + str(dataset) + "\n" +  Style.RESET_ALL)
            if ('_idx_val' in str(dataset)) or ('_y_val' in str(dataset)):
                print(Fore.RED + "\tMake sure to remove '_idx_val', '_y_val' or 'y_val_preds' from the last part of your dataset name\n" +  Style.RESET_ALL)
            ids_executions_saved = []        
            indexes = []            
            result_data = []    


            
            
            
    # ---------------- cambios hasta aqui ----
            

            
           
    if len(indexes)>0:            
        element_dtype = result_data.dtype
        # Caso strings (clasificacion)
        if 'int' not in str(element_dtype) and 'float' not in str(element_dtype):
            result_data_str = []
            for f in range(np.shape(result_data)[0]):
                row_str = []
                for c in range(np.shape(result_data)[1]):
                    row_str.append(result_data[f][c].decode(encoding='UTF-8'))
                result_data_str.append(row_str)
            element_dtype = 'str'
            result_data = np.array(result_data_str, dtype=element_dtype)
    else:
        result_data = []



    file_hdf5.close()
    return result_data

    
    
    
    
    

def hdf5_read_competition_data(dataset_name,DIR_DATA):
    competition_data_file = DIR_DATA+'competition_data.hdf5'
    file_hdf5 = h5py.File(competition_data_file, 'r')
    datasets = [x for x in file_hdf5.keys()]
    
    if dataset_name in datasets:
        if dataset_name == "Features_Correlation_Matrix":
            dataset = file_hdf5[dataset_name]
            data = np.array(dataset[:])
            columns_names = list(dataset.attrs['features_names'])
            # Decodificamos bytes como strings
            columns_names_decoded = [x.decode(encoding='UTF-8') for x in columns_names]
            columns = np.array(columns_names_decoded)
        return pd.DataFrame(data=data,columns=columns,index=columns)
    else:
        print("Error: Requested element",dataset_name,"not found in competition_data.hdf5")
        print("Error: You have to save",dataset_name,"before reading it")
        return None
        

########################
###     HDF5 Save    ###
########################

def hdf5_save_element_in_this_dataset(file_hdf5,element,dataset_name,id_execution,element_name,idx_dtype,y_dtype):
    
    datasets = [x for x in file_hdf5.keys()]
    len_element = element.shape[0]

    # En caso de que y es tipo string (clasificacion) codificamos string como bytes
    if y_dtype=='object' and (("y_test_preds" in dataset_name) or ("y_val_preds" in dataset_name) or ("y_val" in dataset_name)): 
        element_encode = [x.encode(encoding='UTF-8') for x in list(element)]
        element = np.array(element_encode)
        #dt = h5py.special_dtype(vlen=bytes)
                
    if dataset_name in datasets:
        dataset = file_hdf5[dataset_name]
        
        if ("idx_test" in dataset_name) or ("idx_val" in dataset_name) or ("y_val" in dataset_name and not "y_val_preds" in dataset_name and y_dtype=='object'):
            if idx_dtype==np.dtype('S') or np.array_equiv(np.array(dataset),element.reshape(len_element,1)):
                #print (element_name,"values match the ones stored in executions_data.hdf5. ", end="")
                pass
                #dataset[:,0] = element
            else:
                print(element_name,"values DO NOT MATCH the ones stored in executions_data.hdf5")
                print()
                print("Elemento almacenado en HDF5:",dataset_name)
                print("\tShape:",np.array(file_hdf5[dataset_name]).shape)
                print("\tMuestra de los 10 primeros valores:")
                print(np.array(file_hdf5[dataset_name])[0:10])
                print()
                print("Elemento que queremos almacenar en HDF5:",element_name)
                print("\tShape:",element.reshape(len_element,1).shape)
                print("\tMuestra de los 10 primeros valores:")
                print(element.reshape(len_element,1)[0:10])
                file_hdf5.close()
                raise RuntimeError(element_name+" values DO NOT MATCH the ones stored in executions_data.hdf5")
        
        elif "y_val" in dataset_name and not "y_val_preds" in dataset_name and y_dtype!='object':
            try:
                np.testing.assert_allclose(np.array(dataset), element.reshape(len_element,1), rtol=1e-6, atol=0)
                #print (element_name,"values match the ones stored in executions_data.hdf5. ", end="")
                #dataset[:,0] = element
            except:
                print(element_name,"values DO NOT MATCH the ones stored in executions_data.hdf5")
                print()
                print("Element stored in HDF5:",dataset_name)
                print("\tShape:",np.array(file_hdf5[dataset_name]).shape)
                print("\t10 first values:")
                print(np.array(file_hdf5[dataset_name])[0:10])
                print()
                print("Element we would like to store in HDF5:",element_name)
                print("\tShape:",element.reshape(len_element,1).shape)
                print("\t10 first values:")
                print(element.reshape(len_element,1)[0:10])
                counter = 0
                for j in range(len_element):
                    if counter < 10 and np.array(file_hdf5[dataset_name])[j] != element.reshape(len_element,1)[j]:
                        counter += 1
                        print('Elemento',j,'-esimo diferente: hdf5 vs element')
                        print('%.51f' % file_hdf5[dataset_name][j])
                        print('%.51f' % element.reshape(len_element,1)[j])
                file_hdf5.close()
                raise RuntimeError(element_name+" values DO NOT MATCH the ones stored in executions_data.hdf5")
        elif ("y_test_preds" in dataset_name) or ("y_val_preds" in dataset_name):
            dataset.resize((len_element,dataset.shape[1]+1))
            dataset[:,dataset.shape[1]-1] = element
            
            #Save id_execution in attribute ids_executions
            ids_executions = np.array(dataset.attrs['ID_EXECUTION'])
            dataset.attrs.create('ID_EXECUTION',np.append(ids_executions,id_execution))
            #print (element_name,"added to executions_data.hdf5. ", end="")
        
        else:
            file_hdf5.close()
            raise RuntimeError("Error: Non codified element in hdf5_save_element_in_this_dataset function.")
    else:
        if (element_name == "idx_test") or (element_name == "idx_val"):
            maxshape=(len_element, None)
            try:
                if idx_dtype==np.dtype('S'):
                    element_encode = []
                    max_l=0
                    for x in list(element):
                        x=x.encode(encoding='UTF-8')
                        max_l=max(max_l,len(x))
                        element_encode.append(x)
                    data = element_encode
                    idx_dtype=np.dtype('S'+str(max_l))
                    maxshape=(len_element, )
                else:
                    data = element.reshape(len_element,1)
                print(dataset_name)
                print(maxshape)
                print(idx_dtype)
                print(len(data))
                dataset = file_hdf5.create_dataset(dataset_name, data = data , dtype=idx_dtype, maxshape=maxshape)
            except:
                print("We had some issues with file_hdf5.create_dataset. It may already have existed.")
        elif (element_name == "y_val"):
            if y_dtype=='object': # en caso de que y es tipo string
                try:
                    dataset = file_hdf5.create_dataset(dataset_name, data = element.reshape(len_element,1), maxshape=(len_element, None))
                except:
                    print("We had some issues with file_hdf5.create_dataset. It may already have existed.")
            else:
                try:
                    dataset = file_hdf5.create_dataset(dataset_name, data = element.reshape(len_element,1), dtype=y_dtype, maxshape=(len_element, None))
                except:
                    print("We had some issues with file_hdf5.create_dataset. It may already have existed.")
        else:
            if y_dtype=='object': # en caso de que y es tipo string
                try:
                    dataset = file_hdf5.create_dataset(dataset_name, data = element.reshape(len_element,1), maxshape=(len_element, None))        
                except:
                    print("We had some issues with file_hdf5.create_dataset. It may already have existed.")
            else:
                try:
                    dataset = file_hdf5.create_dataset(dataset_name, data = element.reshape(len_element,1), dtype='float64', maxshape=(len_element, None))
                except:
                    print("We had some issues with file_hdf5.create_dataset. It may already have existed.")
        #Save id_execution in attribute ids_executions       
        if ("y_test_preds" in dataset_name) or ("y_val_preds" in dataset_name): 
            dataset.attrs.create('ID_EXECUTION',np.array(id_execution))
        #print (element_name,"added to executions_data.hdf5. ", end="")


        
        

        

def hdf5_save_execution_data(id_execution,d,val_exists,N_test,idx_test,y_test_preds,idx_dtype,y_dtype,fold, execution_CV_stopped, split_cv_folds_orig, N_val=None,idx_val=None,y_val_preds=None,y_val=None):
    
    save_hdf5_y_test_preds = False
    save_hdf5_y_val_preds = False
    save_hdf5_y_val = False
    
    # Comprobamos coherencia

    if d['LEVEL']=='1_ML' or d['LEVEL']=='1_EXTERNAL_SCRIPTS' or d['LEVEL']=='1_EXTERNAL_PREDICTIONS' or d['LEVEL']=='2_ML' or d['LEVEL']=='2_CARUANA' or d['LEVEL']=='2_ENSEMBLE':
        if y_test_preds.shape[0]==N_test:
            save_hdf5_y_test_preds = True
            if val_exists:
                
                if y_val_preds.shape[0]==N_val:
                    save_hdf5_y_val_preds = True
                else:
                    print("y_val_preds length is", y_val_preds.shape[0], ", while it was expected:", N_val)
                
                if y_val.shape[0]==N_val:
                    save_hdf5_y_val = True
                else:
                    print("y_val length is", y_val.shape[0], ", while it was expected:", N_val)

        else:
            print("y_test_preds length is", y_test_preds.shape[0], ", while it was expected:", N_test)
    
    elif d['LEVEL']=='3_CARUANA' or d['LEVEL']=='3_BASIC_ENSEMBLE':
        if y_test_preds.shape[0]==N_test:
            save_hdf5_y_test_preds = True
        else:
            print("y_test_preds length is", y_test_preds.shape[0], ", while it was expected:", N_test)      
    else:
        sys.exit("Error. DO_MODEL_LEVEL takes a non codified value")


    dataset_idx_test_name = ''
    dataset_y_test_preds_name = ''
    dataset_idx_val_name = ''
    dataset_y_val_preds_name = ''
    dataset_y_val_name = ''

    if save_hdf5_y_test_preds == False:
        print("Execution was NOT saved in executions_data.hdf5")
    elif save_hdf5_y_test_preds == True and save_hdf5_y_val_preds != save_hdf5_y_val:
        print("Execution was NOT saved in executions_data.hdf5")
    else:
        executions_data_file = d['DIR_DATA']+'executions_data.hdf5'
        file_hdf5 = h5py.File(executions_data_file, 'a')
    
        if save_hdf5_y_test_preds == True:
            dataset_name = hdf5_dataset_name(d,"y_test_preds",fold,"HDF5", execution_CV_stopped, split_cv_folds_orig)
            hdf5_save_element_in_this_dataset(file_hdf5,y_test_preds,dataset_name,id_execution,"y_test_preds",idx_dtype,y_dtype)            
            dataset_y_test_preds_name = dataset_name 
            
            dataset_name = hdf5_dataset_name(d,"idx_test",fold,"HDF5", execution_CV_stopped, split_cv_folds_orig)
            hdf5_save_element_in_this_dataset(file_hdf5,idx_test,dataset_name,id_execution,"idx_test",idx_dtype,y_dtype)            
            dataset_idx_test_name = dataset_name 
        
        if save_hdf5_y_val_preds == True:
            dataset_name = hdf5_dataset_name(d,"y_val_preds",fold,"HDF5", execution_CV_stopped, split_cv_folds_orig)
            hdf5_save_element_in_this_dataset(file_hdf5,y_val_preds,dataset_name,id_execution,"y_val_preds",idx_dtype,y_dtype)
            dataset_y_val_preds_name = dataset_name     
         
            dataset_name = hdf5_dataset_name(d,"idx_val",fold,"HDF5", execution_CV_stopped, split_cv_folds_orig)
            hdf5_save_element_in_this_dataset(file_hdf5,idx_val,dataset_name,id_execution,"idx_val",idx_dtype,y_dtype)
            dataset_idx_val_name = dataset_name 

            dataset_name = hdf5_dataset_name(d,"y_val",fold,"HDF5", execution_CV_stopped, split_cv_folds_orig)
            hdf5_save_element_in_this_dataset(file_hdf5,y_val,dataset_name,id_execution,"y_val",idx_dtype,y_dtype)
            dataset_y_val_name = dataset_name     
          
        file_hdf5.close()  

    return [dataset_idx_test_name, dataset_y_test_preds_name, dataset_idx_val_name,dataset_y_val_preds_name, dataset_y_val_name]
   

def hdf5_save_Features_Correlation_Matrix(d,element_name,element,element_cols_names):

    competition_data_file = d['DIR_DATA']+'competition_data.hdf5'
    file_hdf5 = h5py.File(competition_data_file, 'a')
    datasets = [x for x in file_hdf5.keys()] 

    dataset_name = element_name
           
    # Codificamos strings como bytes en HDF5
    element_cols_names_encoded = [x.encode(encoding='UTF-8') for x in element_cols_names]
    columns_names = np.array(element_cols_names_encoded)
    
    # Una vez tenemos dataset_name y columns_names:
    if dataset_name in datasets:
        dataset = file_hdf5[dataset_name]
        if np.array_equiv(np.array(dataset),element):
            print(dataset_name,"values match the ones saved in competition_data.hdf5")
        else:
            print(dataset_name,"values DON'T MATCH the ones saved in competition_data.hdf5")
            print(dataset_name,"values have been updated with these new ones")
        dataset[:] = element
        dataset.attrs.create('features_names',columns_names)
    else:
        dataset = file_hdf5.create_dataset(dataset_name, data = element, dtype='float64', maxshape=(element.shape[0], element.shape[1]))
        dataset.attrs.create('features_names',columns_names)
        print(dataset_name,"values have been saved in competition_data.hdf5")
    print()
    
    file_hdf5.close()
    return None        


def hdf5_save_Features_Importance(id_execution,d,element_name,importance,features_names):

    competition_data_file = d['DIR_DATA']+'competition_data.hdf5'
    file_hdf5 = h5py.File(competition_data_file, 'a')
    datasets = [x for x in file_hdf5.keys()]

    #algoritmo = [k for k in d['ML_ALGO_PARAMETROS'].keys()][0]
    dataset_importance = "features_importances"
    dataset_features_names = "features_names"
    #importance = importance.reshape(len(importance),1)
    print("importance.shape",importance.shape)
    
    # Codificamos strings como bytes en HDF5
    features_names_encoded = [x.encode(encoding='UTF-8') for x in features_names]
    features_names_encoded = np.array(features_names_encoded)
    print("features_names_encoded.shape",features_names_encoded.shape)    
    
    # Si ya existen los datasets
    if dataset_importance in datasets and dataset_features_names in datasets:
        
        # Insertamos importances en dataset "features_importances" y aÃ±adimos en atributos el id_execution
        dataset = file_hdf5[dataset_importance]
        dataset.resize((max(dataset.shape[0],len(importance)),dataset.shape[1]+1))
        dataset[:,dataset.shape[1]-1] = importance
        #Save id_execution in attribute ids_executions
        ids_executions = np.array(dataset.attrs['ID_EXECUTION'])
        dataset.attrs.create('ID_EXECUTION',np.append(ids_executions,id_execution))


        # Insertamos features_names en dataset "features_names" y aÃ±adimos en atributos el id_execution
        dataset = file_hdf5[dataset_features_names]
        dataset.resize(max(dataset.shape[0],len(features_names)),dataset.shape[1]+1)
        dataset[:,dataset.shape[1]-1] = features_names_encoded
        #Save id_execution in attribute ids_executions
        ids_executions = np.array(dataset.attrs['ID_EXECUTION'])
        dataset.attrs.create('ID_EXECUTION',np.append(ids_executions,id_execution))

    # Si no existen los datasets
    else:
        dataset = file_hdf5.create_dataset(dataset_importance, data = importance.reshape(len(importance),1), dtype='float64', maxshape=(2000, None))
        dataset.attrs.create('ID_EXECUTION',np.array(id_execution))
        
        print("len(features_names_encoded)",len(features_names_encoded))
        dataset = file_hdf5.create_dataset(dataset_features_names, data = features_names_encoded.reshape(len(features_names_encoded),1), maxshape=(2000, None))
        dataset.attrs.create('ID_EXECUTION',np.array(id_execution))
        
        
        print(dataset_importance,"and", dataset_features_names, "values have been saved in competition_data.hdf5")
    print()
    
    file_hdf5.close()
    return None    


########################
###     HDF5 Utils   ###
########################

def hdf5_dataset_name(d,element_name, fold, caso, execution_CV_stopped = 0, split_cv_folds_orig = 0, level=None):
    
    num_folds = d['SPLIT_CV_KFOLDS']
    

        
    #######################
    # Level
    #######################
    level = level if level is not None else 'level'+d['LEVEL'].split('_')[0]
        

    #######################
    # Train and test sizes
    #######################
    
    if d['IMP_TRAIN_SUBSET_TYPE'] == 'allSamples':
        train_size = 'Train100%'
    elif d['IMP_TRAIN_SUBSET_TYPE'] == 'booleanExpressions':
        train_size = 'Train_booleanExpressions_' + str(','.join(list(map(str, d['IMP_TRAIN_SUBSET_EXPRESSION_LIST']))).__hash__())
    elif d['IMP_TRAIN_SUBSET_TYPE'] == 'interPercentileRange':
        train_size = 'Train_interPercentileRange_' + str(d['IMP_TRAIN_INTER_PERCENTILE_RANGE'])
    elif d['IMP_TRAIN_SUBSET_TYPE'] == 'nConsecutiveSamples':
        train_size = 'Train' + str(d['IMP_TRAIN_FIRST_ROW'])+"to"+str(int(d['IMP_TRAIN_FIRST_ROW'])+int(d['IMP_TRAIN_NUM_SAMPLES'])-1) + 'nCS'
    elif d['IMP_TRAIN_SUBSET_TYPE'] == 'nRandomSamples':
        train_size = 'Train' + str(d['IMP_TRAIN_NUM_SAMPLES']) + 'nRS'
    elif d['IMP_TRAIN_SUBSET_TYPE'] == 'pPercentageRandomSamples':
        train_size = 'Train' + str(d['IMP_TRAIN_PERCENTAGE']) + '%pRS'
    elif d['IMP_TRAIN_SUBSET_TYPE'] == 'pPercentageStratifiedRandomSamples':
        train_size = 'Train' + str(d['IMP_TRAIN_PERCENTAGE']) + '%pSRS'
    else:
        sys.exit("Import error: 'subsetType' value is not valid")
    
    if d['IMP_TEST_SUBSET_TYPE'] == 'allSamples':
        test_size = 'test100%'
    elif d['IMP_TEST_SUBSET_TYPE'] == 'nConsecutiveSamples':
        test_size = 'test' + str(d['IMP_TEST_FIRST_ROW'])+"to"+str(int(d['IMP_TEST_FIRST_ROW'])+int(d['IMP_TEST_NUM_SAMPLES'])-1) + 'nCS'
    elif d['IMP_TEST_SUBSET_TYPE'] == 'nRandomSamples':
        test_size = 'test' + str(d['IMP_TEST_NUM_SAMPLES']) + 'nRS'
    elif d['IMP_TEST_SUBSET_TYPE'] == 'pPercentageRandomSamples':
        test_size = 'test' + str(d['IMP_TEST_PERCENTAGE']) + '%pRS'
    elif d['IMP_TEST_SUBSET_TYPE'] == 'pPercentageStratifiedRandomSamples':
        test_size = 'test' + str(d['IMP_TEST_PERCENTAGE']) + '%pSRS'
    else:
        sys.exit("Import error: 'subsetType' value is not valid")  



    #######################
    # Split
    #######################
    
    
    # execution_CV_stopped 
    ######################
    if execution_CV_stopped == 0:
        execution_CV_stopped_string = ""
    else:
        execution_CV_stopped_string = "|EarlyStopF" + str(fold) + "of" + str(split_cv_folds_orig)
        
        
    
    if caso=="ExternalScriptsSubseting":
        
        if num_folds == 0 or (num_folds == 1 and d['SPLIT_VALIDATION_SET_PERC'] == 0.0):
            split = "Padre" # We are not using it for now
        elif num_folds == 1:
            # Train-Val (Validation percentage)
            train_percent = str(int(100-100*float(d['SPLIT_VALIDATION_SET_PERC'])))+'%'
            val_percent = str(int(100*float(d['SPLIT_VALIDATION_SET_PERC'])))+'%' + execution_CV_stopped_string
            split = "TrainVal" + train_percent + val_percent
        else:
            # Crossvalidation
            split = "CV-Fold" + str(fold+1) + "of" + str(num_folds)

            
            
    elif caso=="HDF5":
        
        # previous_split
        #################
        if d['LEVEL'][0:2] == '2_' and d['L2_CV_OR_DATASET'] == 'DATASET':
            el_dataset = d['L2_LEVEL1_DATASETS_TRAIN']
            
            if "TrainVal" in el_dataset:  
                
                indice_trainval = el_dataset.index("TrainVal", 0,222)
                train_val_size = el_dataset[indice_trainval+8:indice_trainval+14].split('%')
                
                train_percent = str(int(train_val_size[0]))+'%'
                val_percent = str(int(train_val_size[1]))+'%'
                
                #previous_split = "TrainVal" + train_percent + val_percent
                
                previous_split = el_dataset.split('_')[3]
                
            else:
                previous_split = el_dataset.split('_')[-1]
                
        else:
            previous_split = ''
        
            
        # train%  val%  for split
        ##########################
        
        # Timeseries
        if d['SPLIT_CV_TYPE'] == 'TimeSeries':
            # 'TS_CV_ACADEMIC', 'TS_CV_TRAIN-ORIGIN_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
            if 'VAL-FIX' in d['SPLIT_TS_TYPE']:
                if 'NO-OVERLAP' in d['SPLIT_TS_TYPE']:
                    ts_train_size = "Train-" + str(d['SPLIT_TS_train_FIX_NUM']) + '-'
                    ts_val_size   = "Val-" + str(d['SPLIT_TS_VALIDATION_FIX_NUM']) + "-NoOverlap"
                else:
                    ts_train_size = "Train-" + str(d['SPLIT_TS_train_FIX_NUM'])
                    ts_val_size   = "Val-" + str(d['SPLIT_TS_VALIDATION_FIX_NUM'])
            else:  
                ts_val_size = "Acad"
            
            
            if 'NO-OVERLAP' in d['SPLIT_TS_TYPE']:    
                VAL_TS_name = "ValTS-" + ts_train_size + ts_val_size  + execution_CV_stopped_string
            else:
                VAL_TS_name = "ValTS-" + ts_val_size  + execution_CV_stopped_string
                
            if 'NO-OVERLAP' in d['SPLIT_TS_TYPE']:
                CV_TS_name = "CVTS" + str(num_folds)  +  "-" + ts_train_size + ts_val_size  
            else:
                CV_TS_name = "CVTS" + str(num_folds)  +  "-" + ts_val_size  
            
        # NOT Timeseries
        else:  
            if num_folds == 1:
                # Train-Val (Validation percentaje)
                train_percent = str(int(100-100*float(d['SPLIT_VALIDATION_SET_PERC'])))+'%'
                val_percent = str(int(100*float(d['SPLIT_VALIDATION_SET_PERC'])))+'%' + execution_CV_stopped_string
            else:
                # Train-Val (Validation percentaje)
                train_percent = str(100)+'%'
                val_percent = str(0)+'%'
    
                
                
            
        # split 
        ########
        
        # 2_ENSEMBLE
        if d['LEVEL']=='2_ENSEMBLE':
        
            if d['L2_CV_OR_DATASET'] == 'CV':
                split = "CV" 
            
            elif d['L2_CV_OR_DATASET'] == 'DATASET':
                split = previous_split    

            
        # 2_ML     
        elif d['LEVEL']=='2_ML':
            if d['L2_CV_OR_DATASET'] == 'CV':
                if num_folds == 1:
                    split =  "TrainVal" + train_percent + val_percent
                else:
                    split = "CV"
                    
            elif d['L2_CV_OR_DATASET'] == 'DATASET':
                if num_folds == 1:
                    
                    if d['SPLIT_CV_TYPE'] == 'TimeSeries':
                        split = previous_split +'>' + VAL_TS_name
                    else:
                        split = previous_split +'>' + train_percent + val_percent
                else:
                    if d['SPLIT_CV_TYPE'] == 'TimeSeries':
                        split = previous_split +'>' + CV_TS_name
                    else:
                        split = previous_split
                    
        
        # num_folds = 0
        elif num_folds == 0 or (num_folds == 1 and d['SPLIT_VALIDATION_SET_PERC'] == 0.0):
            split = "Padre" # Esta separación no la vamos a usar por ahora
        
            
        # num_folds = 1
        elif num_folds == 1:
            # Timeseries Validation set:
            if d['SPLIT_CV_TYPE'] == 'TimeSeries':
                split = VAL_TS_name
            # 'normal' Validation set:
            else:
                split = "TrainVal" + train_percent + val_percent
            
        # Crossvalidation
        else:
            # Timeseries:
            if d['SPLIT_CV_TYPE'] == 'TimeSeries':
                split = CV_TS_name
            # 'normal' CV:
            else:
                split = "CV"
            
    
    #######################                
    # Winsoring remove y
    ####################### 
    PRP_Y_WINSORING_REMOVE = d.get('PRP_Y_WINSORING_REMOVE', (0,0))
    wr_min = str(PRP_Y_WINSORING_REMOVE[0])
    wr_max = str(PRP_Y_WINSORING_REMOVE[1])
    wr = "(" + wr_min + "," + wr_max + ")"
    

    
    #######################    
    # Dataset_Name
    #######################

    if element_name == 'y_val_preds' or element_name == 'y_val' or element_name == 'idx_val' or element_name == 'idx_train':
        #z="{}_{}_WR{}_{}_{}".format(level,train_size, wr , split,element_name)
        #print(z)
        dataset_name = level + "_" + train_size + "_WR" + wr + "_" + split + "_" + element_name
    elif element_name == 'y_test_preds' or element_name == 'idx_test':
        #dataset_name = level + "_" + test_size + "_" + split + "_" + element_name
        dataset_name = level + "_" + test_size + "_" + element_name 
    else:
        sys.exit("Error. Non codified element on 'hdf5_dataset_name' function.")
    
    return dataset_name



########################################################
#####                                              #####
#####                      PKL                     #####
#####                                              #####
########################################################


###     PKL READ     
########################
    

def import_kfold_idx(dashboard):
    
    filename=dashboard['SPLIT_IMPORT_INDEXES']
    
    if not isinstance(filename, str):
        raise Exception('SPLIT_IMPORT_INDEXES must be a string. SPLIT_IMPORT_INDEXES is {}'.format(dashboard['SPLIT_IMPORT_INDEXES']))
    
    basename, extension=os.path.splitext(filename)
    if extension=="":
        filename+=".pkl"
        
    if not '\\' in filename and not '/' in filename:
        filename = dashboard['DIR_BASE']+'/kfold/' + filename
    
    if extension==".pkl":
        with open(filename, "rb") as pfile:
            kfold_idx = pickle.load(pfile)
    else:
        kfold_idx = pd.read_csv(filename, engine='c')
    return kfold_idx
    
    

def pickle_load(filename):
    return pickle_load_dashboard(filename)
  
def pickle_load_dashboard(filename):
    ### LOAD PICKLE
    with open(filename, "rb") as pfile:
        dashboard = pickle.load(pfile)
    return dashboard


def pickle_load_one_dashboard(id_execution,DIR_BASE):
    filename = DIR_BASE+'/pickles-dashboards/' + str(id_execution) + '.pkl'
    with open(filename, "rb") as pfile:
        dashboard = pickle.load(pfile)
    return dashboard


def pickle_load_one_execution_results_dict(id_execution,DIR_BASE):
    filename = DIR_BASE+'/pickles-dashboards/' + str(id_execution) + '_results.pkl'
    with open(filename, "rb") as pfile:
        this_execution_results_dict = pickle.load(pfile)
    return this_execution_results_dict    


def pickle_load_all_dashboards_and_execution_results_to_dict(DIR_BASE,homogenize):
    inPath = os.path.normpath(DIR_BASE+'/pickles-dashboards')
    dash_filesPaths=sorted(glob.glob(inPath+os.path.normpath("/")+'[0-9]*[0-9].pkl'))
    results_filesPaths=sorted(glob.glob(inPath+os.path.normpath("/")+'[0-9]*[0-9]_results.pkl'))
    executions_info_dict = {}
    for dash_filePath in dash_filesPaths:
        id_execution = int(re.search(r'\d+.pkl',dash_filePath).group()[0:13])
        results_file_searched = inPath + os.path.normpath("/") + str(id_execution) + '_results.pkl'
        if results_file_searched in results_filesPaths:
            this_dashboard = pickle_load_one_dashboard(id_execution,DIR_BASE)
            this_execution_results = pickle_load_one_execution_results_dict(id_execution,DIR_BASE)
            dict_merged = this_dashboard.copy()
            dict_merged.update(this_execution_results)
            executions_info_dict[id_execution] = dict_merged
    if homogenize:
        executions_info_dict = homogenize_dictionaries(executions_info_dict)
    return executions_info_dict


def pickle_load_all_dashboards_to_dict(DIR_BASE):
    inPath = DIR_BASE+'/pickles-dashboards/'
    dash_filesPaths=sorted(glob.glob(inPath+'[0-9]*[0-9].pkl'))
    dashboards = {}
    for dash_filePath in dash_filesPaths:
        id_execution = int(re.search(r'\d+.pkl',dash_filePath).group()[0:13])
        this_dashboard = pickle_load_one_dashboard(id_execution,DIR_BASE)
        dashboards[id_execution]=this_dashboard
    return dashboards


def pickle_load_all_execution_results_to_dict(DIR_BASE):
    inPath = DIR_BASE+'/pickles-dashboards/'
    results_filesPaths=sorted(glob.glob(inPath+'[0-9]*[0-9]_results.pkl'))
    executions_results_dict = {}
    for results_filePath in results_filesPaths:
        id_execution = int(re.search(r'\d+_results.pkl',results_filePath).group()[0:13])
        this_execution_results = pickle_load_one_execution_results_dict(id_execution,DIR_BASE)
        executions_results_dict[id_execution]=this_execution_results
    return executions_results_dict
    
def pickle_load_all_friendly_id_already_used(DIR_BASE):
    dashboards=pickle_load_all_dashboards_to_dict(DIR_BASE)
    
    friendly_ids =[]
    
    for _, dashboard in dashboards.items():
        if 'FRIENDLY_ID' in dashboard:
            friendly_ids.append(dashboard['FRIENDLY_ID'])
            
    return set(friendly_ids)
        
    
def pickle_load_to(filename, dest_dashboard):
    dashboard = pickle_load_dashboard(filename)
    
    dest_dashboard.clear()
    dest_dashboard.update(dashboard)


def pickle_reload_temp_dashboard(dashboard):
    temp_filename=dashboard['DIR_BASE']+'/temp.pkl'
    
    pickle_load_to(temp_filename, dashboard)    
  
  
###     PKL SAVE     
########################


def export_kfold_idx(dashboard, kfold_idx):
    filename = dashboard['DIR_BASE']+'/kfold/' + str(dashboard['SAVE_FOLDS_SPLIT']) + '.pkl'    
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    
    if os.path.isfile(filename):
        print("ERROR: file already exists: ", filename)
    else:
        with open(filename, 'wb') as pfile:
            pickle.dump(kfold_idx, pfile, protocol=PICKLE_PROTOCOL)

def pickle_save_dashboard(dashboard, filename=None):
    if isinstance(dashboard, ReadOnlyDictionary):
        dashboard=dict(dashboard)
    
    print("DASHBOARD GUARDADO COMO: {} ({})".format(dashboard['FRIENDLY_ID'], dashboard['ID']))
    if filename is None:
        filename = dashboard['DIR_BASE']+'/pickles-dashboards/' + str(dashboard['ID']) + '.pkl'    
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'wb') as pfile:
        pickle.dump(dashboard, pfile, protocol=PICKLE_PROTOCOL)

def pickle_save_dashboard_dict(dashboard, id_execution, filename=None):
    if isinstance(dashboard, ReadOnlyDictionary):
        dashboard=dict(dashboard)
        
    if filename is None:
        filename = dashboard['DIR_BASE']+'/pickles-dashboards/' + str(id_execution) + '.pkl'    
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'wb') as pfile:
        pickle.dump(dashboard, pfile, protocol=PICKLE_PROTOCOL)

def pickle_save_execution_results_dict(execution_results_dict, id_execution, DIR_BASE):
    filename = DIR_BASE+'/pickles-dashboards/' + str(id_execution) + '_results.pkl'   
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'wb') as pfile:
        pickle.dump(execution_results_dict, pfile, protocol=PICKLE_PROTOCOL)
        
        
def pickle_save_temp_dashboard(dashboard):
    if isinstance(dashboard, ReadOnlyDictionary):
        dashboard=dict(dashboard)
    
    temp_filename=dashboard['DIR_BASE']+'/temp.pkl'      
    pickle_save_dashboard(dashboard, filename=temp_filename)

def pickle_save_temp_parameters(dashboard,parameters):
    temp_filename=dashboard['DIR_BASE']+'/temp/parameters-'+str(dashboard['ID'])+'.pkl'      
    pickle_save_dashboard(parameters, filename=temp_filename) 
    return temp_filename     

    
   
###     PKL DELETE     
########################
def pickle_delete(d, id_execution):
    
    file_1 = d['DIR_BASE']+'/pickles-dashboards/' + str(id_execution) + '.pkl'
    file_2 = d['DIR_BASE']+'/pickles-dashboards/' + str(id_execution) + '_results.pkl'
     
    try:
        os.remove(file_1)
        print(Fore.BLUE + "\nPICKLE: Succesfully removed: \n\t" +file_1 +  Style.RESET_ALL)
    except:
        print(Fore.RED + "\nPICKLE: We were not able to delete the file: \n\t" +file_1 +  Style.RESET_ALL)
        
    try:
        os.remove(file_2)
        print(Fore.BLUE + "\nPICKLE: Succesfully removed: \n\t" +file_2 +  Style.RESET_ALL)
    except:
        print(Fore.RED + "\nPICKLE: We were not able to delete the file: \n\t" +file_2 +  Style.RESET_ALL)



###     PKL Utils    
########################    
    
def execution_results_dictionary(id_execution,d,results,val_exists, y_test_exists):  
    this_execution_results = {}
    this_execution_results['ID_EXECUTION'] = id_execution    
    file_path = os.path.dirname(os.path.realpath(__file__))
    dir_name = os.path.basename(file_path) 

    # Software Version
    try:
        with open('VERSION', 'r') as f:
            soft_Version = str(f.readline())
    except:
        soft_Version = 'Ver-Unknown'
    this_execution_results['version'] = soft_Version
    
    this_execution_results['path'] = file_path
    this_execution_results['submission_duration'] = results['total_execution_time']
    this_execution_results['rarp_y_train_transf_preds'] = results['rarp_y_train_transf_preds']
    this_execution_results['rarp_y_val_transf_preds'] = results['rarp_y_val_transf_preds']
    this_execution_results['rarp_y_test_transf_preds'] = results['rarp_y_test_transf_preds']
    this_execution_results['rarp_y_train_preds'] = results['rarp_y_train_preds']
    this_execution_results['rarp_y_val_preds'] = results['rarp_y_val_preds']
    this_execution_results['rarp_y_test_preds'] = results['rarp_y_test_preds']
    this_execution_results['rarp_max'] = results['rarp_max']
    this_execution_results['submission_name'] = results['submission_name']
    
    this_execution_results['levelN_TRAIN_ids_executions'] = results['levelN_TRAIN_ids_executions']
    this_execution_results['levelN_Test_ids_executions'] = results['levelN_Test_ids_executions']
    this_execution_results['levelN_columns_label_ordered'] = results['levelN_columns_label_ordered']
    
    this_execution_results['caruana_coeffs'] = results['caruana_coeffs']
    this_execution_results['caruana_rounds'] = results['caruana_rounds']
    
    this_execution_results['so_alg'] = results['so_alg']
    this_execution_results['so_titulo'] = results['so_titulo']
    this_execution_results['so_idScript'] = results['so_idScript']
    this_execution_results['so_autor'] = results['so_autor']
    this_execution_results['so_puntuacion'] = results['so_puntuacion']
    
    this_execution_results['overfit'] = results['overfit']

    this_execution_results['alg_param'] = results['alg_param']
    this_execution_results['score_validation_by_fold'] = results['score_validation_by_fold']
    this_execution_results['best_iteration'] = results['best_iteration']
    this_execution_results['best_iteration_mean'] = results['best_iteration_mean']
    this_execution_results['best_ntree_limit'] = results['best_ntree_limit']

    for dataset_name in results['datasets_names']:
        if "idx_test" in dataset_name:
            this_execution_results['hdf5_dataset_idx_test_name'] = dataset_name 
        elif "y_test_preds" in dataset_name:
            this_execution_results['hdf5_dataset_y_test_preds_name'] = dataset_name
        elif "idx_val" in dataset_name:
            this_execution_results['hdf5_dataset_idx_val_name'] = dataset_name
        elif "y_val_preds" in dataset_name:
            this_execution_results['hdf5_dataset_y_val_preds_name'] = dataset_name
        elif "y_val" in dataset_name and not "y_val_preds" in dataset_name:
            this_execution_results['hdf5_dataset_y_val_name'] = dataset_name
        else:
            sys.exit("Error: dataset_name"+dataset_name+"is not codified in the function execution_results_dict")

    
    this_execution_results['score_train_cv'] = None if results['model_score_cv'] is None else round(float(results['model_score_cv']),10)
    this_execution_results['score_train'] = None if results['model_score_train'] is None else round(float(results['model_score_train']),10)
    this_execution_results['score_train_transf'] = None if results['model_score_train_transf'] is None else round(float(results['model_score_train_transf']),10)
    if val_exists:
        this_execution_results['score_validation_transf'] = None if results['model_score_val_transf'] is None else round(float(results['model_score_val_transf']),10)
        this_execution_results['score_validation'] = None if results['model_score_val'] is None else round(float(results['model_score_val']),10)
    if y_test_exists:
        this_execution_results['score_test'] = None if results['model_score_test'] is None else round(float(results['model_score_test']),10)
    if d['SPLIT_CV_KFOLDS']>=2 and results['model_score_cv'] is not None:
        this_execution_results['model_score_cv'] = None if results['model_score_cv'] is None else round(float(results['model_score_cv']),10)
        this_execution_results['model_score_cv_sd'] = None if results['model_score_cv_sd'] is None else round(float(results['model_score_cv_sd']),10)
        
    return this_execution_results


def homogenize_dictionaries(executions_info_dict):
    
    # Generamos fields_names_all con la union de todos los campos de todos los executions_info_dict
    fields_names_all=[]
    for k in executions_info_dict.keys():
        this_execution_fields_names=sorted(list(executions_info_dict[k].copy().keys()))
        if fields_names_all==[]:
            fields_names_all = this_execution_fields_names
        else:
            fields_names_all = sorted(list(set(fields_names_all)|set(this_execution_fields_names)))
            
            
    # El numero total de campos lo llamamos num_fields_all
    num_fields_all = len(fields_names_all)
    
    # We review each execution to check whether they have all fields
    # If any field is missing, it's added with value=None
    for k in executions_info_dict.keys():
        fields_names_this = sorted(list(executions_info_dict[k].copy().keys()))
        num_fields_this = len(fields_names_this)
        if num_fields_all > num_fields_this: # le faltan campos
            fields_to_add = list(set(fields_names_all)^set(fields_names_this))
            #print('A la ejecucion'+str(executions_info_dict[k]['ID_EXECUTION'])+'le faltan los campos'+str(fields_to_add)+'.Se le incluyen a None')
            # AÃ±adimos los campos que faltan con valor None
            for f in fields_to_add:
                executions_info_dict[k][f]=None
    
    return executions_info_dict


########################################################
#####                                              #####
#####                 QUERIES OLD                  #####
#####                                              #####
########################################################

def select_ids_execution(executions_info_read,query_str):
    df = executions_info_read.query(query_str)
    if len(df)==0:
        print("NONE executions with these criteria:",query_str)
        return []
    else:
        return df['ID_EXECUTION'].values.tolist()       
    

########################################################
#####                                              #####
#####                    SQLITE                    #####
#####                                              #####
########################################################   

########################
###   SQLite Save    ###
######################## 

def sqlite_save_executions_info(executions_info_dict,DIR_DATA):
    
    db_path = DIR_DATA +'executions_info.db'
    
    try:
        if os.path.isfile(db_path):
            os.remove(db_path)
    except:
        pass
            
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    
    # Creamos tabla
    fields_names_sorted = sorted(fields_names(executions_info_dict))
    sqlite_dtypes_sorted = sqlite_dtypes_sorted_by_fields_name(executions_info_dict)
    
    cad = "create table executions_info ("
    for i in [i for (i, x) in enumerate(fields_names_sorted)]:
        cad = cad + fields_names_sorted[i] + " " + sqlite_dtypes_sorted[i] + ","
    cad = cad[:-1] + ")"
        
#    print(cad)

    c.execute(cad)
#    try:
#        c.execute(cad)
#    except:
#        print("Warning: OperationalError: table executions_info already exists")
        
    
    
    # Insertamos executions_info
    values_sorted = values_sorted_by_fields_name(executions_info_dict)

    cadena_question_repetido = ','.join(['?'] * len(fields_names_sorted))
    cadena = "INSERT INTO executions_info VALUES ("+ cadena_question_repetido +")"
    c.executemany(cadena, values_sorted)
    
    
    conn.commit()
    conn.close()




def fields_names(executions_info_dict):
    fields_names=[]
    contador = 0
    for k in executions_info_dict.keys():
        contador += 1
        if contador == 1: # solo una vez
            sub_dict = executions_info_dict[k]
            for subk in sub_dict:
                fields_names.append(subk)
    return fields_names
    
                       
def values_sorted_by_fields_name(executions_info_dict):
    values =[]
    for k in executions_info_dict.keys():
        sub_dict = executions_info_dict[k]
        ordered_values = [value_type_adapt(value) for (key, value) in sorted(sub_dict.items())]
        values.append(tuple(ordered_values))
    return values


def value_type_adapt(value):
    if type(value).__name__ == 'NoneType':
        value_type_adapted = None  
    elif type(value).__name__ != 'int' and type(value).__name__ != 'float' and type(value).__name__ != 'str':
        value_type_adapted = str(value)
    else:
        value_type_adapted = value
    return value_type_adapted
    

def sqlite_dtypes_sorted_by_fields_name(executions_info_dict):
    sqlite_dtype_for_each_field = []
    for k in executions_info_dict.keys():
        sub_dict = executions_info_dict[k]
        ordered_sqlite_dtype_for_each_field = np.array([dtype_adapted_to_sqlite_types(type(value).__name__) for (key, value) in sorted(sub_dict.items())])
        if len(sqlite_dtype_for_each_field) == 0:
            sqlite_dtype_for_each_field = ordered_sqlite_dtype_for_each_field
        else:       
            # sqlite_dtype_for_each_field es una lista de sqlite_dtypes
            # ordered_sqlite_dtype_for_each_field es una lista de sqlite_dtypes
            # puede ocurrir que ambas listas no sean iguales, pues haya un NULL vs INTEGER o un NULL vs REAL o NULL vs TEXT
            # pasarÃ¡ cuando haya ejecuciones con menos variables que otras y se han rellenado con None en python que se habran transformado a NULL      
            
            # Por tanto, en los casos en que sqlite_dtype_for_each_field sea 'NULL' ponemos el valor de ordered_sqlite_dtype_for_each_field
            sqlite_dtype_for_each_field[sqlite_dtype_for_each_field=='NULL']=ordered_sqlite_dtype_for_each_field[sqlite_dtype_for_each_field=='NULL']
    
    # Si quedara algun dtype que en todos las ejecuciones era NULL, lo ponemos a TEXT
    if len(sqlite_dtype_for_each_field)>0:
        sqlite_dtype_for_each_field[sqlite_dtype_for_each_field=='NULL']='TEXT'
    return list(sqlite_dtype_for_each_field)

    
def dtype_adapted_to_sqlite_types(dataType):
    if dataType == 'int':
        sqlite_type = 'INTEGER'
    elif dataType == 'float':
        sqlite_type = 'REAL'
    elif dataType == 'NoneType':
        sqlite_type = 'NULL'
    else:
        sqlite_type = 'TEXT'
    return sqlite_type



def sqlite_save_feature_importance(d, DIR_DATA, df, df_dtypes):
    
    
    def sqlite_get_dtypes_sorted(df_dtypes):
        sqlite_dtype_for_each_field = []
        for k in df_dtypes:
            
            sqlite_dtype_for_each_field.append(str(dtype_adapted_to_sqlite_types(str(k))))
        print('sqlite_dtype_for_each_field',sqlite_dtype_for_each_field)  
        return sqlite_dtype_for_each_field
    
    #print('df',df)
    #print('aaaaaaaaaa',df['feature'])
    #print(df['feature_score'])
    print()
    print()
    print('df_dtypes',df_dtypes)
    db_path = DIR_DATA +'features_importancerreererereere.db'
    
    if os.path.isfile(db_path):
        os.remove(db_path)
    
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    
    # Creamos tabla
    fields_names_sorted = sorted(df.columns.names)
    print('fields_names_sorted',fields_names_sorted)

    sqlite_dtypes_sorted = sqlite_get_dtypes_sorted(df_dtypes)
    print('sqlite_dtypes_sorted',sqlite_dtypes_sorted)
    print()
    
    cad = "create table features_importance ("
    #for i in [i for (i, x) in enumerate(fields_names_sorted)]:
    for i in enumerate(fields_names_sorted):
        cad = cad + fields_names_sorted[i] + " " + sqlite_dtypes_sorted[i] + ","
    cad = cad[:-1] + ")"

    c.execute(cad)
    
    
    # Insertamos executions_info
    values_sorted = df['feature_score']

    cadena_question_repetido = ','.join(['?'] * len(fields_names_sorted))
    cadena = "INSERT INTO features_importance VALUES ("+ cadena_question_repetido +")"
    c.executemany(cadena, values_sorted)
    
    
    conn.commit()
    conn.close()






########################
###   SQLite Query   ###
######################## 

def sqlite_query_select_id_executions(select_list,query_where,query_orderby_field,query_orderby_direction,DIR_DATA):
    
    db_path = DIR_DATA +'executions_info.db'
        
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    query_select = ','.join(select_list)
    
    query_sentence = 'SELECT '+ query_select +' FROM executions_info WHERE '+ query_where +' ORDER BY ' + query_orderby_field + ' ' + query_orderby_direction
    
    print(query_sentence)
    
    result = []   

    for row in c.execute(query_sentence):
        this_row_values = []
        for num_element in range(len(select_list)):
            this_row_values.append(row[num_element])
        result.append(this_row_values)
    
    result_returned = []
    for num_element in range(len(select_list)):
        result_returned.append([row[num_element] for row in result])
    
    conn.close()
    return result_returned


def sqlite_query_select_by_list_order_by_list(select_list,list_id_executions_str,DIR_DATA):
    
    db_path = DIR_DATA +'executions_info.db'
        
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    query_select = ','.join(select_list)
    
    query_where = ""
    for id_execution_str in list_id_executions_str:
        query_where += "id_execution = " + id_execution_str + " OR "
    query_where = query_where[:-3]
        
    query_orderby = query_where.replace("OR","DESC,")+" DESC"
    
    query_sentence = 'SELECT '+ query_select +' FROM executions_info WHERE '+ query_where +' ORDER BY ' + query_orderby
    
    result = []   
    for row in c.execute(query_sentence):
        this_row_values = []
        for num_element in range(len(select_list)):
            this_row_values.append(row[num_element])
        result.append(this_row_values)
    
    result_returned = []
    for num_element in range(len(select_list)):
        result_returned.append([row[num_element] for row in result])
    
    conn.close()
    return result_returned
    


def sqlite_query_select_all(DIR_DATA):
    db_path = DIR_DATA +'executions_info.db'
        
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    
    query_sentence = 'SELECT * FROM executions_info ORDER BY id_execution'
    
    for row in c.execute(query_sentence):
        print(row)
    
    conn.close()



def id_execution_generator():
    #Id de la ejecucion. Valor Integer de la forma AAMMDDhhmmssd

    #now_utc = datetime.utcnow()
    now_utc = datetime.utcnow()

    madrid = pytz.timezone("Europe/Madrid")
    now_utc_madrid = madrid.localize(now_utc)
    madrid_horas_diferencia = int(str(now_utc_madrid)[26:29])
    madrid_horas_diferencia_datetime = timedelta(hours=madrid_horas_diferencia)
    
    now_utc_madrid_ok = now_utc + madrid_horas_diferencia_datetime
    
    id_execution = int(10*np.float64(str(now_utc_madrid_ok)[2:].replace("-","").replace(" ","").replace(":","")))
    return id_execution
    
    


####################################################
###                                              ### 
###                SAVE PREDICTIONS              ###
###                                              ###
####################################################    

def save_predictions(SAVE_SUBMISSION, SAVE_HDF5, SAVE_PICKLE, SAVE_CSV, SAVE_SQLITE, d, results, best_parameters, execution_CV_stopped, split_cv_folds_orig, id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION):


        

    # SUBMISSION 
    ##############
    if SAVE_SUBMISSION:
        if val_exists:
            submission_generation(d, results, best_parameters, CUSTOM_SUBMISSION_CSV, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION, idx_test, y_test_preds, execution_CV_stopped, split_cv_folds_orig, fold, idx_val, y_val_preds)
        else:
            submission_generation(d, results, best_parameters, CUSTOM_SUBMISSION_CSV, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION, idx_test, y_test_preds, execution_CV_stopped, split_cv_folds_orig, fold)
        
        

    #   HDF5
    ##############
    if SAVE_HDF5:
        # Ordenamos los elementos por índice antes de guardarlos  
        p = idx_test.argsort()
        idx_test_sorted = idx_test[p]
        y_test_preds_sorted = y_test_preds[p]
                
        if val_exists:
            p = idx_val.argsort()
            idx_val_sorted = idx_val[p]
            y_val_sorted = y_val[p]
            y_val_preds_sorted = y_val_preds[p]

        if (d['LEVEL']=='1_ML' or d['LEVEL']=='1_EXTERNAL_SCRIPTS' or d['LEVEL']=='1_EXTERNAL_PREDICTIONS' or d['LEVEL']=='2_ML' or d['LEVEL']=='2_CARUANA' or d['LEVEL']=='2_ENSEMBLE') and val_exists:
            results['datasets_names'] = hdf5_save_execution_data(id_execution,d,val_exists,N_test,idx_test_sorted,y_test_preds_sorted,idx_dtype,y_dtype,fold, execution_CV_stopped, split_cv_folds_orig, N_val,idx_val_sorted,y_val_preds_sorted,y_val_sorted)
        else:
            results['datasets_names'] = hdf5_save_execution_data(id_execution,d,val_exists,N_test,idx_test_sorted,y_test_preds_sorted,idx_dtype,y_dtype,fold, execution_CV_stopped, split_cv_folds_orig)
            results['datasets_names'] = [x for x in results['datasets_names'] if x!='']
    else:
        results['datasets_names']=['idx_test','y_test_preds','idx_val','y_val_preds', 'y_val']
    

    #  PICKLE
    ##############
    if SAVE_PICKLE:
        # this dashboard
        pickle_save_dashboard_dict(d,id_execution)   
    
        # results
        results['rarp_max'] = np.max([results['rarp_y_train_transf_preds'],results['rarp_y_val_transf_preds'],results['rarp_y_test_transf_preds'],results['rarp_y_train_preds'],results['rarp_y_val_preds'],results['rarp_y_test_preds']])
        execution_results_dict = execution_results_dictionary(id_execution, d, results, val_exists, y_test_exists)    
        pickle_save_execution_results_dict(execution_results_dict, id_execution, d['DIR_BASE'])
        
        
    #  CSV    
    ##############
    executions_info_dict = pickle_load_all_dashboards_and_execution_results_to_dict(d['DIR_BASE'],homogenize=True)
    
    if SAVE_CSV:
        csv_save_executions_info(executions_info_dict,d['DIR_DATA'])
        
    
    #  SQLITE 
    ##############
    if SAVE_SQLITE:
        sqlite_save_executions_info(executions_info_dict,d['DIR_DATA'])
        #print(executions_info_dict)
    
 
        
        
def save_predictions_CV_stopped(SAVE_SUBMISSION, SAVE_HDF5, SAVE_PICKLE, SAVE_CSV, SAVE_SQLITE, d, results, best_parameters, execution_CV_stopped, split_cv_folds_orig, id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION):
    # You are here because you chose 'SPLIT_CV_KFOLDS_EARLY_STOP' = 1 and the Crossvalidation stopped early.
    #   So we have a bad incomplete CV execution which we will only persist enough so that dashboard is not re-run in the future.
    #   We then leverage the CV executions converting it into a VAl execution and persisting it as if it was a perfetly run submission.
    
    
    # First we save the incomplete CV submission in pickle and sqlite so it's not re-run in the future (marked as 'overfit')
    results['overfit'] = 1

    save_predictions( 0,   0,   SAVE_PICKLE,   0,   SAVE_SQLITE,  d, results, best_parameters, execution_CV_stopped, split_cv_folds_orig, id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION)
    #save_predictions( 0,   0,   SAVE_PICKLE,   1,   SAVE_SQLITE,  d, results, best_parameters, execution_CV_stopped, id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION)
                 # SUBM, HDF5,      PKL,      CSV,    SQL 
    

                 
    # Then we tune the submission and persist it correctly as a validation submission
    results['overfit'] = 0
    id_execution = id_execution + 1
    d['ID'] = id_execution
    
    if d['SPLIT_CV_TYPE'] == 'Random' or d['SPLIT_CV_TYPE'] == 'Stratified':
        d['SPLIT_VALIDATION_SET_PERC'] = float("{0:.2f}".format(1/d['SPLIT_CV_KFOLDS']))
    else:
        if 'VAL-FIX' in d['SPLIT_TS_TYPE']:
            d['SPLIT_TS_VALIDATION_FIX_NUM'] = int(d['SPLIT_TS_VALIDATION_FIX_NUM'] / d['SPLIT_CV_KFOLDS'])

                
    d['SPLIT_CV_KFOLDS'] = 1
    

    save_predictions( 1,   1,   SAVE_PICKLE,   1,   SAVE_SQLITE,  d, results, best_parameters, execution_CV_stopped, split_cv_folds_orig, id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION)

                           
