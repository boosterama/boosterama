# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 09:31:09 2016

@author: DataCousins
"""

from colorama import Fore, Back, Style
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold, StratifiedKFold, TimeSeriesSplit, train_test_split
import sys
from my_Input_Output import export_kfold_idx, import_kfold_idx
from my_Cache import set_random_seed

from my_Prints import *



def ordering(idx, X=None, y=None):
    # Ordena según los valores del índice: el propio índice, así como X e y, si se le pasan
    p = idx.argsort()
    idx = idx[p]
    
    if type(X).__name__ != 'NoneType':
        X = X[p]
        
    if type(y).__name__ != 'NoneType':
        y = y[p]
        
    if type(y).__name__ == 'NoneType':
        if type(X).__name__ == 'NoneType':
            return idx
        else:
            return idx, X
    else:
        return idx, X, y

        
def pandas_to_numpy(idx_TRAIN__pd,X_TRAIN__pd,y_TRAIN__pd,idx_test__pd,X_test__pd, d):

    idx_TRAIN         = np.array(idx_TRAIN__pd)
    if idx_TRAIN.dtype != "object": idx_TRAIN = idx_TRAIN.astype(int)
    X_TRAIN           = np.array(X_TRAIN__pd)
    y_TRAIN           = np.array(y_TRAIN__pd)

    idx_test          = np.array(idx_test__pd) 
    X_test            = np.array(X_test__pd)
            
    N_test         = X_test.shape[0]        
    M              = X_TRAIN__pd.shape[1]
    X_cols_names   = list(X_TRAIN__pd.columns.values)


    # Print Y_Info and X_Info
    if d['PRINT_Y_INFO']: print_info(y_TRAIN,"y_TRAIN",[d['COMP_COLUMN_CLASS']])
    if d['PRINT_X_INFO']: print_info(X_TRAIN,"X_TRAIN",X_cols_names)
    if d['PRINT_X_INFO']: print_info(X_test,"X_test",X_cols_names)
                 
    return idx_TRAIN, X_TRAIN, y_TRAIN, idx_test, X_test, N_test, M, X_cols_names


    
    
    
def print_indices_and_data(idx_train,idx_val,X_train,y_train,X_val,y_val,print_indices,print_data):
   
    if print_indices:
        print("Train indexes:\n", idx_train)
        print("Val indexes:\n", idx_val)
       
    if print_data:
        print('X_train')
        print(X_train)
        print('y_train')
        print(y_train)
        print('X_val')
        print(X_val)
        print('y_val')
        print(y_val)  

        
        
        
# FOR CASE:   d['SPLIT_CV_KFOLDS']==1 and d['SPLIT_VALIDATION_SET_PERC']!=0.0
def split_train_val(d, idx_TRAIN, X_TRAIN, y_TRAIN): 
    
    idx_concat_X_TRAIN = np.concatenate((idx_TRAIN.reshape(len(idx_TRAIN),1), X_TRAIN), axis=1)
    idx_concat_X_train, idx_concat_X_val, y_train, y_val = train_test_split(idx_concat_X_TRAIN, y_TRAIN, test_size=d['SPLIT_VALIDATION_SET_PERC'], random_state=d['SPLIT_VALIDATION_RANDOM_STATE'])
    
    idx_train = idx_concat_X_train[:,0]
    X_train = np.delete(idx_concat_X_train, 0, 1)

    idx_val = idx_concat_X_val[:,0]
    X_val = np.delete(idx_concat_X_val, 0, 1)

    N_train = X_train.shape[0]
    N_val = X_val.shape[0]
    
    return idx_train, X_train, y_train, idx_val, X_val, y_val, N_train, N_val
    
    
    
def split_train_val_timeseries(d, idx_TRAIN, X_TRAIN, y_TRAIN):   
    
    # Concatenate
    idx_concat_X_TRAIN = np.concatenate((idx_TRAIN.reshape(len(idx_TRAIN),1), X_TRAIN), axis=1)

    # Lenghts
    if "TRAIN-FIX" in 'SPLIT_TS_TYPE':
        train_num_obs = d['SPLIT_TS_train_FIX_NUM']
    else:
        train_num_obs = 0
        
    val_num_obs = d['SPLIT_TS_VALIDATION_FIX_NUM']
    
    
    # Splitting
    idx_concat_X_train = idx_concat_X_TRAIN[train_num_obs:-val_num_obs,:]
    idx_concat_X_val   = idx_concat_X_TRAIN[-val_num_obs:,:]
    y_train            = y_TRAIN[train_num_obs:-val_num_obs]
    y_val              = y_TRAIN[-val_num_obs:]

    
    idx_train = idx_concat_X_train[:,0]
    X_train = np.delete(idx_concat_X_train, 0, 1)

    idx_val = idx_concat_X_val[:,0]
    X_val = np.delete(idx_concat_X_val, 0, 1)

    N_train = X_train.shape[0]
    N_val = X_val.shape[0]
    
    return idx_train, X_train, y_train, idx_val, X_val, y_val, N_train, N_val
    

    
    
    
    
def splitting_ordering_and_folding(d, idx_TRAIN, X_TRAIN, y_TRAIN, order):
    
    NUM_FOLDS       = d['SPLIT_CV_KFOLDS']
    SPLIT_CV_TYPE   = d['SPLIT_CV_TYPE']
    SPLIT_TS_TYPE   = d['SPLIT_TS_TYPE']
    
    idx_train_list  = []
    idx_val_list    = []
    X_train_list    = []
    X_val_list      = []
    y_train_list    = []
    y_val_list      = []
    N_train_list    = []
    N_val_list      = []
    

    ##############################  
    # FATHER (no validation set)
    ##############################
    if NUM_FOLDS==0 or (NUM_FOLDS==1 and ((SPLIT_CV_TYPE != 'TimeSeries' and d['SPLIT_VALIDATION_SET_PERC']==0.0) or (SPLIT_CV_TYPE == 'TimeSeries' and d['SPLIT_TS_VALIDATION_FIX_NUM']==0))):
        #print(Fore.BLUE + 'Feature Importance excel created.' + Style.RESET_ALL)
        
        if d['LEVEL'] != '2_ENSEMBLE':
            if NUM_FOLDS==0:
                print(Fore.BLUE + "\nExecution with SPLIT_CV_KFOLDS=0 -> Using the whole TRAIN-set to train the model, without validation dataset\n" + Style.RESET_ALL)
            else:
                print(Fore.BLUE + "\nExecution with SPLIT_CV_KFOLDS=1 and SPLIT_VALIDATION_SET_PERC=0.0 -> Using the whole TRAIN-set to train the model, without validation dataset\n" + Style.RESET_ALL)

        # Ordering
        if order:
            idx_TRAIN, X_TRAIN, y_TRAIN = ordering(idx_TRAIN, X_TRAIN, y_TRAIN)
            
        # Folding
        idx_train_list.append(idx_TRAIN)
        idx_val_list.append(None)
        
        X_train_list.append(X_TRAIN)
        X_val_list.append(None)
        
        y_train_list.append(y_TRAIN)
        y_val_list.append(None)
        
        N_train_list.append(X_TRAIN.shape[0])
        N_val_list.append(None)


    ##############################
    # VALIDATION SET
    ##############################     
    elif NUM_FOLDS==1 and ((SPLIT_CV_TYPE != 'TimeSeries' and d['SPLIT_VALIDATION_SET_PERC']!=0.0) or (SPLIT_CV_TYPE == 'TimeSeries'  and d['SPLIT_TS_VALIDATION_FIX_NUM']>0)):
        
        if SPLIT_CV_TYPE == 'TimeSeries':
            if d['LEVEL'] != '2_ENSEMBLE':
                print(Fore.BLUE + "\nTimeSeries execution with SPLIT_CV_KFOLDS=1 and SPLIT_TS_VALIDATION_FIX_NUM > 0 \n\t-> Dividing TRAIN in two subsets (Train, Val) applying SPLIT_TS_VALIDATION_FIX_NUM=" + str(d['SPLIT_TS_VALIDATION_FIX_NUM'])  + "\n" + Style.RESET_ALL)
            idx_train, X_train, y_train, idx_val, X_val, y_val, N_train, N_val = split_train_val_timeseries(d, idx_TRAIN, X_TRAIN, y_TRAIN)
        else:
            if d['LEVEL'] != '2_ENSEMBLE':
                print(Fore.BLUE + "\nExecution with SPLIT_CV_KFOLDS=1 and SPLIT_VALIDATION_SET_PERC != 0.0 -> Dividing TRAIN in two subsets (Train, Val) applying SPLIT_VALIDATION_SET_PERC\n" + Style.RESET_ALL)
            # Splitting
            idx_train, X_train, y_train, idx_val, X_val, y_val, N_train, N_val = split_train_val(d, idx_TRAIN, X_TRAIN, y_TRAIN)
            # Ordering
            if order:
                idx_train, X_train, y_train = ordering(idx_train, X_train, y_train)
                idx_val, X_val, y_val = ordering(idx_val, X_val, y_val)

        # Folding
        idx_train_list.append(idx_train)
        idx_val_list.append(idx_val)            

        X_train_list.append(X_train)
        X_val_list.append(X_val)
        
        y_train_list.append(y_train)
        y_val_list.append(y_val)

        N_train_list.append(N_train)
        N_val_list.append(N_val)

        
        
        

    ##############################
    # CROSS VALIDATION
    ##############################
    elif NUM_FOLDS>=2:
        if d['LEVEL'] != '2_ENSEMBLE':
            print(Fore.BLUE + "\nExecution with SPLIT_CV_KFOLDS>=2 -> Execution using CrossValidation\n" + Style.RESET_ALL)

        # Splitting type
        #################
        if SPLIT_CV_TYPE=='Random':
            kf = KFold(n_splits=NUM_FOLDS, shuffle=True)
            kf = kf.split(X_TRAIN)
            
        elif SPLIT_CV_TYPE=='Stratified':
            #  https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/model_selection/_split.py
            skf = StratifiedKFold(n_splits=NUM_FOLDS, shuffle=True, random_state=None)
            #skf.get_n_splits(X_TRAIN, y_TRAIN)
            kf = skf.split(X_TRAIN, y_TRAIN)
            
        elif SPLIT_CV_TYPE=='TimeSeries':

            if SPLIT_TS_TYPE == 'TS_CV_ACADEMIC':
                tscv = TimeSeriesSplit(n_splits=NUM_FOLDS)
                kf = tscv.split(X_TRAIN)
                order = 0

            else:  # 'TS_CV_TRAIN-ORIGIN_VAL-FIX',   'TS_CV_TRAIN-FIX_VAL-FIX',    'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
                folds_idx_list = splitting_time_series_crossvalidation(d, idx_TRAIN, NUM_FOLDS)
                order = 0
        elif SPLIT_CV_TYPE=='Index':
            folds_idx_list = import_kfold_idx(d)
            if isinstance(folds_idx_list, pd.DataFrame): #not overlapped folds
                fold_ids=sorted(folds_idx_list['fold'].unique())
                idx_fold=pd.DataFrame({d['COMP_COLUMN_ID']:idx_TRAIN})
                idx_fold=idx_fold.merge(folds_idx_list, on=d['COMP_COLUMN_ID'], how='left')
                idx_fold['i32_index']=range(len(idx_fold))
                
                folds_idx_list=[]
                for fold_id in fold_ids:
                    idx_val_foldK=idx_fold['i32_index'].loc[idx_fold['fold']==fold_id].values
                    idx_train_foldK=idx_fold['i32_index'].loc[idx_fold['fold']!=fold_id].values
                    folds_idx_list.append((idx_train_foldK, idx_val_foldK))  
                del(idx_fold)
            
        else:
            sys.exit('Error: SPLIT_CV_TYPE is not valid')
            
        
        # Get train/test indexes for all folds
        ##########################
        if SPLIT_CV_TYPE!='Index':
            folds_idx_list=[]
            for fold, (idx_train_foldK, idx_val_foldK) in enumerate(kf):   
                folds_idx_list.append((idx_train_foldK, idx_val_foldK))       
            del(kf)
        
        set_random_seed(d) # since 'Index' option works like a cache, we must reset the random numbers generation
        
        
        if isinstance(d['SAVE_FOLDS_SPLIT'], str):
            export_kfold_idx(d, folds_idx_list)
        
        
        
        
        
        # Order and Create Folds
        ##########################
        for fold, (idx_train_foldK, idx_val_foldK) in enumerate(folds_idx_list):  
            #print('fold',fold)
            #print('idx_train_foldK',idx_train_foldK,len(idx_train_foldK))
            #print('idx_val_foldK',idx_val_foldK,len(idx_val_foldK))
            #print('--')
            
                
            # Ordering
            if order:
                idx_train_foldK = ordering(idx_train_foldK)
                idx_val_foldK = ordering(idx_val_foldK)
                     
            # Folding
            idx_train_list.append(idx_TRAIN[idx_train_foldK])
            idx_val_list.append(idx_TRAIN[idx_val_foldK])
            
            X_train_list.append(X_TRAIN[idx_train_foldK])
            X_val_list.append(X_TRAIN[idx_val_foldK])
            
            y_train_list.append(y_TRAIN[idx_train_foldK])
            y_val_list.append(y_TRAIN[idx_val_foldK])

            N_train_list.append(X_TRAIN[idx_train_foldK].shape[0])
            N_val_list.append(X_TRAIN[idx_val_foldK].shape[0])
        
        
        
        
        
            
            
    return idx_train_list, idx_val_list, X_train_list, X_val_list, y_train_list, y_val_list, N_train_list, N_val_list

    
    
    
# Time Series CrossValidation
def splitting_time_series_crossvalidation(d, idx_TRAIN, num_folds):
    
    SPLIT_TS_TYPE = d['SPLIT_TS_TYPE']  # 'TS_CV_TRAIN-ORIGIN_VAL-FIX',   'TS_CV_TRAIN-FIX_VAL-FIX',    'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
    num_obs_val           = d['SPLIT_TS_VALIDATION_FIX_NUM']
    
    # num_obs_train  &  num_obs
    if 'TRAIN-FIX' in SPLIT_TS_TYPE:     # 'TS_CV_TRAIN-FIX_VAL-FIX',    'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
        num_obs_train = d['SPLIT_TS_train_FIX_NUM']
        num_obs = num_obs_train + num_obs_val
    else:
        num_obs_train = 9999999999 # We will ignore it since train will reach up to first observations and will be of variable size.
        num_obs       = 9999999999
    
    X_len = len(idx_TRAIN)
    X_indices = list(range(0, X_len))
    
    kf = []
    
    
    if (SPLIT_TS_TYPE == 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP') and (num_obs * num_folds > X_len):
        error_message = "\nYou defined d['SPLIT_TS_TYPE']='TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'.\n\t Size of train-set(" + str(num_obs_train) + ") plus size of validation-set(" + str(num_obs_val) + ") times number of folds("+ str(num_folds) + ") is equal to '" + str((num_obs_train + num_obs_val) * num_folds) + "', which cannot be greater than the size of the TRAIN set(" + str(X_len) + ")\n\tPlease, decrease train or validation size, decrease the number of folds or select d['SPLIT_TS_TYPE']='TS_CV_TRAIN-FIX_VAL-FIX' which overlaps train sets and may fit."
        sys.exit(error_message)
    
    
    for i in reversed(range(0, num_folds)):
        # 'TS_CV_TRAIN-ORIGIN_VAL-FIX'          # 'TS_CV_TRAIN-FIX_VAL-FIX'          # 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
        
        if 'TRAIN-ORIGIN' in SPLIT_TS_TYPE:
            X_TRAIN_start = 0
            X_TRAIN_end   = X_len - (num_obs_val * (i))
        elif SPLIT_TS_TYPE == 'TS_CV_TRAIN-FIX_VAL-FIX':
            X_TRAIN_start = X_len - (num_obs_val * (i+1)) - (num_obs_train)
            X_TRAIN_end   = X_len - (num_obs_val * (i))
        elif SPLIT_TS_TYPE == 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP':
            X_TRAIN_start = X_len - (num_obs * (i+1))
            X_TRAIN_end   = X_len - (num_obs * (i))
            
            
        
        X_TRAIN = X_indices[X_TRAIN_start:X_TRAIN_end]
                
           
        # folds used to train the model        
        X_train_fold = X_TRAIN[:-num_obs_val]    
        X_val_fold = X_TRAIN[-num_obs_val:]
        
        kf.append([X_train_fold,X_val_fold])
    

    return kf
    
        
    
        




    
