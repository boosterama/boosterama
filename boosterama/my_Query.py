# -*- coding: utf-8 -*-
"""
Created on Sat Dec  5 20:33:43 2015

@author: DataCousins
"""

import collections
from colorama import Fore, Back, Style
import os
import pandas as pd

from my_Feature_Importance import *



# Los query deben empezar SIEMPRE por   'y_*'   o   'o_*'

def query_col(dXT,d,condiciones_query,DATA,function_name):
    #condiciones_query = 'y_int_float_LMas10_LMen500_Bot14'
    #condiciones_query = 'y_num_Alt1-3'   pilla las variables numéricas, pero sólo la primera, cuarta, septima,... 

# Sobre el Y o O:
#     Ponerlo al principio SIEMPRE.   o_int_LMas10_Bot14
#     Es case insensitive.   
#     Sólo actúan entre grupos DTYPE   LMASMEN    TOPBOT.  Si aparecen 2 DTYPES  o 2 LMASMEN, las reglas 'internas' son diferentes:
#                                              Los 2 DTYPE   sacarán una selección de columnas bajo OR
#                                              Los 2 LMASMEN sacarán una selección de columnas bajo AND

    try:
        condiciones_query_orig = condiciones_query.copy()
    except:
        condiciones_query_orig = condiciones_query is condiciones_query+tuple()
    
    condition_has_these =  ['y_','o_']
 

    is_condition = 0    


    if len(condiciones_query) == 1:
        termino = condiciones_query[0].lower()
        for inicio in condition_has_these:
            if termino[0:2] == inicio:
                is_condition = 1
                condiciones_query = termino
                break
                

    
    #print('is_condition',is_condition)
    if is_condition == 1:

        condiciones_query = condiciones_query.lower()
        condiciones_query = condiciones_query.replace('_corrt','_cort').replace('_corrb','_corb')
        
        
        # Preproces Num, Obj, etc..
        ############################

        if condiciones_query.find('_num') >=0:
            condiciones_query = condiciones_query.replace('_num','_int_float')

        if condiciones_query.find('_nonnum') >=0:
            condiciones_query = condiciones_query.replace('_nonnum','_bool_obj_date_str')

        #if condiciones_query.find('_str') >=0:
        #    condiciones_query = condiciones_query.replace('_str','_str_obj')

        if condiciones_query.find('_obj') >=0:
            condiciones_query = condiciones_query.replace('_obj','_obj_date_str')
        


        #  Condición es 'Y' o 'O'
        ##########################

        if condiciones_query[0:2]=='y_':
            condiciones = condiciones_query.split("_")
            condiciones[:] = [x for x in condiciones if (x != 'y') and (x != 'o')]
            cond_es_y = 1
        elif condiciones_query[0:2]=='o_':
            condiciones = condiciones_query.split("_")
            condiciones[:] = [x for x in condiciones if x != 'o']
            cond_es_y = 0
        else:
            #solo un elemento
            condiciones = [condiciones_query]
            cond_es_y = 1

      
        #print('cond_es_y',cond_es_y)
        
        
                  
        ####################
        #
        #     DATATYPES
        #
        ####################
                  
        tipo_datos_python = {'object':'obj','O':'obj','bool':'bool','byte':'byte','int16':'int','int32':'int','int64':'int','float32':'float','float64':'float','str':'str','datetime64[ns]':'date','datetime64[M]':'date','datetime64[D]':'date','datetime64[ms]':'date','timedelta64[ns]':'date'            }
        tipo_datos_query =  ['bool','byte','date','float','int','obj','str']
        
        col_types_selected = []
        cols_selected = []
        
        dtype_found = 0
             
        if condiciones_query.find('_all') >=0:
            cols_selected = list(DATA.dtypes.index)
        else:
            for tipo_dato in condiciones:
                if tipo_dato in tipo_datos_query:
                    dtype_found +=1
                    for clave, valor in tipo_datos_python.items():
                        if valor == tipo_dato:
                            col_types_selected.append(clave)
                            cols_selected.append(list(DATA.dtypes[DATA.dtypes == clave].index))
            if dtype_found > 0:
                cols_selected =  [item for sublist in cols_selected for item in sublist]    
            else:
                # Si el query no contiene all ni ningún dtype ponemos todas las columnas (esto es discutible, pero si no, no podremos hacer que funcione lo de abajo)
                cols_selected = list(DATA.dtypes.index)
        
        cols_selected = uniqfy_query_list(cols_selected)
        

           

          
        #################
        #
        #    ORIG
        #
        ##################
        if (condiciones_query.find('_orig') >=0):
            if len(cols_selected) == 0:
                if cond_es_y == 0:
                    cols_selected = list(set(list(DATA.dtypes.index)).intersection(dXT['VARS_ORIG']))
            else:
                cols_selected = list(set(cols_selected).intersection(dXT['VARS_ORIG']))



          
        #################
        #
        #    LEVELS   
        # 
        #################
        
        #(si hay lmas y lmen siempre se comportarán entre ellas como Y)
        
        lmas_pos = condiciones_query.find('_lmas')
        lmen_pos = condiciones_query.find('_lmen')
        
        if (lmas_pos > 0) or (lmen_pos > 0):
                
            # Reduce scope (para aumentar rapidez)
            if cond_es_y == 1:
                cols_4_levels_init = cols_selected
            else:
                cols_4_levels_init = list(DATA.columns.values)
            
         
            # Get Número from Limite
            if lmas_pos >=0: 
                numero_limSup_lmas = condiciones_query.find('_' , lmas_pos + 1)
                
                if numero_limSup_lmas == -1:
                    el_limite_lmas = int(condiciones_query[(lmas_pos + 5) : 255 ])
                else:
                    el_limite_lmas = int(condiciones_query[(lmas_pos + 5) : numero_limSup_lmas ])
                    
            if lmen_pos >=0: 
                numero_limSup_lmen = condiciones_query.find('_' , lmen_pos + 1)
                if numero_limSup_lmen == -1:
                    el_limite_lmen = int(condiciones_query[(lmen_pos + 5) : 255 ])
                else:
                    el_limite_lmen = int(condiciones_query[(lmen_pos + 5) : numero_limSup_lmen ])
                
            
            # Get COLS_4_LEVELS_LMAS & COLS_4_LEVELS_LMEN  &   COLS_4_LEVELS
            cols_4_levels_lmas = []
            cols_4_levels_lmen = []
        
            for field in cols_4_levels_init:
                grouped = DATA.groupby([field])


                if lmas_pos >=0: 
                    if len(grouped) > el_limite_lmas:
                        cols_4_levels_lmas.append(field)

                if lmen_pos >=0: 

                    if len(grouped) != 0:

                        if len(grouped) < el_limite_lmen: 
                            cols_4_levels_lmen.append(field)
      
            #if cond_es_y == 1:    
            if (lmas_pos >= 0) and (lmen_pos >= 0):
                cols_4_levels = list(set(cols_4_levels_lmas).intersection(cols_4_levels_lmen))
                # Se ha filtrado arriba para que el universo de cols sea únicamente el q tiene q ser   
                
            elif lmas_pos >= 0:
                cols_4_levels = cols_4_levels_lmas
         
            elif lmen_pos > 0:
                cols_4_levels = cols_4_levels_lmen
                    
            # Get COLS_SELECTED depending on cond_es_y  
            if cond_es_y == 1:
                cols_selected = cols_4_levels
            if cond_es_y == 0:
                if len(cols_4_levels) > 0:
                    lists_joined = cols_4_levels + cols_selected
                    cols_selected=list(set(lists_joined))
                    

          
        #################
        #
        #     ORIG
        #
        #################
          
        if (condiciones_query.find('_orig') >=0):
            cols_selected = list(set(cols_selected).intersection(dXT['VARS_ORIG']))
            
            
                   
        ####################
        #
        #     TOP / BOTTOM
        #
        ####################
        if len(cols_selected) > 0:    
    
            if (condiciones_query.find('_top') >=0) or (condiciones_query.find('_bot') >=0) or (condiciones_query.find('_alt') >=0):
                    

                # feature_importance_file   (if it does not exist, we look for another algorithm's feature importance)
                #########################      
                is_feature_file, df_feat_imp =  feature_importance_extract(d)
                

                
                # DO if exists
                ################                    
                if is_feature_file:
                    top_pos = condiciones_query.find('_top')
                    bot_pos = condiciones_query.find('_bot')
                    alt_pos = condiciones_query.find('_alt')
                 
                    # Get Número from Top/Bot/Alt
                    if condiciones_query.find('_top') >=0: 
                        numero_top_fin = condiciones_query.find('_' , top_pos + 1)
                        
                        if numero_top_fin == -1:
                            la_cifra_top = int(condiciones_query[(top_pos + 4) : 255 ])
                        else:
                            la_cifra_top = int(condiciones_query[(top_pos + 4) : numero_top_fin ])
                            
                    elif condiciones_query.find('_bot') >=0: 
                        numero_bot_fin = condiciones_query.find('_' , bot_pos + 1)
                        if numero_bot_fin == -1:
                            la_cifra_bot = int(condiciones_query[(bot_pos + 4) : 255 ])
                        else:
                            la_cifra_bot = int(condiciones_query[(bot_pos + 4) : numero_bot_fin ])
                    
                                                
                    elif condiciones_query.find('_alt') >=0: 
                        numero_alt_fin = condiciones_query.find('_' , alt_pos + 1)
                        if numero_alt_fin == -1:
                            las_cifras_alt = condiciones_query[(alt_pos + 4) : 255 ]
                        else:
                            las_cifras_alt = condiciones_query[(alt_pos + 4) : numero_alt_fin ]
                            
                        la_cifra_alt_inicio = int(las_cifras_alt.split("-")[0])
                        
                        la_cifra_alt_salto = int(las_cifras_alt.split("-")[1])
                        

                    

                    if cond_es_y == 1:                    
                        s1 = pd.Series(cols_selected, name='feature')
                        s2 = pd.Series(cols_selected, name='feat_del')
                        df_Series = pd.concat([s1, s2], axis=1)
                        
                        df_feat_imp = pd.merge(df_feat_imp, df_Series, on='feature', how='right')
                    
                    
                    df_feat_imp.sort_values(by=['feature_score'], ascending=[0], inplace=True)
                    
                    df_feat_imp.reset_index(drop=True,inplace=True)
                    
                    
                    # Ahora entra en juego Alt
                    if condiciones_query.find('_alt') >=0: 
                        las_cifras_seleccionadas = list(range(la_cifra_alt_inicio - 1,len(df_feat_imp),la_cifra_alt_salto))               
                        las_cifras_totales = list(range(0,len(df_feat_imp)))
                        las_cifras_borrar = [x for x in las_cifras_totales if x not in las_cifras_seleccionadas]

                        df_feat_imp = df_feat_imp.drop(df_feat_imp.index[las_cifras_borrar])


                    # Seleccionamos Features Top o Bot
                    if condiciones_query.find('_top') >=0:
                        df_feat_imp = df_feat_imp.head(la_cifra_top)
                    elif condiciones_query.find('_bot') >=0:
                        df_feat_imp = df_feat_imp.tail(la_cifra_bot)


                    # Pasamos a lista
                    cols_topbot = []
                    for idx,elem in enumerate(df_feat_imp['feature']): 
                        cols_topbot.append(str(elem))
 

                                

                    #  Y / O
                    if cond_es_y == 1:
                        cols_selected = cols_topbot
                    else:
                        for idx,elem in enumerate(cols_topbot): 
                            cols_selected.append(str(elem))
                            
                else:
                    print(Fore.RED +  " NO EXISTE ARCHIVO DE FEATURE IMPORTANCE. SE IGNORARÁN PARTES DE QUERIES CON TOP Y BOT" + Style.RESET_ALL)
                    
 

          
        #################
        #
        #     ORIG
        #
        #################
          
        if (condiciones_query.find('_orig') >=0):
            cols_selected = list(set(cols_selected).intersection(dXT['VARS_ORIG']))
            
            
                   
        ##############################
        #
        #     CORRELATIONS TO TARGET
        #
        ##############################                 
                   
        if len(cols_selected) > 0:    
    
            if (condiciones_query.find('_cortop') >=0) or (condiciones_query.find('_corbot') >=0):
                
                # Correlation File
                corr_file_this_exec = d['DIR_OUTPATH_CHARTS'] + 'correlations/' + d['COMP_NAME'] + '_Corr_' + str(d['ID']) + '.xlsx'
                corr_file_general = d['DIR_OUTPATH_CHARTS'] + 'correlations/' + d['COMP_NAME'] + '_Correlations.xlsx'

                if os.path.isfile(corr_file_this_exec):
                    correlation_file = corr_file_this_exec
                else:
                    correlation_file = corr_file_general
            
                
                if os.path.isfile(correlation_file):
                    
                    #  _CorTop/_CorBot
                    cortop_pos = condiciones_query.find('_cortop')
                    corbot_pos = condiciones_query.find('_corbot')
                 
                    # Get Número from _CorTop/_CorBot
                    if condiciones_query.find('_cortop') >=0: 
                        numero_cortop_fin = condiciones_query.find('_' , cortop_pos + 1)
                        
                        if numero_cortop_fin == -1:
                            la_cifra_cortop = int(condiciones_query[(cortop_pos + 7) : 255 ])
                        else:
                            la_cifra_cortop = int(condiciones_query[(cortop_pos + 7) : numero_cortop_fin ])
                            
                    elif condiciones_query.find('_corbot') >=0: 
                        numero_corbot_fin = condiciones_query.find('_' , corbot_pos + 1)
                        if numero_corbot_fin == -1:
                            la_cifra_corbot = int(condiciones_query[(corbot_pos + 7) : 255 ])
                        else:
                            la_cifra_corbot = int(condiciones_query[(corbot_pos + 7) : numero_corbot_fin ])
                

                    # Get Correlations df from file (and sort). 
                    # Partimos de cols_selected hasta este punto. 
                    
                    df_correlations = pd.read_excel(correlation_file,sheet_name='Correlations')
                    
                    df_correlations = df_correlations[pd.notnull(df_correlations['Feat_Other'])]
                    df_correlations.rename(columns={'Feat_Other':'feature'}, inplace=True)
                    
                    if cond_es_y == 1:                    
                        s1 = pd.Series(cols_selected, name='feature')
                        s2 = pd.Series(cols_selected, name='feat_del')
                        df_Series = pd.concat([s1, s2], axis=1)
                        
                        df_correlations = pd.merge(df_correlations, df_Series, on='feature', how='right')
                    
                    
                    df_correlations.sort_values(by=['Corr_Abs'], ascending=[0], inplace=True)
                    
                    df_correlations.reset_index(drop=True,inplace=True)
                    

                    # Seleccionamos Features CorTop o CorBot
                    if condiciones_query.find('_cortop') >=0:
                        df_correlations = df_correlations.head(la_cifra_cortop)
                    elif condiciones_query.find('_corbot') >=0:
                        df_correlations = df_correlations.tail(la_cifra_corbot)


                    # Pasamos a lista
                    cols_cortopbot = []
                    for idx,elem in enumerate(df_correlations['feature']): 
                        cols_cortopbot.append(str(elem))
             

                    #  Y / O
                    if cond_es_y == 1:
                        cols_selected = cols_cortopbot
                    else:
                        for idx,elem in enumerate(cols_cortopbot): 
                            cols_selected.append(str(elem))
                            
                else:
                    print(Fore.RED +  " NO EXISTE ARCHIVO CORRELATIONS. SE IGNORARÁN PARTES DE QUERIES CON CORTOP Y CORBOT" + Style.RESET_ALL)
                    
             


        cols_selected = uniqfy_query_list(cols_selected)
        
        if d['PRINT_QUERY_COLS']:
            print("QUERY: " + str(condiciones_query),"\tCOLS: " + str(cols_selected))


            
    else:              
        # No era un query, sino un listado de Columnas.
        # Dejar sólo las columnas que existan

        cols_init = list(DATA.columns.values)
        cols_selected = condiciones_query
        
        cols_selected = list(set(cols_selected).intersection(cols_init))           

    
    # Populate dictionary with the real columns selected by Queries (in stead of Top5, etc..)
    if is_condition == 1 and function_name != '':
        
        # Check if both are lists containing the same elements
        if collections.Counter(condiciones_query_orig) != collections.Counter(cols_selected):
            if len(cols_selected) == 0: # empty result
                dXT["QUERY_COLS"][function_name] = {'ORIG':condiciones_query_orig,'SELECTED':cols_selected}
                try:
                    if isinstance(d[function_name], tuple):
                        d[function_name] = ()
                    elif isinstance(d[function_name], list):
                        d[function_name] = []
                    elif isinstance(d[function_name], str):
                        d[function_name] = ""
                except:
                    pass
            else:
                try:
                    d = replacer_in_list(d, function_name, condiciones_query_orig[0], cols_selected)
                except:
                    pass
                
    return cols_selected


    
    
    
def replacer_in_list(d, key_name, replace_from, replace_to):
    
    
    if isinstance(d[key_name], tuple):
        e = list(d[key_name])
        e_was_tuple = 1
    else:
        e = d[key_name]
        e_was_tuple = 0 
        

    
    
    if isinstance(e, (list,tuple)):
        
        for id1,elem_level_1 in enumerate(e):
            if isinstance(elem_level_1, (list,tuple)):   

                for id2,elem_level_2 in enumerate(elem_level_1):
                    if isinstance(elem_level_2, (list,tuple)):
                        
                        for id3, elem_level_3 in enumerate(elem_level_2):
                            if isinstance(elem_level_3, (list,tuple)):

                                for id4, elem_level_4 in enumerate(elem_level_3):
                                    if isinstance(elem_level_4, (list,tuple)):
                                        pass

                                    else:
                                        try:
                                            if replace_from in e[id1][id2][id3][id4]:
                                                e[id3] = replace_to
                                        except:
                                            pass                                
                                
                            else:
                                try:
                                    if replace_from in e[id1][id2][id3]:
                                        e[id2] = replace_to
                                except:
                                    pass

                    else:
                        try:
                            if replace_from in e[id1][id2]:
                                e[id1] = replace_to
                        except:
                            pass

            else:
                try:
                    if replace_from in e[id1]:
                        e = replace_to
                except:
                    pass
    else:
        try:
            e = e.replace(replace_from,replace_to)
        except:
            pass

            
    if e_was_tuple == 1:
        d[key_name] = tuple(e)
    else:
        d[key_name] = e
        
        
    return d
    

def uniqfy_query_list(lista):
    new_list =  list(set(lista))
    return new_list