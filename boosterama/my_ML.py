# -*- coding: utf-8 -*-
"""
Created on Sat Jan  9 09:52:49 2016

@author: DataCousins
"""

from colorama import Fore, Back, Style

from my_ML_fx import *
from my_Feature_Importance import *
from my_ExternalScripts import *
from framework.algorithm.factory import AlgorithmFactory
import pickle
import numpy as np




def machine_learning_algorithm(d, results, model_score_cv, val_exists, alg, X_TRAIN__pd, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test, y_test, y_test_exists, N_train, N_val, N_test, fold, fold_idx, score_previous_best_folds):
    
    id_execution = d['ID']
    
    algorithm_factory=AlgorithmFactory()
    algorithm=algorithm_factory.get(d)
    
    # TRANSFORM: y_train, y_val
    ############################
    y_train_original, y_train_transf, y_val_original, y_val_transf = y_transform(d, val_exists, idx_train, y_train, idx_val, y_val)



    # En el caso XGBC hacemos label encode de la 'y' (luego se hará decode) 
#    print("requires_label_encode: ", algorithm.requires_label_encode())
    if algorithm.requires_label_encode():
        y_train_transf, y_val_transf, lab_enc = y_label_encode(val_exists, y_train_transf, y_val_transf)



    # Pasamos a Sparse Xgb o Keras
    ###############################

    # Convertimos de numpy al formato preferido por el algoritmo.  P.e: para XGB convertimos a DMatrix, ...
    
#    X_train, X_val, X_test = xgb_DMatrix_from_numpy(val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf)
    X_train, X_val, X_test = algorithm.convert_to_algorithm_input_format(val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf)


    #Para KERAS convertimos a Sparse
    if alg.upper()[:5] == 'KERAS':
        
        X_train, X_val, X_test = keras_sparse_from_numpy(val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf)




    # ML Algorithm
    ################

    if d['ML_GRIDSEARCHCV_OR_ALGORITHM'].lower() == 'gridsearchcv':  # Execute ML GridSearchCV
        num_folds_CV = d['SPLIT_CV_KFOLDS'] + 2 # Provisionalmente ponemos el num_folds_CV a pelo, como d['SPLIT_CV_KFOLDS'] + 2, ya que por ahora d['SPLIT_CV_KFOLDS']=1 siempre, hasta que terminemos implementar CV
        best_model, best_params, best_parameters, param_original = ml_gridSearchCV(d, results, alg, X_train, X_TRAIN__pd, y_train_transf, num_folds_CV)
        
    elif alg.upper()[:5] == 'KERAS':   # KERAS !!
        best_model, best_params, best_parameters, param_original, y_train_transf_preds, y_val_transf_preds, y_test_transf_preds  = ml_algo_keras(d, alg, val_exists, X_train, y_train_transf, X_val, y_val_transf, X_test, fold)
    
    elif d['LEVEL']=='1_EXTERNAL_SCRIPTS':
        #(idx_test, y_test_preds, N_test, idx_dtype, y_dtype) = external_script_execute(d, results, fold)                             
        #(idx_test, y_test_preds, N_test, idx_val, y_val, y_val_preds, N_val, idx_dtype, y_dtype) = external_script_execute(d, results, fold)
        best_model = ExternalScriptExecutor(d, results, fold)
        best_params, best_parameters, param_original = best_model.execution_results()
        
        
    else: # Execute ML Algorithm
        best_model, best_params, best_parameters, param_original = ml_algorithm(d, alg, val_exists, X_train, X_TRAIN__pd, y_train_transf, X_val, y_val_transf, X_test)        


    # Almacenamos en results los param_original utilizados
    results['alg_param'] = param_original 
    
    
    
    algorithm.set_earlystopping_best_model(best_model, fold)
    results['best_iteration_mean'] = np.round(np.mean(results['best_iteration']), 2)  
    # Para keras usar callback (https://www.kaggle.com/inspector/prudential-life-insurance-assessment/keras-hyperopt-example-sketch)
    
    



    # Print time
    if d['PRINT_TIME']:
        print(Fore.BLUE + 'ELAPSED TIME -  MODEL FIT: ' + str(int(timeit.default_timer()-t0)) + " segundos." +  Style.RESET_ALL)



    # Save model to PICKLE 
    #########################
    if d['ML_MODEL_TO_PICKLE']:
        pickle.dump(best_model, open(d['DIR_OUTPATH_PICKLES']+str(d['ID'])+"_model.pkl", "wb"))



    # PREDICT
    ##############
    if alg.upper()[:5] != 'KERAS':  # Para keras ya tenemos estas tres variables desde antes
        y_train_transf_preds, y_val_transf_preds, y_test_transf_preds = predict(best_model, X_train, X_val, X_test, N_train, N_val, N_test, val_exists, results, alg, d['ML_PREDICT_PROBA'], d['ML_CLASS_PROBA'])                
       #(idx_test, y_test_preds, N_test, idx_val, y_val, y_val_preds, N_val, idx_dtype, y_dtype)

    
    # XGB Y_LABEL_DECODE (xgbc_m)
    #####################
    if algorithm.requires_label_encode():
        y_train_transf, y_train_transf_preds, y_val_transf, y_val_transf_preds, y_test_transf_preds = y_label_decode(lab_enc, val_exists, y_train_transf, y_train_transf_preds, y_val_transf, y_val_transf_preds, y_test_transf_preds)
        



    # RESTRICTIONS: Y TRANSF PREDS
    ###############################
    # Apply restrictions to y_*_transf_preds           
    if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
        y_train_transf_preds, results['rarp_y_train_transf_preds'] = apply_restrictions(y_train_transf_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_train_transf_preds")
        if val_exists:
            y_val_transf_preds, results['rarp_y_val_transf_preds'] = apply_restrictions(y_val_transf_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_transf_preds")
        y_test_transf_preds, results['rarp_y_test_transf_preds'] = apply_restrictions(y_test_transf_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_transf_preds")



    # TRANSF SCORES
    ###############
        
    # Train Transf Score (y_train_transf_preds, y_train_transf)      
    results['model_score_train_transf'] = my_score(y_train_transf, y_train_transf_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train (transf)","y_train_transf", "y_train_transf_preds",d['PRINT_EVAL_INFO'])                    

   
    # Validation Transf Score (y_val_transf_preds, y_val_transf)   
    if val_exists:                    
        results['model_score_val_transf'] = my_score(y_val_transf, y_val_transf_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    

            
    # Inverse transformation y
    y_train, y_train_preds, y_val, y_val_preds, y_test_preds = y_transform_inverse(d, val_exists, y_train_original, y_train_transf, y_train_transf_preds, y_val_original, y_val_transf, y_val_transf_preds, y_test_transf_preds, idx_train, idx_val, idx_test)



    # RESTRICTIONS: Y PREDS
    ########################
    # Apply restrictions to y_train_preds 
    if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:   
        y_train_preds,results['rarp_y_train_preds'] = apply_restrictions(y_train_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_train_preds")
        

    # Print y_info:  y_train_preds                  
    if d['PRINT_Y_INFO']:
        print_info(y_train_preds,"y_train_preds",[d['COMP_COLUMN_CLASS']])


    # Apply restrictions to y_val_preds, y_test_preds
    if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
        if val_exists:
            y_val_preds,results['rarp_y_val_preds'] = apply_restrictions(y_val_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_preds")
        y_test_preds,results['rarp_y_test_preds'] = apply_restrictions(y_test_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_preds")
        
    # Print Info: y_val_preds, y_test_preds            
    if d['PRINT_Y_INFO']:
        if val_exists:
            print_info(y_val_preds,"y_val_preds",[d['COMP_COLUMN_CLASS']])
        print_info(y_test_preds,"y_test_preds",[d['COMP_COLUMN_CLASS']])
        


    # SCORES
    ###############
        
    # Train score   
    results['model_score_train'] = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train","y_train", "y_train_preds",d['PRINT_EVAL_INFO'])

    # Validation score  
    if val_exists:        
        results['model_score_val']                = my_score(y_val, y_val_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])
        model_score_cv[fold]                      = results['model_score_val']
        results['score_validation_by_fold'][fold] = results['model_score_val']

        
    # Test score  
    if y_test_exists:        
        results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Test","y_test", "y_test_preds",d['PRINT_EVAL_INFO'])

            
    return (best_params, best_parameters, best_model, y_train, y_train_preds, y_train_transf, y_train_transf_preds, y_val, y_val_preds, y_val_transf, y_val_transf_preds, y_test_preds)




""" ¿OBSOLETO?

def machine_learning_GridSearchCV(d, results, val_exists, alg, X_TRAIN__pd, X_train, y_train, X_val, y_val, X_test, N_train, N_val, N_test):
    
    id_execution = d['ID']
    algorithm_factory=AlgorithmFactory('algorithms')
    algorithm=algorithm_factory.get(alg)
    
    # Transform (y_train, y_val)
    y_train_original, y_train_transf, y_val_original, y_val_transf = y_transform(d, val_exists, idx_train, y_train, idx_val, y_val)
    
        
    # Para algunos algoritmos como XGBC hacemos label encode        
    if algorithm.requires_label_encode():
        y_train_transf, y_val_transf, lab_enc = xgbc_label_encode(val_exists, y_train_transf, y_val_transf)
    
    
    # Convertimos de numpy al formato preferido por el algoritmo.  P.e: para XGB convertimos a DMatrix, ...
    
#    X_train, X_val, X_test = xgb_DMatrix_from_numpy(val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf)
        X_train, X_val, X_test = algorithm.convert_to_algorithm_input_format(val_exists, X_train, X_val, X_test, y_train_transf, y_val_transf)
        

    best_model, best_params, best_parameters = ml_gridSearchCV(d, results, alg, X_train, y_train_transf, d['SPLIT_CV_KFOLDS'])
    

    if alg.upper()[:3] == 'XGB':
        #results['score_validation_by_fold'].append(best_model.best_score)
        results['best_iteration'].append(best_model.best_iteration)
        print('best_model.best_score',best_model.best_score)
        print('best_model.best_iteration',best_model.best_iteration)
        try:
            results['best_ntree_limit'].append(best_model.best_ntree_limit)
            print('best_model.best_ntree_limit',best_model.best_ntree_limit)
        except:
            print('Tu versión de xgboost está obsoleta. best_model no devuelve el parámetro best_ntree_limit, por lo que la predicción no se ha realizado en la mejor iteración, sino early_stopping_rounds despues')

          
    # Print time
    if d['PRINT_TIME']:
        print(Fore.BLUE + 'ELAPSED TIME -  MODEL FIT: ' + str(int(timeit.default_timer()-t0)) + " segundos." +  Style.RESET_ALL)
      
      
    # Save model to PKL 
    if d['ML_MODEL_TO_PICKLE']:
        pickle.dump(best_model, open(d['DIR_OUTPATH_PICKLES']+str(id_execution)+"_model.pkl", "wb"))


    # Feature Importance      
    #if d['ML_FEATURE_IMPORTANCE']:
    #    feature_importance(d, alg, best_model, X_TRAIN__pd, X_train, y_train) 


    # Predictions
    y_train_transf_preds, y_val_transf_preds, y_test_transf_preds = predict(best_model, X_train, X_val, X_test, N_train, N_val, N_test, val_exists, results, alg, d['ML_PREDICT_PROBA'], d['ML_CLASS_PROBA'])                
    
    
    # Label DECODE for XGboost Classification Multilabel
    if alg.upper() == "XGBC_M":
        y_train_transf, y_train_transf_preds, y_val_transf, y_val_transf_preds, y_test_transf_preds = xgbc_label_decode(lab_enc, val_exists, y_train_transf, y_train_transf_preds, y_val_transf, y_val_transf_preds, y_test_transf_preds)
        

    # Apply restrictions to y_*_transf_preds           
    if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
        y_train_transf_preds, results['rarp_y_train_transf_preds'] = apply_restrictions(y_train_transf_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_train_transf_preds")
        if val_exists:
            y_val_transf_preds, results['rarp_y_val_transf_preds'] = apply_restrictions(y_val_transf_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_transf_preds")
        y_test_transf_preds, results['rarp_y_test_transf_preds'] = apply_restrictions(y_test_transf_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_transf_preds")


    # Train Transf Score (y_train_transf_preds, y_train_transf)      
    results['model_score_train_transf'] = my_score(y_train_transf, y_train_transf_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train (transf)","y_train_transf", "y_train_transf_preds",d['PRINT_EVAL_INFO'])                    

   
    # Validation Transf Score (y_val_transf_preds, y_val_transf)   
    if val_exists:                    
        results['model_score_val_transf'] = my_score(y_val_transf, y_val_transf_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    

            
    # Inverse transformation y
    y_train, y_train_preds, y_val, y_val_preds, y_test_preds = y_transform_inverse(d, val_exists, y_train_original, y_train_transf, y_train_transf_preds, y_val_original, y_val_transf, y_val_transf_preds, y_test_transf_preds, idx_train, idx_val, idx_test)


    # Test Score (if exists)   
    if y_test_exists:                    
        results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    

    
    # Apply restrictions to y_train_preds 
    if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:   
        y_train_preds,results['rarp_y_train_preds'] = apply_restrictions(y_train_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_train_preds")
        

    # Print y_info:  y_train_preds                  
    if d['PRINT_Y_INFO']:
        print_info(y_train_preds,"y_train_preds",[d['COMP_COLUMN_CLASS']])
    
    
    return (best_params, best_parameters, best_model, best_model, y_train, y_train_preds, y_train_transf, y_train_transf_preds, y_val, y_val_preds, y_val_transf, y_val_transf_preds, y_test_preds)
    
    
"""