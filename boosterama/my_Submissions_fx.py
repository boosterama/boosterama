# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 18:33:47 2016

@author: DataCousins
"""

import os
import pandas as pd

########################
###  CSV Subm Save   ###
########################
def submission_file_generate(d, idx_test,y_test_preds, CUSTOM_SUBMISSION_CSV,dir_out_path,submission_name,COLUMN_ID,COLUMN_CLASS):
        
    idx_test__s = pd.Series(data=idx_test)
    idx_test__s.name = COLUMN_ID
    
    y_test_preds__s = pd.Series(data=y_test_preds)
    if d['COMP_COLUMN_CLASS_4_SUBMISSION'] == '':
        y_test_preds__s.name = COLUMN_CLASS
    else:
        y_test_preds__s.name = d['COMP_COLUMN_CLASS_4_SUBMISSION']
    
    submission = pd.concat([idx_test__s,y_test_preds__s],axis=1)
    
    if d['SYS_DO_CUSTOM_SUBMISSION_CSV']:
        CUSTOM_SUBMISSION_CSV(d, submission, idx_test,y_test_preds,dir_out_path,submission_name,COLUMN_ID,COLUMN_CLASS)
    else:
        submission.to_csv(dir_out_path + submission_name, index=False,header=True)

    #print("submission guardada como: " + dir_out_path + submission_name)
    
    return None


    
    
########################
###  CSV Subm Utils  ###
########################
def submission_name_generate(d,id_execution,mod_Score_4_sub,best_parameters, execution_CV_stopped, split_cv_folds_orig, execution_CV_stopped_last_fold):
    
    
    ####################
    # Software Version
    ####################
    try:
        with open('VERSION', 'r') as f:
            soft_Version = str(f.readline())
    except:
        soft_Version = 'Ver-Unknown'
    #soft_Version = str(os.path.dirname(os.path.abspath(__file__)))[-5:].replace("_","")
    
    
    ####################
    # Algoritmo
    ####################
    if d['LEVEL']=='2_ENSEMBLE':
        algoritmo = 'ENSEMBLE'
    else:
        algoritmo = [k for k in d['ML_ALGO_PARAMETROS'].keys()][0]
               
    
    
    ####################
    # Split
    ####################

    
    # execution_CV_stopped 
    ######################
    if execution_CV_stopped == 0:
        execution_CV_stopped_string = ""
    else:
        execution_CV_stopped_string = "-F" + str(execution_CV_stopped_last_fold) + "of" + str(split_cv_folds_orig)
        
    '''
    # previous_split
    #################
    if d['LEVEL'][0:2] == '2_' and d['L2_CV_OR_DATASET'] == 'DATASET':
        el_dataset = d['L2_LEVEL1_DATASETS_TRAIN']
        
        if "TrainVal" in el_dataset:  
            
            indice_trainval = el_dataset.index("TrainVal", 0,222)
            train_val_size = el_dataset[indice_trainval+8:indice_trainval+14].split('%')
            
            train_percent = str(int(train_val_size[0]))+'%'
            val_percent = str(int(train_val_size[1]))+'%'
            #previous_split = "TrainVal" + train_percent + val_percent
            previous_split = el_dataset.split('_')[3]
        else:
            previous_split = el_dataset.split('_')[-1]
            
    else:
        previous_split = ''    
    '''
        
        
    # train%  val%  for split
    ##########################
    
    # Timeseries
    if d['SPLIT_CV_TYPE'] == 'TimeSeries':
        # 'TS_CV_ACADEMIC', 'TS_CV_TRAIN-ORIGIN_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
        if 'VAL-FIX' in d['SPLIT_TS_TYPE']:
            if 'NO-OVERLAP' in d['SPLIT_TS_TYPE']:
                ts_train_size = "Train=" + str(d['SPLIT_TS_train_FIX_NUM']) + "-NoOverlap"
                ts_val_size   = "Val=" + str(d['SPLIT_TS_VALIDATION_FIX_NUM']) + "-NoOverlap"
            else:
                ts_train_size = "Train=" + str(d['SPLIT_TS_train_FIX_NUM'])
                ts_val_size   = "Val=" + str(d['SPLIT_TS_VALIDATION_FIX_NUM'])
        else:  
            ts_val_size = "Acad"
            
        VAL_TS_name = "ValTS-" + ts_val_size  + execution_CV_stopped_string
        CV_TS_name = "CVTS" + str(d['SPLIT_CV_KFOLDS'])  +  "-" + ts_val_size  
    '''
    # NOT Timeseries
    else:  
        if d['SPLIT_CV_KFOLDS'] == 1:
            # Train-Val (Validation percentaje)
            train_percent = str(int(100-100*float(d['SPLIT_VALIDATION_SET_PERC'])))+'%'
            val_percent = str(int(100*float(d['SPLIT_VALIDATION_SET_PERC'])))+'%' + execution_CV_stopped_string
        else:
            # Train-Val (Validation percentaje)
            train_percent = str(100)+'%'
            val_percent = str(0)+'%'
    '''      


    # Split
    #################            
    
    # Level 2
    if d['LEVEL']=='2_ENSEMBLE' and d['L2_CV_OR_DATASET'] == 'CV':
        #if d['L2_CV_OR_DATASET'] == 'CV':
        split = "CV=1" 
        
        #elif d['L2_CV_OR_DATASET'] == 'DATASET':
        #split = previous_split   
            
    elif d['LEVEL']=='2_ENSEMBLE' and d['L2_CV_OR_DATASET'] == 'DATASET':
        el_dataset = d['L2_LEVEL1_DATASETS_TRAIN']
        
        if "TrainVal" in el_dataset:             
            indice_trainval = el_dataset.index("TrainVal", 0, 222)
            train_val_size = el_dataset[indice_trainval+8:indice_trainval+14].split('%')

            val_percent = str(float(train_val_size[1])/100).replace(".","") 
            
            if d['SPLIT_CV_TYPE'] == 'TimeSeries':
                split = "ValTS=" + val_percent
            else:
                split = "Val=" + val_percent
            #split = el_dataset.split('_')[3]
        else:
            split = 'CV=1'
    
    # Padre
    elif d['SPLIT_CV_KFOLDS'] == 0:
        split = 'Val=0'
    elif d['SPLIT_CV_KFOLDS']==1 and d['SPLIT_VALIDATION_SET_PERC']==0.0:
        split = 'Val=0'
    
        
    # Validation set
    elif d['SPLIT_CV_KFOLDS']==1 and d['SPLIT_VALIDATION_SET_PERC']!=0.0:
        # Timeseries Validation set:
        if d['SPLIT_CV_TYPE'] == 'TimeSeries':
            split = VAL_TS_name
        # 'normal' Validation set:
        else:
            split = 'Val='+str(d['SPLIT_VALIDATION_SET_PERC']).replace(".","")  + execution_CV_stopped_string

            
    # Crossvalidation
    elif d['SPLIT_CV_KFOLDS']>=2:
        if d['SPLIT_CV_TYPE'] == 'TimeSeries':
            split = "CVTS=" + str(d['SPLIT_CV_KFOLDS'])
            
            # 'TS_CV_ACADEMIC', 'TS_CV_TRAIN-ORIGIN_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
            if 'VAL-FIX' in d['SPLIT_TS_TYPE']:
                if 'NO-OVERLAP' in d['SPLIT_TS_TYPE']:
                    split = "CVTS=" + str(d['SPLIT_CV_KFOLDS']) + "-train" + str(d['SPLIT_TS_train_FIX_NUM']) + "-Val" + str(d['SPLIT_TS_VALIDATION_FIX_NUM']) + "-NoOver"
                else:
                    if 'TRAIN-FIX' in d['SPLIT_TS_TYPE']:
                        split = "CVTS=" + str(d['SPLIT_CV_KFOLDS']) + "-train" + str(d['SPLIT_TS_train_FIX_NUM']) + "-Val" + str(d['SPLIT_TS_VALIDATION_FIX_NUM'])
                    else:
                        split = "CVTS=" + str(d['SPLIT_CV_KFOLDS']) + "-trainOrig-Val" + str(d['SPLIT_TS_VALIDATION_FIX_NUM'])
            else:  
                split = "CVTS=" + str(d['SPLIT_CV_KFOLDS']) + "-Acad"
        
        
            
            
        else:
            split = 'CV=' + str(d['SPLIT_CV_KFOLDS'])
        
    str_meta=""
    if 'FRIENDLY_ID' in d and 'HASHTAGS' in d and isinstance(d['HASHTAGS'], list):
        str_meta="({},{})".format(d['FRIENDLY_ID'], "_".join(sorted(d['HASHTAGS'])))
        str_meta=str_meta[:30] # truncate if len >30

    ####################
    ## Submission Name
    ####################
        
    y_transform_str="".join([c for c in str(d['ML_PRP_Y_TRANSFORM'])  if c.isalpha() or c.isdigit() or c in (' ', '_', '-', ';', '.', ',')]).strip()
    submission_name_pre = mod_Score_4_sub + "_" + split + "_" + algoritmo + "_" + y_transform_str + "_" + d['COMP_EVALUATION_METRIC']  + "_" + soft_Version + "_L"+ str(d['LEVEL'])
    submission_name = str(submission_name_pre.replace(".",""))[0:235] + "_"+ str(id_execution) + str_meta + ".csv"

    return submission_name