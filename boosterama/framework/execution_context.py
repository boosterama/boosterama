#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  1 14:22:22 2018

@author: virilo
"""

from framework.metaclasses import singleton


@singleton
class ExecutionContext():  
    def __init__(self):
        self.contexts={}
        self.dashboard_ids=[]
        
    def clean(self, dashboard):
        #del(self.contexts[dashboard['ID']])
        #self.dashboard_ids.remove(dashboard['ID'])
        
        
        if dashboard['ID'] in self.contexts:
            del(self.contexts[dashboard['ID']])
        else:
            print("WARNING. ExecutionContext. {} wasn't in self.contexts !!!".format(dashboard['ID']))
            
        if dashboard['ID'] in self.dashboard_ids:
            self.dashboard_ids.remove(dashboard['ID'])
        else:
            print("WARNING. ExecutionContext. {} wasn't in self.dashboard_ids !!!".format(dashboard['ID']))
        
    def create_context(self, d, dXT, results):
        self.contexts[d['ID']]=(d, dXT, results)
        self.dashboard_ids.append(d['ID'])
        
    def get(self, dashboard):
        d, dXT, results = self.contexts[d['ID']]
        return dXT, results
    
    def get_by_id(self, dashboard_id):
        d, dXT, results = self.contexts[dashboard_id]
        return d, dXT, results
    
    def get_current_context(self):
        d, dXT, results = self.get_by_id(self.dashboard_ids[-1])
        return d, dXT, results
    
    

        
