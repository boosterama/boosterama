#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 30 20:11:36 2017

@author: virilo
"""

from framework.metaclasses import singleton
from config.config_manager import ConfigurationLoader
from framework.execution_context import ExecutionContext

import glob, os, ast
from pathlib import Path
import chardet
#import inspect

import imp
import sys
#import importlib



SCRIPT_RELATIVE_PATH='/framework/algorithm/factory.py'

def read_package(package_name):
    file, path, description = imp.find_module(package_name) # file, full_path, description
    full_path = os.path.abspath(path)
    
    
    modules_path_list = [module for module in glob.glob(full_path+"/*.py") if not module.endswith('__init__.py')]
    
    modules_list=[]
    
#    print("os.path.realpath(__file__): ", )
    this_script_path=os.path.realpath(__file__)
    boosterama_root_path=os.path.realpath(__file__)
    
    sys_path = sys.path.copy() + [os.getcwd()]
    for p in sys_path:
        sys_path=sys_path+[os.path.realpath(p)]
    if boosterama_root_path.endswith(SCRIPT_RELATIVE_PATH):
        sys_path = sys_path + [this_script_path[:-len(SCRIPT_RELATIVE_PATH)]]
    sys_path = set(sys_path)
    sys_path = sorted(sys_path , key = len)
    
    for module_path in modules_path_list:
#        print('module_path: ', module_path)
        module_name=module_path
        for path in reversed(sys_path):
            if module_name.startswith(path):
                module_name=module_name[len(path):]
        
        module_name=module_name.replace('\\','.')
        module_name=module_name.replace('/','.')
        if module_name[0] in ('.', '\\', '/'):
            module_name=module_name[1:]
        if module_name.endswith(".py"):
            module_name=module_name[:-3]           
        
        modules_list.append((module_name, module_path))
        
    
    
    return modules_list


def labelled_classnames(module, full_path, label="algorith"):
    module = ast.parse("logging.config")
#    print("ok!")
    
    rawdata = open(full_path, 'rb').read()
    result = chardet.detect(rawdata)
    encoding = result['encoding']
    content = Path(full_path).read_text(encoding=encoding)
    
#    print(module)
#    print(full_path)
    
    module = ast.parse(content)
    for node in ast.walk(module):
#        print("=========>>>> ", node.__class__.__name__)
        if not isinstance(node, ast.ClassDef):
            continue
        
        if any(isinstance(n, ast.Name) and n.id == label for n in node.decorator_list):
            print("ERROR: Skipped "+node.name+". Annotation "+ label+ " requires 1 positional parameter.  Example: @"+label+"('PEPE')")
        for n in node.decorator_list:
            if isinstance(n, ast.Call):
                yield node.name, n.args[0].s
#            print(n.func.id)
#            print(n.args[0].s)

@singleton
class AlgorithmFactory():  
    def __init__(self):
        
        boosterama_config = ConfigurationLoader().get_config()
        declarations_module=boosterama_config.algorithm_factory_sources
        declarations_path=declarations_module.replace(".", "/")
        self.declarations={}
        self.instances={}
        
        for module, filename in read_package(declarations_path):
#            print (module)
#            print(filename)
            
            for class_name, tags in labelled_classnames(module, filename):
                if isinstance(tags,str):
                    tags=tags.split("|")
                
                for tag in tags:
                    self.declarations[tag]=(module, class_name)
#                print('--->',class_name, ", ",  tags)
        
        
    def get(self,dashboard):
#        print(inspect.getmembers(testpkg, inspect.ismodule))

        global ugly_temp_var;
        
        dashboard_id=dashboard.get('ID', None)
        
        if dashboard_id in self.instances:
            return self.instances[dashboard_id]
        
        
        
        tag=dashboard['ML_ALGO'][0]
    
        if tag not in self.declarations:
            tag="--default--"
        
        
        element=None
        if tag in self.declarations:
            package_name, class_name=self.declarations[tag]
            
#            globals().update(importlib.import_module(package_name).__dict__)
            
#            constructor_sentence = "ugly_temp_var="+class_name+"();"
            constructor_sentence = "from " + package_name + " import " + class_name + ";ugly_temp_var="+class_name+"();"
            
            print(constructor_sentence)
            exec(constructor_sentence, globals() )
#            element=ret['element']
            element=ugly_temp_var
            element.algorithm_id=tag
            
            del(ugly_temp_var)
            
            if dashboard_id is not None:
                element.dashboard, element.dXT, element.results = ExecutionContext().get_by_id(dashboard_id)
                element.col_names=None
     
                self.instances[dashboard_id] = element
        
        return element
    
    def clean(self, dashboard):
        if 'ID' in dashboard and dashboard['ID'] in self.instances:
            del(self.instances[dashboard['ID']])
        else:
            print("WARNING. AlgorithmFactory.clean. {} wasn't in self.instances !!!".format(dashboard['ID']))
