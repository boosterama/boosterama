# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 21:25:03 2017

@author: virilo.tejedor
"""

def singleton(cls):
    instances = {}
    def getinstance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return getinstance


# -------------------------------------------
#
#                ANNOTATIONS
#
# -------------------------------------------

def algorithm(logging_level):
    def class_decorator(cls_to_decorate):
        '''
        the_module = cls_to_decorate.__module__
        setattr(cls_to_decorate, 'attr1', 1234)
        '''
        return cls_to_decorate
    return class_decorator

