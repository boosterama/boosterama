# -*- coding: utf-8 -*-
"""
Created on Sun Oct 25 07:53:35 2015

@author: DataCousins
"""

import numpy as np
import pandas as pd
import pytz
from sklearn import metrics
import sys

from my_Evaluation_Metrics import *
from my_Input_Output import *




def ensemble(predictions, coefficient_factors):
	ensemble = weighted_sum(predictions, coefficient_factors)

	total_coefficients = float(coefficient_factors.sum(0))
	ensemble = ensemble / total_coefficients

	ensemble = ensemble[:, None].T

	return ensemble

def weighted_sum(predictions, coefficient_factors):
	weighted_sum = predictions * coefficient_factors[:, None]
	weighted_sum = weighted_sum.sum(0)

	
	return weighted_sum


def is_equal_or_better_score(old_score, new_score, greater_is_better):

	if greater_is_better:
		return old_score <= new_score 
	else:
		return old_score >= new_score 
  

def caruana_selection_ensemble(columns_label_ordered, d, X_test_transp, X_train_transp, y_train_transp, val_exists, X_val_transp,idx_train,idx_test):
    '''
    X_test_transp - array[num selected executions, num test observations]
    X_train_transp - array[num selected executions, num training observations]
    y_train_transp - array [1, num training observations]
    '''
    
    greater_is_better = d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']
    y_train = np.transpose(y_train_transp)
    y_train = np.reshape(y_train,(-1,))
    
    re_sort=False
    indexes = list(range(len(columns_label_ordered)))
    
    score_sort = []
    if d['L2_CARUANA_SORT']=='score':
        re_sort=True
        
        i=0 # Coefficient index
        for id in columns_label_ordered:
            score = my_score(y_train, X_train_transp[i],d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train Caruana","y_train","y_train_preds_ensemble",d['PRINT_EVAL_INFO'])
            score_sort.append((id, score))
            i+=1
        
        score_sort__pd=pd.DataFrame(np.array(score_sort))
        score_sort__pd.columns=["id", "score"]
        score_sort__pd.sort_values(by=['score'], ascending=[not greater_is_better], inplace=True)
        
        #re_sorted_columns_label =  score_sort__pd['id'].values
        indexes = score_sort__pd.index.values
        
        print ("Resorted indexes: " + str(indexes))
        
        num_top_executions = min(9999,len(indexes))
        print ("Nuevo top "+ str(num_top_executions) + " según validation score:")
        executed_dashboards = pickle_load_all_dashboards_to_dict(d['DIR_BASE'])
        for i in list(range(0,num_top_executions)):
            id=int(score_sort__pd["id"].values[indexes[i]])
            script_filename = executed_dashboards[id]['EXTERNAL_SCRIPTS_IMPORT']
            print (script_filename + " ; " + str(indexes[i]) + " , " + str(id) + " , " + script_filename)
    
    ensemble_score = float ("-Inf")
    if not greater_is_better:
        ensemble_score = float ("Inf")
    
    #
    # ################## COEFFICIENTS INITILIZATION
    #
    coefficient_factors = np.zeros(len(columns_label_ordered))
    caruana_initialization = d['L2_CARUANA_INITIALIZATION']
    if len(caruana_initialization)>len(coefficient_factors):
        print("ERROR: CARUANA_INITIALIZATION was longer than number of predictions in X_train")
        caruana_initialization = caruana_initialization[0:len(coefficient_factors)]
    elif len(caruana_initialization)<len(coefficient_factors):
        caruana_initialization = caruana_initialization + list(np.zeros(len(coefficient_factors)-len(caruana_initialization)))
    
    caruana_initialization = [float(i) for i in caruana_initialization]
    coefficient_factors = coefficient_factors + np.array(caruana_initialization)
        
        
        
    
    
    rounds = 0
    rounds_withouth_improvements = 0
    do_ensemble = True
    y_train_weighted_sum = None
    while do_ensemble:
    
        	i=0 # Coefficient index
        	num_changes_on_this_round = 0
        	for i in indexes:
        		new_coefficient_factors = coefficient_factors.copy()
        		new_coefficient_factors[i] += 1
        		
        		if y_train_weighted_sum is None:
        		    y_train_weighted_sum_transp = weighted_sum(X_train_transp, coefficient_factors)
        		    y_train_weighted_sum = np.transpose(y_train_weighted_sum_transp)
        		    y_train_weighted_sum = np.reshape(y_train_weighted_sum,(-1,))
        		
        		new_y_train_weighted_sum = X_train_transp[i,] + y_train_weighted_sum
        		y_train_preds = new_y_train_weighted_sum / float(new_coefficient_factors.sum(0))
        		
        		
        		
          
        		#new_score = my_score(y_train_transp, y_train_transp_preds_ensemble,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train Caruana","y_train","y_train_preds_ensemble",d['PRINT_EVAL_INFO'])
        		new_score = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train Caruana","y_train","y_train_preds_ensemble",d['PRINT_EVAL_INFO'])
        		
        		if is_equal_or_better_score(ensemble_score, new_score, greater_is_better):
        			ensemble_score = new_score
        			coefficient_factors = new_coefficient_factors
        			y_train_weighted_sum = new_y_train_weighted_sum
        			num_changes_on_this_round +=1
        
        	
        	rounds += 1
        	if rounds>=d['L2_CARUANA_MAX_ROUNDS']:
        		do_ensemble = False
        	#elif num_changes_on_this_round > 0:
        	#	rounds_withouth_improvements =0
        	elif num_changes_on_this_round == 0:
        		rounds_withouth_improvements += 1
        		
        		if rounds_withouth_improvements >= d['L2_CARUANA_MAX_ROUNDS_WITHOUT_IMPROVEMENTS']:
        			do_ensemble = False
        		else:
        			coefficient_factors = coefficient_factors * 2
        			y_train_weighted_sum = y_train_weighted_sum * 2
    
    
    
    y_train_preds_ensemble = ensemble(X_train_transp, coefficient_factors)
    
    if val_exists:
        y_val_preds_ensemble = ensemble(X_val_transp, coefficient_factors)
    else:
        y_val_preds_ensemble = []
        
    y_test_preds_ensemble = ensemble(X_test_transp, coefficient_factors)
    
     
    #print ("Ensemble done in " + str(rounds) + " rounds." + str("  After " + str(rounds_withouth_improvements) + " rounds without improvements" if rounds_withouth_improvements >= d['L2_CARUANA_MAX_ROUNDS_WITHOUT_IMPROVEMENTS'] else ""))
    
    #print ("Coeficients: " + str (coefficient_factors))

    total_coefficients = float(coefficient_factors.sum(0))
    normalized_coefficients = [c / total_coefficients for c in coefficient_factors]
    
    print ("")    
    print ("Aplicado algoritmo CARUANA")
    print ("Rounds: " + str(rounds) + ", Coeficients: " + str(normalized_coefficients))
    #print ("Coeficients as fractional numbers: " + str([str(int(c)) + "/" + str(int(total_coefficients)) for c in coefficient_factors]))
    print ("Debug: coeficients before normalization: " + str(coefficient_factors))

     
    return y_train_preds_ensemble, y_test_preds_ensemble, y_val_preds_ensemble, normalized_coefficients, rounds 

#                                       (columns_label_ordered, d, X_test_transp, X_train_transp, y_train_transp, X_val_transp,idx_train,idx_test):
def caruana_selection_ensemble_by_blocks(test_idx, X_test_transp, test_ensemble_block, X_train_transp, y_train_transp, val_ensemble_block, d, columns_label_ordered=None):

    unique_ids = list(set(test_ensemble_block))
    
    y_train_transp = np.reshape(y_train_transp.T, y_train_transp.shape[1])
    
    if columns_label_ordered is None:
        columns_label_ordered = range(X_test_transp.shape[0])
    column_names = [("y_test_preds_" + str(predictions_id)) for predictions_id in columns_label_ordered]

    test_predictions__pd = pd.DataFrame(data=X_test_transp.T, columns=column_names )
    test_predictions__pd['idx'] = pd.Series(test_idx, index=test_predictions__pd.index)
    test_predictions__pd['ensemble_block'] = pd.Series(test_ensemble_block, index=test_predictions__pd.index)
    
    val_predictions__pd = pd.DataFrame(data=X_train_transp.T, columns=column_names )    
    val_predictions__pd['y_train_transp'] = pd.Series(y_train_transp, index=val_predictions__pd.index)
    val_predictions__pd['ensemble_block'] = pd.Series(val_ensemble_block, index=val_predictions__pd.index)
    
    
    y_test_preds_ensemble__pd = None
    for block_id in unique_ids:
        print('Caruana for block ' + str(block_id) + ": ", end="")
        test_block__pd = test_predictions__pd.loc[test_predictions__pd['ensemble_block'] == block_id]
        val_block__pd = val_predictions__pd.loc[val_predictions__pd['ensemble_block'] == block_id]
        
        test_block_idx = test_block__pd['idx'] 
        test_block_predictions = test_block__pd.as_matrix(columns=column_names).T
        
        y_train_transp_block = val_block__pd['y_train_transp'] 
        val_block_predictions = val_block__pd.as_matrix(columns=column_names).T
                
        # Incompleto VIRILO
        y_test_preds_ensemble_block, y_train_transp_preds_ensemble_block, block_normalized_coefficients  = caruana_selection_ensemble(test_block_predictions, val_block_predictions, y_train_transp_block, d)
        y_test_preds_ensemble_block = np.reshape(y_test_preds_ensemble_block.T, y_test_preds_ensemble_block.shape[1])
        
        y_test_preds_ensemble_block__pd = pd.DataFrame({'idx':test_block_idx, 'y_test_preds_ensemble':y_test_preds_ensemble_block})
        
        if y_test_preds_ensemble__pd is None:
            y_test_preds_ensemble__pd = y_test_preds_ensemble_block__pd
        else:
            y_test_preds_ensemble__pd = pd.concat([y_test_preds_ensemble__pd, y_test_preds_ensemble_block__pd])
        
        
    
    y_test_preds_ensemble__pd.sort(['idx'], ascending=[True], inplace=True)
    
    y_test_preds_ensemble = y_test_preds_ensemble__pd['y_test_preds_ensemble'].values[:, None].T
    
    print ("ENSEMBLE BY BLOCKS!!!")
    
    return y_test_preds_ensemble

