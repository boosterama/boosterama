# -*- coding: utf-8 -*-

############################################
### IMPORTS - Respetar ORDEN ALFABÉTICO  ###
############################################


import numpy as np
import scipy
import sys



#########################################
###    Funciones (orden alfabético)   ###
#########################################



def print_import_info(trainNumRowsOriginal,trainNumRowsAfterSampling,testNumRowsOriginal,testNumRowsAfterSampling,TRAIN_SUBSET_TYPE,TEST_SUBSET_TYPE):
    
    if TRAIN_SUBSET_TYPE != 'allSamples':
        print ("Rows in train.csv:", trainNumRowsOriginal,"Rows in train (after sampling):", trainNumRowsAfterSampling)
    else: 
        print ("Rows in train.csv:", trainNumRowsOriginal,"We are NOT subsampling the Train set")
        
    
    if TEST_SUBSET_TYPE != 'allSamples':
        print ("Rows in test.csv:", testNumRowsOriginal,"Rows in test (after sampling):", testNumRowsAfterSampling)
    else: 
        print ("Rows in test.csv:", testNumRowsOriginal,"We are NOT subsampling the Test set")
        
    print()



def print_info(a, a_name, a_cols_names):
    if (len(a_cols_names)>1 or (len(a_cols_names)==1 and (str(a.dtype)[:3]=='int' or str(a.dtype)[:3]=='flo'))):
        d = scipy.stats.describe(a, axis=0, ddof=1)
        print(a_name,"  Observaciones =",d.nobs)
        print("   {f:^20s} {ty:12s} {mi:^11s} {ma:^11s} {me:^11s} {va:^11s} {sk:^11s} {ku:^11s} {ze:^7s} {nz:^7s} {na:^7s}".format(f='Feature',ty='Type',mi='Min',ma='Max',me='Mean',va='Variance',sk='Skewness',ku='Kurtosis',ze='Zero',nz='NonZero',na='NAs'))
       
        if str(type(a)) == "<class 'pandas.core.frame.DataFrame'>":  
     
            if (len(a_cols_names)==1):
                Zeros = (a == 0.0).astype(int).sum()
                NonZeros = (a != 0.0).astype(int).sum()
                numNAs = np.sum(a.isnull())
                print("   {f:20s} {ty:12s} {mi: 11.5f} {ma: 11.5f} {me: 11.5f} {va: 11.5f} {sk: 11.5f} {ku: 11.5f} {ze: 7d} {nz: 7d} {na: 7d}".format(f=a_cols_names[0],ty=str(a.dtype),mi=d.minmax[0],ma=d.minmax[1],me=d.mean,va=d.variance,sk=d.skewness,ku=d.kurtosis,ze=Zeros,nz=NonZeros,na=numNAs))
            else:
                for j in range(len(a_cols_names)):
                    if (a[a_cols_names[j]].dtype != "object"):
                        Zeros = (a[a_cols_names[j]] == 0.0).astype(int).sum()
                        NonZeros = (a[a_cols_names[j]] != 0.0).astype(int).sum()
                    else:
                        Zeros = None
                        NonZeros = None
                    numNAs = np.sum(a[a_cols_names[j]].isnull())
                    print("   {f:20s} {ty:12s} {mi: 11.5f} {ma: 11.5f} {me: 11.5f} {va: 11.5f} {sk: 11.5f} {ku: 11.5f} {ze: 7d} {nz: 7d} {na: 7d}".format(f=a_cols_names[j],ty=str(a[a_cols_names[j]].dtype),mi=d.minmax[0][j],ma=d.minmax[1][j],me=d.mean[j],va=d.variance[j],sk=d.skewness[j],ku=d.kurtosis[j],ze=Zeros,nz=NonZeros,na=numNAs))
        
        elif str(type(a)) == "<class 'numpy.ndarray'>": 
            
            if (len(a_cols_names)==1):
                Zeros = (a == 0.0).astype(int).sum()
                NonZeros = (a != 0.0).astype(int).sum()
                numNAs = np.sum(np.isnan(a))
                print("   {f:20s} {ty:12s} {mi: 11.5f} {ma: 11.5f} {me: 11.5f} {va: 11.5f} {sk: 11.5f} {ku: 11.5f} {ze: 7d} {nz: 7d} {na: 7d}".format(f=a_cols_names[0],ty=str(a.dtype),mi=d.minmax[0],ma=d.minmax[1],me=d.mean,va=d.variance,sk=d.skewness,ku=d.kurtosis,ze=Zeros,nz=NonZeros,na=numNAs))
            else:
                for j in range(len(a_cols_names)):
                    Zeros = (a[:,j] == 0.0).astype(int).sum()
                    NonZeros = (a[:,j] != 0.0).astype(int).sum()
                    numNAs = np.sum(np.isnan(a[:,j]))
                    print("   {f:20s} {ty:12s} {mi: 11.5f} {ma: 11.5f} {me: 11.5f} {va: 11.5f} {sk: 11.5f} {ku: 11.5f} {ze: 7d} {nz: 7d} {na: 7d}".format(f=a_cols_names[j],ty=str(a[:,j].dtype),mi=d.minmax[0][j],ma=d.minmax[1][j],me=d.mean[j],va=d.variance[j],sk=d.skewness[j],ku=d.kurtosis[j],ze=Zeros,nz=NonZeros,na=numNAs))
        else: sys.exit("Error print_info")
        
        print("")
    
    
def print_numeric_and_nonnumeric_variables(Xy_TRAIN__pd,X_test__pd,train_numeric_variables,train_non_numeric_variables,test_numeric_variables,test_non_numeric_variables):
    print ("\tTRAIN - Numeric Variables (" + str(len(train_numeric_variables))+"):")    
    print (train_numeric_variables)
    print ("")      
    print ("\tTRAIN - Non Numeric Variables ("+str(len(train_non_numeric_variables))+"):")    
    print (train_non_numeric_variables)
    print ("")  
    print ("")   

    print ("\tTEST - Numeric Variables (" + str(len(test_numeric_variables))+"):")    
    print (test_numeric_variables)
    print ("")      
    print ("\tTEST - Non Numeric Variables ("+str(len(test_non_numeric_variables))+"):")    
    print (test_non_numeric_variables)
    print ("")   

    print_Separador()   

    print ("\tNumeric Variables - Head:") 
    print ("")      
    print (Xy_TRAIN__pd[train_numeric_variables].head())
    print ("")   


    print ("\tNonNumeric Variables - Head:") 
    print ("")      
    print (Xy_TRAIN__pd[train_non_numeric_variables].head())
    print ("")   

    print_Separador()   
    
    print ("\tNumeric Variables - train.describe():")   
    print ("")    
    print(Xy_TRAIN__pd.describe())    
    print ("")   
    print ("")   



    
def print_Separador():
    print ("")     
    print ("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.")
    print ("")  

def print_types(df):
    fields = list(df.columns.values)           
    for field in fields:
        print(field,df[field].dtype)