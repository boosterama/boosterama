# -*- coding: utf-8 -*-

"""  ensemble """

####################################
###          Imports             ###
####################################

from colorama import Fore, Back, Style
from datetime import datetime, timedelta 
import numpy as np
import os
from pandas.io.parsers import read_csv
import pandas as pd
from scipy import stats
from scipy.optimize import minimize
from sklearn.metrics import mean_absolute_error
import sys
import timeit

from my_Prepro_fx import *


    
####################################
###           Funciones          ###
####################################

      
def ensemble_optim(d, results, val_exists, columns_label_ordered, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test):
    
    
    # Configuración
    ######################
    ensemble_rounds = d['L2_ENSEMBLE_NUM_ROUNDS']
    
    
    
    if val_exists:
        X = X_val.copy()
        y = y_val.copy()
    else:
        X = X_train.copy()
        y = y_train.copy()
        
    
    
    
    # Calculamos Weights
    ######################
    
    #''' This code gets a ensemble of models, and tries to find the optimum weights through MCMC magic''''
    
    def mae_loss_func(weights):
        ''' scipy minimize will pass the weights as a numpy array '''
        final_prediction = 0
        
        for weight, prediction in zip(weights, predictions):
            final_prediction += weight*prediction
    
        el_error = mean_absolute_error(y, final_prediction)
        
        # Gestionamos maximize/minimize
        if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'] == 1:
            error_con_signo = -1 * el_error
        else:
            error_con_signo = el_error

        return error_con_signo
    
   
    
    # finding the optimum weights
    predictions = []
    
        
    for i in range(0,X.shape[1]):
        pred = X[:, [i]][:,0]
        pred = np.transpose(pred)
        predictions.append((pred))
        
    
    for n in range(0,ensemble_rounds):
        
        t0_fold = timeit.default_timer()

        # the algorithms need a starting value, right now we chose 0.5 for all weights
        # its better to choose many random starting points and run minimize a few times  
        starting_values = np.random.uniform(size=len(predictions))
        
        # adding constraints and a different solver as suggested by user 16universe
        cons = ({'type':'eq','fun':lambda w: 1-sum(w)})
        
        
        # our weights are bound between 0 and 1
        bounds = [(0,1)]*len(predictions)
        
        res = minimize(mae_loss_func, 
                   starting_values, 
                   method = 'SLSQP', 
                   bounds = bounds, 
                   constraints = cons,
                   options={'maxiter': 100000})
        
        best_score = res['fun']
        weights = res['x']
        
        best_score = '{:.6f}'.format(best_score)
        
        weights_to_display = ["{:.5f}".format(weight) for weight in weights]
        
        
        # Imprime Score y Weights                      
        print(Fore.GREEN + 'Ensemble Score(' + str(n + 1) + '/' + str(ensemble_rounds) + '): ' + str(best_score)  + Style.RESET_ALL,'\tBest Weights: ',weights_to_display,'')
        
        # Imprime tiempo
        if (n + 1) < ensemble_rounds:
            time_delta_fold = int(timeit.default_timer()- t0_fold)
            time_delta_para_hora = int(time_delta_fold * (ensemble_rounds - (n + 1)))
            hora_estimada = (datetime.now() + timedelta(seconds=time_delta_para_hora)).strftime('%H:%M')
            if time_delta_para_hora > 20:
                print(Fore.BLUE + 'ESTIMATED SUBMISSION FINISH TIME IS ' + hora_estimada + '\n' + Style.RESET_ALL)
        else:
            pass
        
        if n == 0:
            score_max   = best_score
            score_min   = best_score
            weights_max = weights
            weights_min = weights
        else:
            if best_score > score_max:
                score_max   = best_score
                weights_max = weights
            elif best_score < score_min:
                score_min   = best_score    
                weights_min = weights
    

    
    if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER'] == 0:
        ensemble_coeffs = weights_min.copy()
        weights_min = ["{:.6f}".format(weight) for weight in weights_min]
        print(Fore.BLUE + '\nBest Overall Score: ', score_min,'   weights: ',weights_min, '\n' + Style.RESET_ALL)
        
        #best_score = '{:.6f}'.format(best_score)
    else:
        ensemble_coeffs = weights_max.copy()
        weights_max = ["{:.6f}".format(weight) for weight in weights_max]
        print(Fore.BLUE + '\nBest Overall Score: ', score_max,'   weights: ',weights_max, '\n' + Style.RESET_ALL)
            
        
    del X, y
        
    # 'Predecimos'
    ###############
    y_train_preds = np.average(X_train, axis=1, weights=ensemble_coeffs) # Weighted Mean along the first axis  
    
    if val_exists:
        y_val_preds = np.average(X_val, axis=1, weights=ensemble_coeffs) # Weighted Mean along the first axis  
        
    y_test_preds = np.average(X_test, axis=1, weights=ensemble_coeffs)
        
            

    # Guardamos información para Resultados
    ########################################
    results['ensemble_coeffs'] = ensemble_coeffs
    results['ensemble_rounds'] = ensemble_rounds
    
    
    # Aseguramos que los vectores columna tienen dimension (filas,) igual que en la salida de 1_ML o 2_ML
    ###############################################################
    y_train = np.reshape(y_train,(-1,))

    #if val_exists:
    #    idx_val = np.reshape(idx_val,(-1,))
    #    y_val_preds = np.reshape(y_val_preds,(-1,))
        
    idx_test = np.reshape(idx_test,(-1,))
    y_test_preds = np.reshape(y_test_preds,(-1,))
    
    
    

    # return
    ##########
    if val_exists:
        return(idx_test, y_test_preds, idx_val, y_val, y_val_preds, idx_train, y_train, y_train_preds)
    else:
        return(idx_test, y_test_preds, idx_train, y_train, y_train_preds)
    












#################################
###   NO USAMOS LO SIGUIENTE  ###
#################################

def ensembler_classifiers(score,preds,Id_Submissions,method,idx,COLUMN_ID,COLUMN_CLASS,outPath,preds_dtype):

    Wnorm = score/sum(score) # Weight vector normalized
    
    if method=="mean":
        preds_ensemble = np.average(preds, axis=0, weights=Wnorm) # Weighted Mean along the first axis  
        #preds_ensemble = np.round(preds_ensemble) #round the weighted mean
        preds_ensemble = preds_ensemble.astype(preds_dtype)
        #preds_ensemble=np.mean(preds, axis=0)  
    if method=="gmean":
        preds_ensemble =  stats.mstats.gmean(preds, axis=0) # Geometric Mean along the first axis  
        #preds_ensemble = np.round(preds_ensemble) #round the weighted mean
        preds_ensemble = preds_ensemble.astype(preds_dtype)
    if method=="median":
        preds_ensemble = np.median(preds, axis=0)
        preds_ensemble = preds_ensemble.astype(preds_dtype)
    if method=="mode":
        preds_ensemble = stats.mode(preds, axis=0)[0][0]   # Mode along the first axis     
    if method=="max":
        preds_ensemble = np.amax(preds, axis=0)            # Maxima along the first axis
    if method=="min":
        preds_ensemble = np.amin(preds, axis=0)            # Min along the first axis    

    # Create submissions files
    submission = pd.DataFrame({COLUMN_ID: idx ,COLUMN_CLASS: preds_ensemble})
    submission.to_csv(outPath+ 'sub_' + method + '_' + Id_Submissions + '.csv', index=False)

    return preds_ensemble #Prediction ensembled


    
    
    


    
    
    ####################################
    ###              Main            ###
    ####################################

def ensembler_main(COLUMN_ID,COLUMN_CLASS,inPath,outPath,inPath_Val,d):
    
    if not os.path.exists(outPath):
        os.makedirs(outPath)
    
    
    #       Submissions import
    csv_PathFiles,files = file_names_get_all(inPath)
    
    num_files = len(csv_PathFiles)
    score = np.zeros(num_files, dtype=float)
    
    firstFile_data = (read_csv(csv_PathFiles[0]))
    
    num_records=len(firstFile_data[[COLUMN_CLASS]].values[:,0])
    preds_dtype = firstFile_data[[COLUMN_CLASS]].values[:,0].dtype
    print ('\nClass datatype is: ',preds_dtype,'. There are',num_files,'predictions to combine:\n')
    preds=np.zeros([num_files,num_records],dtype=preds_dtype)
    
    ensemble_Name=''
    
    
    
    #------------------
    #  Loop por files
    ##################
    counter=0
    
    for submis in files:
        submis_Name_Slots = str(submis.replace(".csv","")).split('_')
        ensemble_Name += submis_Name_Slots[-1] + '-'
        score[counter] = float('0.'+ submis_Name_Slots[0])
        
        data = read_csv(inPath+submis).sort(ascending=True)
        preds[counter,:] = np.array(data[[COLUMN_CLASS]].values[:,0])
        print ('Prediction',counter + 1,'is :', preds[counter,:])
        if d['L3_DO_CONVERT_2_RANKING']:
            order = preds[counter,:].argsort()
            preds[counter,:] = order.argsort()
            #print(preds[counter,:].min(axis=0))
            preds[counter,:]=(preds[counter,:] - preds[counter,:].min(axis=0)) / (preds[counter,:].max(axis=0) - preds[counter,:].min(axis=0))
            #x_normed = x / x.max(axis=0)
            #row_sums = a.sum(axis=1)
            #new_matrix = a / row_sums[:, numpy.newaxis]
        
        print ('Pred. after Rank',counter + 1,'is :', preds[counter,:])
        counter=counter+1
    
    ensemble_Name=ensemble_Name[:-1]
    idx = np.array(data[[COLUMN_ID]].values[:,0])
    
    
    print()
    print()
    
    
    #---------------------------------------------------
    #  Loop por métodos (mean, max,...) para submission
    ####################################################
    
    #score=np.array([0.8, 0.5, 0.95, 0.8, 0.76])
    #preds= np.array([[2,0,4,3,1,3], [3,0,4,2,1,4], [2,0,3,2,1,3], [3,2,4,2,1,4], [4,2,3,1,4,3]])
    
    methods=np.array(['mean', 'gmean','median','mode','max','min'])
    
    #if str(preds_dtype).find("float", beg=0, end=len(string))>0:
    if str(preds_dtype)[0:5]=="float":
        class_Is_Float = 1
    else:
        class_Is_Float = 0
    
    for method in methods:
        if (method=="mode") and (class_Is_Float==1):
            print ("\nMODE is NOT calculated for float classes.\n")
        else:
            predicciones_ensemble = ensembler_classifiers(score,preds,ensemble_Name,method,idx,COLUMN_ID,COLUMN_CLASS,outPath,preds_dtype)
            print ('The emsemble prediction due to ' + method.upper() + ' is :', predicciones_ensemble)  
    
    

