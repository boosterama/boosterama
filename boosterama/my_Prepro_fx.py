# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from collections import defaultdict
from colorama import Fore, Back, Style
from datetime import datetime,timedelta
import dateutil

import itertools
from nltk.stem.porter import *
import numpy as np
import os
import pandas as pd
from pandas.tseries.offsets import *
import platform
import random
import re
from scipy import stats
from scipy.stats import boxcox, mstats, skew

from sklearn import metrics, preprocessing
from sklearn.decomposition import TruncatedSVD
from sklearn import preprocessing
from sklearn.ensemble import AdaBoostClassifier, AdaBoostRegressor, ExtraTreesClassifier, GradientBoostingRegressor, RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer, FeatureHasher, text
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble.forest import RandomForestRegressor
from sklearn.linear_model import ElasticNet,LinearRegression,LogisticRegression, Ridge, SGDClassifier, SGDRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier,DecisionTreeRegressor

import sys

from my_Debug import *
from my_Prints import *
from my_Query import *
from my_Input_Output import *



#########################################
###    Funciones (orden alfabético)   ###
#########################################

def boolean_encode(d,nonNumVars_X_TRAIN,X_test__pd,X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        for field in nonNumVars_X_TRAIN: 
            X_TRAIN__pd[field]=X_TRAIN__pd[field].apply(int)
            X_test__pd[field]=X_test__pd[field].apply(int)
    
    
        object_fields = vars_non_numeric(X_TRAIN__pd)
        
        for object_field in object_fields: 
            X_TRAIN__pd[object_field]=X_TRAIN__pd[object_field].replace([True, False],['True','False']) 
            X_test__pd[object_field]=X_test__pd[object_field].replace([True, False],['True','False']) 
            
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After BooleanEncode",showFields=doPrintNAShowFields)        
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd



def change_cols_names(X_TRAIN__pd, X_test__pd,OldColNames,NewColNames):
    for counter, col_Source in enumerate(OldColNames):
        X_TRAIN__pd.rename(columns={OldColNames[counter]:NewColNames[counter]}, inplace=True)
        X_test__pd.rename(columns={OldColNames[counter]:NewColNames[counter]}, inplace=True)
    X_cols_names = list(X_TRAIN__pd.columns.values)
    return X_TRAIN__pd, X_test__pd, X_cols_names



                                                        
def cols_operation(dXT,d, condiciones_query_list, DO_OWEN, DO_DROP_ORIG_AFTER, DO_DROP_NEW_AFTER, DO_EXPLODE, operation, X_test__pd, X_TRAIN__pd,y_TRAIN__pd,separator, function_name,doPrintNA,doPrintNAShowFields):
    try:
        # Separo la Información que viene en 4 partes
        if len(condiciones_query_list)>0:
    
            for condiciones_query in condiciones_query_list:
    
                # Si es un query, es posible que haya dejado de ser lista. Arreglamos.
                if isinstance(condiciones_query, str):
                    condiciones_query = [condiciones_query]
                    
                                      
                fields_to_combine = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
                
                if len(fields_to_combine) <2:            
                    print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO ALGUNA COLUMNA PARA APLICAR LA FUSIÓN DE LAS COLUMNAS." + Style.RESET_ALL)
        
                
                if DO_EXPLODE[0:3].lower() == 'one':
                    combos = [fields_to_combine]
                elif DO_EXPLODE[0:3].lower() == 'man':
                    grupos_de = int(DO_EXPLODE[5:6])
                    combos = []
                    for subset in itertools.combinations(fields_to_combine, grupos_de):
                        combos.append(subset)
                else:
                    print(Fore.RED + 'Argumento NO válido en cuarto parámetro de DO_COLS_' + operation + Style.RESET_ALL) 
                    sys.exit()
                    
                
                for fields in combos:
            
                    columnas_can_create = 1        
                    
                    for idx,elem in enumerate(fields):
                        # NO crear si hay algún elemento que no está en TRAIN
                        if fields[idx] not in X_TRAIN__pd.columns:
                            columnas_can_create = columnas_can_create * 0
                            
                            
                        
                        # Creando nombre de columna compuesta
                        if idx == 0:
                            new_field_name = fields[0]
                        else:
                            new_field_name += separator + fields[idx]
    
    
                    # Creamos la nueva columna
                    if columnas_can_create:
                        
                        for idx,elem in enumerate(fields):
                            
                            if idx == 0:
                                if operation == 'join':
                                    X_TRAIN__pd[new_field_name] =  X_TRAIN__pd[fields[0]].astype(str)
                                    X_test__pd[new_field_name]  =  X_test__pd[fields[0]].astype(str)
                                elif operation == 'diff':
                                    X_TRAIN__pd[new_field_name] =  X_TRAIN__pd[fields[0]]
                                    X_test__pd[new_field_name]  =  X_test__pd[fields[0]]
                                elif operation == 'multiply':
                                    X_TRAIN__pd[new_field_name] =  X_TRAIN__pd[fields[0]]
                                    X_test__pd[new_field_name]  =  X_test__pd[fields[0]]                    
                            else:
                                if operation == 'join':
                                    X_TRAIN__pd[new_field_name] =  X_TRAIN__pd[new_field_name] +  X_TRAIN__pd[fields[idx]].astype(str)
                                    X_test__pd[new_field_name]  =  X_test__pd[new_field_name]  +  X_test__pd[fields[idx]].astype(str)
                                elif operation == 'diff':
                                    X_TRAIN__pd[new_field_name] =  X_TRAIN__pd[new_field_name] -  X_TRAIN__pd[fields[idx]]
                                    X_test__pd[new_field_name]  =  X_test__pd[new_field_name]  -  X_test__pd[fields[idx]]
                                elif operation == 'multiply':
                                    X_TRAIN__pd[new_field_name] =  X_TRAIN__pd[new_field_name] *  X_TRAIN__pd[fields[idx]]
                                    X_test__pd[new_field_name]  =  X_test__pd[new_field_name]  *  X_test__pd[fields[idx]]
            
                        # Do OWEN
                        if DO_OWEN:
                            fields_to_owen = [new_field_name]
                            owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
                    #owen_trick_list_agg ( dXT, d ,fields_to_owen, DO_DROP_NEW_AFTER, X_TRAIN__pd, y_TRAIN__pd, X_test__pd, COLUMN_2_AGG, function_name, doPrintNA, doPrintNAShowFields, owen_metrics=None)
        
                        # DROP New field (only Owen is left)
                        #if DO_DROP_NEW_AFTER:
                        #    X_TRAIN__pd.drop(new_field_name, axis=1, inplace=True)
                        #    X_test__pd.drop(new_field_name, axis=1, inplace=True)   
                               
                    else:
                        print()
                        print('Falta una de estas columnas:',fields)
                        print()
        
        
    
        
        ## DROP Original fields (only Owen is left)
        #if Do_Drop_Original:
        #    X_TRAIN__pd.drop(nombre_col, axis=1, inplace=True)
        #    X_test__pd.drop(nombre_col, axis=1, inplace=True)
    
        
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After cols " + operation,showFields=doPrintNAShowFields)   
        
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd, X_TRAIN__pd





def cols_TRAIN_test_with_diferent_dtypes(TRAIN__pd,test__pd):
    cols_TRAIN = list(TRAIN__pd.columns.values)
    cols_test = list(test__pd.columns.values)
    cols_both = list(set(cols_TRAIN) & set(cols_test)) #Elementos comunes en ambas listas
    for col in cols_both:
        T_dtype = TRAIN__pd[col].dtypes
        t_dtype = test__pd[col].dtypes
        if T_dtype != t_dtype:
            print("Field",col,"is type",T_dtype,"in TRAIN while is type",t_dtype,"in test")
            if ((T_dtype=="object" and t_dtype=="float64") or (T_dtype=="float64" and t_dtype=="object")):
                print("Convertimos a object")
                print()
                TRAIN__pd[col]=TRAIN__pd[col].astype('object')
                test__pd[col]=test__pd[col].astype('object')
                #test__pd[col]=test__pd[col].fillna('')
            else:
                print("Caso aún no codificado: Implementar en funcion cols_TRAIN_test_with_diferent_dtypes()")
    return TRAIN__pd,test__pd


def column_2_date(COLS_2_DATE,X_test__pd,X_TRAIN__pd,function_name,doPrintNA,doPrintNAShowFields, doPrintDropInfo=False,dropMessage=None):
    try:
            
        numColsConverted_TRAIN = 0
        numColsConverted_test = 0
        for col_2_date in COLS_2_DATE:
    
            if col_2_date in X_TRAIN__pd.columns:
                try:
                    X_TRAIN__pd[col_2_date] = X_TRAIN__pd[col_2_date].astype('datetime64')
                except:
                    try:
                        X_TRAIN__pd[col_2_date] = pd.to_datetime(X_TRAIN__pd[col_2_date])
                    except:
                        print()
                        print(Fore.RED + "--------------------------\n CONVERSION_TO_DATE ERROR:" + Style.RESET_ALL)                
                        print(Fore.RED + " (in column '" + col_2_date + "')"  + Style.RESET_ALL)
                        print()
                        
    
                        print(Fore.RED + "\tWe tried two different methods but were not able to convert to date the feature: '" + col_2_date + "' on your Train csv." + Style.RESET_ALL)
                        print(Fore.RED + "\tYou could try to reformat column '"  + col_2_date + "' from the CSV." + Style.RESET_ALL)
                    
                        print()
                        
                        print(Fore.RED + "----------------------------------------------------------------------" + Style.RESET_ALL)
                        print()
                        sys.exit()
                numColsConverted_TRAIN += 1
            else:
                print(Fore.RED + col_2_date+ " NO SE HA PODIDO CONVERTIR A DATE EN TRAIN. POSIBLEMENTE NO SE HAYA ENCONTRADO LA COLUMNA " + col_2_date + " EN EL TRAIN DATAFRAME!!" + Style.RESET_ALL)
                
            if col_2_date in X_test__pd.columns:
                try:
                    X_test__pd[col_2_date] = X_test__pd[col_2_date].astype('datetime64')
                except:
                    try:
                        X_test__pd[col_2_date] = pd.to_datetime(X_test__pd[col_2_date])
                    except:
                        print()
                        print(Fore.RED + "--------------------------\n CONVERSION_TO_DATE ERROR:" + Style.RESET_ALL)                
                        print(Fore.RED + " (in column '" + col_2_date + "')"  + Style.RESET_ALL)
                        print()
                        
    
                        print(Fore.RED + "\tWe tried two different methods but were not able to convert to date the feature: '" + col_2_date + "' on your Test csv." + Style.RESET_ALL)
                        print(Fore.RED + "\tYou could try to reformat column '"  + col_2_date + "' from the CSV." + Style.RESET_ALL)
                    
                        print()
                        
                        print(Fore.RED + "----------------------------------------------------------------------" + Style.RESET_ALL)
                        print()
                        sys.exit()
                numColsConverted_test += 1
            else:
                print(Fore.RED + col_2_date+ " NO SE HA PODIDO CONVERTIR A DATE EN TEST. POSIBLEMENTE NO SE HAYA ENCONTRADO LA COLUMNA " + col_2_date + " EN EL TEST DATAFRAME!!" + Style.RESET_ALL) 
            allcolumns = list(X_TRAIN__pd.columns.values)
        
        if doPrintNA: print_na_info(X_TRAIN__pd,"TRAIN__pd",X_test__pd,"test__pd",calculate=True,message="After converting to date columns specified in Dashboard",showFields=doPrintNAShowFields)
        
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return allcolumns,X_test__pd, X_TRAIN__pd  



# counts is a tuple containing different values list to count for each count
# or a map, if you like to count over a subset of columns: {'values':[0, np.nan], 'columns':'q_.*'}
def counts_by_rows(dXT,d, dataset, counts,function_name): 
    try:
        
            
        new_columns = []
        
        
        for i, values in enumerate(counts):
            if isinstance(values, dict):
                columns_regex=values['columns']
                values=values['values']
            else:
                columns_regex=None
                
            values = list(set(values)) # supress duplicated values, (doesn't work for duplicted nans)
            count = count_by_rows(dataset, values, columns_regex=columns_regex)
            
            new_column_name=d['SEPARATOR_NA_COUNT'].format(i) + "_".join(map(str,values))
            
            new_column_tuple = (new_column_name, count)
            new_columns.append(new_column_tuple)
        
        for (column_name, column_data) in new_columns:
            dataset[column_name] = column_data
    
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
           
    return dataset
    

    
def count_by_rows(dataset, values, columns_regex=None):
    
    def value_is_nan(value):
        return value=="np.nan" or (isinstance(value, float) and np.isnan(value))
        
    # Lo que viene a continuación sólo es necesario si count_by_rows ocurre antes de split Id, X, y
    #dataset = dataset_orig.copy()    
    #for col in del_cols:
    #    dataset.drop(d['COMP_COLUMN_CLASS'], axis=1, inplace=True)
    #    dataset.drop(d['COMP_COLUMN_ID'], axis=1, inplace=True)
    
    # Hago esto porque "in" funciona mal con nan, dependiendo de si es np.nan o float("nan")
    nan_count=False
    for value in values:
        if value_is_nan(value):
            nan_count=True
    
    if nan_count:
        values = [x for x in values if not value_is_nan(x)]
    
    print("elementos_a_contar: " + str(values), end="")
    print(" y nan" if nan_count else "" )
    
    if columns_regex is not None:
        regex = "^({})$".format(columns_regex)
        columns=[col for col in dataset.columns if re.search(regex, col)]
        dataset=dataset[columns]
    
    count=[]
    for row in dataset.itertuples():
    
        #print(row, end=": ")
        
        i=0

        for cell in row[1:]: #ecluimos el elto 0, índice
            
            #print(str(cell), end= ",")
            
            if cell in values:
                #print("\n--- cell in values:" + str (cell) )
                i+=1
            
            if nan_count and isinstance(cell, float) and np.isnan(cell):
                #print("\n---" + str (cell) + " is NaN")
                i+=1
        
        count.append(i)
        
    return count




def create_dirs(dirList):
    for directory in dirList:
        if not os.path.exists(directory):
            os.makedirs(directory)



def date_encode(dXT,d,condiciones_query,DO_TRANSFORMATIONS,DO_OWEN,DO_DROP_NEW_AFTER,X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)    
    
        variables_gestionadas_por_python = ['year','month','dayofyear','dayofweek','day','weekofyear','quarter','hour']
        
        the_datasets = [X_TRAIN__pd,X_test__pd]
    
        DO_DROP_ORIG_AFTER = 1  # Siempre borramos la variable original
        
        if len(fields) == 0:
            print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR DATE_ENCODE." + Style.RESET_ALL)
    
        #--------------------------------------------------------------------------
        for field in fields:
            
            # Convert field to Date
            ########################
            for theDataset in the_datasets:
                #theDataset[field] = theDataset[field].astype('datetime64[ns]')
                try:
                    theDataset[field] = theDataset[field].apply(dateutil.parser.parse)
                except:
                    pass # it was already ok
            
            
            #--------------------------------------------------------------------------
            for theTransformation in DO_TRANSFORMATIONS:
                
                theTransformation = theTransformation.lower()
                
                new_field_name = field + d['SEPARATOR_DATE_ENCODE'] + theTransformation
                ##########################################
                # Transformaciones Gestionadas por Python
                ##########################################
                if theTransformation in variables_gestionadas_por_python:
                    
                    #--------------------------------------------------------------------------
                    for theDataset in the_datasets:
                        theDataset[new_field_name] = getattr(getattr(theDataset, field).dt, theTransformation)
    
    
                #########################################
                # Transformaciones por Código
                #########################################
    
                # Owen a la Fecha
                elif theTransformation[0:4] == 'owen':
                    # example:  owen_Day_-1
                
                    # slot_period
                    slot_period =  theTransformation.split('_')[1].lower()
                    
                    # slot_far_from_original_date
                    if theTransformation.count('_') > 1:
                        slot_far_from_original_date = int(theTransformation.split('_')[2])
                    else:
                        slot_far_from_original_date = 0
                                        
                    # new_field_name
                    new_field_name      = field + d['SEPARATOR_DATE_ENCODE'] + slot_period + '_' + str(slot_far_from_original_date)
                    new_field_name_temp = new_field_name + "_temp"
    
                    
                    # DAY or WEEK or MONTH
                    #######################
                    if slot_period == 'day' or slot_period == 'week' or slot_period == 'month':
                        
                        X_test__pd, X_TRAIN__pd = date_owen_back_period(d, X_test__pd, X_TRAIN__pd, y_TRAIN__pd, field, new_field_name, new_field_name_temp, slot_period, slot_far_from_original_date)
                        
    
                    # ROW   
                    ############### 
                    elif slot_period == 'row':
                        
                        numRows_X_TRAIN__pd = len(X_TRAIN__pd)
                        
                        Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)
                        Xy_TRAIN_test__pd = pd.concat([Xy_TRAIN__pd,X_test__pd],axis=0)
                        
    
                        Xy_TRAIN_test__pd[new_field_name] = Xy_TRAIN_test__pd[d['COMP_COLUMN_CLASS']]
                        
                        Xy_TRAIN_test__pd[new_field_name] = Xy_TRAIN_test__pd[new_field_name].shift(-slot_far_from_original_date)
    
                        # Clean and get back X_TRAIN__pd & X_test__pd
                        X_TRAIN_test__pd = Xy_TRAIN_test__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)
                        X_TRAIN__pd = X_TRAIN_test__pd.iloc[range(0,numRows_X_TRAIN__pd)]
                        X_test__pd = X_TRAIN_test__pd.iloc[range(numRows_X_TRAIN__pd,len(X_TRAIN_test__pd))]          
    
                        
                    # DAYOFWEEK   
                    ############### 
                    elif slot_period == 'dayofweek':
                        
                        dayofweek_col = field + d['SEPARATOR_DATE_ENCODE'] + 'dayofweek'
                        
                        if dayofweek_col in X_TRAIN__pd:
                            X_TRAIN__pd[new_field_name] = X_TRAIN__pd[dayofweek_col].astype(str)
                            X_test__pd[new_field_name] = X_test__pd[dayofweek_col].astype(str)
      
                        else:
                            X_TRAIN__pd[dayofweek_col] = getattr(getattr(X_TRAIN__pd,field).dt, 'dayofweek')
                            X_test__pd[dayofweek_col] = getattr(getattr(X_test__pd,field).dt, 'dayofweek')
                            
                            X_TRAIN__pd[new_field_name] = X_TRAIN__pd[dayofweek_col].astype(str)
                            X_test__pd[new_field_name] = X_test__pd[dayofweek_col].astype(str)
    
                            X_TRAIN__pd.drop(dayofweek_col, axis=1, inplace=True)
                            X_test__pd.drop(dayofweek_col, axis=1, inplace=True)                    
    
    
                        fields_to_owen = [new_field_name]
                        
                        owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    
                
                # isweekend
                ###############
                elif theTransformation == 'isweekend':
                    #--------------------------------------------------------------------------
                    for theDataset in the_datasets:                
                        
                        dayofweek_col = field + d['SEPARATOR_DATE_ENCODE'] + 'dayofweek'
                        
                        if dayofweek_col in theDataset:
                            theDataset[new_field_name] = theDataset[dayofweek_col].apply(lambda x: 1 if (x == 5) or (x == 6) else 0) #Monday is 0 and Sunday is 6.
                        else:
                            theDataset[dayofweek_col] = getattr(getattr(theDataset, field).dt, 'dayofweek')
                            theDataset[new_field_name] = theDataset[dayofweek_col].apply(lambda x: 1 if (x == 5) or (x == 6) else 0) #Monday is 0 and Sunday is 6.
                            theDataset.drop(dayofweek_col, axis=1, inplace=True)
                    
                # isofficehours
                ###############
                elif theTransformation == 'isofficehours':
                    #--------------------------------------------------------------------------
                    for theDataset in the_datasets:                
                        
                        isofficehour_col = field + d['SEPARATOR_DATE_ENCODE'] + 'hour'
                        
                        if isofficehour_col in theDataset:
                            theDataset[new_field_name] = theDataset[isofficehour_col].apply(lambda x: 1 if (x > 18) or (x < 8) else 0) 
                        else:
                            theDataset[isofficehour_col] = getattr(getattr(theDataset, field).dt, 'hour')
                            theDataset[new_field_name] = theDataset[isofficehour_col].apply(lambda x: 1 if (x > 18) or (x < 8) else 0) 
                            theDataset.drop(isofficehour_col, axis=1, inplace=True)
    
    
                # daysfromfirst
                ###############
                elif theTransformation == 'daysfromfirst':
                    #--------------------------------------------------------------------------
                    for theDataset in the_datasets:       
                                            
                        Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)
                        minimum_date = Xy_TRAIN__pd[field].min()
                        
                        min_date_col = field + d['SEPARATOR_DATE_ENCODE'] + 'minDate'
                        theDataset[min_date_col] = minimum_date
                        
                        theDataset[new_field_name] = theDataset[field] - theDataset[min_date_col] # formato timedelta terrible del tipo '56 days'
                        theDataset[new_field_name] = (theDataset[new_field_name] / np.timedelta64(1, 'D')).astype(int) # pasamos formato a int
                        theDataset.drop(min_date_col, axis=1, inplace=True)
    
    
                # YearMonth
                ###############
                elif theTransformation == 'yearmonth':
                    #--------------------------------------------------------------------------
                    for theDataset in the_datasets:                
                                            
                        theDataset[new_field_name] = theDataset[field].map(lambda x: 100*x.year + x.month)
                        
    
                # Do OWEN
                ###############
                if DO_OWEN:
                    fields_to_owen = [new_field_name]
                    owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    
    
            # DROP Original fields (only Owen is left)
            ######################
            if DO_DROP_ORIG_AFTER:
                X_TRAIN__pd.drop(field, axis=1, inplace=True)
                X_test__pd.drop(field, axis=1, inplace=True)
                    
    
                
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After DateEncode",showFields=doPrintNAShowFields)        
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd




def date_owen_2_feature(dXT,d ,  condiciones_query, DATE_COLUMN , START_END_PERIOD_PRE  ,X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    # [([scope], 'date column', starting_period for owen, end_period for owen)]
    
    try:
            
        # FUNCTIONS
        ############
        def row_value_4_owen_2_feature(d, row, Xy_TRAIN_test__pd, START_PERIOD, END_PERIOD, field, DATE_COLUMN, metric):
            current_date = row[DATE_COLUMN]
            start_date   = current_date + timedelta(days=START_PERIOD)
            end_date     = current_date + timedelta(days=(END_PERIOD+1))
    
                                    # df.loc[(df["B"] > 50) & (df["C"] == 900), "A"]
            df = Xy_TRAIN_test__pd.loc[(Xy_TRAIN_test__pd[DATE_COLUMN]>=start_date) & (Xy_TRAIN_test__pd[DATE_COLUMN]<end_date) & (Xy_TRAIN_test__pd[field]==row[field])]
    
            if metric == 'mode':
                target_metric_value = df[d['COMP_COLUMN_CLASS']].mode().iloc[0]
            else:            
                target_metric_value = df[d['COMP_COLUMN_CLASS']].mean()
          
            return target_metric_value    
            
            
    
        # VARIABLES & CONSTANTS
        ########################
        fields_2_owen = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)  
        
    
            
        numRows_X_TRAIN__pd = len(X_TRAIN__pd)
        
        
        Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)
        Xy_TRAIN_test__pd = pd.concat([Xy_TRAIN__pd,X_test__pd],axis=0)
        
        minimum_date_overall = Xy_TRAIN_test__pd[DATE_COLUMN].min()
        minimum_date_test = X_test__pd[DATE_COLUMN].min()
        
        min_date_col = DATE_COLUMN + d['SEPARATOR_DATE_ENCODE'] + 'minDate'
                
        owen_metrics=d['PRP_OWEN_TRICK_METRICS']
        if owen_metrics==['default']:
            if Xy_TRAIN_test__pd[DATE_COLUMN].dtype == 'object' or d['COMP_PROBLEM_TYPE'].lower()[:7] == 'class_m':
                owen_metrics = ['mode']
            else:
                owen_metrics = ['mean']                    
                        
    
        transform = d['PRP_OWEN_TRANSFORM']
        transform_prefix= transform + "_"
        if transform == "No":
            transform = None
            transform_prefix=""        
    
            
        #--------------------------------------------------------------------------
        for field in fields_2_owen:
            
            for (START_PERIOD_PRE,END_PERIOD_PRE) in START_END_PERIOD_PRE:
            
                
                        
                if END_PERIOD_PRE > START_PERIOD_PRE:
                    START_PERIOD = START_PERIOD_PRE
                    END_PERIOD   = END_PERIOD_PRE
                else:
                    START_PERIOD = END_PERIOD_PRE
                    END_PERIOD   = START_PERIOD_PRE
        
                
                # new_field_name
                new_field_name      = field + d['SEPARATOR_DATE_OWEN2FEAT'] + '_' + str(START_PERIOD) + 'to' + str(END_PERIOD) 
                new_field_name_temp = new_field_name + "_temp"
                            
                for metric in owen_metrics:
                    
        
            
                    Xy_TRAIN_test__pd[new_field_name] = Xy_TRAIN_test__pd.apply(lambda row: row_value_4_owen_2_feature(d, row, Xy_TRAIN_test__pd, START_PERIOD, END_PERIOD, field, DATE_COLUMN, metric), axis=1)
        
                
                # Clean and get back X_TRAIN__pd & X_test__pd
                
                X_TRAIN_test__pd = Xy_TRAIN_test__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)
                
                X_TRAIN__pd = X_TRAIN_test__pd.iloc[range(0,numRows_X_TRAIN__pd)]
                X_test__pd = X_TRAIN_test__pd.iloc[range(numRows_X_TRAIN__pd,len(X_TRAIN_test__pd))]

            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd, X_TRAIN__pd


        
    
    
      
def date_owen_back_period(d, X_test__pd, X_TRAIN__pd, y_TRAIN__pd, field, new_field_name, new_field_name_temp, slot_period, slot_far_from_original_date):
    
    pd.options.mode.chained_assignment = None

    # IMPORT
    ############
    from calendar import monthrange
    
    
    # FUNCTIONS
    ############
    def month_delta(row, d1, f2):
        #d1 = row[f1]
        d2 = row[f2]
        delta = 0
        while True:
            mdays = monthrange(d1.year, d1.month)[1]
            d1 += timedelta(days=mdays)
            if d1 <= d2:
                delta += 1
            else:
                break
        return delta

        

    def week_delta(row, d1, f2):
        d2 = row[f2]
        
        monday1 = (d1 - timedelta(days=d1.weekday()))
        monday2 = (d2 - timedelta(days=d2.weekday()))
        
        delta = int((monday2 - monday1).days / 7)

        return delta
                


        
        
        
    # VARIABLES & CONSTANTS
    ########################
                
    COLUMN_2_AGG = d['COMP_COLUMN_CLASS']
    
    numRows_X_TRAIN__pd = len(X_TRAIN__pd)
    
    
    Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)
    Xy_TRAIN_test__pd = pd.concat([Xy_TRAIN__pd,X_test__pd],axis=0)
    
    minimum_date_overall = Xy_TRAIN_test__pd[field].min()
    minimum_date_test = X_test__pd[field].min()
    
    min_date_col = field + d['SEPARATOR_DATE_ENCODE'] + 'minDate'
            
    owen_metrics=d['PRP_OWEN_TRICK_METRICS']
    if owen_metrics==['default']:
        if Xy_TRAIN_test__pd[COLUMN_2_AGG].dtype == 'object' or d['COMP_PROBLEM_TYPE'].lower()[:7] == 'class_m':
            owen_metrics = ['mode']
        else:
            owen_metrics = ['mean']                    
                    

    transform = d['PRP_OWEN_TRANSFORM']
    transform_prefix= transform + "_"
    if transform == "No":
        transform = None
        transform_prefix=""


    # DAY or MONTH
    ###############
    if slot_period == 'day':
        Xy_TRAIN_test__pd[min_date_col] = minimum_date_overall # Col DaysFromOrigin
        Xy_TRAIN_test__pd[new_field_name_temp] = Xy_TRAIN_test__pd[field] - Xy_TRAIN_test__pd[min_date_col] # formato timedelta terrible del tipo '56 days'
        Xy_TRAIN_test__pd[new_field_name_temp] = (Xy_TRAIN_test__pd[new_field_name_temp] / np.timedelta64(1, 'D')) # pasamos formato a int
        Xy_TRAIN_test__pd.drop(min_date_col, axis=1, inplace=True)                    
    elif slot_period == 'week':              
        Xy_TRAIN_test__pd[new_field_name_temp] = Xy_TRAIN_test__pd.apply(lambda row: week_delta(row, minimum_date_test, field), axis=1)
    elif slot_period == 'month':              
        Xy_TRAIN_test__pd[new_field_name_temp] = Xy_TRAIN_test__pd.apply(lambda row: month_delta(row, minimum_date_overall, field), axis=1)
                
        
    # Substracting the defined offset
    Xy_TRAIN_test__pd[new_field_name] = (Xy_TRAIN_test__pd[new_field_name_temp] + slot_far_from_original_date).astype(int)
    
    # renaming fields to make it clearer
    field_WITHOUT_offset = new_field_name_temp
    field_WITH_offset = new_field_name

    
    grouped = Xy_TRAIN_test__pd.groupby([field_WITHOUT_offset])
    

                    
    for metric in owen_metrics:
        
        # Name field_owen
        if metric == 'mode':
            field_owen_name = field_WITH_offset + transform_prefix + d['SEPARATOR_OWEN'] + 'mode' + '_' + COLUMN_2_AGG[0:6]
        else:
            field_owen_name = field_WITH_offset + transform_prefix + d['SEPARATOR_OWEN'] + metric + '_' + COLUMN_2_AGG[0:6]
        
        # Do (if field_owen NOT already there) and (if field NOT already a owen).
        if (field_owen_name in X_TRAIN__pd.columns):
            pass
        
        else:
            # Hacemos OWEN
            if metric == 'mode':
                train_grouped = grouped[COLUMN_2_AGG].agg({'mode' : lambda x:x.value_counts().index[0]})
                train_grouped.rename(columns={'mode': field_owen_name}, inplace=True)  
            else:
                train_grouped = grouped[COLUMN_2_AGG].agg([getattr(np, metric)])   # getattr(x, 'foobar') is equivalent to x.foobar 
                train_grouped.rename(columns={metric: field_owen_name}, inplace=True)
    
            # Metemos el owen en Xy_TRAIN_test__pd
            Xy_TRAIN_test__pd = pd.merge(Xy_TRAIN_test__pd, train_grouped, how='left',left_on=field_WITH_offset,right_index=True)
      
            
    
    # Clean and get back X_TRAIN__pd & X_test__pd
    Xy_TRAIN_test__pd.drop(field_WITHOUT_offset, axis=1, inplace=True)
    Xy_TRAIN_test__pd.drop(field_WITH_offset, axis=1, inplace=True)
    
    X_TRAIN_test__pd = Xy_TRAIN_test__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)
    
    X_TRAIN__pd = X_TRAIN_test__pd.iloc[range(0,numRows_X_TRAIN__pd)]
    X_test__pd = X_TRAIN_test__pd.iloc[range(numRows_X_TRAIN__pd,len(X_TRAIN_test__pd))]
                                 
                                       
    # factor aleatorio para Train
    for metric in owen_metrics:       
                                    
        # Al owen del Train le aplicamos un factor aleatorio
        if metric != 'mode':
            
            RANDOM_RANGE = d['PRP_OWEN_RANDOM_RANGE'] 
            
            field_owen_name = field_WITH_offset + transform_prefix + d['SEPARATOR_OWEN'] + metric + '_' + COLUMN_2_AGG[0:6]
            
            X_TRAIN__pd[field_owen_name + '_rnd'] = pd.Series(np.random.uniform(-RANDOM_RANGE,RANDOM_RANGE,len(X_TRAIN__pd[field_owen_name])), index=X_TRAIN__pd.index)                    

            COLUMN_2_AGG=X_TRAIN__pd[field_owen_name]*(1+X_TRAIN__pd[field_owen_name + '_rnd'])
            if transform:
                X_TRAIN__pd[field_owen_name] = np.log1p(COLUMN_2_AGG)
            else:
                X_TRAIN__pd[field_owen_name] = COLUMN_2_AGG
            
            X_TRAIN__pd.drop(field_owen_name + '_rnd', axis=1, inplace=True)                    
    
    
    return X_test__pd, X_TRAIN__pd


    
    
    

def dictionary_dtypes_train_and_test(TRAIN__pd,test__pd):
    
    dic_dtypes_train_original={}
    dic_dtypes_train={}
    cols_TRAIN = list(TRAIN__pd.columns.values)
    for col in cols_TRAIN:
        dic_dtypes_train_original[col]=TRAIN__pd[col].dtypes
        dic_dtypes_train[col]=TRAIN__pd[col].dtypes

    dic_dtypes_test_original={}
    dic_dtypes_test={}
    cols_test = list(test__pd.columns.values)
    for col in cols_test:
        dic_dtypes_test_original[col]=test__pd[col].dtypes
        dic_dtypes_test[col]=test__pd[col].dtypes
     
    differences = []
    cols_both = list(set(cols_TRAIN) & set(cols_test)) #Elementos comunes en ambas listas
    for col in cols_both:
        T_dtype = TRAIN__pd[col].dtypes
        t_dtype = test__pd[col].dtypes
        if T_dtype != t_dtype:
            differences.append(col)
            if (T_dtype=="object" and t_dtype!="object"):
                dic_dtypes_train[col] = T_dtype
                dic_dtypes_test[col] = T_dtype
            elif (T_dtype!="object" and t_dtype=="object"):
                dic_dtypes_train[col] = t_dtype
                dic_dtypes_test[col] = t_dtype            
            else:
                sys.exit("Caso aún no codificado: Codificar en la funcion dictionary_dtypes_train_and_test()")
    
    return dic_dtypes_train_original,dic_dtypes_test_original,dic_dtypes_train, dic_dtypes_test, differences


def drop_constants(X_test__pd,X_TRAIN__pd,doPrint, function_name,doPrintNA,doPrintNAShowFields):
    #columnas_constantes=list(train.loc[:,(train != train.ix[0]).any()].columns.values)
    #columnas_constantes=train.loc[:,train.apply(pd.Series.nunique)!=1]
    numColsDropped = 0
    columnasDropped = []
    for columna in list(X_TRAIN__pd.columns.values):
        #print (columna)
        if max(X_TRAIN__pd[columna])==min(X_TRAIN__pd[columna]):
            columnasDropped.append(columna)
            numColsDropped += 1
            X_TRAIN__pd = X_TRAIN__pd.drop(columna, axis=1)
            X_test__pd = X_test__pd.drop(columna, axis=1)
            #print("dropeamos ",columna," por ser una constante")
    if doPrint:
        print("XXX Dropeamos",numColsDropped,"features por ser todos sus valores iguales")
        print("columnasDropped: " + str(columnasDropped))
    if doPrint: print()
    if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After DropConstants",showFields=doPrintNAShowFields)        
    return X_test__pd, X_TRAIN__pd


def drop_features_all_na_in_train_or_test(dXT,d,TRAIN__pd,test__pd,TRAIN_name,test_name,doPrintDropInfo, function_name,doPrintNA,doPrintNAShowFields):
    try:
        cols_names = list(TRAIN__pd.columns.values)
        fieldsWithNAs_TRAIN,fieldsWithAllRowsNAs_TRAIN,fieldsWithNAs_test,fieldsWithAllRowsNAs_test,totalNAs_TRAIN,totalNAs_test = na_info(TRAIN__pd,"TRAIN__pd",test__pd,"test__pd")
        fieldsWithAllRowsNAs_TRAIN_or_test = list(set(fieldsWithAllRowsNAs_TRAIN + fieldsWithAllRowsNAs_test))
        if len(fieldsWithAllRowsNAs_TRAIN_or_test)>0:
            cols_names,test__pd,TRAIN__pd = drop_these_columns(dXT,d,fieldsWithAllRowsNAs_TRAIN_or_test, 0, test__pd,TRAIN__pd,False,False,doPrintDropInfo,"Motivo: Feature de Train o test con todos los valores NA")
        if doPrintNA: print_na_info(TRAIN__pd,TRAIN_name,test__pd,test_name,calculate=True,message="After DropFeaturesWithAllNAs",showFields=doPrintNAShowFields)        
        
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return cols_names,test__pd,TRAIN__pd


def drop_near_zero_variance(dXT, d, near_zero_threshold ,X_test__pd, X_TRAIN__pd, doPrint, function_name,doPrintNA,doPrintNAShowFields):
    #columnas_constantes=list(train.loc[:,(train != train.ix[0]).any()].columns.values)
    #columnas_constantes=train.loc[:,train.apply(pd.Series.nunique)!=1]
    try:
            
        numColsDropped = 0
        for columna in list(X_TRAIN__pd.columns.values):
            #print (columna)
            if X_TRAIN__pd[columna].var() < near_zero_threshold:
                numColsDropped += 1
                X_TRAIN__pd = X_TRAIN__pd.drop(columna, axis=1)
                X_test__pd = X_test__pd.drop(columna, axis=1)
                #print("dropeamos ",columna," por ser una constante")
        if doPrint: print("Dropeamos",numColsDropped,"features por tener varianza por debajo del umbral de drop_near_zero_variance")
        if doPrint: print()
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Drop NearZero Variance",showFields=doPrintNAShowFields)        
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd, X_TRAIN__pd


    
    
def drop_these_columns(dXT,d,condiciones_query,do_inverse_query,X_test__pd,X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields, doPrintDropInfo=False,dropMessage=None):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        if do_inverse_query == 1:
            #fields = [item for item in fields if item not in X_TRAIN__pd.columns.values.tolist()]
            fields = list(set(X_TRAIN__pd.columns.values.tolist()) - set(fields))
    
        print('Dropeamos',fields)    
        
        numColsDropped_TRAIN = 0
        numColsDropped_test = 0
        
        if len(fields) == 0:
            print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA DROPEAR." + Style.RESET_ALL)
            allcolumns = list(X_TRAIN__pd.columns.values)
        
        
        for col_2_drop in fields:
    
            if col_2_drop in X_TRAIN__pd.columns:
                X_TRAIN__pd = X_TRAIN__pd.drop(col_2_drop, axis = 1)
                numColsDropped_TRAIN += 1
            else:
                print(Fore.RED + col_2_drop+ " NO SE HA PODIDO DROPEAR DE TRAIN. POSIBLEMENTE NO SE HAYA ENCONTRADO LA COLUMNA " + col_2_drop + " EN EL TRAIN DATAFRAME!!" + Style.RESET_ALL)
                
            if col_2_drop in X_test__pd.columns:
                X_test__pd = X_test__pd.drop(col_2_drop, axis = 1)
                numColsDropped_test += 1
            else:
                print(Fore.RED + col_2_drop+ " NO SE HA PODIDO DROPEAR DE TEST. POSIBLEMENTE NO SE HAYA ENCONTRADO LA COLUMNA " + col_2_drop + " EN EL TEST DATAFRAME!!" + Style.RESET_ALL) 
            allcolumns = list(X_TRAIN__pd.columns.values)
        if doPrintDropInfo: print("Dropeamos",numColsDropped_TRAIN,"features del TRAIN y",numColsDropped_test,"del test",dropMessage)
        if doPrintDropInfo: print()  
    
        if doPrintNA: print_na_info(X_TRAIN__pd,"TRAIN__pd",X_test__pd,"test__pd",calculate=True,message="After dropping columns specified in Dashboard",showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return allcolumns,X_test__pd, X_TRAIN__pd  

    
    
def do_tf_idf(dXT,d,field_name, params_dict, X_test__pd,X_TRAIN__pd):
    
    default_params_dict={'min_df':3,  'max_features':None, 
                'strip_accents':'unicode', 'analyzer':'word','token_pattern':r'\w{1,}',
                'ngram_range':(1, 2), 'use_idf':1,'smooth_idf':1,'sublinear_tf':1,
                'stop_words' : 'english'}
    default_params_dict.update(params_dict)
    params_dict=default_params_dict
    
#    try:
    prefix=params_dict.pop('prefix', '{}_'.format(field_name))
    vectorizer=TfidfVectorizer(**params_dict)
    
    
    to_fit=pd.concat([X_TRAIN__pd[field_name], X_test__pd[field_name]], ignore_index=True, sort=False)
    vectorizer.fit(to_fit)
    del to_fit
    
    tf_idf_features = vectorizer.transform(X_TRAIN__pd[field_name])
    tf_idf_features = pd.DataFrame(tf_idf_features.todense())
    tf_idf_features = tf_idf_features.add_prefix(prefix)
    X_TRAIN__pd = pd.concat([X_TRAIN__pd, tf_idf_features], axis=1) 
    
    tf_idf_features = vectorizer.transform(X_test__pd[field_name])
    tf_idf_features = pd.DataFrame(tf_idf_features.todense())
    tf_idf_features = tf_idf_features.add_prefix(prefix)
    X_test__pd = pd.concat([X_test__pd, tf_idf_features], axis=1) 
    
    allcolumns = list(X_TRAIN__pd.columns.values)
        
#    except Exception as e: dXT["PRP_ERRORS"]['do_tf_idf'] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(do_tf_idf) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return allcolumns,X_test__pd, X_TRAIN__pd  

    
def do_regex_counts(dXT,d,options_dict, X_test__pd,X_TRAIN__pd):
#    try:
    field_name=options_dict['field']
    regex_list=options_dict['regex_list']
    
    alphanum_regex = re.compile('[\W_]+')
    for i, regex in enumerate(regex_list):
        new_col_name="re_count_{}_{}".format(i, alphanum_regex.sub('', regex)[:10])
        regex = re.compile(regex)
        X_TRAIN__pd[new_col_name] = X_TRAIN__pd[field_name].apply(lambda x: len(regex.findall(x)))
        X_test__pd[new_col_name] = X_test__pd[field_name].apply(lambda x: len(regex.findall(x)))
    
    allcolumns = list(X_TRAIN__pd.columns.values)
        
#    except Exception as e: dXT["PRP_ERRORS"]['do_tf_idf'] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(do_tf_idf) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return allcolumns,X_test__pd, X_TRAIN__pd  




def file_names_get_all(directory, substring='.csv'):
    csv_files_pre = os.listdir(directory)
    csv_files = [s for s in csv_files_pre if substring in s]
    csv_PathFiles = [directory + s for s in csv_files]
    return csv_PathFiles, csv_files


def do_PCA(dXT,d,condiciones_query,do_inverse_query,X_test__pd,X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields, doPrintDropInfo=False,dropMessage=None):
    try:
        train_rows=X_TRAIN__pd.shape[0]
        
        use_all_columns=isinstance(condiciones_query, str) and condiciones_query.lower()=='all'
        
        if use_all_columns:
            df=X_TRAIN__pd.append(X_test__pd)
            del X_TRAIN__pd, X_test__pd
        else:
            fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
            print('do_pca:',fields)  
        
            if do_inverse_query == 1:
                #fields = [item for item in fields if item not in X_TRAIN__pd.columns.values.tolist()]
                fields = list(set(X_TRAIN__pd.columns.values.tolist()) - set(fields))
            df=X_TRAIN__pd[fields].append(X_test__pd[fields])
        
        df=df.values
        pca, standard_scaler, pca_cols=fit_transform_PCA(df,
                                    standarize = d['PRP_PCA_STANDARIZE'].lower()=='yes',
                                    n_components  = d['PRP_PCA_N_COMPONENTS'],
                                    random_state = d['PRP_PCA_RANDOM_SEED'] if d['PRP_PCA_RANDOM_SEED']!='dashboard' else d['RANDOM_SEED'],
                                    svd_solver = d['PRP_PCA_SVD_SOLVER'],
                                    tol = d['PRP_PCA_TOL'],
                                    iterated_power = d['PRP_PCA_ITERATED_POWER'],
                                    whiten = d['PRP_PCA_WHITEN'].lower()=='yes',
                                    predict=True)
        
        del(df)
        pca_cols_train=pca_cols[:train_rows]
        pca_cols_test=pca_cols[train_rows:]
        del pca_cols
        
        pca_col_names=['pca_' + str(x) for x in range(d['PRP_PCA_N_COMPONENTS'])]
        
        '''
        pca_cols_train__pd=pd.DataFrame(pca_cols_train, columns=pca_col_names)
        pca_cols_test__pd=pd.DataFrame(pca_cols_test, columns=pca_col_names)
        
        del pca_cols_train, pca_cols_test
        '''
        
        '''
        if d['PRP_PCA_STANDARIZE']:
            from sklearn.preprocessing import StandardScaler
            X_test= self.standard_scaler.transform(X_test.values)
        
        X_test = self.pca.transform(X_test)
        
        y = self.knn.predict(X_test)
        '''
        
        if use_all_columns:
            #X_TRAIN__pd, X_test__pd=pca_cols_train__pd, pca_cols_test__pd
            X_TRAIN__pd=pd.DataFrame(pca_cols_train, columns=pca_col_names)
            X_test__pd=pd.DataFrame(pca_cols_test, columns=pca_col_names)
        
            
        else:
            print('Dropeamos',fields)    
            
            numColsDropped_TRAIN = 0
            numColsDropped_test = 0
            
            if len(fields) == 0:
                print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA DROPEAR." + Style.RESET_ALL)
                
            
            
            for col_2_drop in fields:
        
                if col_2_drop in X_TRAIN__pd.columns:
                    X_TRAIN__pd = X_TRAIN__pd.drop(col_2_drop, axis = 1)
                    numColsDropped_TRAIN += 1
                else:
                    print(Fore.RED + col_2_drop+ " NO SE HA PODIDO DROPEAR DE TRAIN. POSIBLEMENTE NO SE HAYA ENCONTRADO LA COLUMNA " + col_2_drop + " EN EL TRAIN DATAFRAME!!" + Style.RESET_ALL)
                    
                if col_2_drop in X_test__pd.columns:
                    X_test__pd = X_test__pd.drop(col_2_drop, axis = 1)
                    numColsDropped_test += 1
                else:
                    print(Fore.RED + col_2_drop+ " NO SE HA PODIDO DROPEAR DE TEST. POSIBLEMENTE NO SE HAYA ENCONTRADO LA COLUMNA " + col_2_drop + " EN EL TEST DATAFRAME!!" + Style.RESET_ALL) 
                
            if doPrintDropInfo: print("Dropeamos",numColsDropped_TRAIN,"features del TRAIN y",numColsDropped_test,"del test",dropMessage)
            if doPrintDropInfo: print()  
        
            if doPrintNA: print_na_info(X_TRAIN__pd,"TRAIN__pd",X_test__pd,"test__pd",calculate=True,message="After dropping columns specified in Dashboard",showFields=doPrintNAShowFields)
            
            X_TRAIN__pd=pd.concat([X_TRAIN__pd,pd.DataFrame(pca_cols_train, columns=pca_col_names)],axis=1)
            X_test__pd=pd.concat([X_test__pd,pd.DataFrame(pca_cols_test, columns=pca_col_names)],axis=1)
            
        del pca_cols_train, pca_cols_test
        allcolumns = list(X_TRAIN__pd.columns.values)
                
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return allcolumns,X_test__pd, X_TRAIN__pd  

def fit_transform_PCA(dataset, standarize, n_components , random_state, svd_solver, tol, iterated_power, whiten, predict=False):
    from sklearn.decomposition import PCA
    
    if standarize:
        from sklearn.preprocessing import StandardScaler
        standard_scaler=StandardScaler().fit(dataset)
        dataset= standard_scaler.transform(dataset)
    else:
        standard_scaler=None
    
    pca = PCA(n_components=n_components,
            random_state=random_state,
            svd_solver=svd_solver,
            tol=tol,
            iterated_power=iterated_power,
            whiten=whiten
            )
    pca = pca.fit(dataset)
    
    if predict:
        preds=pca.transform(dataset)
    else:
        preds=None
    
    return pca, standard_scaler, preds





        
def float_quantile(dXT,d, condiciones_query, DO_OWEN, DO_DROP_ORIG_AFTER, DO_DROP_NEW_AFTER, X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        for field in fields:
    
            unique_Values = X_TRAIN__pd.groupby([field])
            #print(field,'unique_Values',len(unique_Values))
    
    
    
            #   QUANTILES
            ###############
    
            #      TRAIN
            quantile_ok = 0
            if len(unique_Values)>10:
                quantile_num = 10
            else:
                quantile_num = len(unique_Values)
                
            while quantile_ok == 0:
                if quantile_num == 0:
                    X_TRAIN__pd[field + d['SEPARATOR_QUANTILES']] = 5
                    quantile_ok = 1
                else:
                    X_TRAIN__pd, quantile_ok, quantile_num = float_quantile_try_except(field, quantile_num, d, X_TRAIN__pd)
    
            #      test        
            quantile_ok = 0
            if len(unique_Values)>10:
                quantile_num = 10
            else:
                quantile_num = len(unique_Values) 
            
            while quantile_ok == 0:
                if quantile_num == 0:
                    X_test__pd[field + d['SEPARATOR_QUANTILES']] = 5
                    quantile_ok = 1
                else:
                    X_test__pd, quantile_ok, quantile_num = float_quantile_try_except(field, quantile_num, d, X_test__pd)
    
    
    
    
            #   OWEN
            ###############
            try:
                if DO_OWEN:
                    fields_to_owen = [(field + d['SEPARATOR_QUANTILES'])]               
                    owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
            except:
                pass
    
            #   DROP
            ############### 
            if DO_DROP_ORIG_AFTER:
                X_TRAIN__pd = X_TRAIN__pd.drop([field], axis=1)
                X_test__pd = X_test__pd.drop([field], axis=1)
    
                
            #try:
            #    if DO_DROP_NEW_AFTER:
            #        X_TRAIN__pd = X_TRAIN__pd.drop([field + d['SEPARATOR_QUANTILES']], axis=1)
            #except:
            #    pass
            #
            #try:
            #    if DO_DROP_NEW_AFTER:
            #        X_test__pd = X_test__pd.drop([field + d['SEPARATOR_QUANTILES']], axis=1)
            #except:
            #    pass
    
    
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Float Quantile",showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd   



def float_quantile_try_except(field, quantile_num, d, DATA):
    try:
        normalizo = {10 : 0, 9 : 0, 8 : 1, 7 : 1, 6 : 2, 5 : 2, 4 : 3, 3 : 3, 2 : 4, 1 : 4}
        DATA[field + d['SEPARATOR_QUANTILES']] = pd.qcut(DATA[field], quantile_num, labels=False) + (quantile_num + normalizo[quantile_num])
        quantile_ok = 1
        quantile_num = quantile_num - 1
        
    except: 
        quantile_ok = 0
        quantile_num = quantile_num - 1

    return DATA, quantile_ok, quantile_num

def hashing_trick_tokens(doc):
    """Extract tokens from doc.
    This uses a simple regex to break strings into tokens. For a more
    principled approach, see CountVectorizer or TfidfVectorizer.
    """
    return (tok.lower() for tok in re.findall(r"\w+", doc))
  
def hashing_trick_token_freqs(doc):
    """Extract a dict mapping tokens from doc to their frequencies."""
    freq = defaultdict(int)
    for tok in hashing_trick_tokens(doc):
        freq[tok] += 1
    return freq




def hashing_trick(nonNumVars_X_TRAIN,nonNumVars_X_test,TRESHOLD_HASHING_TRICK,X_test__pd,X_TRAIN__pd):
    # loop over all features

    for feature in X_TRAIN__pd[nonNumVars_X_test].columns:          
        unique_Values = np.size(np.unique(X_TRAIN__pd[feature]))
        print ('La feature',feature,'tiene',unique_Values,'valores únicos.')  
    print('- - - - -')
    vectorizer = DictVectorizer()
    vectorizer.fit_transform(hashing_trick_token_freqs(d) for d in X_TRAIN__pd)
    hasher = FeatureHasher(n_features=100, input_type="string")
    X_TRAIN__pd = hasher.transform(hashing_trick_tokens(d) for d in X_TRAIN__pd) 
    
    #hasher = FeatureHasher(n_features=10, non_negative=True, input_type='string')
    #X_TRAIN__pd = hasher.transform(X_TRAIN__pd)
    print (X_TRAIN__pd.shape)

    #X_test__pd[nonNumVars_X_test] = hasher.transform(X_test__pd[nonNumVars_X_test])

    return X_test__pd, X_TRAIN__pd
    #test = hasher.transform(test)
    #X_train = hasher.transform(X_train)
    #y_train = hasher.transform(y_train)
    #submission_test = hasher.transform(submission_test)

 


def label_encode(nonNumVars_X_TRAIN,X_test__pd,X_TRAIN__pd,doPrintNA,doPrintNAShowFields):

    for field in nonNumVars_X_TRAIN: 
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(X_TRAIN__pd[field].values) + list(X_test__pd[field].values))
        X_TRAIN__pd[field] = lbl.transform(X_TRAIN__pd[field].values)
        X_test__pd[field]  = lbl.transform(X_test__pd[field].values)
        
    if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After LabelEncode",showFields=doPrintNAShowFields)        
    return X_test__pd,X_TRAIN__pd



def label_encode_lexical(dXT,d, condiciones_query, DO_OWEN, DO_DROP_ORIG_AFTER, DO_DROP_NEW_AFTER, X_test__pd,X_TRAIN__pd,y_TRAIN__pd,numRows_X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
            
        # FROM KAGGLE
        # # # # # # # # # #    
        def lab_encode(charcode):
                r = 0
                ln = len(charcode)

                if ln == 0:
                    r = -1
                else:
                    for i in range(ln):
                        r += (ord(charcode[i])-ord('A')+1)*26**(ln-i-1)
                return r   
    
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
    
        if len(fields) == 0:
            print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR LABELENCODE-LEXICAL." + Style.RESET_ALL)
    
    
        
        X_TRAIN_test__pd = pd.concat([X_TRAIN__pd,X_test__pd],axis=0)
    
        new_field_names = []
        
        for field in fields:
    
            # Name field_LabEnc
            new_field_name = field + d['SEPARATOR_LABEL_ENCODE_LEX']
            
            # Do (if field_LabEnc NOT already there) and (if field NOT already a owen).
            if (new_field_name in X_TRAIN__pd.columns) or (d['SEPARATOR_LABEL_ENCODE_LEX'] in field):
                pass
            
            else:
                new_field_names.append(new_field_name)
            
                field_values = X_TRAIN_test__pd[field].tolist()
                field_values = list(set(field_values))
                
                field_values_id = []
                
                for x in field_values:
                    field_values_id.append(lab_encode(x))
                
                df_group = pd.DataFrame({field:field_values, new_field_name :field_values_id})
                    
    
                # Metemos el Label_Enc en TrainTest                                 #left_on=field,right_index=True
                X_TRAIN_test__pd = X_TRAIN_test__pd.reset_index().merge(df_group, how="left").set_index('index')
                                    
                #   DROP
                if DO_DROP_ORIG_AFTER:
                    X_TRAIN_test__pd.drop([field], axis=1, inplace=True)
              
    
        X_TRAIN__pd = X_TRAIN_test__pd.iloc[range(0,numRows_X_TRAIN__pd)]
        X_test__pd = X_TRAIN_test__pd.iloc[range(numRows_X_TRAIN__pd,len(X_TRAIN_test__pd))]
    
        # Do OWEN
        for new_field in new_field_names:
            if DO_OWEN:
                fields_to_owen = [new_field_name]
                owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
                        
    
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After LabelEncode_Lexical",showFields=doPrintNAShowFields)

                
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
           
    return X_test__pd,X_TRAIN__pd



def monotonic_transformation(dXT,d,the_data_type,condiciones_query,X_test__pd,X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):  
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        for field in fields:
            grouped = X_TRAIN__pd.groupby([field])
            train_grouped = grouped[field].agg([np.count_nonzero])
                        
            train_grouped.rename(columns={'count_nonzero': field+d['SEPARATOR_MONOTONIC']}, inplace=True)

            X_TRAIN__pd = pd.merge(X_TRAIN__pd, train_grouped, how='left',left_on=field,right_index=True)
            X_test__pd = pd.merge(X_test__pd, train_grouped, how='left',left_on=field,right_index=True)
            
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Monotonic Transformation "+the_data_type,showFields=doPrintNAShowFields)        
        
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
        
    return X_test__pd,X_TRAIN__pd



def na_binarize(dXT,d, condiciones_query, X_test__pd, X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
            
        if len(fields) == 0:
            print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR NA_BINARIZE." + Style.RESET_ALL)
                
    
        for field in fields:
            num_NAs=np.sum(X_TRAIN__pd[field].isnull())
            if num_NAs > 0:
                X_TRAIN__pd[field + d['SEPARATOR_NA_BINARIZE'] + 'NA_bin']= 0
                X_TRAIN__pd[field + d['SEPARATOR_NA_BINARIZE'] + 'NA_bin'].loc[X_TRAIN__pd[field].isnull()]= 1
                X_test__pd[field + d['SEPARATOR_NA_BINARIZE'] + 'NA_bin']= 0
                X_test__pd[field + d['SEPARATOR_NA_BINARIZE'] + 'NA_bin'].loc[X_test__pd[field].isnull()]= 1
    
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After na_binarize",showFields=doPrintNAShowFields)
                
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")

    return X_test__pd, X_TRAIN__pd
        



def na_fill(dXT,d,PRP_NA_FILL,X_test__pd,X_TRAIN__pd,doPrintNA,doPrintNAShowFields):
    # Quita Infinitos
    X_TRAIN__pd = X_TRAIN__pd.replace([np.inf, -np.inf], -1)
    X_test__pd  = X_test__pd.replace( [np.inf, -np.inf], -1)
    
    # Quita NA
    for NA_FILL in PRP_NA_FILL:
        
        condiciones_query = NA_FILL[0]
        function_name = ''
        Vars_X_TRAIN = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name) 
        Vars_X_test = Vars_X_TRAIN
        
        
        if len(Vars_X_TRAIN) == 0:
            if doPrintNA:
                print(Fore.RED + str(NA_FILL[0]) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR NA_FILL." + Style.RESET_ALL)
            
        
        NEW_VALUE = NA_FILL[1]
    
    
        X_test__pd, X_TRAIN__pd = na_fill_dates(NEW_VALUE,vars_date(X_TRAIN__pd),vars_date(X_test__pd),X_test__pd,X_TRAIN__pd,doPrintNA,doPrintNAShowFields)   
        
    
        if NEW_VALUE == 'mode':
            X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(X_TRAIN__pd[Vars_X_TRAIN].mode())
            X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(X_test__pd[Vars_X_test].mode())
            X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna('--')
            X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna('--') 
        elif NEW_VALUE == 'mean':
            X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(X_TRAIN__pd[Vars_X_TRAIN].mean())       
            X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(X_test__pd[Vars_X_test].mean())
            X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(0)
            X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(0)
        elif NEW_VALUE == 'median':
            X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(X_TRAIN__pd[Vars_X_TRAIN].median())
            X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(X_test__pd[Vars_X_test].median())
            X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(0)
            X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(0)
        else:
            try:
               val = int(NEW_VALUE)
               X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(int(NEW_VALUE))
               X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(int(NEW_VALUE))
            except:
                try:
                    X_TRAIN__pd[Vars_X_TRAIN]=X_TRAIN__pd[Vars_X_TRAIN].fillna(NEW_VALUE)
                    X_test__pd[Vars_X_test]=X_test__pd[Vars_X_test].fillna(NEW_VALUE)
                except:
                    pass
        
        
    
    if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After na_fill",showFields=doPrintNAShowFields)
         
    return X_test__pd, X_TRAIN__pd




def na_fill_dates(PRP_NA_FILL_NON_NUMERIC,nonNumVars_X_TRAIN,nonNumVars_X_test,X_test__pd,X_TRAIN__pd,doPrintNA,doPrintNAShowFields):

    if PRP_NA_FILL_NON_NUMERIC == 'mode':
        X_TRAIN__pd[nonNumVars_X_TRAIN]=X_TRAIN__pd[nonNumVars_X_TRAIN].fillna(X_TRAIN__pd[nonNumVars_X_TRAIN].mode())
        X_test__pd[nonNumVars_X_test]=X_test__pd[nonNumVars_X_test].fillna(X_test__pd[nonNumVars_X_test].mode())
    #else:
    #    X_TRAIN__pd[nonNumVars_X_TRAIN]=X_TRAIN__pd[nonNumVars_X_TRAIN].fillna(PRP_NA_FILL_NON_NUMERIC)
    #    X_test__pd[nonNumVars_X_test]=X_test__pd[nonNumVars_X_test].fillna(PRP_NA_FILL_NON_NUMERIC) 

    #if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After na_fill_Dates",showFields=doPrintNAShowFields)

    return X_test__pd, X_TRAIN__pd



# pandas.isnull() checks for missing values in both numeric and string/object arrays.
# From the documentation, it checks for: NaN in numeric arrays, None/NaN in object arrays
def na_info(dfA__pd,dfA_name,dfB__pd,dfB_name):
    
    fieldsWithNAs_dfA = []
    fieldsNumNAs_dfA = []
    fieldsWithAllRowsNAs_dfA = []
    cols_names = list(dfA__pd.columns.values)    
    for field in cols_names:
        numNAs=np.sum(dfA__pd[field].isnull())
        if numNAs>0:
            fieldsWithNAs_dfA.append(field)
            fieldsNumNAs_dfA.append(numNAs)
            if numNAs == dfA__pd.shape[0]:
                fieldsWithAllRowsNAs_dfA.append(field)

    fieldsWithNAs_dfB = []
    fieldsNumNAs_dfB = []
    fieldsWithAllRowsNAs_dfB = []
    cols_names = list(dfB__pd.columns.values)    
    for field in cols_names:
        numNAs=np.sum(dfB__pd[field].isnull())
        if numNAs>0:
            fieldsWithNAs_dfB.append(field)
            fieldsNumNAs_dfB.append(numNAs)
            if numNAs == dfB__pd.shape[0]:
                fieldsWithAllRowsNAs_dfB.append(field)
    
    totalNAs_dfA = np.sum(fieldsNumNAs_dfA)
    totalNAs_dfB = np.sum(fieldsNumNAs_dfB)
    
    return fieldsWithNAs_dfA,fieldsWithAllRowsNAs_dfA,fieldsWithNAs_dfB,fieldsWithAllRowsNAs_dfB,totalNAs_dfA,totalNAs_dfB



    

def nlp_tfidf_svm(dXT,d, condiciones_query, DO_DROP_ORIG_AFTER, TRUNCATEDSVD_COMP, X_test__pd,X_TRAIN__pd,y_TRAIN__pd, COLUMN_CLASS, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name) 
        
        
        X_TRAIN__pd['autoinc'] = range(len(X_TRAIN__pd))
        X_test__pd['autoinc']  = range(len(X_test__pd))
        
        
        train = X_TRAIN__pd[fields]
        test = X_test__pd[fields]
        
    
    
        # drop columns
        column_names_drop = [c for c in list(train.columns.values) if c not in fields]
        
        for c in column_names_drop:
            try:
                train.drop(c, axis=1, inplace=True)
            except:
                pass
        
            try:
                test.drop(c, axis=1, inplace=True)
            except:
                pass
        
    
        # Stop words
        stop_w = []
        stop_words = ['http','www','img','border','0','1','2','3','4','5','6','7','8','9']
        stop_words = text.ENGLISH_STOP_WORDS.union(stop_words)
        for stw in stop_words:
            stop_w.append("q"+stw)
            stop_w.append("z"+stw)
        stop_words = text.ENGLISH_STOP_WORDS.union(stop_w)
        
        
        
        #remove html, remove non text or numeric, make keyword and title unique features for counts using prefix (accounted for in stopwords tweak)
        stemmer = PorterStemmer()
        
        X_train_list = []
        for i in range(len(train)):
            s=(" ").join(["q"+ z for z in BeautifulSoup(train["keyword"][i], "lxml").get_text(" ").split(" ")]) + " " + (" ").join(["z"+ z for z in BeautifulSoup(train['title_text'][i], "lxml").get_text(" ").split(" ")]) #+ " " + BeautifulSoup(train.product_description[i], "lxml").get_text(" ")
    
            s=re.sub("[^a-zA-Z0-9]"," ", s)
            s= (" ").join([stemmer.stem(z) for z in s.split(" ")])
            X_train_list.append(s)
            #s_labels.append(str(train["median_relevance"][i]))
            
        X_test_list = []
        for i in range(len(test)):
        #for i in test_idx:
            s=(" ").join(["q"+ z for z in BeautifulSoup(test["keyword"][i], "lxml").get_text().split(" ")]) + " " + (" ").join(["z"+ z for z in BeautifulSoup(test['title_text'][i], "lxml").get_text().split(" ")]) #+ " " + BeautifulSoup(test.product_description[i], "lxml").get_text()
    
            s=re.sub("[^a-zA-Z0-9]"," ", s)
            s= (" ").join([stemmer.stem(z) for z in s.split(" ")])
            X_test_list.append(s)
        
        
        # the infamous tfidf vectorizer (Do you remember this one?)
        tfv = TfidfVectorizer(min_df=3,  max_features=None, 
                strip_accents='unicode', analyzer='word',token_pattern=r'\w{1,}',
                ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1,
                stop_words = 'english')
    
    
        # Fit TFIDF
        tfv.fit(X_train_list)
        X_train_list =  tfv.transform(X_train_list)
        X_test_list = tfv.transform(X_test_list)
        
        
        ##################################################################
        
        svd = TruncatedSVD(n_components = TRUNCATEDSVD_COMP, n_iter=5)
        svd.fit(X_train_list)
        
        train_data = svd.transform(X_train_list)
        test_data = svd.transform(X_test_list)
        
            
        
        train_data = pd.DataFrame(train_data)
        test_data = pd.DataFrame(test_data)
        
        
        
        train_data.columns = [d['SEPARATOR_TFIDF'] + str(col) for col in train_data.columns]
        test_data.columns  = [d['SEPARATOR_TFIDF'] + str(col) for col in test_data.columns]
        
        
        train_data['autoinc'] = range(len(train_data))
        test_data['autoinc']  = range(len(test_data))
         
        
        X_TRAIN__pd = pd.merge(X_TRAIN__pd, train_data, on='autoinc', how='outer')
        X_test__pd = pd.merge(X_test__pd, test_data, on='autoinc', how='outer')
        
        
        X_TRAIN__pd.drop(['autoinc'], axis=1, inplace=True)
        X_test__pd.drop(['autoinc'], axis=1, inplace=True)
    
    
        #   DROP
        ############### 
        if DO_DROP_ORIG_AFTER:
            for c in fields:
                X_TRAIN__pd.drop([c], axis=1, inplace=True)
                X_test__pd.drop([c], axis=1, inplace=True)
    
    
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After TFIDF",showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd   










def normalize_numeric(dXT,d,condiciones_query,normType, X_TRAIN__pd, X_test__pd, numRows_X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    
    # Quita Infinitos
    X_TRAIN__pd = X_TRAIN__pd.replace([np.inf, -np.inf], -1)
    X_test__pd  = X_test__pd.replace( [np.inf, -np.inf], -1)
    
    try:
            
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        df__pd = pd.concat([X_TRAIN__pd,X_test__pd],axis=0)
        
        if normType == "Standarizing":
            for field in fields:
                df__pd[field] =  (df__pd[field] - df__pd[field].mean()) / df__pd[field].std() 
        elif normType == "Rescaling":
            for field in fields:
                df__pd[field] =  (df__pd[field] - df__pd[field].min()) / (df__pd[field].max() - df__pd[field].min()) 
        elif normType == "Other":
            for field in fields:
                df__pd[field] =  (df__pd[field] - df__pd[field].mean()) / (df__pd[field].max() - df__pd[field].min())
        else:
            sys.exit("Error")
        
        X_TRAIN__pd = df__pd.iloc[range(0,numRows_X_TRAIN__pd)]
        X_test__pd = df__pd.iloc[range(numRows_X_TRAIN__pd,len(df__pd))]
        
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Normalization",showFields=doPrintNAShowFields)    
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_TRAIN__pd,X_test__pd




def number_encode(dXT,d, condiciones_query, DO_OWEN, DO_DROP_ORIG_AFTER, DO_DROP_NEW_AFTER, dtype_2_encode, X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
    
        for field in fields:
    
            new_field_name = str(field) + d['SEPARATOR_NUMBER_ENCODE']
            
            T_dtype = str(X_TRAIN__pd[field].dtypes)[0:3]
            if T_dtype == dtype_2_encode[0:3]:
                X_TRAIN__pd[new_field_name]=X_TRAIN__pd[field].apply(str)
                X_test__pd[new_field_name]=X_test__pd[field].apply(str)
                #print("Field",field,"is type",T_dtype,"in TRAIN. Unique Values:",unique_Values,". Threshold:",THRESHOLD_NUMBER_ENCODE)
    
                # Do OWEN
                if DO_OWEN:
                    fields_to_owen = [new_field_name]
                    owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
                    
    
                # DROP NumberEncoded fields (only Owen is left)
                #if DO_DROP_NEW_AFTER:
                #    X_TRAIN__pd.drop(new_field_name, axis=1, inplace=True)
                #    X_test__pd.drop(new_field_name, axis=1, inplace=True)
                
            # DROP Original fields (only Owen is left)
            if DO_DROP_ORIG_AFTER:
                X_TRAIN__pd.drop(field, axis=1, inplace=True)
                X_test__pd.drop(field, axis=1, inplace=True)
                
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After " + dtype_2_encode + " Encode",showFields=doPrintNAShowFields)        
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd




def one_hot_encode(dXT,d, condiciones_query, DO_OWEN, DO_DROP_ORIG_AFTER, DO_DROP_NEW_AFTER, PRP_ONE_HOT_ENCODE_THRESHOLD,  SEPARATOR_HOT_ENCODE,X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        if len(fields) == 0:
            print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR ONE HOT ENCODE." + Style.RESET_ALL)
    
                
        for field in fields:      
            T_dtype = str(X_TRAIN__pd[field].dtypes)[0:3]
            if (T_dtype == 'obj') or (T_dtype == 'int'):
    
    
    
    
                # Duplicate field for OHE
                field_OHE              = field + SEPARATOR_HOT_ENCODE
                
                if len(d['PRP_NA_FILL'])==0:
                    empty_value='empty' if not ('empty' in X_TRAIN__pd[field].unique() or 'empty' in X_TRAIN__pd[field].unique() ) else id_execution_generator()
                    X_TRAIN__pd[field_OHE] = X_TRAIN__pd[field].fillna(empty_value)
                    X_test__pd[field_OHE]  = X_test__pd[field].fillna(empty_value)
                else:
                    X_TRAIN__pd[field_OHE] = X_TRAIN__pd[field]
                    X_test__pd[field_OHE]  = X_test__pd[field]
                
                
    
          
                # THRESHOLD
                X_test__pd, X_TRAIN__pd = replace_rare_strings(dXT,d, [field_OHE], 0, 0, PRP_ONE_HOT_ENCODE_THRESHOLD, X_test__pd,X_TRAIN__pd,y_TRAIN__pd,0,0)
    
                
                # LABELENCODE
                lblenc = preprocessing.LabelEncoder()
                lblenc.fit(list(X_TRAIN__pd[field_OHE].values.astype(str)) + list(X_test__pd[field_OHE].values.astype(str)))
                X_TRAIN__pd[field_OHE] = lblenc.transform(X_TRAIN__pd[field_OHE].values)
                X_test__pd[field_OHE]  = lblenc.transform(X_test__pd[field_OHE].values)        
        
                      
                
                
                train_values = X_TRAIN__pd[field_OHE].values
                test_values  = X_test__pd[field_OHE].values
                train_and_test_values = np.concatenate((train_values,test_values),axis=0)
                
                # ONE_HOT_ENCODE
                lbl = preprocessing.OneHotEncoder()
                lbl.fit(np.resize(np.array(train_and_test_values).astype(float), (len(train_and_test_values),1)))
                
                train_values = lbl.transform(np.resize(np.array(train_values).astype(float), (len(train_values),1))).toarray()        
                test_values  = lbl.transform(np.resize(np.array(test_values).astype(float), (len(test_values),1))).toarray()
                
                for i in range(train_values.shape[1]):
                    new_field_name = field_OHE + str(lblenc.inverse_transform([i]))[:10].replace("'","").replace("[","").replace("]","")
                    
                    # Creamos New Features (loop)
                    X_TRAIN__pd[new_field_name] = train_values[:,i]
                    X_test__pd[new_field_name]  = test_values[:,i]
                    
                    # Do OWEN
                    if DO_OWEN:
                        fields_to_owen = [new_field_name]
                        owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
                        
                
                # Drop temporary field_OHE ALWAYS
                X_TRAIN__pd.drop(field_OHE, axis=1, inplace=True)
                X_test__pd.drop(field_OHE, axis=1, inplace=True)
                
                    
                    
                if d['PRP_ONE_HOT_ENCODE_DROP_ONE_DUMMY']:
                    # Me cargo la última columna del onehotencode por ser redundante para el algoritmo
                    X_TRAIN__pd.drop([new_field_name], axis=1, inplace=True)
                    X_test__pd.drop([new_field_name], axis=1, inplace=True)
        
        
                # DROP Original fields (only Owen is left)
                if DO_DROP_ORIG_AFTER:
                    X_TRAIN__pd.drop(field, axis=1, inplace=True)
                    X_test__pd.drop(field, axis=1, inplace=True)
            else:
                print(Fore.RED + '\n\n"El campo ' + str(field) + ' es un campo ' + str(X_TRAIN__pd[field].dtypes) + ' y NO se le puede aplicar ONE_HOT_ENCODE\n\n"' + Style.RESET_ALL)
                
        X_cols_names = list(X_TRAIN__pd.columns.values)
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After OneHotEncode",showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_cols_names,X_test__pd,X_TRAIN__pd


    
    
    
    
def owen_trick_list_agg ( dXT, d ,fields_to_owen, DO_DROP_NEW_AFTER, X_TRAIN__pd, y_TRAIN__pd, X_test__pd, COLUMN_2_AGG, doPrintNA, doPrintNAShowFields, owen_metrics=None):
    
    if owen_metrics is None:
        owen_metrics=d['PRP_OWEN_TRICK_METRICS']

    if len(fields_to_owen) == 0:
        print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR OWEN TRICK." + Style.RESET_ALL)
        
    Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)  

    if owen_metrics==['default']:
        if Xy_TRAIN__pd[COLUMN_2_AGG].dtype == 'object' or d['COMP_PROBLEM_TYPE'].lower()[:7] == 'class_m':
            owen_metrics = ['mode']
        else:
            owen_metrics = ['mean']
    
    #allowed_metrics = ['mean','mode','std','max','min','sum','median']
    
    #print("owen with mean or mode:",owen_metrics,d['COMP_PROBLEM_TYPE'].lower()[:7],y_TRAIN__pd.dtype)
    
    for field in fields_to_owen:
        
        if field not in dXT["OWEN"]["Fields"]:
            dXT["OWEN"]["Fields"].append(field)
            dXT["OWEN"]["Drop"].append(DO_DROP_NEW_AFTER)
            dXT["OWEN"]["Metrics"].append(owen_metrics)

            
            
            
def owen_trick(dXT, d ,condiciones_query, DO_DROP_ORIG_AFTER, X_TRAIN__pd, y_TRAIN__pd, X_test__pd, COLUMN_2_AGG, function_name,doPrintNA,doPrintNAShowFields, owen_metrics=None):
    try:
        RANDOM_RANGE = d['PRP_OWEN_RANDOM_RANGE']  # multiplicará al owen en train. El rango será random(-RANDOM_RANGE , +RANDOM_RANGE)
    
        if owen_metrics is None:
            owen_metrics=d['PRP_OWEN_TRICK_METRICS']
    
        transform = d['PRP_OWEN_TRANSFORM']
        transform_prefix= transform + "_"
        if transform == "No":
            transform = None
            transform_prefix=""
    
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        
        numRows_X_TRAIN__pd = len(X_TRAIN__pd)
        
        
        
        
        
    
        Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)      
        
        Xy_TRAIN_test__pd = pd.concat([Xy_TRAIN__pd,X_test__pd],axis=0)
        
        
        
        
        
        
        if len(fields) == 0:
            print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR OWEN TRICK." + Style.RESET_ALL)
    
    
        #Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)  
    
        if owen_metrics==['default']:
            if Xy_TRAIN_test__pd[COLUMN_2_AGG].dtype == 'object' or d['COMP_PROBLEM_TYPE'].lower()[:7] == 'class_m':
                owen_metrics = ['mode']
            else:
                owen_metrics = ['mean']
        
        
        allowed_metrics = ['mean','mode','std','max','min','sum','median']
        
        
        #print("owen with mean or mode:",owen_metrics,d['COMP_PROBLEM_TYPE'].lower()[:7],y_TRAIN__pd.dtype)
                
    
        for field in fields:  
            for metric in owen_metrics:
                
                # Name field_owen
                if metric == 'mode':
                    field_owen_name = transform_prefix + field + d['SEPARATOR_OWEN'] + 'mode' + '_' + COLUMN_2_AGG[0:6]
                else:
                    field_owen_name = transform_prefix + field + d['SEPARATOR_OWEN'] + metric + '_' + COLUMN_2_AGG[0:6]
                
                # Do (if field_owen NOT already there) and (if field NOT already a owen).
                if (field_owen_name in X_TRAIN__pd.columns) or (d['SEPARATOR_OWEN'] in field):
                    pass
                
                else:
                
                    grouped = Xy_TRAIN_test__pd.groupby([field])
                    
                    
                    if metric == 'mode':
                        train_grouped = grouped[COLUMN_2_AGG].agg({'mode' : lambda x:x.value_counts().index[0]})
                        train_grouped.rename(columns={'mode': field_owen_name}, inplace=True)  
                    else:
                        train_grouped = grouped[COLUMN_2_AGG].agg([getattr(np, metric)])   # getattr(x, 'foobar') is equivalent to x.foobar 
                        train_grouped.rename(columns={metric: field_owen_name}, inplace=True)
    
                        
                    # Metemos el owen en Test
                    #X_test__pd = pd.merge(X_test__pd, train_grouped, how='left',left_on=field,right_index=True)
                    
                    # Metemos el owen en Train
                    Xy_TRAIN_test__pd = pd.merge(Xy_TRAIN_test__pd, train_grouped, how='left',left_on=field,right_index=True)
    
                    
                    
                    # Al owen del Train le aplicamos un factor aleatorio
                    if metric == 'mode':
                        pass
                    else:
                        
                        Xy_TRAIN_test__pd[field_owen_name + '_rnd'] = pd.Series(np.random.uniform(-RANDOM_RANGE,RANDOM_RANGE,len(Xy_TRAIN_test__pd[field_owen_name])), index=Xy_TRAIN_test__pd.index)                    
    
                        owen_column=Xy_TRAIN_test__pd[field_owen_name]*(1+Xy_TRAIN_test__pd[field_owen_name + '_rnd'])
                        if transform:
                            Xy_TRAIN_test__pd[field_owen_name] = np.log1p(owen_column)
                        else:
                            Xy_TRAIN_test__pd[field_owen_name] = owen_column
                        
                        Xy_TRAIN_test__pd = Xy_TRAIN_test__pd.drop(field_owen_name + '_rnd', axis=1)
    
                        
                        
                        
                # DROP Original fields (only Owen is left)
                if DO_DROP_ORIG_AFTER:
                    try:
                        Xy_TRAIN_test__pd.drop(field, axis=1, inplace=True)
                        #X_test__pd.drop(field, axis=1, inplace=True)
                    except:
                        print("Se intentó dropear el campo " + str(field) + " sin éxito.\n")
                        
    
                
                        
                        
        X_TRAIN_test__pd = Xy_TRAIN_test__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)
        
        
        
        X_TRAIN__pd = X_TRAIN_test__pd.iloc[range(0,numRows_X_TRAIN__pd)]
        X_test__pd = X_TRAIN_test__pd.iloc[range(numRows_X_TRAIN__pd,len(X_TRAIN_test__pd))]
              
        #X_TRAIN__pd = Xy_TRAIN__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)
        
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd

    
    
    
def owen_trick_legacy(dXT, d ,condiciones_query, DO_DROP_ORIG_AFTER, X_TRAIN__pd, y_TRAIN__pd, X_test__pd, COLUMN_2_AGG, function_name,doPrintNA,doPrintNAShowFields, owen_metrics=None):

    RANDOM_RANGE = d['PRP_OWEN_RANDOM_RANGE']  # multiplicará al owen en train. El rango será random(-RANDOM_RANGE , +RANDOM_RANGE)
    

    if owen_metrics is None:
        owen_metrics=d['PRP_OWEN_TRICK_METRICS']

    transform = d['PRP_OWEN_TRANSFORM']
    transform_prefix= transform + "_"
    if transform == "No":
        transform = None
        transform_prefix=""

    fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
    
    if len(fields) == 0:
        print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR OWEN TRICK." + Style.RESET_ALL)


    Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)  

    if owen_metrics==['default']:
        if Xy_TRAIN__pd[COLUMN_2_AGG].dtype == 'object' or d['COMP_PROBLEM_TYPE'].lower()[:7] == 'class_m':
            owen_metrics = ['mode']
        else:
            owen_metrics = ['mean']
    
    
    allowed_metrics = ['mean','mode','std','max','min','sum','median']
    
    
    #print("owen with mean or mode:",owen_metrics,d['COMP_PROBLEM_TYPE'].lower()[:7],y_TRAIN__pd.dtype)
            

    for field in fields:  
        for metric in owen_metrics:
            
            # Name field_owen
            if metric == 'mode':
                field_owen_name = transform_prefix + field + d['SEPARATOR_OWEN'] + 'mode' + '_' + COLUMN_2_AGG[0:6]
            else:
                field_owen_name = transform_prefix + field + d['SEPARATOR_OWEN'] + metric + '_' + COLUMN_2_AGG[0:6]
            
            # Do (if field_owen NOT already there) and (if field NOT already a owen).
            if (field_owen_name in X_TRAIN__pd.columns) or (d['SEPARATOR_OWEN'] in field):
                pass
            
            else:
            
                grouped = Xy_TRAIN__pd.groupby([field])
                
                
                if metric == 'mode':
                    train_grouped = grouped[COLUMN_2_AGG].agg({'mode' : lambda x:x.value_counts().index[0]})
                    train_grouped.rename(columns={'mode': field_owen_name}, inplace=True)  
                else:
                    train_grouped = grouped[COLUMN_2_AGG].agg([getattr(np, metric)])   # getattr(x, 'foobar') is equivalent to x.foobar 
                    train_grouped.rename(columns={metric: field_owen_name}, inplace=True)
        
                # Metemos el owen en Test
                X_test__pd = pd.merge(X_test__pd, train_grouped, how='left',left_on=field,right_index=True)
                
                # Metemos el owen en Train
                Xy_TRAIN__pd = pd.merge(Xy_TRAIN__pd, train_grouped, how='left',left_on=field,right_index=True)
          
                # Al owen del Train le aplicamos un factor aleatorio
                if metric == 'mode':
                    pass
                else:
                    
                    Xy_TRAIN__pd[field_owen_name + '_rnd'] = pd.Series(np.random.uniform(-RANDOM_RANGE,RANDOM_RANGE,len(Xy_TRAIN__pd[field_owen_name])), index=Xy_TRAIN__pd.index)                    

                    owen_column=Xy_TRAIN__pd[field_owen_name]*(1+Xy_TRAIN__pd[field_owen_name + '_rnd'])
                    if transform:
                        Xy_TRAIN__pd[field_owen_name] = np.log1p(owen_column)
                    else:
                        Xy_TRAIN__pd[field_owen_name] = owen_column
                    
                    Xy_TRAIN__pd = Xy_TRAIN__pd.drop(field_owen_name + '_rnd', axis=1)

            # DROP Original fields (only Owen is left)
            if DO_DROP_ORIG_AFTER:
                try:
                    X_TRAIN__pd.drop(field, axis=1, inplace=True)
                    X_test__pd.drop(field, axis=1, inplace=True)
                except:
                    print("Se intentó dropear el campo " + str(field) + " sin éxito.\n")
                    

                    
    X_TRAIN__pd = Xy_TRAIN__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)

    return X_test__pd,X_TRAIN__pd

    
    
    
    
    
    
def owen_in_fold(dXT, d , X_train__pd, X_val__pd, y_train__pd, X_test__pd, COLUMN_2_AGG, doPrintNA, doPrintNAShowFields):
    # he quitado    condiciones_query, DO_DROP_ORIG_AFTER, owen_metrics
    
    the_fields  = dXT['OWEN']['Fields']
    the_metrics = dXT['OWEN']['Metrics']
    the_drops   = dXT['OWEN']['Drop']
    

    RANDOM_RANGE = d['PRP_OWEN_RANDOM_RANGE']  # multiplicará al owen en train. El rango será random(-RANDOM_RANGE , +RANDOM_RANGE)
    
    
    #if len(the_fields) == 0:
    #   print(Fore.RED + "NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR OWEN TRICK." + Style.RESET_ALL)


    transform = d['PRP_OWEN_TRANSFORM']
    transform_prefix= transform + "_"
    if transform == "No":
        transform = None
        transform_prefix=""

    
    Xy_train__pd = pd.concat([X_train__pd, y_train__pd], axis=1)  


    for idx, field in enumerate(the_fields):  
        for metric in the_metrics[idx]:
            
            DO_DROP_ORIG_AFTER = the_drops[idx]
            
            #print(idx, field,metric,DO_DROP_ORIG_AFTER)
            
            # Name field_owen
            if metric == 'mode':
                field_owen_name = transform_prefix + field + d['SEPARATOR_OWEN'] + 'mode' + '_' + COLUMN_2_AGG[0:6]
            else:
                field_owen_name = transform_prefix + field + d['SEPARATOR_OWEN'] + metric + '_' + COLUMN_2_AGG[0:6]
            
                
                #print('field_owen_name',field_owen_name)

            # Do (if field_owen NOT already there) and (if field NOT already a owen).
            if (field_owen_name in X_train__pd.columns) or (d['SEPARATOR_OWEN'] in field):
                pass
            
            else:
                try:
                            
                    grouped = Xy_train__pd.groupby([field])
                    
    
                              
                    
                    if metric == 'mode':
                        train_grouped = grouped[COLUMN_2_AGG].agg({'mode' : lambda x:x.value_counts().index[0]})
                        train_grouped.rename(columns={'mode': field_owen_name}, inplace=True)  
                    else:
                        train_grouped = grouped[COLUMN_2_AGG].agg([getattr(np, metric)])   # getattr(x, 'foobar') is equivalent to x.foobar 
                        train_grouped.rename(columns={metric: field_owen_name}, inplace=True)
    
            
                    # Metemos el owen en Test
                    X_test__pd = pd.merge(X_test__pd, train_grouped, how='left',left_on=field,right_index=True) 
                    
                    # Metemos el owen en Val
                    X_val__pd = pd.merge(X_val__pd, train_grouped, how='left',left_on=field,right_index=True)
                    
                    # Metemos el owen en Train
                    Xy_train__pd = pd.merge(Xy_train__pd, train_grouped, how='left',left_on=field,right_index=True)
    
                    
                    # Al owen del Train le aplicamos un factor aleatorio
                    if metric == 'mode':
                        pass
                    else:
                        
                        Xy_train__pd[field_owen_name + '_rnd'] = pd.Series(np.random.uniform(-RANDOM_RANGE,RANDOM_RANGE,len(Xy_train__pd[field_owen_name])), index=Xy_train__pd.index)                    
    
                        owen_column=Xy_train__pd[field_owen_name]*(1+Xy_train__pd[field_owen_name + '_rnd'])
                        if transform:
                            Xy_train__pd[field_owen_name] = np.log1p(owen_column)
                        else:
                            Xy_train__pd[field_owen_name] = owen_column
                        
                        Xy_train__pd = Xy_train__pd.drop(field_owen_name + '_rnd', axis=1)
                        
                        
                        #print()
                        #print(Xy_train__pd)
                        #print()  
                        
                        
                except:
                    pass
            
                    
            # DROP Original fields (only Owen is left)
            #######################
            if DO_DROP_ORIG_AFTER:
                try:
                    Xy_train__pd.drop(field, axis=1, inplace=True)
                except:
                    print("Se intentó dropear el campo " + str(field) + " en X_train__pd sin éxito.\n")
                    
                try:
                    X_val__pd.drop(field, axis=1, inplace=True)
                except:
                    print("Se intentó dropear el campo " + str(field) + " en X_val__pd sin éxito.\n")
                    
                try:
                    X_test__pd.drop(field, axis=1, inplace=True)
                except:
                    print("Se intentó dropear el campo " + str(field) + " en X_test__pd sin éxito.\n")
                    

                 
    X_train__pd = Xy_train__pd.drop(d['COMP_COLUMN_CLASS'], axis=1)
    
##########################################fffffffffffffffffffff###############################################
    return X_train__pd, X_val__pd, X_test__pd

    



def print_na_info(dfA__pd,dfA_name,dfB__pd,dfB_name,calculate=False,fieldsWithNAs_dfA=None,fieldsWithAllRowsNAs_dfA=None,fieldsWithNAs_dfB=None,fieldsWithAllRowsNAs_dfB=None,totalNAs_dfA=None,totalNAs_dfB=None,message="",showFields=False):
    if calculate:    
        fieldsWithNAs_dfA,fieldsWithAllRowsNAs_dfA,fieldsWithNAs_dfB,fieldsWithAllRowsNAs_dfB,totalNAs_dfA,totalNAs_dfB = na_info(dfA__pd,dfA_name,dfB__pd,dfB_name)
    print("Info NAs -",message) 
    #print()         
    print("   ",dfA_name,dfA__pd.shape," FieldsWithNAs:",len(fieldsWithNAs_dfA)," FieldsWithAllRowsNAs:",len(fieldsWithAllRowsNAs_dfA)," TotalNAs:",totalNAs_dfA)
    #print()    
    if showFields:
        if len(fieldsWithNAs_dfA)>0:
            print("FieldsWithNAs:",fieldsWithNAs_dfA)
            print()
        if len(fieldsWithAllRowsNAs_dfA)>0:
            print("FieldsWithAllRowsNAs:",fieldsWithAllRowsNAs_dfA)
            print()
    print("    ",dfB_name,dfB__pd.shape," FieldsWithNAs:",len(fieldsWithNAs_dfB)," FieldsWithAllRowsNAs:",len(fieldsWithAllRowsNAs_dfB)," TotalNAs:",totalNAs_dfB)
    print()    
    if showFields: 
        if len(fieldsWithNAs_dfB)>0:
            print("FieldsWithNAs:",fieldsWithNAs_dfB)
            print()
        if len(fieldsWithAllRowsNAs_dfB)>0:
            print("FieldsWithAllRowsNAs:",fieldsWithAllRowsNAs_dfB) 
            print()
    #print("")
    return None



        
def replacer(dXT,d, X_test__pd, X_TRAIN__pd,y_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        for reemplazamientos in d[function_name]:
          
            condiciones_query = reemplazamientos[0]
            REPLACE_THIS = reemplazamientos[1]
            PUT_THIS = reemplazamientos[2]
            
            fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
            
            if len(fields) == 0:
                print(Fore.RED + str(condiciones_query) + ": NO SE HA ENCONTRADO NINGUNA COLUMNA PARA APLICAR REPLACER." + Style.RESET_ALL)
            
            for field in fields:
                
                X_TRAIN__pd[field].replace(REPLACE_THIS, PUT_THIS, inplace=True)
                X_test__pd[field].replace(REPLACE_THIS, PUT_THIS, inplace=True)   
    
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Replacer",showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_test__pd,X_TRAIN__pd

    
    
    
def replace_rare_strings(dXT,d, condiciones_query, DO_OWEN, DO_DROP_NEW_AFTER, RARE_STRINGS_THRESHOLD, X_test__pd,X_TRAIN__pd,y_TRAIN__pd,doPrintNA,doPrintNAShowFields,function_name=''):
    
    fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
    
    numRows_X_TRAIN__pd = X_TRAIN__pd.shape[0]
    
    X_TRAIN_test__pd = pd.concat([X_TRAIN__pd,X_test__pd],axis=0)
                
    for field in fields:
        X_TRAIN_test__pd.loc[X_TRAIN_test__pd[field].value_counts()[X_TRAIN_test__pd[field]].values < (RARE_STRINGS_THRESHOLD-1), field] = "RAREVAL"
           
    X_TRAIN__pd = X_TRAIN_test__pd.iloc[range(0,numRows_X_TRAIN__pd)]
    X_test__pd = X_TRAIN_test__pd.iloc[range(numRows_X_TRAIN__pd,len(X_TRAIN_test__pd))]                      
                       
                       
    for field in fields:
        # Do OWEN
        if DO_OWEN:
            fields_to_owen = [field]
            owen_trick_list_agg(dXT,d,fields_to_owen,DO_DROP_NEW_AFTER,X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])

        # DROP Original fields (only Owen is left)
        #if DO_DROP_NEW_AFTER:
        #    X_TRAIN__pd.drop(field, axis=1, inplace=True)
        #    X_test__pd.drop(field, axis=1, inplace=True)

    if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After RareEventsRemove",showFields=doPrintNAShowFields)

    return X_test__pd,X_TRAIN__pd




def split_train__pd_test__pd_components(TRAIN__pd,test__pd,COLUMN_ID,COLUMN_CLASS):
        
    if COLUMN_ID in TRAIN__pd.columns:
        idx_TRAIN__pd = TRAIN__pd.pop(COLUMN_ID)
    else:
        if COLUMN_ID in test__pd.columns:
            indice_max_test = max(test__pd[COLUMN_ID].values)
        else:
            indice_max_test = -1
        idx_TRAIN__pd = np.arange(indice_max_test+1,indice_max_test+1+len(TRAIN__pd))
            
 
    y_TRAIN__pd = TRAIN__pd.pop(COLUMN_CLASS)
    X_TRAIN__pd = TRAIN__pd

    if COLUMN_ID in test__pd.columns:
        idx_test__pd = test__pd.pop(COLUMN_ID)
    else:
        indice_max_train = max(idx_TRAIN__pd)
        idx_test__pd = np.arange(indice_max_train+1,indice_max_train+1+len(test__pd))
        
    X_test__pd = test__pd
    
    X_cols_names = list(X_TRAIN__pd.columns.values)
    
    vars_numeric_orig = vars_numeric(X_TRAIN__pd)
    vars_non_numeric_orig = vars_non_numeric(X_TRAIN__pd)
    vars_date_orig = vars_date(X_TRAIN__pd)
    
    return idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd, X_cols_names, vars_numeric_orig, vars_non_numeric_orig, vars_date_orig, X_cols_names
    






def submissions_as_feat(COLUMN_CLASS,OUTPATH_SUBMISSIONS_LEVEL_2,OUTPATH_SUBMISSIONS_LEVEL_1,OUTPATH_SUBMISSIONS_VALIDATION,test__pd,TRAIN__pd):
    
    #       Submissions import
    csv_PathFiles,files = file_names_get_all(OUTPATH_SUBMISSIONS_VALIDATION)
    
    submis_Num = len(csv_PathFiles)
    
    if submis_Num > 0:
    
        firstFile_data = (pd.read_csv(csv_PathFiles[0]))
        preds_dtype = firstFile_data[[COLUMN_CLASS]].values[:,0].dtype
        print ('\nClass datatype is: ',preds_dtype,'. There are',submis_Num,'predictions to combine:\n')
        
        ensemble_Name=''
        
        submis_Score = np.zeros(submis_Num, dtype=float)    
        
        #--------------------------------
        #  Loop por files
        ##################
        counter=0
        
        for submis in files:
            #sum_from_csv(each)   
            submis_Name_Slots = str(submis.replace(".csv","")).split('_')
            submis_ID = submis_Name_Slots[-1]
            ensemble_Name += submis_Name_Slots[5] + '-'
            submis_Score[counter] = float('0.'+ submis_Name_Slots[0])*10
            
            submis_Train = pd.read_csv(OUTPATH_SUBMISSIONS_VALIDATION+submis)
            submis_Train.rename(columns={COLUMN_CLASS:COLUMN_CLASS+"_"+submis_ID}, inplace=True)
            TRAIN__pd = pd.concat([TRAIN__pd, submis_Train[[COLUMN_CLASS+"_"+submis_ID]]], axis=1)
            
            submis_Test = pd.read_csv(OUTPATH_SUBMISSIONS_LEVEL_1+submis)
            submis_Test.rename(columns={COLUMN_CLASS:COLUMN_CLASS+"_"+submis_ID}, inplace=True)
            test__pd = pd.concat([test__pd, submis_Test[[COLUMN_CLASS+"_"+submis_ID]]], axis=1)
    
            counter=counter+1

    return test__pd,TRAIN__pd


def uniqfy_list(lista):
    new_list =  list(set(lista))
    return new_list
    
def uniqfy_list_of_lists(list_of_listas):
    new_list = list(set(x for l in list_of_listas for x in l))
    return new_list


def vars_boolean(df__pd):
    return list(df__pd.dtypes[df__pd.dtypes == "bool"].index)

def vars_numeric(df__pd):
    return list(df__pd.dtypes[df__pd.dtypes != "object"][df__pd.dtypes != "datetime64[ns]"].index)

def vars_non_numeric(df__pd):
    return list(df__pd.dtypes[df__pd.dtypes == "object"].index)

def vars_date(df__pd):
    return list(df__pd.dtypes[df__pd.dtypes == "datetime64[ns]"].index)




def x_transform(dXT,d,condiciones_query,XTRANSFORM, X_TRAIN__pd,X_test__pd,numRows_X_TRAIN__pd, function_name,doPrintNA,doPrintNAShowFields):
    try:
        
        def rename_xtrans_field(df__pd, field, XTRANSFORM):
            df__pd.rename(columns={field: field + '|Xtr-' + str(XTRANSFORM) + '|'}, inplace=True)
            return df__pd
        
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        df__pd = pd.concat([X_TRAIN__pd,X_test__pd],axis=0)
    
        if XTRANSFORM[0:6] == 'boxcox':
            skewed_fields = df__pd[fields].apply(lambda x: skew(x.dropna()))
            # transform features with skew > skew_thres
            if XTRANSFORM[6:] == "":
                skew_thres = 0.25
            else:
                skew_thres = float(XTRANSFORM[6:])
                
            skewed_fields = skewed_fields[skewed_fields > skew_thres]
            skewed_fields = skewed_fields.index
            
            for field in fields:
                df__pd[field] = df__pd[field] + 1
                df__pd[field], lam = boxcox(df__pd[field])
                df__pd = rename_xtrans_field(df__pd, field, XTRANSFORM)
    
        elif XTRANSFORM == 'exp':
            for field in fields:
                df__pd[field] = np.expm1(df__pd[field])
                df__pd = rename_xtrans_field(df__pd, field, XTRANSFORM)
            
        elif XTRANSFORM == 'int':
            for field in fields:
                df__pd[field] = df__pd[field].astype(int)
                df__pd = rename_xtrans_field(df__pd, field, XTRANSFORM)
        
        elif XTRANSFORM == 'log':
            for field in fields:
                df__pd[field] = np.log1p(df__pd[field])
                df__pd = rename_xtrans_field(df__pd, field, XTRANSFORM)
                
        elif XTRANSFORM == 'sqrt':
            for field in fields:
                df__pd[field] = np.sqrt(df__pd[field])
                df__pd = rename_xtrans_field(df__pd, field, XTRANSFORM)
    
        elif XTRANSFORM == 'square':
            for field in fields:
                df__pd[field] = df__pd[field]**2
                df__pd = rename_xtrans_field(df__pd, field, XTRANSFORM)
                
        else:
            print("XTRANSFORM does not match any of the available ones (exp, int, log, sqrt,...)")
                
        
        X_TRAIN__pd = df__pd.iloc[range(0,numRows_X_TRAIN__pd)]
        X_test__pd = df__pd.iloc[range(numRows_X_TRAIN__pd,len(df__pd))]
        
        
        #if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After X transform     ",showFields=doPrintNAShowFields)    :
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message=("After X transform" + XTRANSFORM), showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
              
    return X_TRAIN__pd,X_test__pd
  


def winsoring(dXT,d,condiciones_query,X_test__pd,X_TRAIN__pd,THRESHOLD_WINSORING_LOWER_LIMIT,THRESHOLD_WINSORING_UPPER_LIMIT, function_name,doPrintNA,doPrintNAShowFields):
    try:
        fields = query_col(dXT,d,condiciones_query,X_TRAIN__pd,function_name)
        
        print(fields)
    
        numRows_X_TRAIN__pd = X_TRAIN__pd.shape[0]
        X_T_t = pd.concat([X_TRAIN__pd,X_test__pd],axis=0)
        
        for field in fields:
            X_T_t[field] = mstats.winsorize(a=X_T_t[field], limits=(THRESHOLD_WINSORING_LOWER_LIMIT,1-THRESHOLD_WINSORING_UPPER_LIMIT), inplace=False, axis=None)
        if doPrintNA: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Winsoring",showFields=doPrintNAShowFields)
                
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return X_T_t.iloc[0:numRows_X_TRAIN__pd], X_T_t.iloc[numRows_X_TRAIN__pd:X_T_t.shape[0]]




def winsoring_remove_y(d,TRAIN__pd,THRESHOLD_WINSORING_LOWER_LIMIT,THRESHOLD_WINSORING_UPPER_LIMIT, function_name,doPrintNA,doPrintNAShowFields):
    try:
        print("Filas en TRAIN antes de winsoring_remove_y",TRAIN__pd.shape[0])
        if THRESHOLD_WINSORING_LOWER_LIMIT > 0:
            if THRESHOLD_WINSORING_UPPER_LIMIT > 0:
                print("L1U1")
                TRAIN__pd=TRAIN__pd.loc[(TRAIN__pd[d['COMP_COLUMN_CLASS']] > TRAIN__pd[d['COMP_COLUMN_CLASS']].quantile(THRESHOLD_WINSORING_LOWER_LIMIT)) & (TRAIN__pd[d['COMP_COLUMN_CLASS']] < TRAIN__pd[d['COMP_COLUMN_CLASS']].quantile(THRESHOLD_WINSORING_UPPER_LIMIT))]
            else:
                print("L1U0")
                TRAIN__pd=TRAIN__pd.loc[(TRAIN__pd[d['COMP_COLUMN_CLASS']] > TRAIN__pd[d['COMP_COLUMN_CLASS']].quantile(THRESHOLD_WINSORING_LOWER_LIMIT))]
        else:        
            if THRESHOLD_WINSORING_UPPER_LIMIT > 0:
                print("L0U1")
                TRAIN__pd=TRAIN__pd.loc[(TRAIN__pd[d['COMP_COLUMN_CLASS']] < TRAIN__pd[d['COMP_COLUMN_CLASS']].quantile(THRESHOLD_WINSORING_UPPER_LIMIT))]
                   
        print("Filas en TRAIN después de winsoring_remove_y",TRAIN__pd.shape[0])
        #if doPrintNA: print_na_info(TRAIN__pd,"TRAIN__pd",calculate=True,message="After Winsoring Remove Y",showFields=doPrintNAShowFields)
            
    except Exception as e: dXT["PRP_ERRORS"][function_name] = e; print("\n" + str("-"*70) + "\nThe Preprocessing " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n")
       
    return TRAIN__pd






