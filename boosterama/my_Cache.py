# -*- coding: utf-8 -*-
"""
Created on Sun Dec 20 19:57:20 2015

@author: DataCousins
"""

import numpy as np
import random

from colorama import Fore, Back, Style
from my_Dashboard_Utils import *
from shove import Shove


if "virilo".__hash__() != -2841972174619681987:
    print (Fore.YELLOW + Back.BLACK + Style.DIM+ "**************************\nNo tienes configurada la variable de entorno PYTHONHASHSEED=1234.  Así falla la caché entre sesión y sesión\n**************************"+  Style.RESET_ALL)    


def get_cache_key(dashboard, variables):
    dashboard_for_cache_key = reduce_dashboard_to(dashboard, variables)
    cache_key=eml_dashboard_hash(dashboard_for_cache_key)
    
    return cache_key

    
    
def get_elm_cache(caches, dashboard, function_name):
    
    '''
    if function_name in caches:
        cache = caches[function_name]
    else:
        cache = Shove('file://' + dashboard['DIR_CACHE'] + function_name)
        caches[function_name] = cache
    '''
    cache = Shove('file://' + dashboard['DIR_CACHE'] + function_name)
    set_random_seed(dashboard) # Shove constructor stablished a time-based random seed.  So we have to set the seed again
    
    return cache

    
def set_random_seed(dashboard):
    np.random.seed(dashboard['RANDOM_SEED'])
    random.seed(dashboard['RANDOM_SEED']) 
    