# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 16:14:01 2015

@author: DataCousins
"""


import numpy as np
import pandas as pd
from random import randrange, uniform

'''

Reiterator functions receive three parameters: value, variable_scores, greater_is_better

    value:
    
        value for a variable on the current winner dashboard
    
    variable_scores
    
        dictionary with variable values as keys, and their best scores on any dashboard executed
        
        notice that the best score couldn't be on winner dashboard


· It is not necessary to inclue "value" on reiterator results because the framework take care of it

· Reiterators could return duplicated values.  The framework removes duplicated values




'''

DEBUG_MODE=True

def increment_reiterator(value, variable_scores, greater_is_better):
    return [value+1, value+2, value+3, value+4, value+5]

def surrounding_values_intermediate_reiterator(value, variable_scores, greater_is_better):
    values = []
    
    (lower, upper) = get_surrounding_values(value, variable_scores)
    
    if DEBUG_MODE:
        print ("surrounding_values_intermediate_reiterator - value: " + str(value) + ", lower: " + str(lower)+ ", upper: " + str(upper))
    
    if not upper is None:
        values.append(value + (upper - value) / 2)
    
    if not lower is None:
        values.append(value - (value - lower) / 2)
    
    return values

def best_values_intermediate_reiterator(value, variable_scores, greater_is_better):
    values = []
    
    (lower, upper) = get_best_surrounding_values(value, variable_scores, greater_is_better)
    
    
    if DEBUG_MODE:
        print ("best_values_intermediate_reiterator - value: " + str(value) + ", lower: " + str(lower)+ ", upper: " + str(upper))
    
    if not upper is None:
        values.append(value + (upper - value) / 2)
        values.append(value + (upper - value) / 4)
        values.append(value + (upper - value) / 8)
    
    if not lower is None:
        values.append(value - (value - lower) / 2)
        values.append(value - (value - lower) / 4)
        values.append(value - (value - lower) / 8)
    
    return values

def get_surrounding_values(value, variable_scores):
    (lower, upper) = (None, None)
    
    variable_scores__pd = variable_scores_to_pandas(variable_scores)
    
    upper__pd = variable_scores__pd.loc[variable_scores__pd['value'] > value]
    lower__pd = variable_scores__pd.loc[variable_scores__pd['value'] < value]
    
    upper__pd.sort(['value'], ascending=[True], inplace=True)
    lower__pd.sort(['value'], ascending=[False], inplace=True)
    
    if not upper__pd.empty:
        upper = upper__pd.iloc[0]['value']
    
    if not lower__pd.empty:
        lower = lower__pd.iloc[0]['value']
    
    
    return (lower, upper)

def get_best_surrounding_values(value, variable_scores, greater_is_better):
    (lower, upper) = (None, None)
    
    variable_scores__pd = variable_scores_to_pandas(variable_scores)
    
    upper__pd = variable_scores__pd.loc[variable_scores__pd['value'] > value]
    lower__pd = variable_scores__pd.loc[variable_scores__pd['value'] < value]
    
    upper__pd.sort(['best_score'], ascending=[not greater_is_better], inplace=True)
    lower__pd.sort(['best_score'], ascending=[not greater_is_better], inplace=True)
    
    if not upper__pd.empty:
        upper = upper__pd.iloc[0]['value']
    
    if not lower__pd.empty:
        lower = lower__pd.iloc[0]['value']
    
    
    return (lower, upper) 

def variable_scores_to_pandas(variable_scores):
    #variable_scores_array = np.array(variable_scores.items())
    variable_scores__pd = pd.DataFrame(list(variable_scores.items()), columns=['value', 'best_score'])
    return variable_scores__pd