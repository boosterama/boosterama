# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 10:28:23 2016

@author: DataCousins
"""




import numpy as np

from my_Caruana_fx import *

def caruana(d, results, val_exists, columns_label_ordered, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test):
    
    # Trasponemos pues la funcion de Caruana lo espera traspuesto 
    X_test_transp = np.transpose(X_test)
    X_train_transp = np.transpose(X_train)
    y_train_transp = np.transpose(y_train)
    
    if val_exists:
        X_val_transp = np.transpose(X_val)
    else:
        X_val_transp = []

    
    # Ejecutamos Caruana
    y_train_preds_transpose, y_test_preds_transpose, y_val_preds_transpose, caruana_coeffs, caruana_rounds = caruana_selection_ensemble(columns_label_ordered, d,X_test_transp, X_train_transp, y_train_transp, val_exists, X_val_transp,idx_train,idx_test)

    # Seleccionamos los campos indicados devolviendo la información en el mismo
    # orden que la lista de id_execution que pasamos a la funcion
    external_scripts_order = sqlite_query_select_by_list_order_by_list(['EXTERNAL_SCRIPTS_IMPORT'],columns_label_ordered,d['DIR_DATA'])
    print(external_scripts_order)
    
    # Guardamos información para Resultados
    results['caruana_coeffs'] = caruana_coeffs
    results['caruana_rounds'] = caruana_rounds
    

    # Trasponemos pues la funcion de Caruana lo devuelve traspuesto
    y_train_preds = np.transpose(y_train_preds_transpose)
    if val_exists:
        y_val_preds = np.transpose(y_val_preds_transpose)
    y_test_preds = np.transpose(y_test_preds_transpose)
    
    # Aseguramos que los vectores columna tienen dimension (filas,) igual que en la salida de 1_ML o 2_ML
    y_train = np.reshape(y_train,(-1,))
    idx_test = np.reshape(idx_test,(-1,))
    y_test_preds = np.reshape(y_test_preds,(-1,))
    if val_exists:
        y_val = np.reshape(y_val,(-1,))            
        y_val_preds = np.reshape(y_val_preds,(-1,))

    if val_exists:
        return(idx_test, y_test_preds, idx_val, y_val, y_val_preds, idx_train, y_train, y_train_preds)
    else:
        return(idx_test, y_test_preds, idx_train, y_train, y_train_preds)

        


        