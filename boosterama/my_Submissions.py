# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 18:25:11 2016

@author: DataCousins
"""

from my_Submissions_fx import *

def submission_generation(d, results, best_parameters, CUSTOM_SUBMISSION_CSV, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION, idx_test, y_test_preds, execution_CV_stopped, split_cv_folds_orig, execution_CV_stopped_last_fold, idx_val=None, y_val_preds=None):
    
    if d['SYS_DO_SUBMISSION']:
        
        if 'ToInt' in d['ML_PRP_Y_APPLY_RESTRICTIONS']:
            y_test_preds = y_test_preds.astype(int)
            
            
        # mod_Score_4_sub_name
        #######################
        if d['ID_FAMILY']==0 and d['LEVEL']!='1_EXTERNAL_SCRIPTS':
            mod_Score_4_sub= "NA_"+str(results['model_score_train'])[0:5].replace(".",",")
        
        elif d['ID_FAMILY']==0 and d['LEVEL']=='1_EXTERNAL_SCRIPTS':
            mod_Score_4_sub= str(results['so_puntuacion'])[0:8].replace(".",",")+"_"+str(results['model_score_val'])[0:8].replace(".",",")+"_NA"
        
        elif d['ID_FAMILY']!=0 and d['LEVEL']!='1_EXTERNAL_SCRIPTS':
            mod_Score_4_sub= "NA_"+str(results['model_score_val'])[0:8].replace(".",",")+"_"+str(results['model_score_train'])[0:5].replace(".",",")
        
        elif d['ID_FAMILY']!=0 and d['LEVEL']=='1_EXTERNAL_SCRIPTS':
            mod_Score_4_sub= str(results['so_puntuacion'])[0:8].replace(".",",")+"_"+str(results['model_score_val'])[0:8].replace(".",",")+str(results['model_score_train'])[0:5].replace(".",",")

            
         
        if d['LEVEL']=='2_ENSEMBLE':
            mod_Score_4_sub= "NA_" + str(results['model_score_train'])[0:8].replace(".",",") + "_" + str(results['model_score_train'])[0:5].replace(".",",")
          
            
            
        # Name   
        results['submission_name'] = submission_name_generate(d,d['ID'],mod_Score_4_sub,best_parameters, execution_CV_stopped, split_cv_folds_orig, execution_CV_stopped_last_fold)            
        
        # Generate   
        submission_file_generate(d, idx_test,y_test_preds, CUSTOM_SUBMISSION_CSV,DIR_OUTPATH_SUBMISSIONS,results['submission_name'],d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])
    
        
        if (d['LEVEL']<'3'):
            # submission VALIDATION
            if d['SPLIT_CV_KFOLDS']>1 or (d['SPLIT_CV_KFOLDS']==1 and d['SPLIT_VALIDATION_SET_PERC']!=0.0) or d['LEVEL']=='2_ENSEMBLE':
                submission_file_generate(d, idx_val,y_val_preds, CUSTOM_SUBMISSION_CSV,DIR_OUTPATH_SUBMISSIONS_VALIDATION,results['submission_name'],d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])

    return None