# Changelog

## 0.3.54 (2017-05-05)
- Several changes accommodating Electricity use case.

## 0.3.53 (2017-02-25)
- TryExcepts in all preprocessings. Now if an error occurs in a specific preprocessing, the submission will end correctly and the error will be printed at the end of submission (sometimes the error may lead to further uncatchable errors).

## 0.3.52 (2017-02-13)
- Fastloop now working after some issues after substitution of queries by the actual columns.

## 0.3.51 (2017-01-31)
- Feature Importance reports added, showing best features and comparing features with the best previous 'similar' submission.

## 0.3.50 (2017-01-23)
- Early stopping for CV. If first fold score is worst than a previous score for that specific fold, it will stop and persist the execution as a normal Validation execution. Folds are randomized so we end up having 'partial' executions for all folds.

## 0.3.49 (2017-01-19)
- All kind of comparisons with previous best submission.
- Diff of current dashboard vs best dashboard for similar execution.

## 0.3.48 (2017-01-17)
- We can now delete executions completely (as if they never happened)


## 0.3.47 (2017-01-13)
- Owen Trick is now correctly done in the fold. Previously, there was leakage in the way it was done.


## 0.3.46 (2017-01-12)
- Time Series Cross Validation with many different options for fix-sized train or val, with overlap, etc...
- Time Series validation set where we can take the last n observations
- It's now possible to include a y_test.csv file in order to get a test_score (besides the train and out of fold score)
- After first fold, you will see an estimation of the finish time of the submission.

## 0.3.45 (2017-01-08)
- Time Series Cross Validation now possible.


## 0.3.44 (2017-01-04)
- We can now do Ensembles which use scikit learn optimize minimize.


## 0.3.43 (2017-01-03)
- ExternalScript can be used with cross validation

- Bugs:
	· Fixed: num_jobs hyperparameter working
	· Fixed: ML_MODEL_TO_PICKLE working (import pickle dependency on my_ML.py)

- PRP_OWEN_TRICK_METRICS: 'default','mean','mode','std','max','min','sum'.  
 using 'default' as metric, boosterama uses:
      - 'mode' for multiclass problems, also to aggregate objects
      - 'mean' otherwise

- 'PRP_OWEN_TRANSFORM': 'No',  # 'No', 'log' to apply log after the agg/metric


## 0.3.42 (2017-01-03)
- Fixed: [#2: boosterama ignores COMP_TRAIN_FILES and COMP_TEST_FILES configuration](https://bitbucket.org/boosterama/boosterama/issues/2/boosterama-ignores-comp_train_files-and)