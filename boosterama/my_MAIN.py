# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 12:54:33 2015

@author: DataCousins
"""

############################################
### IMPORTS - Respetar ORDEN ALFABÉTICO  ###
############################################

#sys.path.append("C:\\Users\\mesal_000\\Anaconda3\\Lib\\site-packages\\")

import colorama
from colorama import Fore, Back, Style
from datetime import datetime, timedelta
import numpy as np
import os
import pandas as pd
import pickle
import platform
from pylab import *
import random
from sklearn.preprocessing import StandardScaler
import sys
import timeit


# Código Propio
from my_Cache import *
from my_Caruana import *
from my_Charts import *
from my_Debug import *
from my_Dashboard_Utils import *
from my_Ensembler import *
from my_ExternalPredictions_fx import *
from my_ExternalScripts import *
from my_Flow_Control import *
from my_Input_Output import *
from my_Mail_Alerts import *
from my_ML import *
from my_ML_fx import *
from my_Prepro import *
from my_Prepro_cache import *
from my_Prepro_fx import *
from my_Prints import *
from my_Read_L1_Metafeatures import *
from my_Score import *
from my_Split import *
from my_Submissions import *

from framework.algorithm.factory import AlgorithmFactory
from framework.execution_context import ExecutionContext



#main_caches = {}


   

def go_Go_Go (d, CUSTOM_IMPORTER, CUSTOM_ML, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, test__pd=None,TRAIN__pd=None):
    
    DEBUG = True

    set_random_seed(d) # initial random seed
    
    dXT = {}
    results = {}
    ExecutionContext().create_context(d, dXT, results)

    algorithm_factory=AlgorithmFactory()
    algorithm=algorithm_factory.get(d)

    
    # Val Exists
    ############################
    
    val_exists = d['SPLIT_CV_KFOLDS']>1 or ('SPLIT_CV_TYPE' != 'TimeSeries' and d['SPLIT_CV_KFOLDS']==1 and d['SPLIT_VALIDATION_SET_PERC']!=0.0) or ('SPLIT_CV_TYPE' == 'TimeSeries' and d['SPLIT_CV_KFOLDS']==1 and d['SPLIT_TS_VALIDATION_FIX_NUM']>0)
    print('val_exists',val_exists)
    

    ##   Get y_test if exists    
    ############################
    try:
        y_test = import_y_test(d['DIR_Y_TEST_PATH'],encodingType=d['IMP_TRAIN_ENCODING_TYPE'],delimiter=d['IMP_TRAIN_DELIMITER']) 
        y_test = y_test[d['COMP_COLUMN_CLASS']].as_matrix()
        y_test_exists = 1
        
    except:
        y_test = []
        y_test_exists = 0  
        
        
    # Linea comentada pendiente de que se genere una id_execution por ejecucion (por dashboard distinto)
    id_execution = d['ID']
   
    t0 = timeit.default_timer()

    # Inicializamos la capacidad de imprimir en colores
    colorama.init()

    # Imprimimos el DO_MODEL_LEVEL que estamos ejecutando
    if d['LEVEL']<'2':
        fondo = Back.GREEN
    elif d['LEVEL']<'3':
        fondo = Back.YELLOW
    else:
        fondo = Back.RED
    print ("\nDO_MODEL_LEVEL: " + d['LEVEL'])
    print (fondo + ("_"*35 + "\n")*3 + Style.RESET_ALL + "\n")

    
    ############################################
    ###            Inicializamos             ###
    ############################################
    execution_CV_stopped = 0
    
    
    
    results['submission_name'] = None
    results['model_score_cv'] = None
    results['model_score_train'] = None
    results['model_score_train_transf'] = None
    results['model_score_val_transf'] = None    
    if d['COMP_EVALUATION_METRIC_GREATER_IS_BETTER']:
        results['model_score_val']  = -99999
        results['model_score_test'] = -99999
    else:
        results['model_score_val']  = 99999
        results['model_score_test'] = 99999
    results['levelN_TRAIN_ids_executions'] = None
    results['levelN_Test_ids_executions'] = None
    results['levelN_columns_label_ordered'] = None
    results['caruana_coeffs'] = None
    results['caruana_rounds'] = None
    results['ensemble_coeffs'] = None
    results['ensemble_rounds'] = None
    results['overfit']= 0
    results['so_alg']= None
    results['so_titulo']= None
    results['so_idScript']= None
    results['so_autor']= None
    results['so_puntuacion']= None
    results['total_execution_time']= 0
    results['datasets_names']=['idx_test','y_test_preds','idx_val','y_val_preds', 'y_val']
    results['rarp_y_train_transf_preds'] = 0
    results['rarp_y_val_transf_preds'] = 0
    results['rarp_y_test_transf_preds'] = 0
    results['rarp_y_train_preds'] = 0
    results['rarp_y_val_preds'] = 0
    results['rarp_y_test_preds'] = 0   
    results['rarp_max']= None    
    results['score_validation_by_fold']=[]
    results['best_iteration']=[]
    results['best_iteration_mean']=0
    results['best_ntree_limit']=[]
    results['alg_param'] = None  
    
    
    ############################################
    #     ADAPTACIONES SEGUN NIVEL ML          #
    ############################################
    if d['LEVEL']=='1_ML' or d['LEVEL']=='1_EXTERNAL_SCRIPTS' or d['LEVEL']=='1_EXTERNAL_PREDICTIONS':
        DIR_OUTPATH_SUBMISSIONS = d['DIR_OUTPATH_SUBMISSIONS_LEVEL_1']
        DIR_OUTPATH_SUBMISSIONS_VALIDATION = d['DIR_OUTPATH_SUBMISSIONS_LEVEL_1_VALIDATION']
        create_dirs([DIR_OUTPATH_SUBMISSIONS,DIR_OUTPATH_SUBMISSIONS_VALIDATION])

    elif d['LEVEL'][0:2] == '2_':   #'2_ML' or d['LEVEL']=='2_CARUANA' or d['LEVEL']=='2_ENSEMBLE':
        DIR_OUTPATH_SUBMISSIONS = d['DIR_OUTPATH_SUBMISSIONS_LEVEL_2']
        DIR_OUTPATH_SUBMISSIONS_VALIDATION = d['DIR_OUTPATH_SUBMISSIONS_LEVEL_2_VALIDATION']
        create_dirs([DIR_OUTPATH_SUBMISSIONS,DIR_OUTPATH_SUBMISSIONS_VALIDATION])
        
    elif d['LEVEL']=='3_CARUANA' or d['LEVEL']=='3_BASIC_ENSEMBLE':
        DIR_OUTPATH_SUBMISSIONS =d['DIR_OUTPATH_SUBMISSIONS_LEVEL_3']
        create_dirs([DIR_OUTPATH_SUBMISSIONS])
     
    elif d['LEVEL'][0:2]=='0_':
        pass
        
    else:
        sys.exit("Error: LEVEL no valido")    

    # Create dirs if do not exist
    create_dirs([d['DIR_EXTERNAL_SCRIPTS']+'input/',d['DIR_TRAIN_TEST_DIRECTORY'],d['DIR_OUTPATH_PICKLES'],d['DIR_DATA'],d['DIR_EXTERNAL_SCRIPTS'],d['DIR_EXTERNAL_SUBMISSIONS'],d['DIR_OUTPATH_CHARTS']+'feature_importance/',d['DIR_OUTPATH_CHARTS']+'correlations/', d['DIR_BASE']+'/external_predictions/',d['DIR_BASE']+'/keras_checkpoints/'])

    
    
    ##################################################
    ###            GET PREVIOUS BEST SCORES        ###
    ##################################################
    score_previous_best, score_previous_best_folds, dashboard_best, features_best_historic_df, is_feature_file = scores_get_previous_best(d, d['SPLIT_CV_KFOLDS'])


    

    ###########################################################################
    ###                                                                     ### 
    ###                        DELETE SUBMISSIONS                           ### 
    ###                                                                     ###
    ###########################################################################

    if d['LEVEL']=='0_DELETE':

        delete_execution_data(d)

        sys.exit("Finished deleting executions.")



    ###########################################################################
    ###                                                                     ### 
    ###                               LEVEL 3                               ### 
    ###                                                                     ###
    ###########################################################################

    if d['LEVEL']=='3_CARUANA' or d['LEVEL']=='3_BASIC_ENSEMBLE':
        ensembler_main(d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'],d['DIR_OUTPATH_SUBMISSIONS_LEVEL_2'],d['DIR_OUTPATH_SUBMISSIONS_LEVEL_3'],d['DIR_OUTPATH_SUBMISSIONS_LEVEL_2_VALIDATION'],d)
        
        if d['PRINT_TIME']:
            print(Fore.BLUE + 'ELAPSED TIME -  ML Level 3: ' + str(int(timeit.default_timer()-t0)) + " segundos." +  Style.RESET_ALL)
        
        pickle_save_dashboard(d)
        
        return None #This is the end


    ###########################################################################
    #                                                                         # 
    #     LEVELS 2_ML, 2_CARUANA, 2_ENSEMBLE, 3_CARUANA, 3_BASIC_ENSEMBLE     # 
    #           SELECCIÓN Y LECTURA DE EJECUCIONES DE NIVEL INFERIOR          #
    #                         (EN CONSTRUCCION LEVEL 3)                       # 
    ###########################################################################



    ###########################################################################
    ###                                                                       # 
    ###                    LEVEL 2_CARUANA, 2_ENSEMBLE                        # 
    ###                                                                       #
    ###########################################################################    

    if d['LEVEL']=='2_CARUANA' or d['LEVEL']=='2_ENSEMBLE':
        
        if d['COMP_PROBLEM_TYPE'] != 'Regr':
            print()
            print('ERROR')
            print('2_ENSEMBLE sólo funciona de momento para d[COMP_PROBLEM_TYPE] = Regr')
            print()
            sys.exit()
        
        idx_TRAIN_L1__pd, X_TRAIN_L1__pd, y_TRAIN_L1__pd, idx_test_L1__pd, X_test_L1__pd, columns_label_ordered = read_level1_metafeatures(d, results)
        

        if d['COMP_PROBLEM_TYPE'].lower() == 'class_b':
            y_TRAIN_L1__pd = y_TRAIN_L1__pd.astype(int)          
            
            
        # Tipo de datos de idx e y (Nos servirán para el almacenamiento HDF5)
        idx_dtype = pd.concat([idx_TRAIN_L1__pd,idx_test_L1__pd],axis=0).values.dtype
        if idx_dtype==np.dtype('O'):
            idx_dtype=np.dtype('S')
        y_dtype = y_TRAIN_L1__pd.values.dtype        

        
        #######################################################
        #                  Convertimos a NUMPY                #
        #######################################################
        idx_TRAIN, X_TRAIN, y_TRAIN, idx_test, X_test, N_test, M, X_cols_names = pandas_to_numpy(idx_TRAIN_L1__pd[d['COMP_COLUMN_ID']], X_TRAIN_L1__pd, y_TRAIN_L1__pd, idx_test_L1__pd[d['COMP_COLUMN_ID']], X_test_L1__pd, d)
        
        

        y_TRAIN = np.reshape(y_TRAIN,(-1,))


        #######################################################
        #         Splitting Ordering and Folding TRAIN        #
        #######################################################

        idx_train_list, idx_val_list, X_train_list, X_val_list, y_train_list, y_val_list, N_train_list, N_val_list = splitting_ordering_and_folding(d, idx_TRAIN, X_TRAIN, y_TRAIN, order=True)
        

        #######################################################
        #                   Bucle por Fold                    #
        #######################################################
        
        y_train_preds_list = []
        y_val_preds_list = []
        y_test_preds_list = []
        
        num_folds = np.max([d['SPLIT_CV_KFOLDS'],1])

        model_score_cv = []
                
        for fold in range(0,num_folds):  # 2_ENSEMBLE forced to num_folds=1 (regardless of value specificied in d[SPLIT_CV_KFOLDS])
            
            idx_train = idx_train_list[fold]
            idx_val = idx_val_list[fold]
            X_train = X_train_list[fold]
            X_val = X_val_list[fold]
            y_train = y_train_list[fold]
            y_val = y_val_list[fold]
            N_train = N_train_list[fold]
            N_val = N_val_list[fold]
            
            if d['SPLIT_CV_KFOLDS'] > 1:
                print(Fore.MAGENTA + '\n--------------------------------' +  Style.RESET_ALL)
                print(Fore.MAGENTA + '------------ Fold', fold+1, '------------' +  Style.RESET_ALL)
            else:
                print(Fore.MAGENTA + '\n------------ Execution ------------' +  Style.RESET_ALL)
            print_indices_and_data(idx_train,idx_val,X_train,y_train,X_val,y_val,print_indices=False,print_data=False)


            # Ejecutamos Caruana o Ensemble
            ###################################
            
            if d['LEVEL']=='2_ENSEMBLE':
                
                #val_exists = 0 # Si no,
                if val_exists:
                    (idx_test, y_test_preds, idx_val, y_val, y_val_preds, idx_train, y_train, y_train_preds) = ensemble_optim(d, results, val_exists, columns_label_ordered, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test)
                else:
                    (idx_test, y_test_preds, idx_train, y_train, y_train_preds)                              = ensemble_optim(d, results, val_exists, columns_label_ordered, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test)
                    
                    idx_val = idx_train
                    y_val_preds = y_train_preds
                    y_val = y_train
                    N_val = len(y_val)
                    val_exists = 1
                

                alg = "ENSEMBLE"
                best_parameters = "ensemble_coefs"
                
            elif d['LEVEL']=='2_CARUANA':
                if val_exists:
                    (idx_test, y_test_preds, idx_val, y_val, y_val_preds, idx_train, y_train, y_train_preds) = caruana(d, results, val_exists, columns_label_ordered, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test)
                else:
                    (idx_test, y_test_preds, idx_train, y_train, y_train_preds) = caruana(d, results, val_exists, columns_label_ordered, idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test)
                
                alg = "CARUANA"
                best_parameters = "caruana_coefs"
                      
            
            best_params = None
            

            # Apply restrictions to y_val_preds, y_test_preds
            if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
                if val_exists:
                    y_val_preds,results['rarp_y_val_preds'] = apply_restrictions(y_val_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_preds")
                y_test_preds,results['rarp_y_test_preds'] = apply_restrictions(y_test_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_preds")
                
            # Print Info: y_val_preds, y_test_preds            
            if d['PRINT_Y_INFO']:
                if val_exists:
                    print_info(y_val_preds,"y_val_preds",[d['COMP_COLUMN_CLASS']])
                print_info(y_test_preds,"y_test_preds",[d['COMP_COLUMN_CLASS']])
                
            # Train score   
            results['model_score_train'] = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train","y_train", "y_train_preds",d['PRINT_EVAL_INFO'])
        
            
            # Validation score 
            if d['LEVEL']=='2_ENSEMBLE':               
                results['model_score_val']     = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])                    
       
                
            elif d['LEVEL']=='2_CARUANA':
                if val_exists:        
                    results['model_score_val'] = my_score(y_val, y_val_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])                    
                    model_score_cv.append(results['model_score_val'])
            
            # Test Score (if exists)   
            if y_test_exists:                    
                results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    
                            

            # Print scores  
            did_improve_overall, did_improve_fold = scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best)
            
            # Confusion Matrix
            if val_exists and (d['COMP_PROBLEM_TYPE'].lower() == 'class_m' or d['COMP_PROBLEM_TYPE'].lower() == 'class_b'): 
                confusion_matrix_compute_and_print(d, y_val, y_val_preds)       

            # En el caso de CrossValidation
            if d['SPLIT_CV_KFOLDS']>=2:
                y_train_preds_list.append(y_train_preds) # Concatenación de los y_train_preds de cada fold
                y_val_preds_list.append(y_val_preds) # Concatenación de los y_val_preds de cada fold
                y_test_preds_list.append(y_test_preds) # Concatenación de los y_test_preds de cada fold
        
        # Fin bucle por fold
        
        
        if d['LEVEL']=='2_CARUANA':
            # En el caso de CrossValidation
            if d['SPLIT_CV_KFOLDS']>=2:
                
                # Flattening de los distintos vectores concatenados
                N_val = sum(N_val_list) # Suma de los Nval de cada fold
                idx_val = np.array([item for sublist in idx_val_list for item in sublist]) 
                y_val = np.array([item for sublist in y_val_list for item in sublist]) 
                y_val_preds = np.array([item for sublist in y_val_preds_list for item in sublist])
                y_train = np.array([item for sublist in y_train_list for item in sublist])
                y_train_preds = np.array([item for sublist in y_train_preds_list for item in sublist]) 
                
    
                if d['COMP_PROBLEM_TYPE'] == 'Regr' or d['COMP_PROBLEM_TYPE'] == 'Class_B':
                    y_test_preds = np.array(np.mean(y_test_preds_list, axis=0))
                else:
                    # Ojo, en caso de que la moda de más de un resultado (en caso de empate, caso raro pero se puede dar), nos estamos quedano arbitrariamente con el primer resultado
                    y_test_preds = []
                    num_observaciones_test = len(y_test_preds_list[0])
                    for num_observacion in range(0,num_observaciones_test):
                        y_test_preds_list_array = np.array(y_test_preds_list)
                        moda_prediccion = moda(list(y_test_preds_list_array[:,num_observacion]))
                        moda_prediccion_elegida = moda_prediccion[0] # Aquí estamos escogiendo arbitrariamente el primer resultado de la moda
                        y_test_preds.append(moda_prediccion_elegida)
                    y_test_preds = np.array(y_test_preds)
    
    

        # Apply restrictions to y_val_preds, y_test_preds
        if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
            if val_exists:
                y_val_preds,results['rarp_y_val_preds'] = apply_restrictions(y_val_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_preds")
            y_test_preds,results['rarp_y_test_preds'] = apply_restrictions(y_test_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_preds")
            
        # Print Info: y_val_preds, y_test_preds            
        if d['PRINT_Y_INFO']:
            if val_exists:
                print_info(y_val_preds,"y_val_preds",[d['COMP_COLUMN_CLASS']])
            print_info(y_test_preds,"y_test_preds",[d['COMP_COLUMN_CLASS']])
            
        # Train score       
        results['model_score_train'] = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train","y_train", "y_train_preds",d['PRINT_EVAL_INFO'])
        
        
        
                    
    ###########################################################################
    ###                                                                     ### 
    ###                       LEVEL 1_EXTERNAL_SCRIPTS                      ### 
    ###                                                                     ###
    ###########################################################################        
    
    if d['LEVEL']=='1_EXTERNAL_SCRIPTS' and d['ID_FAMILY']==0: 

        # Provisionalmente
        fold = 0               

        (idx_test, y_test_preds, N_test, idx_dtype, y_dtype) = external_script_execute(d, results, fold)                             
        #(idx_test, y_test_preds, N_test, idx_val, y_val, y_val_preds, N_val, idx_dtype, y_dtype) = external_script_execute(d, results, fold)
            
        # En este punto ya tenemos y_test_preds(padre), si se trata de una ejecución padre
        # O bien, y_test_preds(hijo) e y_val_preds(hijo), si se trata de una ejecución hijo
        
        alg = 'ScriptOthers'
        best_params = ['SO']
        best_parameters = 'SO'
        
        
        # Apply restrictions to y_val_preds, y_test_preds
        if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
            if val_exists:
                y_val_preds,results['rarp_y_val_preds'] = apply_restrictions(y_val_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_preds")
            y_test_preds,results['rarp_y_test_preds'] = apply_restrictions(y_test_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_preds")
            
        # Print Info: y_val_preds, y_test_preds            
        if d['PRINT_Y_INFO']:
            if val_exists:
                print_info(y_val_preds,"y_val_preds",[d['COMP_COLUMN_CLASS']])
            print_info(y_test_preds,"y_test_preds",[d['COMP_COLUMN_CLASS']])
            
        # Validation score  
        if val_exists:        
            results['model_score_val'] = my_score(y_val, y_val_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])                    

        # Test Score (if exists)   
        if y_test_exists:                    
            results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    
                        

        # Print scores  
        did_improve_overall, did_improve_fold = scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best)
        
        # Confusion Matrix
        if val_exists and (d['COMP_PROBLEM_TYPE'].lower() == 'class_m' or d['COMP_PROBLEM_TYPE'].lower() == 'class_b'):        
            confusion_matrix_compute_and_print(d, y_val, y_val_preds) 


    ###########################################################################
    ###                                                                     ### 
    ###                     LEVEL 1_EXTERNAL_PREDICTIONS                    ### 
    ###                                                                     ###
    ###########################################################################        
   
    # Necesitamos: y_TRAIN_preds_CV, y_test_preds 
    if d['LEVEL']=='1_EXTERNAL_PREDICTIONS':

        # Provisionalmente
        fold = 0 
        
        
        # We import idx_TRAIN__pd, y_TRAIN__pd
        idx_TRAIN__pd, y_TRAIN__pd = import_idx_TRAIN__pd_and_y_TRAIN__pd(d['DIR_TRAIN_PATH'],d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])
        
        # We sort by idx_TRAIN__pd
        idx_TRAIN__pd, y_TRAIN__pd, nothing = order_datasets_by_idx(d, idx_TRAIN__pd, y_TRAIN__pd)
        
        # paths
        path_external_predictions = d['DIR_BASE']+'/external_predictions/'
        file_cvPreds_path = path_external_predictions + d['EXTERNAL_PREDICTIONS_IMPORT'][0]
        file_testPreds_path = path_external_predictions + d['EXTERNAL_PREDICTIONS_IMPORT'][1]
        
        # We import cvPreds file
        idx_TRAIN_preds_CV__pd, y_TRAIN_preds_CV__pd = import_external_predictions(file_cvPreds_path,d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])
        
        # We sort by idx_TRAIN_preds_CV__pd
        idx_TRAIN_preds_CV__pd, y_TRAIN_preds_CV__pd, nothing = order_datasets_by_idx(d, idx_TRAIN_preds_CV__pd, y_TRAIN_preds_CV__pd)
            
        # Confirmamos coherencia índices
        if not idx_TRAIN__pd.equals(idx_TRAIN_preds_CV__pd):
            sys.exit('Error: idx_TRAIN__pd incoherente respecto a idx_TRAIN_preds_CV__pd')



       
        # We import idx_test__pd
        idx_test__pd = import_idx_test__pd(d['DIR_TEST_PATH'],d['COMP_COLUMN_ID'])
        
        # We sort by idx_test__pd
        idx_test__pd.sort_values(ascending=[True], inplace = True)
        
        # We import testPreds file
        idx_test_preds_CV__pd, y_test_preds_CV__pd = import_external_predictions(file_testPreds_path,d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])        
        
        # We sort by idx_test_preds_CV__pd
        idx_test_preds_CV__pd, y_test_preds_CV__pd, nothing = order_datasets_by_idx(d, idx_test_preds_CV__pd, y_test_preds_CV__pd)
        
        # Confirmamos coherencia índices
        if not idx_test__pd.equals(idx_test_preds_CV__pd):
            sys.exit('Error: idx_test__pd incoherente respecto a idx_test_preds_CV__pd')




        # Tipo de datos de idx e y. Nos servirán para el almacenamiento HDF5
        idx_dtype = pd.concat([idx_TRAIN__pd,idx_test__pd],axis=0).values.dtype
        if d['COMP_PROBLEM_TYPE'] == 'Regr' or d['COMP_PROBLEM_TYPE'] == 'Class_B':
            y_dtype = np.float64
        else:
            y_dtype = y_TRAIN__pd.values.dtype

        # Convertimos a NUMPY        
        idx_val, y_val_preds, idx_test, y_test_preds, N_test = pandas_to_numpy_external_predictions_version(idx_TRAIN__pd,y_TRAIN_preds_CV__pd,idx_test_preds_CV__pd, y_test_preds_CV__pd)
        
        # Al provenir de CrossValidation:
        y_val = np.array(y_TRAIN__pd)
        N_val = len(y_val)
                
        alg = 'ExternalPredictions'
        best_params = [str(d['EXTERNAL_PREDICTIONS_IMPORT'])+' '+str(d['ID'])]
        best_parameters = 'PO'
        
        
        # Apply restrictions to y_val_preds, y_test_preds
        if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
            if val_exists:
                y_val_preds,results['rarp_y_val_preds'] = apply_restrictions(y_val_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_preds")
            y_test_preds,results['rarp_y_test_preds'] = apply_restrictions(y_test_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_preds")
            
        # Print Info: y_val_preds, y_test_preds            
        if d['PRINT_Y_INFO']:
            if val_exists:
                print_info(y_val_preds,"y_val_preds",[d['COMP_COLUMN_CLASS']])
            print_info(y_test_preds,"y_test_preds",[d['COMP_COLUMN_CLASS']])
            
        # Validation score  
        if val_exists:        
            results['model_score_val'] = my_score(y_val, y_val_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])                    

        # Test Score (if exists)   
        if y_test_exists:                    
            results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    
                        
                    
        # Print scores  
        did_improve_overall, did_improve_fold = scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best)
        
        # Confusion Matrix
        if val_exists and (d['COMP_PROBLEM_TYPE'].lower() == 'class_m' or d['COMP_PROBLEM_TYPE'].lower() == 'class_b'):        
            confusion_matrix_compute_and_print(d, y_val, y_val_preds) 

            
    ###########################################################################
    ###                                                                     ### 
    ###                          LEVEL 1_ML o 2_ML                          ### 
    ###                                                                     ###
    ###########################################################################

    # TO-DO: Revisar por qué d['LEVEL']=='1_EXTERNAL_SCRIPTS' and d['ID_FAMILY']==0 se está ejecutando fuera de este IF

    if d['LEVEL']=='1_ML' or d['LEVEL']=='2_ML' or d['LEVEL']=='0_FEAT_IMP' or (d['LEVEL']=='1_EXTERNAL_SCRIPTS' and d['ID_FAMILY']!=0):
     
        
        # cached_import_and_preprocessing() 
        ####################################
        (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd, dXT) = cached_import_and_preprocessing(d, dXT, CUSTOM_IMPORTER, TRAIN__pd, test__pd)


        # import_and_preprocessing validation
        ####################################
        train_cols=X_TRAIN__pd.columns
        test_cols=X_test__pd.columns
        
        not_in_test = [x for x in train_cols if x not in test_cols]
        not_in_train = [x for x in test_cols if x not in train_cols]
        
#        print("test_cols:",test_cols)
#        print("train_cols:",train_cols)
#        print("not_in_test :",not_in_test )
#        print("not_in_train :", not_in_train )
        if len(not_in_test)>0 or len(not_in_train)>0:
            error_str=""
            if len(not_in_test)>0:
                error_str+="\n\nthere are columns in X_TRAIN__pd not in X_test__pd: "+str(not_in_test)
            
            if len(not_in_train)>0:
                error_str+="\n\nthere are columns in X_test__pd not in X_TRAIN__pd: "+str(not_in_train)
            
            raise ValueError('Columns in TRAIN and TEST mismatch' + error_str + '\n\nColumns in TRAIN and TEST mismatch' )
        


        
        # Sort TRAIN & test by their idx
        #################################
        (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd) = order_by_idx(d, idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd)

        

        # Feature Importance: Calculations
        ##################################
        if d['LEVEL']=='0_FEAT_IMP':
            feature_importance_aggregator(d, X_TRAIN__pd)
            print(Fore.BLUE + 'Feature Importance excel created.' + Style.RESET_ALL)
            sys.exit("Fin de Ejecución")  
    
            

        # 2_ML: sort and check Id-match
        #################################
        if d['LEVEL']=='2_ML':        
            
            # Import Level1 metafeatures
            idx_TRAIN_L1__pd, X_TRAIN_L1__pd, y_TRAIN_L1__pd, idx_test_L1__pd, X_test_L1__pd, columns_label_ordered = read_level1_metafeatures(d, results)  
            
            # Sort TRAIN and test by their idx
            (idx_TRAIN_L1__pd, X_TRAIN_L1__pd, y_TRAIN_L1__pd, idx_test_L1__pd, X_test_L1__pd) = order_by_idx(d, idx_TRAIN_L1__pd, X_TRAIN_L1__pd, y_TRAIN_L1__pd, idx_test_L1__pd, X_test_L1__pd)
            
            # 2_ML with 'L2_CV_OR_DATASET' == 'DATASET' only has metafeatures (it will never include original features)
            if d['L2_CV_OR_DATASET'] == 'DATASET':
                y_TRAIN__pd   = y_TRAIN_L1__pd
                idx_TRAIN__pd = idx_TRAIN_L1__pd
                idx_test__pd  = idx_test_L1__pd
                X_TRAIN__pd   = X_TRAIN_L1__pd
                X_test__pd    = X_test_L1__pd
            else:
                
                # Confirmamos cuadre de los idx
                if idx_TRAIN__pd.equals(idx_TRAIN_L1__pd) and idx_test__pd.equals(idx_test_L1__pd):
                    X_TRAIN__pd = pd.concat([X_TRAIN__pd, X_TRAIN_L1__pd], axis=1)
                    X_test__pd = pd.concat([X_test__pd, X_test_L1__pd], axis=1)
                else:
                    print()
                    print(Fore.RED + "---------------\n INDEXES ERROR:" + Style.RESET_ALL)                
                    print(Fore.RED + " (indexes from Features & Metafeatures do NOT match)"  + Style.RESET_ALL)
                    print()
                    
                    if d['L2_CV_OR_DATASET'] == 'DATASET':
                        print(Fore.RED + "\tVariable d['L2_CV_OR_DATASET'] is set to 'DATASET', so the METAFEATURES are executions from the DATASET '" + d['L2_LEVEL1_DATASETS_TRAIN'] + "'" + Style.RESET_ALL)
                        print(Fore.RED + "\tOriginal features have a different shape than the metafeatures of this Level2 execution, which is currently NOT supported. \n\tTry to set d['L2_CV_OR_DATASET']=='CV', so you can do a Level2 to crossvalidated submissions."  + Style.RESET_ALL)
                    
                    print()
                    
                    print(Fore.RED + "----------------------------------------------------------------------" + Style.RESET_ALL)
                    print()
                    sys.exit()
                
            print('Shapes after adding Metafeatures')
            print('X_TRAIN__pd.shape',X_TRAIN__pd.shape)
            print('X_test__pd.shape',X_test__pd.shape)


        
            
            
        # PERSIST INFO AFTER PREPROCESSING
        ##################################
        
        # SAVE_CSV_PREPROCESSED_DATASETS
        if d['SAVE_CSV_PREPROCESSED_DATASETS']:
            csv_save_preprocessed_data(d, X_TRAIN__pd, y_TRAIN__pd, idx_TRAIN__pd, idx_test__pd, X_test__pd)
        
        # Persiste Xtrain.head(20)
        X_TRAIN__pd.head(20).to_excel(d['DIR_OUTPATH_CHARTS'] + d['COMP_NAME'].replace('/','_') + '_head.xlsx',index=False)
        

        
        # Print time                 
        if d['PRINT_TIME']:
            print(Fore.BLUE + 'ELAPSED TIME -  PREPROCESSING: ' + str(int(timeit.default_timer()-t0)) + " seconds." +  Style.RESET_ALL)
            
      
            
        # Tipo de datos de idx e y. Nos servirán para el almacenamiento HDF5
        idx_dtype = pd.concat([idx_TRAIN__pd,idx_test__pd],axis=0).values.dtype
        if idx_dtype==np.dtype('O'):
            idx_dtype=np.dtype('S')
        if d['COMP_PROBLEM_TYPE'] == 'Regr' or d['COMP_PROBLEM_TYPE'] == 'Class_B':
            y_dtype = np.float64
        else:
            y_dtype = y_TRAIN__pd.values.dtype
        #print("idx dtype", idx_dtype)
        #print("y dtype", y_dtype)  




        
        #######################################################
        #                  Convertimos a NUMPY                #
        #######################################################
        
        idx_TRAIN, X_TRAIN, y_TRAIN, idx_test, X_test, N_test, M, X_cols_names = pandas_to_numpy(idx_TRAIN__pd,X_TRAIN__pd,y_TRAIN__pd,idx_test__pd,X_test__pd, d)

        
        

        # KERAS: STANDARDIZATION
        ##########################
        if d['ML_ALGO'][0].upper()[:5] == 'KERAS':

            rows_train = X_TRAIN__pd.shape[0]
            '''
            train_test = pd.concat((X_TRAIN__pd, X_test__pd), axis = 0)
            
            numericas_cols   =  X_TRAIN__pd.columns.values
            
            bad_indices = np.where(np.isinf(train_test))

            
            scaler = StandardScaler()
            
            try:
                train_test = scaler.fit_transform(train_test[numericas_cols])
            except:
                
                train_test = train_test.replace([np.inf, -np.inf], 0)
                           
                print()
                print('bad_indices')
                print(bad_indices)

                train_test = scaler.fit_transform(train_test[numericas_cols])
            
            X_TRAIN__pd = train_test[:rows_train, :]
            X_test__pd  = train_test[rows_train:, :]
            
            del(train_test)
            '''

            

            
        #######################################################
        ###                                                 ### 
        ###              Features Correlations              ###
        ###                                                 ### 
        #######################################################

        if (d['COMP_PROBLEM_TYPE']!='Class_M') and (d['LEVEL']=='1_ML' or (d['LEVEL']=='2_ML' and d['ID_FAMILY']!=0)): # and d['L2_CV_OR_DATASET'] != 'DATASET'
            cache_key = get_cache_key(d, IMPORT_AND_PREPROCESSING_VARIABLES)
            prefix = '1_ML' if d['LEVEL']=='1_ML' else '2_ML'
            sufix = str(cache_key) if d['LEVEL']=='1_ML' else str(cache_key)+'_'+str(d['ID'])
            feat_corr_file_name = d['DIR_BASE']+'/correlations/'+ prefix + '_Features_correlations_' + sufix + '.csv'
        
            if not os.path.exists(feat_corr_file_name):
                
                def corr_matrix_to_pairs(corr_matrix, features_names, feature_class):
                    #print(features_names)
                    M = len(corr_matrix)
                    pares=[]
                    for i in range(M):
                        for j in range(i+1,M):
                            r = corr_matrix[i][j]
                            hasClass = 1 if (feature_class in features_names[i] or feature_class in features_names[j]) else 0
                            if hasClass:
                                Feat_Other = features_names[j] if feature_class in features_names[i] else features_names[i]
                            else:
                                Feat_Other = ''
                            pares.append([features_names[i], features_names[j], r, abs(r), r**2, hasClass, Feat_Other])
                            #print(features_names[i], features_names[j], r, abs(r), r**2, hasClass)
                    return pd.DataFrame(pares,columns=['Feat_A','Feat_B','Corr','Corr_Abs','Corr2','HasClass','Feat_Other'])
            
                features_names = X_cols_names + [d['COMP_COLUMN_CLASS']]
                
                if d['LEVEL']=='2_ML' and d['L2_CV_OR_DATASET'] == 'DATASET':
                    corr_matrix = np.corrcoef(X_TRAIN,rowvar=0)
                else:
                    try:
                        corr_matrix = np.corrcoef(np.concatenate([X_TRAIN, y_TRAIN.reshape(len(y_TRAIN),1)],axis=1),rowvar=0)
                    except:
                        pass
                
                try:
                    corr_pares_DF = corr_matrix_to_pairs(corr_matrix,features_names,d['COMP_COLUMN_CLASS'])
                    pd.DataFrame(corr_pares_DF).to_csv(feat_corr_file_name)
                except:
                    print(Fore.RED + '\nWe could not create feature correlations file.\n' +  Style.RESET_ALL)
                    
        
            
                
                
                
                
        #######################################################
        #      FOLDS:  Splitting Ordering and Folding TRAIN   #
        #######################################################
        idx_train_list, idx_val_list, X_train_list, X_val_list, y_train_list, y_val_list, N_train_list, N_val_list = splitting_ordering_and_folding(d, idx_TRAIN, X_TRAIN, y_TRAIN, order=True)

        
        
        
        
        #######################################################
        ###                                                 ### 
        ###    External Scripts: Saving relevant info       ###
        ###                                                 ###
        #######################################################
        if val_exists: # Si no es una ejecución padre
        
            num_folds = np.max([d['SPLIT_CV_KFOLDS'],1])
        
            for fold in range(0,num_folds):
                
                idx_train = idx_train_list[fold]
                idx_val = idx_val_list[fold]
                                
                idx_train_name = hdf5_dataset_name(d,"idx_train",fold,"ExternalScriptsSubseting")+'.csv'
                idx_val_name = hdf5_dataset_name(d,"idx_val",fold,"ExternalScriptsSubseting")+'.csv'
                
                if not os.path.exists(d['DIR_EXTERNAL_SCRIPTS']+idx_train_name):
                    pd.Series(idx_train).to_csv(d['DIR_EXTERNAL_SCRIPTS']+idx_train_name, index=False)
                if not os.path.exists(d['DIR_EXTERNAL_SCRIPTS']+idx_val_name):
                    pd.Series(idx_val).to_csv(d['DIR_EXTERNAL_SCRIPTS']+idx_val_name, index=False)

   

        
        #######################################################
        ###                                                 ### 
        ###                  CUSTOM ML                      ###
        ###                                                 ### 
        #######################################################

        if d['SYS_DO_CUSTOM_ML']:
            print("Custom ML!!!!")
            
            #                   Bucle por Fold                    
            #                  (en construccion)                  
            #######################################################
            
            num_folds = np.max([d['SPLIT_CV_KFOLDS'],1])
        
            for fold in range(0,num_folds):
                
                idx_train = idx_train_list[fold]
                idx_val = idx_val_list[fold]
                X_train = X_train_list[fold]
                X_val = X_val_list[fold]
                y_train = y_train_list[fold]
                y_val = y_val_list[fold]
                N_train = N_train_list[fold]
                N_val = N_val_list[fold]
                
            
                
                
                if d['SPLIT_CV_KFOLDS'] > 1:
                    print(Fore.MAGENTA + '\n--------------------------------' +  Style.RESET_ALL)
                    print(Fore.MAGENTA + '------------ Fold', fold+1, '------------' +  Style.RESET_ALL)
                else:
                    print(Fore.MAGENTA + '------------ Execution ------------' +  Style.RESET_ALL)
                    
                print_indices_and_data(idx_train,idx_val,X_train,y_train,X_val,y_val,print_indices=False,print_data=False)
            
            
            y_train_preds, y_val_preds, y_test_preds = CUSTOM_ML(d,idx_train, X_train, y_train, idx_val, X_val, y_val, idx_test, X_test)
            
            # Apply restrictions to y_val_preds, y_test_preds
            if len(d['ML_PRP_Y_APPLY_RESTRICTIONS']) > 0:
                if val_exists:
                    y_val_preds,results['rarp_y_val_preds'] = apply_restrictions(y_val_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_val_preds")
                y_test_preds,results['rarp_y_test_preds'] = apply_restrictions(y_test_preds,d['ML_PRP_Y_APPLY_RESTRICTIONS'],"y_test_preds")
                
            # Print Info: y_val_preds, y_test_preds            
            if d['PRINT_Y_INFO']:
                if val_exists:
                    print_info(y_val_preds,"y_val_preds",[d['COMP_COLUMN_CLASS']])
                print_info(y_test_preds,"y_test_preds",[d['COMP_COLUMN_CLASS']])
                
            # Train score       
            results['model_score_train'] = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train","y_train", "y_train_preds",d['PRINT_EVAL_INFO'])
        
            # Validation score  
            if val_exists:        
                results['model_score_val'] = my_score(y_val, y_val_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])                    
                
            # Test Score (if exists)   
            if y_test_exists:                    
                results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    
                            

            # Print scores  
            did_improve_overall, did_improve_fold = scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best)
            
            # Confusion Matrix
            if val_exists and (d['COMP_PROBLEM_TYPE'].lower() == 'class_m' or d['COMP_PROBLEM_TYPE'].lower() == 'class_b'):        
                #confusion_matrix_compute_and_print(d, y_val, y_val_preds)
                pass

                
    
        #######################################################
        ###                                                 ### 
        ###                        ML                       ###
        ###                                                 ###
        #######################################################

        else:
            alg=d['ML_ALGO'][0]


            
            
            # DECLARING VARIABLES
            #######################                
            
            num_folds  = np.max([d['SPLIT_CV_KFOLDS'],1])
            
            
            y_train_preds_list        = [0] * num_folds
            y_train_transf_list       = [0] * num_folds
            y_train_transf_preds_list = [0] * num_folds
            y_val_transf_list         = [0] * num_folds
            y_val_transf_preds_list   = [0] * num_folds
            y_val_preds_list          = [0] * num_folds
            y_test_preds_list         = [0] * num_folds
            

            model_score_cv                      = [0] * num_folds
            results['score_validation_by_fold'] = [0] * num_folds # quizas redundante con model_score_cv
            
            
            if algorithm.uses_early_stopping():
                results['best_iteration']       = [0] * num_folds
                results['best_ntree_limit']     = [0] * num_folds
                


            
            
            #######################################################
            #                                                     #
            #                   Bucle por Fold                    #
            #                                                     #
            #######################################################                
            
            folds_enumerate = list(range(0,num_folds))
            
        
            if d['SPLIT_CV_KFOLDS_EARLY_STOP'] == 1:
                
                # Quitamos temporalmente el seed de random para poder desordenar folds aleatoriamente
                num_aleatorio_time = time.time()
                num_aleatorio = int(str(num_aleatorio_time-int(num_aleatorio_time))[2:])%100
                random.seed(num_aleatorio) 
                
                # SHUFFLE folds
                random.shuffle((folds_enumerate))
                
                # volvemos a establecer el seed de random
                set_random_seed(d)

                
                folds_shuffled = 1
                print("Reordenamos folds: " + str(folds_enumerate))
            else:
                folds_shuffled = 0

                
                
                
            feature_importance_per_fold={}    
            # --------------------------------------------------------------------------------------------------------
            for fold_idx, fold in enumerate(folds_enumerate):
                # fold_idx is a sequential integer up to num_folds (1 will come after 0, etc..) 
                # fold     is a random integer if d['SPLIT_CV_KFOLDS_EARLY_STOP'] == 1 (otherwise equal to fold_idx)
                
                t0_fold = timeit.default_timer()
                
                _, dXT_WTF, _ = ExecutionContext().get_current_context()
                dXT_WTF['CURRENT_FOLD']=fold

                # VARIABLES
                #######################
                idx_train = idx_train_list[fold]
                X_train   = X_train_list[fold]
                y_train   = y_train_list[fold]
                idx_val   = idx_val_list[fold]
                X_val     = X_val_list[fold]
                y_val     = y_val_list[fold]
                
                N_train   = N_train_list[fold]
                N_val     = N_val_list[fold]
                
                
                # PRINT FOLD info
                #######################
                if d['SPLIT_CV_KFOLDS'] > 1:
                    # Shuffled folds
                    if d['SPLIT_CV_KFOLDS_EARLY_STOP'] == 1: 
                        print(Fore.MAGENTA + '\n------------------------------------------' +  Style.RESET_ALL)
                        print(Fore.MAGENTA + '------------ Fold', fold+1, '(shuffled)------------' +  Style.RESET_ALL)
                        
                        remaining_folds = d['SPLIT_CV_KFOLDS'] - fold_idx - 1
                        if remaining_folds > 2:
                            print(Fore.MAGENTA + '           (' + str(remaining_folds) + ' more folds to go)' +  Style.RESET_ALL)
                        elif remaining_folds ==2:
                            print(Fore.MAGENTA + '          (Two more folds to go)' +  Style.RESET_ALL)
                        elif remaining_folds ==1:
                            print(Fore.MAGENTA + '          (One more fold to go)' +  Style.RESET_ALL)
                        elif remaining_folds ==0:
                            print(Fore.MAGENTA + '                (Last fold)' +  Style.RESET_ALL)
                            
                    # Non-shuffled folds        
                    else:
                        print(Fore.MAGENTA + '\n--------------------------------' +  Style.RESET_ALL)
                        print(Fore.MAGENTA + '------------ Fold', fold+1, '------------' +  Style.RESET_ALL)
                
                # Execution with num_folds=1
                else:
                    print(Fore.MAGENTA + '\n------------ Execution ------------' +  Style.RESET_ALL)
                    
                    
                    
                sys.stdout.flush()
                print_indices_and_data(idx_train,idx_val,X_train,y_train,X_val,y_val,print_indices=False,print_data=False)


                # OWEN preprocessing
                #####################
                
                # This preprocessing HAS to be done IN the fold, since it averages 'y'. Otherwise our validation score would be useless.
                if len(dXT['OWEN']['Fields'])>0:
                    X_train__pd     = X_TRAIN__pd.loc[idx_TRAIN__pd.isin(idx_train)]
                    X_val__pd       = X_TRAIN__pd.loc[idx_TRAIN__pd.isin(idx_val)]
                    y_train__pd     = y_TRAIN__pd.loc[idx_TRAIN__pd.isin(idx_train)]
                    X_test__pd_temp = X_test__pd.copy()
                    
                    
                    X_train__pd, X_val__pd, X_test__pd_temp  =   owen_in_fold(dXT, d, X_train__pd, X_val__pd, y_train__pd, X_test__pd_temp, d['COMP_COLUMN_CLASS'], d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])


                    
                    if len(d['PRP_NA_FILL']) > 0:
                        X_test__pd_temp,X_train__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd_temp,X_train__pd,0,d['PRINT_NA_INFO_SHOW_FIELDS'])
                        X_val__pd,X_train__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_val__pd,X_train__pd,0,d['PRINT_NA_INFO_SHOW_FIELDS'])
                                         
                    # Volvemos a pasar a Numpy tras Owen                        
                    X_TRAIN__pd_copy = pd.concat([X_train__pd,X_val__pd],axis=0)
                    
                    X_train = np.array(X_train__pd)
                    X_val   = np.array(X_val__pd)
                    X_test  = np.array(X_test__pd_temp)
                else:
                    X_TRAIN__pd_copy = X_TRAIN__pd
                    
              
                    #Alguno de estos está mal despues de que owen borre columnas: X_train, y_train_transf, X_val, y_val_transf
                
                    
                ##  Machine_learning_algorithm
                ###############################
                (best_params, best_parameters, best_model, y_train, y_train_preds, y_train_transf, y_train_transf_preds, y_val, y_val_preds, y_val_transf, y_val_transf_preds, y_test_preds) = \
                    machine_learning_algorithm(d, results, model_score_cv, val_exists, alg, \
                                               X_TRAIN__pd_copy, \
                                               idx_train, X_train, y_train, \
                                               idx_val, X_val, y_val, \
                                               idx_test, X_test, y_test, y_test_exists, \
                                               N_train, N_val, N_test, \
                                               fold, fold_idx, None if len(score_previous_best_folds)==0 else score_previous_best_folds[fold])

                print()
                print()
                print('val_exists, y_test_exists, alg, best_params, score_previous_best, score_previous_best_folds[fold]')
                print(val_exists, y_test_exists, alg, best_params, score_previous_best, score_previous_best_folds[fold])
                print()
                print()
                # Print scores  
                ################
                did_improve_overall, did_improve_fold = scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best, score_previous_best_folds[fold])
                
                
                
                # Feature Importance      
                ######################
                if d['ML_FEATURE_IMPORTANCE']: # and fold_idx==0:
                     
                    try:
                        features_best_df, feature_importance_best_file_name = feature_importance(d, alg, best_model, did_improve_fold, fold, X_TRAIN__pd_copy, X_train, y_train)
                        feature_importance_per_fold[fold_idx]=(features_best_df, feature_importance_best_file_name)
                    except:
                        features_best_df             = []
                        feature_importance_best_file_name = ""
                        print()
                        print(Fore.RED + "\t-----------------------------\n\t FEATURE IMPORTANCE WARNING:" + Style.RESET_ALL)
                        print()
                        print()
                        print(Fore.RED + "\tSeems we were not able to find the feature-importance file for algo: '" + str(alg) + "'\n\t\tQueries using 'TopX' will ignore the 'top' part." + Style.RESET_ALL)
                        print()
                
                        
                        
                # Confusion Matrix
                #####################
                if val_exists and (d['COMP_PROBLEM_TYPE'].lower() == 'class_m' or d['COMP_PROBLEM_TYPE'].lower() == 'class_b'): 
                    confusion_matrix_compute_and_print(d, y_val, y_val_preds)       
                    
                
                # Early CV stopping
                ##################################
                if fold_idx == 0 and (did_improve_fold=="no" or did_improve_fold=="same") and d['SPLIT_CV_KFOLDS']>1 and d['SPLIT_CV_KFOLDS_EARLY_STOP']==1:
                    execution_CV_stopped = 1
                    print()
                    print(Fore.YELLOW + Back.BLACK + Style.DIM+ '\n         EARLY STOPPING !!!!         ' +  Style.RESET_ALL) 
                    print(Fore.BLACK + Back.YELLOW + Style.DIM+  '\n            EARLY STOPPING !!!!             ' +  Style.RESET_ALL) 
                    print(Fore.YELLOW + Back.BLACK + Style.DIM+ '\n         EARLY STOPPING !!!!         ' +  Style.RESET_ALL) 
                    print(Fore.BLACK + Back.YELLOW + Style.DIM+ '\n            EARLY STOPPING !!!!             ' +  Style.RESET_ALL) 
                    print(Fore.YELLOW + Back.BLACK + Style.DIM+ '\n         EARLY STOPPING !!!!         ' +  Style.RESET_ALL) 
                    print(Fore.BLACK + Back.YELLOW + Style.DIM+ '\n            EARLY STOPPING !!!!             ' +  Style.RESET_ALL) 
                    print()
                    break

                    

                    
                # Aggregate values to dataframes
                ##################################
                if d['SPLIT_CV_KFOLDS'] > 1:
                                                                    
                    y_train_preds_list[fold]        = y_train_preds        # Concatenación de los y_train_preds de cada fold
                    y_train_transf_list[fold]       = y_train_transf       # Concatenación de los y_train_transf de cada fold
                    y_train_transf_preds_list[fold] = y_train_transf_preds # Concatenación de los y_train_transf_preds de cada fold
                    y_val_transf_list[fold]         = y_val_transf         # Concatenación de los y_val_transf de cada fold
                    y_val_transf_preds_list[fold]   = y_val_transf_preds   # Concatenación de los y_val_transf_preds de cada fold
                    y_val_preds_list[fold]          = y_val_preds          # Concatenación de los y_val_preds de cada fold
                    y_test_preds_list[fold]         = y_test_preds         # Concatenación de los y_test_preds de cada fold


                  
                # Estimated SUBMISSION FINISH TIME
                ###################################
                try:
                    if (fold_idx + 1) < num_folds:
                        time_delta_fold = int(timeit.default_timer()- t0_fold) 
                        
                        time_delta_para_hora = int(time_delta_fold * (num_folds - (fold_idx + 1))) # time for remaining folds in seconds
                        time_delta_para_hora_in_mins = int(time_delta_para_hora/60) 
                        if time_delta_para_hora > 20:
                            hora_estimada = (datetime.now() + timedelta(seconds=time_delta_para_hora)).strftime('%H:%M')
                            print(Fore.BLUE + '\tESTIMATED SUBMISSION FINISH TIME IS ' + hora_estimada + ' (' + str(time_delta_para_hora_in_mins) + ' minutes from now)\n\n' + Style.RESET_ALL)  
                except:
                    pass
                

                # Fin bucle por fold   # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
                # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
                ## # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
                # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
            
                

            #############################################
            #         CONSOLIDAMOS info de FOLDS        #
            #      (en el caso de CrossValidation)      #
            #############################################
            
            if d['SPLIT_CV_KFOLDS'] > 1 and execution_CV_stopped == 0:

                # Flattening de los distintos vectores concatenados
                N_val       = sum(N_val_list) # Suma de los Nval de cada fold
                
                y_train              = np.array([item for sublist in y_train_list for item in sublist])
                idx_val              = np.array([item for sublist in idx_val_list for item in sublist])  
                y_val                = np.array([item for sublist in y_val_list for item in sublist]) 
                
                y_train_preds        = np.array([item for sublist in y_train_preds_list for item in sublist]) 
                y_train_transf       = np.array([item for sublist in y_train_transf_list for item in sublist]) 
                y_train_transf_preds = np.array([item for sublist in y_train_transf_preds_list for item in sublist])
                
                y_val_transf         = np.array([item for sublist in y_val_transf_list for item in sublist]) 
                y_val_transf_preds   = np.array([item for sublist in y_val_transf_preds_list for item in sublist]) 
                y_val_preds          = np.array([item for sublist in y_val_preds_list for item in sublist])
                
                
                #  RESULTADOS
                ######################
                
                # Train score   
                results['model_score_train'] = my_score(y_train, y_train_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train","y_train", "y_train_preds",d['PRINT_EVAL_INFO'])

                # Train Transf Score (y_train_transf_preds, y_train_transf)      
                results['model_score_train_transf'] = my_score(y_train_transf, y_train_transf_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Train (transf)","y_train_transf", "y_train_transf_preds",d['PRINT_EVAL_INFO'])                    
                               
                # Validation Transf Score (y_val_transf_preds, y_val_transf)   
                if val_exists:                    
                    results['model_score_val_transf'] = my_score(y_val_transf, y_val_transf_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    

                # y_test_preds
                if d['COMP_PROBLEM_TYPE'] == 'Regr' or d['COMP_PROBLEM_TYPE'] == 'Class_B':
                    y_test_preds = np.array(np.mean(y_test_preds_list, axis=0))
                else:
                    # Ojo, en caso de que la moda de más de un resultado (en caso de empate, caso raro pero se puede dar), nos estamos quedano arbitrariamente con el primer resultado
                    y_test_preds = []
                    num_observaciones_test = len(y_test_preds_list[0])
                    for num_observacion in range(0,num_observaciones_test):
                        y_test_preds_list_array = np.array(y_test_preds_list)
                        moda_prediccion = moda(list(y_test_preds_list_array[:,num_observacion]))
                        moda_prediccion_elegida = moda_prediccion[0] # Aquí estamos escogiendo arbitrariamente el primer resultado de la moda
                        y_test_preds.append(moda_prediccion_elegida)
                    y_test_preds = np.array(y_test_preds)
                    
                # Test Score (if exists)   
                if y_test_exists:                    
                    results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    
                                
               
                
                #if val_exists:
                #    print("idx_val.shape", idx_val.shape)
                #    print("y_val_preds.shape", y_val_preds.shape)
                #    print("y_val.shape", y_val.shape)
                #print("idx_test.shape", idx_test.shape)
                #print("y_test_preds.shape", y_test_preds.shape)



    # FIN  if d['LEVEL']=='1_ML' or d['LEVEL']=='2_ML'  ######################
    #####################################################################
    ################################################################
    ############################################################
    ########################################################
    ####################################################
    ################################################
    
    
    
    
    # -------------------------------------------------------------------------
    # Aquí enlazamos con 1_ML, 2_ML, 2_CARUANA, tras CUSTOM_ML, External Scripts
    # -------------------------------------------------------------------------

    
    ######################
    # XGBoost stuff
    ######################

    # En el caso XGB comprobamos si existe el parámetro early_stopping_rounds
    try:
        if results['alg_param']['early_stopping_rounds']>0:
            EXISTS_early_stopping_rounds = True
    except:
        EXISTS_early_stopping_rounds = False
    
    # En el caso XGB y de que exista early_stopping_rounds
    if alg.upper()[:3] == 'XGB' and EXISTS_early_stopping_rounds:
        print()
        print('Resultados por fold de XGB:')
        print("results['score_validation_by_fold']",results['score_validation_by_fold'])
        print("results['best_iteration']",results['best_iteration'])
        print("results['best_ntree_limit']",results['best_ntree_limit'])
        if results['best_ntree_limit']==[]:
            print(Fore.RED + 'Tu versión de xgboost es antigua pues best_model no devuelve el parámetro best_ntree_limit, por lo que la predicción no se ha realizado en la mejor iteración, sino early_stopping_rounds iteraciones despues' + Style.RESET_ALL)
            

            
    #######################
    # SCORES consolidation  (for CrossValidation)
    #######################
    if d['SPLIT_CV_KFOLDS']> 1 and d['LEVEL']!='1_EXTERNAL_PREDICTIONS' and execution_CV_stopped == 0:  

        # Validation score  
        if val_exists:        
            results['model_score_val'] = my_score(y_val, y_val_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation","y_val", "y_val_preds",d['PRINT_EVAL_INFO'])                    
            #try:
            results['model_score_cv'] = np.mean(model_score_cv)
            #except:
            #    results['model_score_cv'] = results['model_score_val']
                
            #try:
            results['model_score_cv_sd'] = np.std(model_score_cv)
            #except:
            #    results['model_score_cv_sd'] = results['model_score_val']

        # Test Score (if exists)   
        if y_test_exists:                    
            results['model_score_test'] = my_score(y_test, y_test_preds,d['COMP_EVALUATION_METRIC'],d['COMP_NAME'],"Score Validation (transf)","y_val_transf", "y_val_transf_preds",d['PRINT_EVAL_INFO'])                    
                     
      

    #######################################################
    ###                                                 ### 
    ###   Feature Importance Persist-if-Best & Print    ###
    ###                                                 ### 
    #######################################################
    
    #did_improve_fold
    if d['SPLIT_CV_KFOLDS']> 1:
        did_improve_overall, did_improve_fold = scores_did_improve(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best)
    else:
        did_improve_overall =  did_improve_fold

    if d['ML_FEATURE_IMPORTANCE'] and 'features_best_df' in locals():
        if len(features_best_df)>0:
            # PERSIST
            if did_improve_overall.lower() == 'yes' or did_improve_overall.lower() == 'unknown' or (os.path.isfile(feature_importance_best_file_name) == False):
                
                features_best_df.to_csv(feature_importance_best_file_name, index=False)

            if d['PRINT_FEATURE_IMPORTANCE'] and fold_idx==len(folds_enumerate)-1:
                # PRINT
                try:
#                    feature_importance_print(d, features_best_df, features_best_historic_df, did_improve_overall)
                    feature_importance_print_average(d, feature_importance_per_fold, features_best_historic_df, did_improve_overall)
                except:
                    pass
                                        
                        
    #######################################################
    ###                                                 ### 
    ###              DASHBOARD DIFFERENCES              ###
    ###                                                 ### 
    #######################################################


    print_dashboard_differences(d, dashboard_best, did_improve_overall)                    
            
            
            
    #######################
    # SCORES consolidation  (for CrossValidation)
    #######################
    if d['SPLIT_CV_KFOLDS']> 1 and d['LEVEL']!='1_EXTERNAL_PREDICTIONS' and execution_CV_stopped == 0:  

        print(Fore.YELLOW + Back.BLACK + Style.DIM+ '\n            CROSSVALIDATION SCORE             ' +  Style.RESET_ALL) 

        
        # Print scores  
        did_improve_overall, did_improve_fold = scores_print(d, results, val_exists, y_test_exists, alg, best_params, score_previous_best)
        

        #######################
        # Confusion Matrix
        #######################
        if d['CHART_CONFUSION_MATRIX'] and (d['COMP_PROBLEM_TYPE'].lower() == 'class_m' or d['COMP_PROBLEM_TYPE'].lower() == 'class_b'): 
            confusion_matrix_compute_and_print(d, y_val, y_val_preds)    
            

            
            
            
        
        
    #######################################################
    ###                                                 ### 
    ###                CHARTS / HISTOGRAMS              ###
    ###                                                 ### 
    #######################################################
        
    # Histograma 
    if d['CHART_HISTOGRAMS']:
        if int(max(y_train))>int(max(y_test_preds)):
            max_histogram=int(max(y_train))
        else:
            max_histogram=int(max(y_test_preds))
            print("max_histogram es de prediction",max_histogram)
        
            
        if int(min(y_train))>int(min(y_test_preds)):
            min_histogram=int(min(y_train))
        else:
            min_histogram=int(min(y_test_preds))
            
        if np.size(np.unique(y_train.astype(int))) > np.size(np.unique(y_test_preds.astype(int))):
            num_bins=np.size(np.unique(y_train.astype(int)))
        else:
            num_bins=np.size(np.unique(y_test_preds.astype(int)))
            
        charts_histograms(y_train,"Class - TRAIN: " + d['COMP_COLUMN_CLASS'],d['DIR_OUTPATH_CHARTS'],(min_histogram,max_histogram),num_bins)
        
        charts_histograms(y_test_preds,"Class Predicted - TEST: " + d['COMP_COLUMN_CLASS'],d['DIR_OUTPATH_CHARTS'],(min_histogram,max_histogram),num_bins)
    
    if d['CHART_BOXPLOT']:    #Histograma Features Numéricas        
        charts_boxplot(y_train,"Class - TRAIN: " + d['COMP_COLUMN_CLASS'],d['DIR_OUTPATH_CHARTS'],columnas=vars_numeric_orig) 
        charts_boxplot(y_test_preds,"Class Predicted - TEST: " + d['COMP_COLUMN_CLASS'],d['DIR_OUTPATH_CHARTS'],columnas=vars_numeric_orig)      
    
        
        


    ####################
    # Software Version
    ####################
    try:
        with open('VERSION', 'r') as f:
            soft_Version = str(f.readline())
    except:
        soft_Version = 'Unknown'
        
    
        


    ####################################################
    ###                                              ### 
    ###         SAVE SUBMISSION & PREDICTIONS        ###
    ###                                              ###
    ####################################################    
    if execution_CV_stopped == 0:

        save_predictions(           1,    d['SAVE_HDF5_EXECUTIONS'],  1,    1,   d['SAVE_SQLITE_EXECUTIONS'], d, results, best_parameters, execution_CV_stopped, d['SPLIT_CV_KFOLDS'], id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION)
                 #     SUBM,          HDF5,               PKL,  CSV,           SQL
    else:
        save_predictions_CV_stopped(1,    d['SAVE_HDF5_EXECUTIONS'],  1,    1,   d['SAVE_SQLITE_EXECUTIONS'], d, results, best_parameters, execution_CV_stopped, d['SPLIT_CV_KFOLDS'], id_execution, CUSTOM_SUBMISSION_PRE, CUSTOM_SUBMISSION_CSV, idx_val, y_val, y_val_preds, idx_test, y_test_preds, val_exists, y_test_exists, N_val, N_test, idx_dtype, y_dtype, fold, DIR_OUTPATH_SUBMISSIONS, DIR_OUTPATH_SUBMISSIONS_VALIDATION)
                            #     SUBM,          HDF5,               PKL,  CSV,           SQL


        
        
    #################
    # Send email    #
    #################
    # It has to be configured in my_Mail_Alerts.py !!!
    send_email_with_submission(d, results, DIR_OUTPATH_SUBMISSIONS)

        

        
    ############################################
    #            Correlation Matrix            #
    ############################################
    if os.path.exists(d['DIR_DATA']+'competition_data.hdf5'):
        corr_matrix__pd = hdf5_read_competition_data("Features_Correlation_Matrix",d['DIR_DATA'])
        print(corr_matrix__pd)
    
    
    
    

    
    # Queries returning 0 cols
    ############################
    try:
        if len(dXT["QUERY_COLS"]) > 0:
            print('---------------------------------------------------------------------------')
            print("The following queries did NOT return any columns:")
            for key, value in dXT["QUERY_COLS"].items():
                print(Fore.RED + "\t" + str(key) + ": " + str(value['ORIG']) + ""  + Style.RESET_ALL)
    except:
        pass
    
    # PRP CALL ERRORS
    ############################
    try:
        if len(dXT["PRP_CALL_ERRORS"]) > 0:
            print('---------------------------------------------------------------------------')
            print("The PARAMETERS sent to the following PREPROCESSINGs returned ERRORs:")
            for key, value in dXT["PRP_CALL_ERRORS"].items():
                print(Fore.RED + "\t" + str(key) + ": " + str(value) + ""  + Style.RESET_ALL)
    except:
        pass
    
    # PRPs with ERRORS
    ############################
    try:
        if len(dXT["PRP_ERRORS"]) > 0:
            print('---------------------------------------------------------------------------')
            print("The following PREPROCESSINGs returned an ERROR:")
            for key, value in dXT["PRP_ERRORS"].items():
                print(Fore.RED + "\t" + str(key) + ": " + str(value) + ""  + Style.RESET_ALL)
    except:
        pass

    # Execution Time
    #################
    print('---------------------------------------------------------------------------')
    results['total_execution_time'] = int(timeit.default_timer()-t0)
    print(Fore.BLUE + '\n\t OVERALL TIME: ' + str(results['total_execution_time']) + " segundos.\n" +  Style.RESET_ALL)
    
    ExecutionContext().clean(d)
    AlgorithmFactory().clean(d)

    
