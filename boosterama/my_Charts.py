# -*- coding: utf-8 -*-

############################################
### IMPORTS - Respetar ORDEN ALFABÉTICO  ###
############################################

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pylab import savefig
import re
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
#from string import letters


############################################
###         FUNCIONES - CHARTS           ###
############################################


def charts_boxplot(data,frase,OUTPATH_CHARTS,columnas=[]):
    
    if frase.find('TEST')>-1:
        color_bars='#BD2031'
        color_median='#3F5D7D'
        
    else:
        color_bars='#3F5D7D'
        color_median='#BD2031'
    

    #Pandas Dataframe Multicolumna
    if isinstance(data, pd.DataFrame):
        num_cols = len(columnas)
        
        chart_width = int(num_cols*0.4)
        chart_height = int(num_cols/chart_width)

        fig, axes = plt.subplots(nrows=chart_height, ncols=chart_width, figsize=(num_cols, int(num_cols*0.8)))
        plt.suptitle(frase, fontsize=16)
        
        numeros = list(range(0,num_cols))
        
        for numero in numeros:
            labels = list([columnas[numero]])       
            bp = axes[numero%chart_height, numero%chart_width].boxplot(data[columnas[numero]], labels=labels, patch_artist=True)
            
            for box in bp['boxes']:
                box.set( facecolor = color_bars )
            for median in bp['medians']:
                median.set(color=color_median, linewidth=2)  
        
        fig.subplots_adjust(hspace=0.4)
        
    #Pandas Serie (Clase probablemente)
    else:

        clean_data = np.array(data)
        clean_data = clean_data[~np.isnan(clean_data)] 
        
        # Create a figure instance
        fig = plt.figure(1, figsize=(9, 6))
        plt.suptitle(frase, fontsize=16)
        
        # Create an axes instance
        ax = fig.add_subplot(111)
        
        # Create the boxplot
        bp = ax.boxplot(clean_data, patch_artist=True)
        
        
        for box in bp['boxes']:
            box.set( facecolor = color_bars )
        for median in bp['medians']:
            median.set(color=color_median, linewidth=2)  

    plt.show()
    
    fig.savefig(OUTPATH_CHARTS+"boxplot-" + re.sub(r'[^a-zA-Z0-9\-]','', frase)+".png", bbox_inches='tight')



def charts_histograms(data,frase,OUTPATH_CHARTS,min_max_histogram=None,num_bins=100,columnas=[]):
    
    if frase.find('TEST')>-1:
        color_bars='#BD2031'
    else:
        color_bars='#3F5D7D'
        
        

    #Pandas Dataframe Multicolumna
    if isinstance(data, pd.DataFrame):
        columnas_2_print=[]
        #columns_remove=[]
        for col in columnas:
            if col in data.columns:
                columnas_2_print.append(col)

        data=data[columnas_2_print]
        
     
        num_cols=data.shape[1]
        fig = plt.figure(figsize=(num_cols, int(num_cols*0.8))) #ancho, altura en pulgadas
        plt.suptitle(frase, fontsize=16)
        
        # loop over all features
        for i in range(0, num_cols):
            plt.subplot(6, int(num_cols/6)+1, i+1)
            f = plt.gca()
            f.axes.get_yaxis().set_visible(False)
            f.set_title(data.columns.values[i])
            # f.axes.set_ylim([0, data.shape[0]])
            
            clean_data = np.array(data.iloc[:, i])
            clean_data = clean_data[~np.isnan(clean_data)]            
            vals = np.size(np.unique(clean_data))
            #print("vals:",vals,"min:",np.min(clean_data),"max:",np.max(clean_data),"isnan:",np.unique(np.isnan(clean_data)),"isinf:",np.unique(np.isinf(clean_data)))
            if vals > 200:
                vals = 200
                
            if (data.iloc[:, i].dtypes) != 'datetime64[ns]' and (np.max(clean_data)>np.min(clean_data)):
                plt.hist(clean_data, bins=vals, color=color_bars)
            else:
                pass
        plt.tight_layout()
        
    #Pandas Serie (Clase probablemente)
    elif isinstance(data, pd.Series):
        fig = plt.figure(figsize=(3, 3))
        plt.suptitle(frase, fontsize=16)
        fig = plt.gca()
        fig.axes.get_yaxis().set_visible(True)
        
        clean_data = np.array(data)
        clean_data = clean_data[~np.isnan(clean_data)]            
        vals = np.size(np.unique(clean_data))
        
        #○if vals < 10:
        plt.hist(clean_data, bins=vals, color=color_bars)
        #else:
        #    plt.hist(data, bins=10, color='#3F5D7D')
    
    #Numpy con Clase
    else:
        fig = plt.figure(figsize=(3, 3))
        plt.suptitle(frase, fontsize=16)
        fig = plt.gca()
        fig.axes.get_yaxis().set_visible(True)
        
        clean_data = data[~np.isnan(data)]            
        
        #vals = np.size(np.unique(data.astype(int)))
        plt.hist(clean_data, bins=num_bins, color=color_bars,range=min_max_histogram)
        #else:
        #    plt.hist(data, bins=10, color='#3F5D7D')
    
        
    plt.savefig(OUTPATH_CHARTS+"histogram-distribution-" + re.sub(r'[^a-zA-Z0-9\-]','', frase)+".png")



def charts_correlations(data,OUTPATH_CHARTS,COLUMN_CLASS):
    #data.drop(['Id'],1,inplace=True)
    columns = data.dtypes[data.dtypes == 'object'].keys()

    for column in columns:
        lbl = LabelEncoder()
        data[column] = lbl.fit_transform(data[column])
   
    sns.pairplot(data, hue=COLUMN_CLASS, diag_kind="kde")
    
    savefig(OUTPATH_CHARTS+"correlation-plot.png")


def charts_corr_keep_numeric_and_class(d, X_TRAIN__pd, y_TRAIN__pd):
    
    def vars_numeric(df__pd):
        return list(df__pd.dtypes[df__pd.dtypes != "object"][df__pd.dtypes != "datetime64[ns]"].index)

    Xy_TRAIN__pd = pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)
    Xy_TRAIN__pd = Xy_TRAIN__pd[vars_numeric(Xy_TRAIN__pd)]
    
    if d['COMP_COLUMN_CLASS'] not in Xy_TRAIN__pd.columns.values:
        # Aquí sabemos que column class NO es numérica así que hacemos label encode
        Xy_TRAIN__pd = pd.concat([Xy_TRAIN__pd, y_TRAIN__pd], axis=1)
        lbl = LabelEncoder()
        Xy_TRAIN__pd[d['COMP_COLUMN_CLASS']] = lbl.fit_transform(Xy_TRAIN__pd[d['COMP_COLUMN_CLASS']])
        
    return Xy_TRAIN__pd



def charts_correlations_colors(corr,OUTPATH_CHARTS,multiplicador_size):
    
    multiplicador =  int(multiplicador_size/10)
    
    if multiplicador < 1:
        multiplicador = 1
    
    sns.set(style="white")

    # Generate a mask for the upper triangle
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True
    
    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(11 * multiplicador, 9 * multiplicador))
    
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    
    corr4max = corr.copy()
    corr4max = corr4max.replace(1,0)
    corr4max = corr4max.abs()
    
    max_corr = corr4max.max().max()
    print(corr4max)
    print('max_corr',max_corr)
    
    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(corr, mask=mask, cmap=cmap, vmax=max_corr, square=True,  linewidths=.5, cbar_kws={"shrink": .5}, ax=ax)
    plt.yticks(rotation=0) 
    plt.xticks(rotation=90)
    plt.suptitle("Correlation - NonNumeric Features", fontsize=16 * multiplicador)
    #xticklabels=2, yticklabels=2,
    savefig(OUTPATH_CHARTS+"correlation-colors.png")
    
    print()






def print_feature_importances(the_model, feature_names, summarized_columns,OUTPATH_CHARTS, autoscale=True,headroom=0.05, width=10):
    #graphs the feature importances of a random decision forest using a horizontal bar chart.
    #probably works but untested on other sklearn.ensembles
    
    #parameters
    #------------
    #ensemble=name of the ensemble whose features you would like graphed
    #feature_names = A list of the names of those features, displayed on the Y axis
    #autoscale = True (automatically adjust the X axis size to the largest feature +.headroom)/ False = scale from 0to1
    #headroom = used with autoscale, .005 default
    #width=figure width in inches
    #summarized_columns =  a list of column prefixes to summarize on, for dummy variables (e.g. day_ would summarize al days)
    
    if autoscale:
        x_scale=the_model.feature_importances_.max()+headroom
    else:
        x_scale=1

    
    feature_dict=dict(zip(feature_names,the_model.feature_importances_))

    if summarized_columns:
        #some dummy columns need to be summarized
        for col_name in summarized_columns:
            #sum all the features that contain col_name, store in temp sum value
            sum_value = sum(x for i, x in feature_dict.items() if col_name in i)
            
            #now remove all keys that are part of col_name
            keys_to_remove = [i for i in feature_dict.keys() if col_name in i]
            
            for i in keys_to_remove:
                feature_dict.pop(i)
                
            #lastly, read the summarized field
            feature_dict[col_name]= sum_value


    # PANDAS Dataframe con la info de Importancia
    results=pd.DataFrame(list(feature_dict.items()),columns=['columnas','importancia'])  
   
    results = results.sort_values(by=['importancia'], ascending=[1])    

    factor_corrector=1  #Si quieres cambiar la escala de las importancias para que no sean tan bajas
    results['importancia'] = results['importancia'] * factor_corrector


    results['columnas'] = results['columnas'].str.replace('|', '  -|')

    results.reset_index(inplace=True)    
    
    # DIBUJA
    mi_chart=results['importancia'].plot(kind="barh",figsize=(width,len(results['importancia'])/4), xlim=(0,x_scale*factor_corrector))
    mi_chart.set_yticklabels(results['columnas'])
    mi_chart.set_xlabel('Importance')
    mi_chart.grid(True)
    
    
    # SAVE
    savefig(OUTPATH_CHARTS+"feature-importance.png")


    # PRINT nombres de columna en orden ascendente según importancia (menos importante primero)
    #De aquí se puede sacar una selección para Drop
    vector_Columnas_Importancia="["
    
    for f in results['columnas']:
        vector_Columnas_Importancia+=("'" + f.replace('  -|','') + "',")

    vector_Columnas_Importancia=vector_Columnas_Importancia[:-1]+"]"
    print ("")
    print ("A continuación nombres de columna por importancia ascendente por si se quiere Drop algunas")
    print(vector_Columnas_Importancia)





