# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 14:09:01 2016

@author: DataCousins
"""

import numpy as np
import pandas as pd
import sys


#################################
##  Import external predictions  ##
################################# 

def import_idx_TRAIN__pd_and_y_TRAIN__pd(filePath,id_col_name,y_col_name):
    print(filePath)
    data = pd.read_csv(filePath, usecols=[id_col_name,y_col_name], encoding='utf-8', sep=',', engine='c')
    
    if len(data.columns)!=2:
        print('Error: Two columns were expected, and found ', len(data.columns))
        sys.exit('End')
         
    data = data.sort_values(id_col_name, axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    idx__pd = data.pop(id_col_name)
    y__pd = data.pop(y_col_name)
    
    return idx__pd, y__pd


def import_idx_test__pd(filePath,id_col_name):

    data = pd.read_csv(filePath, usecols=[id_col_name], encoding='utf-8', sep=',', engine='c')
    
    if len(data.columns)!=1:
        print('Error: One column was expected, and found ', len(data.columns))
        sys.exit('End')
         
    data = data.sort_values(id_col_name, axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    idx__pd = data.pop(id_col_name)
    
    return idx__pd
    

def import_external_predictions(filePath,id_col_name,y_col_name):

    data = pd.read_csv(filePath, encoding='utf-8', sep=',', engine='c')
    
    
    # Comprobamos si los nombres de las columnas del dataframe son los esperados
    # Si hay un problema de mayusculas/minusculas lo corregimos, si no, abortamos
    cols_names = list(data.columns)
    if len(data.columns)!=2:
        print('Error: Two columns submission was expected, and found ', len(data.columns))
        sys.exit('End')
    else:
        if cols_names == [id_col_name,y_col_name] or cols_names == [y_col_name,id_col_name]:
            pass
        else:
            cols_names_lower = [col_name.lower() for col_name in cols_names]
            if cols_names_lower == [id_col_name.lower(),y_col_name.lower()] or cols_names_lower[0] == id_col_name.lower():
                data=data.rename(columns = {data.columns[0]:id_col_name})
                data=data.rename(columns = {data.columns[1]:y_col_name})
            elif cols_names_lower == [y_col_name.lower(),id_col_name.lower()] or cols_names_lower[1] == id_col_name.lower():
                data=data.rename(columns = {data.columns[0]:y_col_name})
                data=data.rename(columns = {data.columns[1]:id_col_name})
            else:
                print('Error: Columns names submission [',data.columns[0],',',data.columns[1],'] are different of expected ones: [',id_col_name,',',y_col_name,']')
                sys.exit('End')            
                                
                
    data = data.sort_values(id_col_name, axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    idx__pd = data.pop(id_col_name)
    y__pd = data.pop(y_col_name)
    #y = np.array(data).reshape(data.shape[0],)
    
    return idx__pd, y__pd


def pandas_to_numpy_external_predictions_version(idx_TRAIN__pd,y_TRAIN_preds_CV__pd,idx_test__pd, y_test_preds__pd):

    idx_TRAIN = np.array(idx_TRAIN__pd)
    if idx_TRAIN.dtype != "object": idx_TRAIN = idx_TRAIN.astype(int)
    y_TRAIN_preds_CV = np.array(y_TRAIN_preds_CV__pd)

    idx_test = np.array(idx_test__pd) 
    y_test_preds = np.array(y_test_preds__pd)
            
    N_test = idx_test.shape[0]        
            
    return idx_TRAIN, y_TRAIN_preds_CV, idx_test, y_test_preds, N_test