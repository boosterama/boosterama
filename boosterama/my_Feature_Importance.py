# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 15:49:23 2016

@author: DataCousins
"""

from colorama import Fore, Back, Style
import numpy as np

import os
import pandas as pd
import pylab as pl
from sklearn.ensemble import *

import sys

from my_Charts import *


from framework.algorithm.factory import AlgorithmFactory


def feature_importance(d, alg, best_model, did_improve_fold, fold, X_TRAIN__pd, X_train, y_train):
    
    id_execution = d['ID']
    
    # Persiste Features
    #######################
    X_cols_names = list(X_TRAIN__pd.columns.values)
    
    '''
    TO-DO: Check if feature_importance/features.csv is still necesary.
    I think it's not being used anymore
    '''
    features_file_name = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/features.csv'
    feature_importance_this_execution_file_name = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/features_'           + d['ML_ALGO'][0] + '_' + str(id_execution) +  '_fold_' + str(fold) +  '.csv'
    feature_importance_best_file_name =           d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL']      + '_' + d['ML_ALGO'][0] + '.csv'
    #feature_importance_this_execution_file_name =  d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL'] + '_' + d['ML_ALGO'][0] + '_' + str(id_execution) + '.csv'

    # Escribe el archivo de Feature Importance sólo si NO existe

    if os.path.isfile(feature_importance_this_execution_file_name):
        df_feat_imp = []
    else:  
        feature_importance_persist(d, features_file_name, X_cols_names)
        feature_importance_persist(d, feature_importance_this_execution_file_name, X_TRAIN__pd.columns.values)  
        
        algorithm_factory=AlgorithmFactory()
        algorithm=algorithm_factory.get(d)

        if algorithm.implements_feature_importance():
            
        
            importance_fscore, importance_gain, importance_array, df_feat_imp=algorithm.feature_importance(best_model)


        else:
   
            try:
                importance_fscore                   = best_model.feature_importances_   
                importance_array                    = importance_fscore.copy()
                importance_fscore_with_column_names = list(zip(X_cols_names,importance_array))
                df_feat_imp                         = pd.DataFrame(importance_fscore_with_column_names, columns=['feature', 'feature_score'])
                #Creamos tabla con Feature, Dtype, Feat_Imp
                df_feat_imp['feature_score'] = df_feat_imp['feature_score'] / df_feat_imp['feature_score'].sum()   
            except:
                importance_array = []


        if len(importance_array) > 0:
                        
            vect   = []
            levels = []
            
            for idx,elem in enumerate(X_TRAIN__pd.dtypes): 
                vect.append(str(elem))
    
            for idx,elem in enumerate(X_TRAIN__pd.columns.values):                 
                levels.append(len(X_TRAIN__pd.groupby([elem])))
    

            s1 = pd.Series(X_TRAIN__pd.columns, name='feature')
            s2 = pd.Series(vect, name='Dtypes')
            s3 = pd.Series(levels, name='Levels')
            
            df_Cols_Dtypes = pd.concat([s1, s2, s3], axis=1)
    
            df_feat_imp = pd.merge(df_Cols_Dtypes, df_feat_imp, on='feature', how='outer')
            
            # Clean df_feat_imp
            #df_feat_imp.drop('feature', axis=1, inplace=True)
            df_feat_imp.fillna(0, inplace=True)
            
            df_feat_imp.sort_values(by=['feature_score'], ascending=[0], inplace=True)
            
            df_feat_imp.reset_index(drop=True,inplace=True)            
            
            
            df_feat_imp.to_csv(feature_importance_this_execution_file_name, index=False)      

        #if d['SAVE_HDF5_FEATURES_IMPORTANCE']:
        #    sqlite_save_feature_importance(d,d['DIR_OUTPATH_CHARTS'] + 'feature_importance/', df_feat_imp, df_feat_imp.dtypes)


    return df_feat_imp, feature_importance_best_file_name




# Consolidador de varios FeatureImportance files en uno único.
def feature_importance_aggregator(d, X_TRAIN__pd):
    
    X_cols_names = list(X_TRAIN__pd.columns.values)

    feature_importance_folder =  d['DIR_OUTPATH_CHARTS'] + 'feature_importance/'
    features_file_name = feature_importance_folder + 'features.csv'
    feature_importance_file_prefix = 'features_' + d['ML_ALGO'][0] + '_'
    feature_importance_file_persist_csv = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/' + 'features_average_' + d['ML_ALGO'][0] + '_'
    
    # Hay que pillar el executions info para tener ID execution y Validation Score
    # Hay que pillar todos los feature_importance de executions por encima del umbral
    df_executions = pd.read_excel(d['DIR_DATA'] + 'Executions_Simplified.xlsx',sheetname=0)
    
    executions = []
    
    for execution in df_executions['ID_EXECUTION']:
        executions.append(str(execution))

    # Los rankeamos y añadimos nuevas columnas donde la cabecera es el id execution.
    
    df_feat_imp = feature_importance_aggregator_import(d, X_cols_names, executions, feature_importance_folder, features_file_name, feature_importance_file_prefix)
    

    # la media de ranking de cada feature se re-rankea y ahí tenemos un muy buen feature importance
    # se añade una columna para las que no fueran features originales
    
    # se persiste en xlsx
    
    df_feat_imp.to_excel(feature_importance_file_persist_csv + '.xlsx', sheet_name='Sheet1', na_rep='', index=False)
    
    # se persiste tb en csv en un formato idéntico al de feature_importance por si se quiere sustituir
    df_feat_imp = df_feat_imp[['feature','Dtypes','Levels','feature_score']]
    df_feat_imp.to_csv(feature_importance_file_persist_csv + '.csv', index=False)

    return None


    


def feature_importance_aggregator_get_IDs(feature_importance_folder, feature_importance_file_prefix):
    csv_files_pre = os.listdir(feature_importance_folder)
    csv_files = [s.replace('.csv','').replace(feature_importance_file_prefix,'') for s in csv_files_pre if feature_importance_file_prefix in s]
    return csv_files

   

    

def feature_importance_aggregator_import(d, X_cols_names, executions, feature_importance_folder, features_file_name, feature_importance_file_prefix):

    feature_files_exec_ids = feature_importance_aggregator_get_IDs(feature_importance_folder, feature_importance_file_prefix)

    executions_to_merge = list(set(list(executions)).intersection(feature_files_exec_ids))
    

    execution_first = executions_to_merge.pop(0)  

    df_feat_imp = pd.read_csv(feature_importance_folder + feature_importance_file_prefix + execution_first + '.csv')
    
    df_feat_imp.rename(columns={'feature_score':execution_first}, inplace=True)    

    
    if len(executions_to_merge) > 0:
        for execution in executions_to_merge:
            
            df_feat_imp_new = pd.read_csv(feature_importance_folder + feature_importance_file_prefix + execution + '.csv')
            
            df_feat_imp_new.rename(columns={'feature_score':execution}, inplace=True)

            df_feat_imp = pd.merge(df_feat_imp, df_feat_imp_new, on=['feature','Dtypes','Levels'], how='outer')     

            df_feat_imp.reset_index(drop=True,inplace=True)
            
            
    df_feat_imp['Levels'] = df_feat_imp['Levels'].astype(str)
    
    df_feat_imp['feature_score'] = df_feat_imp.mean(axis=1, skipna=True, numeric_only=True)
        
    df_feat_imp['Levels'] = df_feat_imp['Levels'].astype(int)
  
    df_feat_imp.sort_values(by=['feature_score'], ascending=[0], inplace=True)
    
    #Hay q meter los Levels IsOrig

    return df_feat_imp



    

def feature_importance_extract(d, feature_importance_file=''):
    # feature_importance_file   (if it does not exist, we look for another algorithm's feature importance)
    #########################      
    if feature_importance_file == '': 
        
        
        feature_importance_file = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL'] + '_' + d['ML_ALGO'][0] + '.csv'                    
        
        if d['COMP_PROBLEM_TYPE'].lower() == 'regr':   # ('regr', 'class_B', 'class_M')
            feature_importance_file_planB = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL'] + '_' + d['ML_ALGO'][0] + '.csv'
        elif d['COMP_PROBLEM_TYPE'].lower() == 'class_b':
            feature_importance_file_planB = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL'] + '_' + d['ML_ALGO'][0] + '_B.csv'
        elif d['COMP_PROBLEM_TYPE'].lower() == 'class_m':
            feature_importance_file_planB = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL'] + '_' + d['ML_ALGO'][0] + '_M.csv'
        else:
            feature_importance_file_planB = d['DIR_OUTPATH_CHARTS'] + 'feature_importance/feature_importance_' + d['LEVEL'] + '_' + d['ML_ALGO'][0] + '.csv'
        
    else:
        feature_importance_file_planB = ''

            
    # DO if exists
    ################                    
    if os.path.isfile(feature_importance_file):
        is_feature_file = True
    else:
        is_feature_file = False
            
    if os.path.isfile(feature_importance_file_planB):
        is_feature_file_planB = True
    else:
        is_feature_file_planB = False     
        
        
    # Get Feature Importance df from file
    if is_feature_file or is_feature_file_planB:

        try:
            df_feat_imp = pd.read_csv(feature_importance_file)
        except:
            df_feat_imp = pd.read_csv(feature_importance_file_planB)
            
    else:
        df_feat_imp = []
        
        
    return is_feature_file, df_feat_imp
        


        


def feature_importance_persist(d, feature_file, features):
    outfile = open(feature_file, 'w')
    i = 0
    for feat in features:
        try:
            outfile.write('{0}\t{1}\tq\n'.format(i, feat))
        except:
            print()
            print('There was an issue with the encoding of feature name:',feat)
            print('You may get an error in the future because of this.')
            print()
            outfile.write('{0}\t{1}\tq\n'.format(i, feat[1:]))
        i = i + 1
    outfile.close()
    

def feature_importance_print_average(d, feature_importance_per_fold, features_best_historic_df, did_improve_overall):
    #feature_importance_per_fold
    #feature_importance_per_fold[fold_idx]=(features_best_df, feature_importance_best_file_name)
    all_fi_list=[]
    for fold_id in feature_importance_per_fold:
        features_best_df, feature_importance_best_file_name = feature_importance_per_fold[fold_id]
        all_fi_list.append(features_best_df)
    
    all_fi_df=pd.concat(all_fi_list)
    
    fi_mean_df = all_fi_df.groupby(['feature', 'Dtypes']).agg(['min', 'max', 'mean'])
    fi_mean_df.reset_index(inplace=True)
    new_col_names=[]
    for col_name in fi_mean_df.columns:
        if col_name[1]=='' or col_name[1]=='mean':
            new_col_names.append(str(col_name[0]))
        else:        
            new_col_names.append("{}_{}".format(col_name[0], col_name[1]))
    
    fi_mean_df.columns=new_col_names
        
    fi_mean_df['Levels']=fi_mean_df['Levels'].round()
    
    fi_mean_df.sort_values(by=['feature_score'], ascending=[0], inplace=True)
        
    feature_importance_print(d, fi_mean_df, features_best_historic_df, did_improve_overall)


def feature_importance_print(d, features_best, features_best_historic, did_improve_overall):
    
    max_rows_2_check = 100
    max_rows_2_print = 50
    
    top_features_we_dont_have = [] #current model doesn't have these features when compared to best scoring one.
    
    features_to_plot = []
    
    
    if len(features_best)>0:
        
        print()
        print('---------------------------------------------------------------------------')       


        ###############################################
        ##   COLUMNS MISSING/ADDED RELATIVE TO BEST  ##
        ###############################################
        the_title = 'Features in Current submission'
        color_2_use = 'g'
        
        # Limit
        ############          
        num_feat_2_plot_orig = len(features_best)                                    
        num_feat_2_plot      = min(num_feat_2_plot_orig , max_rows_2_print)        
     
        if len(features_best) > max_rows_2_print:
            the_title = "Best " + the_title
            the_title += "\n(only showing Top" + str(max_rows_2_print) + " out of " + str(num_feat_2_plot_orig) +  ")"
            
        # Plot
        ############
        if num_feat_2_plot > 0:
            
            
            val    = np.array(list(reversed(list(features_best['feature_score'])[0:num_feat_2_plot])))    # the bar lengths
            labels = list(reversed(list(features_best['feature'])[0:num_feat_2_plot]))
            pos    = pl.arange(num_feat_2_plot)+.5    # the bar centers on the y axis
            
            min_val= np.array(list(reversed(list(features_best['feature_score_min'])[0:num_feat_2_plot]))  ) / features_best['feature_score'].sum() # percentage (divide by sum)
            max_val= np.array(list(reversed(list(features_best['feature_score_max'])[0:num_feat_2_plot]))  ) / features_best['feature_score'].sum()
            err_list=[min_val, max_val]
    
            pl.figure(num=1, figsize=(6,  max(int(num_feat_2_plot/3),1)  )   , dpi=80, facecolor='w', edgecolor='k')
            pl.barh(pos, val, align='center', color=color_2_use, xerr=err_list)#.set_color('r')#,xerr=rand(5), ecolor='r')
            pl.yticks(pos, labels)
            pl.xlabel('Feature Importance')
            pl.title(the_title)
            pl.show()                       
                
            
            

        
        
        ###############################################
        ##   COLUMNS MISSING/ADDED RELATIVE TO BEST  ##
        ###############################################

        # Did improve score
        ######################
        if did_improve_overall.lower() == 'yes':
            the_title = 'Features in Current submission NOT in Previous Best'
            color_2_use = 'b'
            
            if len(features_best_historic)>0:
                for feat in list(features_best['feature'][0:max_rows_2_check]):
                    if feat in list(features_best_historic['feature']):
                        pass
                    else:
                        top_features_we_dont_have.append(feat)
                        
            features_to_plot = features_best.loc[features_best['feature'].isin(top_features_we_dont_have)]
                                            
                                                          
        # Didn't improve score  
        ######################                                                        
        else:
            the_title = 'Missing features in Current submission compared to Best'
            color_2_use = 'r'
            
            if len(features_best_historic)>0:
                for feat in list(features_best_historic['feature'][0:max_rows_2_check]):
                    if feat in list(features_best['feature']):
                        pass
                    else:
                        top_features_we_dont_have.append(feat)
                        
            features_to_plot = features_best_historic.loc[features_best_historic['feature'].isin(top_features_we_dont_have)]
                                          #df[df['feature'].isin(list)]
            
                                              
                                              
        # Limit
        ############          
        num_feat_2_plot_orig = len(features_to_plot)                                    
        num_feat_2_plot      = min(num_feat_2_plot_orig , max_rows_2_print)
        
        if len(features_to_plot) > max_rows_2_print:
            the_title += "\n(only showing Top" + str(max_rows_2_print) + " out of " + str(num_feat_2_plot_orig) +  ")"
            
        
        
        
        # Plot
        ############
        if num_feat_2_plot > 0:
            
                        

            print(Fore.BLACK + Back.GREEN + Style.DIM + "\n    DIFF FEATURES    " +  Style.RESET_ALL)
            
                    
            
            
            val    = list(reversed(list(features_to_plot['feature_score'])[0:num_feat_2_plot]))    # the bar lengths
            labels = list(reversed(list(features_to_plot['feature'])[0:num_feat_2_plot]))
            pos    = pl.arange(num_feat_2_plot)+.5    # the bar centers on the y axis
    
            pl.figure(num=2, figsize=(6,  max(int(num_feat_2_plot/3),1)  )   , dpi=80, facecolor='w', edgecolor='k')
            pl.barh(pos, val, align='center', color=color_2_use)#.set_color('r')#,xerr=rand(5), ecolor='r')
            pl.yticks(pos, labels)
            pl.xlabel('Feature Importance')
            pl.title(the_title)
            pl.show()


    
