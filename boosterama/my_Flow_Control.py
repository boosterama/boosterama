# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 14:59:33 2015

@author: DataCousins
"""

from my_Input_Output import *

def end_this_execution(id_execution, d, results, error):
    
    ############################################
    ###    Save this dashboard to PKL        ###
    ###                                      ###
    ############################################   
    pickle_save_dashboard_dict(d,id_execution)   
    
    ############################################
    ###  Save this execution results to PKL  ###
    ###                                      ###
    ############################################
    execution_results_dict = execution_results_dictionary(id_execution,d,results)    
    pickle_save_execution_results_dict(execution_results_dict, id_execution, d['DIR_BASE'])
    
    return
