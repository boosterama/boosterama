# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 18:31:02 2016

@author: DataCousins
"""

from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
import os
from os.path import basename
import smtplib
import sys
import zipfile

from config.config_manager import *









def send_email_with_submission(d, results, DIR_OUTPATH_SUBMISSIONS):
# Para que funcione, antes de ejecutar el script hay que loguearse manualmente en gmail con usuario xxx@zzz.com y password 
    
    if d['SYS_DO_SUBMISSION'] and d['SYS_SEND_EMAIL']:
        
        #define variables
        subject = d['COMP_NAME'] + ": " + results['submission_name'][:20]
        send_to = [boosterama_config.destination_email]     # añadir más emails si se desea recibir en otro email
        text = d['COMP_NAME'] + "\n\nSubmission generada:\n\n" + results['submission_name'] + '.csv  adjunto.\n'
        submission_file_unzipped = DIR_OUTPATH_SUBMISSIONS + results['submission_name']
        submission_file_zipped = DIR_OUTPATH_SUBMISSIONS + results['submission_name'] + '.zip'
        
        #zip before send
        file_2_zip = zipfile.ZipFile(submission_file_zipped, "w")
        file_2_zip.write(submission_file_unzipped, os.path.basename(submission_file_unzipped), zipfile.ZIP_DEFLATED)
        file_2_zip.close()
        
        #send
        send_email(send_to, subject, text, files=[submission_file_zipped])

    return None
    
    

    
    

def send_email(send_to, subject, text, files=None):
    try:
        assert isinstance(send_to, list)
        msg = MIMEMultipart(
            From=boosterama_config.email,
            To=COMMASPACE.join(send_to),
            Date=formatdate(localtime=True)
        )
        msg['Subject']=subject
        msg.attach(MIMEText(text))
    
        for f in files or []:
            with open(f, "rb") as fil:
                msg.attach(MIMEApplication(
                    fil.read(),
                    Content_Disposition='attachment; filename="%s"' % basename(f),
                    Name=basename(f)
                ))
    
        smtpserver = smtplib.SMTP("smtp.gmail.com", 587)
        smtpserver.ehlo()
        smtpserver.starttls()
        smtpserver.ehlo
        smtpserver.login(boosterama_config.email_login, boosterama_config.email_password)
        smtpserver.sendmail(boosterama_config.destination_email, send_to, msg.as_string())
        smtpserver.close()
    except:
        print("ERROR en el envío de correo, revisa tu configuración en boosterama.ini y accede vía web a tu correo (si es correo web tipo gmail): ", sys.exc_info()[0])
