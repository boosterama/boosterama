# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 10:05:48 2016

@author: JavierTejedorAguilera
"""

import math

from my_Charts import *
from my_Input_Output import *
from my_Prepro_fx import *

import numpy as np

def import_and_preprocessing(d, dXT, CUSTOM_IMPORTER, TRAIN__pd = None, test__pd = None):
    
    
    # Add to dXT to hold data
    ###########################
    
    dXT["QUERY_COLS"]     = {}
    dXT["PRP_ERRORS"]     = {}
    dXT["PRP_CALL_ERRORS"]     = {}
    
    dXT["OWEN"]           = {}
    
    dXT["OWEN"]["Fields"] = []
    dXT["OWEN"]["Drop"]   = []
    dXT["OWEN"]["Metrics"]= []



    
    ###########################################################################
    # Import & Prep
    ##############################
    
    if d['SYS_DO_IMPORT']:  
        (test__pd, TRAIN__pd) = doImport(d, CUSTOM_IMPORTER,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        
        if d['PRINT_Y_INFO']: print_info(np.array(TRAIN__pd[d['COMP_COLUMN_CLASS']]),"y_TRAIN",[d['COMP_COLUMN_CLASS']])
        if d['PRINT_X_INFO']: print_info(TRAIN__pd[vars_numeric(TRAIN__pd)],"TRAIN__pd vars_numeric - After Import",list(TRAIN__pd[vars_numeric(TRAIN__pd)].columns.values))
        if d['PRINT_X_INFO']: print_info(test__pd[vars_numeric(test__pd)],"test__pd vars_numeric - After Import",list(test__pd[vars_numeric(test__pd)].columns.values))
    
    
        function_name = 'PRP_COLUMN_2_DATE'
        try:
            if len(d[function_name])>0:
                cols_names,test__pd, TRAIN__pd = column_2_date(d[function_name],test__pd, TRAIN__pd, function_name, d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
            
        
        function_name = 'PRP_DROP_FEATURES_ALL_NA_IN_TRAIN_OR_TEST'
        try:
            if d[function_name]:
                cols_names,test__pd,TRAIN__pd = drop_features_all_na_in_train_or_test(dXT,d,TRAIN__pd,test__pd,"TRAIN__pd","test__pd",d['PRINT_DROP_INFO'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    
    function_name = 'PRP_Y_WINSORING_REMOVE'
    try:
        if d[function_name] != (0,0):
            TRAIN__pd = winsoring_remove_y(d,TRAIN__pd,d[function_name][0],d[function_name][1],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
         
    # Split Id, X, y
    ###################
    idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd, X_cols_names, vars_numeric_orig, vars_non_numeric_orig, vars_date_orig, dXT['VARS_ORIG'] = split_train__pd_test__pd_components(TRAIN__pd,test__pd,d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])


    # import validation
    ####################################
    '''
    train_cols=X_TRAIN__pd.columns
    test_cols=X_test__pd.columns
        
    not_in_test = [x for x in train_cols if x not in test_cols]
    not_in_train = [x for x in test_cols if x not in train_cols]
    
#        print("test_cols:",test_cols)
#        print("train_cols:",train_cols)
#        print("not_in_test :",not_in_test )
#        print("not_in_train :", not_in_train )
    if len(not_in_test)>0 or len(not_in_train)>0:
        error_str=""
        if len(not_in_test)>0:
            error_str+="\n\nthere are columns in X_TRAIN__pd not in X_test__pd: "+str(not_in_test)
        
        if len(not_in_train)>0:
            error_str+="\n\nthere are columns in X_test__pd not in X_TRAIN__pd: "+str(not_in_train)
        
        raise ValueError('First check. Columns in TRAIN and TEST mismatch' + error_str + '\n\nColumns in TRAIN and TEST mismatch' )
    '''
    
    train_cols=X_TRAIN__pd.columns
    test_cols=X_test__pd.columns
    
    not_in_test = [x for x in train_cols if x not in test_cols]
    not_in_train = [x for x in test_cols if x not in train_cols]
    
    if len(not_in_test)>0 or len(not_in_train)>0:
        for col_2_drop in not_in_test:
            X_TRAIN__pd.drop(col_2_drop, axis = 1, inplace=True)
        for col_2_drop in  not_in_train:
            X_test__pd.drop(col_2_drop, axis = 1, inplace=True)
            
        error_str=""
        if len(not_in_test)>0:
            error_str+="\n\nDELETED columns because they was in X_TRAIN__pd not in X_test__pd: "+str(not_in_test)
        
        if len(not_in_train)>0:
            error_str+="\n\nDELETED columns because they was in X_test__pd not in X_TRAIN__pd: "+str(not_in_train)
        
        #raise ValueError('Columns in TRAIN and TEST mismatch' + error_str + '\n\nColumns in TRAIN and TEST mismatch' )
        dXT["PRP_ERRORS"]['import_and_preprocessing validation'] =  ValueError('Columns in TRAIN and TEST mismatch' + error_str + '\n\nColumns in TRAIN and TEST mismatch' )

    X_cols_names = list(X_TRAIN__pd.columns.values)
    
#    if not np.all(X_test__pd.columns==X_cols_names): #not necessary
#        X_test__pd = X_test__pd.reindex(X_cols_names, axis=1)
    
    
    
    
    
    ###########################################################################
    # Preprocessing
    #################
    
    function_name = 'PRP_TF_IDF'
    #try:
    if len(d[function_name])>0:
        for field_name, params_dict in d[function_name]:
            cols_names,X_test__pd, X_TRAIN__pd = do_tf_idf(dXT,d,field_name, params_dict, X_test__pd,X_TRAIN__pd)
    #except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    function_name = 'PRP_REGEX_COUNT'
    try:
        if len(d[function_name])>0:
            for options_dict in d[function_name]:
                cols_names,X_test__pd, X_TRAIN__pd = do_regex_counts(dXT,d,options_dict, X_test__pd,X_TRAIN__pd)
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    
    function_name = 'PRP_DROP_ALL_BUT_THESE'
    try:
        if len(d[function_name])>0:
            cols_names,X_test__pd, X_TRAIN__pd = drop_these_columns(dXT,d, d[function_name],1,X_test__pd, X_TRAIN__pd, function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'],d['PRINT_DROP_INFO'],"Motivo: Indicado en Dashboard")
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
         
   
         
    
    function_name = 'PRP_COUNT_BY_ROWS'
    try:
        if len(d[function_name])>0:
            X_TRAIN__pd = counts_by_rows(dXT,d, X_TRAIN__pd, d[function_name],function_name)
            X_test__pd  = counts_by_rows(dXT,d, X_test__pd , d[function_name],function_name)    
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
            
    
    function_name = 'PRP_DROP_THESE_COLUMNS'
    try:
        if len(d[function_name])>0:
            cols_names,X_test__pd, X_TRAIN__pd = drop_these_columns(dXT,d, d[function_name],0,X_test__pd, X_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'],d['PRINT_DROP_INFO'],"Motivo: Indicado en Dashboard")
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    
    

    function_name = 'PRP_REPLACER'
    try:
        if len(d[function_name]) > 0:
            X_test__pd,X_TRAIN__pd = replacer(dXT,d, X_test__pd,X_TRAIN__pd,y_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
               
    
        
    function_name = 'PRP_BOOLEAN_ENCODE'
    try:
        if d[function_name]:
            X_test__pd, X_TRAIN__pd = boolean_encode(d, vars_boolean(X_TRAIN__pd),X_test__pd,X_TRAIN__pd, function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
            
                     

    function_name = 'PRP_CHANGE_COL_NAMES'
    try:
        if len(d[function_name + "_OLD"])>0:
            X_TRAIN__pd, X_test__pd, X_cols_names = change_cols_names(X_TRAIN__pd, X_test__pd,d[(function_name) + "_OLD"],d[(function_name) + "_NEW"])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e

        

    function_name = 'PRINT_ANALYSIS_ON_TRAIN_TEST'
    try:
        if d[function_name]:
            print_numeric_and_nonnumeric_variables(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1),X_test__pd,vars_numeric(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)),vars_non_numeric(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1)),vars_numeric(X_test__pd),vars_non_numeric(X_test__pd))
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e

      
  
    function_name = 'PRP_NA_BINARIZE'
    try:
        if len(d[function_name]) > 0:
            X_test__pd, X_TRAIN__pd = na_binarize(dXT,d, d[function_name], X_test__pd,X_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e

 
    if len(d['PRP_NA_FILL']) > 0:
        X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        #print_numeric_and_nonnumeric_variables(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1),X_test__pd,vars_numeric(X_TRAIN__pd),vars_non_numeric(X_TRAIN__pd),vars_numeric(X_test__pd),vars_non_numeric(X_test__pd))

        
    function_name = 'CHART_BOXPLOT'
    try:
        if d[function_name]:    #Histograma Features Numéricas        
            charts_boxplot(X_TRAIN__pd,"Numerical Variables - TRAIN:",d['DIR_OUTPATH_CHARTS'],columnas=vars_numeric_orig) 
            charts_boxplot(X_test__pd,"Numerical Variables - TEST:",d['DIR_OUTPATH_CHARTS'],columnas=vars_numeric_orig) 
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e


    function_name = 'PRP_MONOTONIC_INT'
    try:
        if len(d[function_name])>0:
            X_test__pd, X_TRAIN__pd = monotonic_transformation(dXT,d,'int',d[function_name],X_test__pd,X_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    
    function_name = 'PRP_MONOTONIC_STR'
    try:    
        if len(d[function_name])>0:
            X_test__pd, X_TRAIN__pd = monotonic_transformation(dXT,d,'str',d[function_name],X_test__pd,X_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e



    function_name = 'PRP_INTEGER_ENCODE'
    try:    
        if len(d[function_name]) > 0:
            X_test__pd, X_TRAIN__pd = number_encode(dXT,d,d[function_name][0],d[function_name][1],d[function_name][2],d[function_name][3],'integer', X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e

            
    function_name = 'PRP_FLOAT_ENCODE'
    try:                    
        if len(d[function_name]) > 0:
            X_test__pd, X_TRAIN__pd = number_encode(dXT,d,d[function_name][0],d[function_name][1],d[function_name][2],d[function_name][3],'float', X_test__pd,X_TRAIN__pd,y_TRAIN__pd, function_name, d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e


    
    if len(d['PRP_NA_FILL']) > 0:
        X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])



    function_name = 'PRP_FLOAT_QUANTILE'
    try:                    
        if len(d[function_name])>0:
            X_test__pd, X_TRAIN__pd = float_quantile(dXT,d, d[function_name][0], d[function_name][1], d[function_name][2], d[function_name][3], X_test__pd, X_TRAIN__pd, y_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e




    function_name = 'PRP_NLP_TFIDF_SVM'
    try:                    
        if len(d[function_name])>0:
            X_test__pd, X_TRAIN__pd = nlp_tfidf_svm(dXT,d, d[function_name][0], d[function_name][1], d[function_name][2], X_test__pd, X_TRAIN__pd, y_TRAIN__pd,d['COMP_COLUMN_CLASS'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e


    function_name = 'PRP_WINSORING'
    try:     
        if len(d[function_name])>0:
            X_TRAIN__pd,X_test__pd = winsoring(dXT,d,d[function_name][0],X_test__pd,X_TRAIN__pd,d[function_name][1],d[function_name][2],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e


    function_name = 'PRP_OWEN_TRICK'
    try:     
        if len(d[function_name])>0:
            fields_2_owen = query_col(dXT,d,d[function_name][0],X_TRAIN__pd,function_name)
    
            if len(fields_2_owen) > 0:
                owen_trick_list_agg(dXT,d,fields_2_owen,d[function_name][2],X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d['COMP_COLUMN_CLASS'],d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'],d[function_name][1])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
                


    function_name = 'PRP_OWEN_2_FEATURE'
    try:     
        if len(d[function_name])>0:
            X_test__pd,X_TRAIN__pd = owen_trick(dXT,d,d[function_name][0],d[function_name][3],X_TRAIN__pd,y_TRAIN__pd,X_test__pd,d[function_name][2],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'],d[function_name][1])
    
            if d['PRINT_NA_INFO']: print_na_info(X_TRAIN__pd,"X_TRAIN__pd",X_test__pd,"X_test__pd",calculate=True,message="After Owen",showFields=d['PRINT_NA_INFO_SHOW_FIELDS'])
    
            if len(d['PRP_NA_FILL']) > 0:
                X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
            

    
    function_name = 'PRP_REPLACE_RARE_STRINGS'
    try:  
        if len(d[function_name]) > 0:
            X_test__pd,X_TRAIN__pd = replace_rare_strings(dXT,d, d[function_name][0], d[function_name][1], d[function_name][2], d[function_name][3],X_test__pd,X_TRAIN__pd,y_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
            
        
        
    function_name = 'PRP_OWEN_2_DATE_2_FEATURE'
    try:  
        if len(d[function_name])>0: 
            # [(['Time_'],'Date_',-1,-7)]
            X_test__pd, X_TRAIN__pd = date_owen_2_feature(dXT,d,d[function_name][0],d[function_name][1],d[function_name][2],X_test__pd,X_TRAIN__pd,y_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
      

        
    function_name = 'PRP_DATE_ENCODE'
    try:  
        if len(d[function_name])>0: 
            X_test__pd, X_TRAIN__pd = date_encode(dXT,d,d[function_name][0],d[function_name][1],d[function_name][2],d[function_name][3],X_test__pd,X_TRAIN__pd,y_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        else:
            try:
                X_TRAIN__pd = X_TRAIN__pd.drop(vars_date_orig, axis = 1)
            except:
                pass
            try:
                X_test__pd = X_test__pd.drop(vars_date_orig, axis = 1)
            except:
                pass
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
    
    
    
    function_name = 'PRP_COLS_JOIN'
    try:  
        if len(d[function_name])>0:               #dXT,d, condiciones_query, DO_OWEN, DO_DROP_ORIG_AFTER, DO_DROP_NEW_AFTER, DO_EXPLODE, operation, X_test__pd, X_TRAIN__pd,y_TRAIN__pd,separator, doPrintNA,doPrintNAShowFields):
            X_test__pd, X_TRAIN__pd = cols_operation(dXT,d, d[function_name][0], d[function_name][1], 0, d[function_name][2], d[function_name][3], 'join', X_test__pd,X_TRAIN__pd,y_TRAIN__pd,d['SEPARATOR_COLS_JOIN'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
            #condiciones_query = query[0]   #Do_Owen = query[1]   #Do_Drop_New = query[2]   #Do_Explode = query[3]
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
                
    function_name = 'PRP_COLS_DIFF'
    try:  
        if len(d[function_name])>0:
            X_test__pd, X_TRAIN__pd = cols_operation(dXT,d, d[function_name][0], d[function_name][1], 0, d[function_name][2], d[function_name][3], 'diff', X_test__pd,X_TRAIN__pd,y_TRAIN__pd,d['SEPARATOR_COLS_DIFF'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
        
    function_name = 'PRP_COLS_MULTIPLY'
    try:  
        if len(d[function_name]) > 0:
            X_test__pd, X_TRAIN__pd = cols_operation(dXT,d, d[function_name][0], d[function_name][1], 0, d[function_name][2], d[function_name][3], 'multiply', X_test__pd,X_TRAIN__pd,y_TRAIN__pd,d['SEPARATOR_COLS_MULTIPLY'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
        

        
    if len(d['PRP_NA_FILL']) > 0:
        X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])


        
    function_name = 'PRP_ONE_HOT_ENCODE'
    try:  
        if len(d[function_name]) > 0: 
            X_cols_names,X_test__pd,X_TRAIN__pd = one_hot_encode(dXT,d,d[function_name][0],d[function_name][1],d[function_name][2],d[function_name][3],d['PRP_ONE_HOT_ENCODE_THRESHOLD'],d['SEPARATOR_HOT_ENCODE'],X_test__pd,X_TRAIN__pd,y_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
         
        
        
    function_name = 'PRP_DROP_CONSTANTS'
    try:  
        if d[function_name]:
            X_test__pd,X_TRAIN__pd = drop_constants(X_test__pd,X_TRAIN__pd,d['PRINT_DROP_INFO'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     


    function_name = 'PRP_LABEL_ENCODE_LEX'
    try:  
        if d[function_name]:
            X_test__pd, X_TRAIN__pd = label_encode_lexical(dXT,d, d[function_name][0],d[function_name][1],d[function_name][2],d[function_name][3],X_test__pd,X_TRAIN__pd,y_TRAIN__pd,len(X_TRAIN__pd),function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
                                                              
            if d['PRINT_X_INFO']: print_info(X_TRAIN__pd,"X_TRAIN__pd - After Label Encode Lexical",list(X_TRAIN__pd.columns.values))
            if d['PRINT_X_INFO']: print_info(X_test__pd,"X_test__pd - After Label Encode Lexical",list(X_test__pd.columns.values))
        else:
            if len(d['PRP_NA_FILL']) > 0:
                X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
                #print_numeric_and_nonnumeric_variables(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1),X_test__pd,vars_numeric(X_TRAIN__pd),vars_non_numeric(X_TRAIN__pd),vars_numeric(X_test__pd),vars_non_numeric(X_test__pd))
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
    
    
    function_name = 'PRP_LABEL_ENCODE'
    try:  
        if d[function_name]:
            X_test__pd, X_TRAIN__pd = label_encode(vars_non_numeric(X_TRAIN__pd),X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
            
            if d['PRINT_X_INFO']: print_info(X_TRAIN__pd,"X_TRAIN__pd - After Label Encode",list(X_TRAIN__pd.columns.values))
            if d['PRINT_X_INFO']: print_info(X_test__pd,"X_test__pd - After Label Encode",list(X_test__pd.columns.values))
        else:
            X_TRAIN__pd = X_TRAIN__pd.drop(vars_non_numeric_orig, axis = 1)
            X_test__pd = X_test__pd.drop(vars_non_numeric_orig, axis = 1)
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     

    function_name = 'CHART_HISTOGRAMS'
    try:  
        if d[function_name]:    #Histograma Features Numéricas
            charts_histograms(X_TRAIN__pd,"Numerical Variables - TRAIN:",d['DIR_OUTPATH_CHARTS'],columnas=vars_numeric_orig) 
            charts_histograms(X_test__pd,"Numerical Variables - TEST:",d['DIR_OUTPATH_CHARTS'],columnas=vars_numeric_orig) 
    
        
        if d[function_name] and d['PRP_LABEL_ENCODE']:  #Histograma Features Categóricas
            charts_histograms(X_TRAIN__pd,"NON numerical Variables - TRAIN:",d['DIR_OUTPATH_CHARTS'],columnas=vars_non_numeric_orig) 
            charts_histograms(X_test__pd,"NON numerical Variables - TEST:",d['DIR_OUTPATH_CHARTS'],columnas=vars_non_numeric_orig) 
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
    

    
    if len(d['PRP_NA_FILL']) > 0:
        X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        #print_numeric_and_nonnumeric_variables(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1),X_test__pd,vars_numeric(X_TRAIN__pd),vars_non_numeric(X_TRAIN__pd),vars_numeric(X_test__pd),vars_non_numeric(X_test__pd))

        
        
    function_name = 'PRP_NORMALIZE_NUMERIC'
    try: 
        if len(d[function_name]) > 0:
            X_TRAIN__pd,X_test__pd = normalize_numeric(dXT,d,d[function_name][0],d[function_name][1], X_TRAIN__pd, X_test__pd, len(X_TRAIN__pd),function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
    
    
    if len(d['PRP_NA_FILL']) > 0:
        X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        #print_numeric_and_nonnumeric_variables(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1),X_test__pd,vars_numeric(X_TRAIN__pd),vars_non_numeric(X_TRAIN__pd),vars_numeric(X_test__pd),vars_non_numeric(X_test__pd))
    
        
    function_name = 'PRP_X_TRANSFORM'
    try:     
        if len(d[function_name]) > 0:
            X_TRAIN__pd,X_test__pd = x_transform(dXT,d,d[function_name][0],d[function_name][1], X_TRAIN__pd, X_test__pd, len(X_TRAIN__pd), function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     

    
    if len(d['PRP_NA_FILL']) > 0:
        X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
        #print_numeric_and_nonnumeric_variables(pd.concat([X_TRAIN__pd, y_TRAIN__pd], axis=1),X_test__pd,vars_numeric(X_TRAIN__pd),vars_non_numeric(X_TRAIN__pd),vars_numeric(X_test__pd),vars_non_numeric(X_test__pd))
    
    
        
    function_name = 'PRP_DROP_NEAR_ZERO_VARIANCE'
    try:    
        if d[function_name] != 'No':
            X_test__pd,X_TRAIN__pd = drop_near_zero_variance(dXT,d,d[function_name],X_test__pd,X_TRAIN__pd,d['PRINT_DROP_INFO'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
 
    function_name = 'PRP_DROP_CONSTANTS'
    try:  
        if d[function_name]:
            X_test__pd,X_TRAIN__pd = drop_constants(X_test__pd,X_TRAIN__pd,d['PRINT_DROP_INFO'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     

            
            
    function_name = 'PRP_DROP_FEATURES_ALL_NA_IN_TRAIN_OR_TEST'
    try:  
        if d[function_name]:
            cols_names,test__pd,TRAIN__pd = drop_features_all_na_in_train_or_test(dXT,d,TRAIN__pd,test__pd,"TRAIN__pd","test__pd",d['PRINT_DROP_INFO'],function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
   
           

    function_name = 'CHART_CORRELATIONS'
    try:  
        if d[function_name]:
            # Remove NAs
            if len(d['PRP_NA_FILL']) > 0:
                X_test__pd,X_TRAIN__pd = na_fill(dXT,d,d['PRP_NA_FILL'],X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    
            # Keep only Numeric and Class (labelencoded if necessary)
            Xy_TRAIN__pd = charts_corr_keep_numeric_and_class(d, X_TRAIN__pd, y_TRAIN__pd)
    
            # Get Correlations
            ###################          
            corr_features_names = list(Xy_TRAIN__pd.columns.values)
            corr = Xy_TRAIN__pd.corr(method='pearson')
            
            # Get all Pairs
            #################
            pares = corr.unstack()
            pares = pd.DataFrame(pares)
            pares.reset_index(inplace=True)
            pares.columns = ['FeatA', 'FeatB', 'Corr']
            pares['Corr_Abs'] = pares['Corr'].abs()
            pares = pares[pares.FeatA != pares.FeatB]    
            
            #  Arrange Alphabetically
            pares['Feat_A'] = pares[['FeatA','FeatB']].min(axis=1)
            pares['Feat_B'] = pares[['FeatA','FeatB']].max(axis=1)
            
            
    
            pares['HasClass']=0
            pares.HasClass.loc[(pares.Feat_A == d['COMP_COLUMN_CLASS']) | (pares.Feat_B == d['COMP_COLUMN_CLASS'])] = 1
            
            
            pares['Feat_Other'] = ''
            pares.Feat_Other.loc[(pares.Feat_A == d['COMP_COLUMN_CLASS'])] = pares.Feat_B.loc[(pares.Feat_A == d['COMP_COLUMN_CLASS'])]
            pares.Feat_Other.loc[(pares.Feat_B == d['COMP_COLUMN_CLASS'])] = pares.Feat_A.loc[(pares.Feat_B == d['COMP_COLUMN_CLASS'])]
            
    
            pares.drop(['FeatA','FeatB'], axis=1, inplace=True)
            
            pares.drop_duplicates(['Feat_A','Feat_B'], inplace=True)
    
            pares.sort_values(by=['Corr_Abs'], ascending=[0], inplace=True)        
            
            # Persist
            ###############
            file_name_this_exec = d['DIR_OUTPATH_CHARTS'] + 'correlations/' + d['COMP_NAME'] + '_Corr_' + str(d['ID']) + '.xlsx'
            file_name_general = d['DIR_OUTPATH_CHARTS'] + 'correlations/' + d['COMP_NAME'] + '_Correlations.xlsx'
    
            pd.DataFrame(pares).to_excel(file_name_this_exec,sheet_name='Correlations',columns=['Feat_A','Feat_B','Corr','Corr_Abs','HasClass','Feat_Other'],index=False)
            pd.DataFrame(pares).to_excel(file_name_general,sheet_name='Correlations',columns=['Feat_A','Feat_B','Corr','Corr_Abs','HasClass','Feat_Other'],index=False)
            
    
            
            # La siguiente persistencia comentada NO está funcionando
            #if d['SAVE_HDF5_CORR_MATRIX']:
            #    hdf5_save_Features_Correlation_Matrix(d,"Features_Correlation_Matrix",corr,corr_features_names)
    
            # Do 2 correlation charts
            multiplicador_size = int(math.sqrt(len(pares)))
            
            charts_correlations_colors(corr,d['DIR_OUTPATH_CHARTS'],multiplicador_size)
            
            if d['CHART_CORRELATIONS_LONG']:
                charts_correlations(Xy_TRAIN__pd,d['DIR_OUTPATH_CHARTS'],d['COMP_COLUMN_CLASS'])
    
            Xy_TRAIN__pd = None
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
     
   


    function_name = 'PRP_DROP_THESE_COLUMNS_b4_ML'
    try:  
        if len(d[function_name])>0:
            cols_names,X_test__pd, X_TRAIN__pd = drop_these_columns(dXT,d, d[function_name],0,X_test__pd, X_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'],d['PRINT_DROP_INFO'],"Motivo: Indicado en Dashboard")
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    
    
    function_name = 'PRP_PCA'
    try:  
        if d[function_name].lower()=='yes':
            cols_names,X_test__pd, X_TRAIN__pd = do_PCA(dXT,d, d['PRP_PCA_COLUMNS'],0,X_test__pd, X_TRAIN__pd,function_name,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'],d['PRINT_DROP_INFO'],"Motivo: Indicado en Dashboard")
    except Exception as e: print("\n" + str("-"*70) + "\nParameters in Dashboard for " + Fore.RED + str(function_name) + Style.RESET_ALL + " returned an ERROR:" + Fore.RED +  "\n\t" + str(e) + "\n" + Style.RESET_ALL + str("-"*70) + "\n"); dXT["PRP_CALL_ERRORS"][function_name] = e
    
    

    # At this point we have to make sure there are NO NAs at all.
    X_test__pd,X_TRAIN__pd = na_fill(dXT,d,([['y_num'],-1],[['y_str_obj'],'xxx']),X_test__pd,X_TRAIN__pd,d['PRINT_NA_INFO'],d['PRINT_NA_INFO_SHOW_FIELDS'])
    

        
    return (idx_TRAIN__pd, X_TRAIN__pd, y_TRAIN__pd, idx_test__pd, X_test__pd, dXT)
    # End function import_and_preprocessing()
