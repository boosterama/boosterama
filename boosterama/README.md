
# BOOSTERAMA - README #

Boosterama is a powerful full cycle Machine Learning python library for business purposes or winning kaggle competitions.

Boosterama is opensourced under the Apache 2.0 license.

Writen in Python 3.


### Boosterama handles all phases of the ML cycle: ###

* Import
* Preprocessing
* CV
* Modeling using the best algorithms (Xgb, Keras,...)
* Submission
* Ensembling (Stacking, blending,...)


# Installation #

### Install python and Tkinker: ###

Boosterama uses Python 3.5+  (perhaps 3.x) and has dependencies on Tkinter:

	sudo apt-get install python3-tk

### Configure cache ###

Set PYTHONHASHSEED in your environment variables.

Ubuntu users can do it on this way:

	echo >>~/.profile
	echo export PYTHONHASHSEED=1234 >>~/.profile
	tail ~/.profile

This step is needed in order to have control of your preprocessed datasets and cache.
This seed has nothing to do with ML argorithms seeds, which is configurable on your dashboard.
Setting PYTHONHASHSEED ensures that your cache is going to be reusable between python sessions /  consoles. 

### python package dependencies ###

	pip3 install numpy pandas colorama scipy matplotlib shove h5py xlsxwriter scikit-learn seaborn xgboost 
	pip3 install shove

After installing, you should close and restart your IDE.  Otherwise you’ll get an “Key error” ( http://stackoverflow.com/a/34576964/612837 )


### How do I get started? ###

* Soon to come
* For now, you can just launch my_Dsh_IRIS_reg.py which is a dashboard for the Iris dataset (with Train & Test) included in the sample_datasets.zip.  
* Take all data from the competition and place it on the 'input' folder you should create inside your boosterama folder.  


### Version ###

* 0.3.53

### Current Contributors ###

* Fernando Constantino Aguilera ([linkedin](https://es.linkedin.com/in/fernandoconstantino), [kaggle](https://www.kaggle.com/fernandoconstantino))
* Javier Tejedor Aguilera ([linkedin](https://es.linkedin.com/in/javier-tejedor-aguilera), [kaggle](https://www.kaggle.com/javiertag))
* Virilo Tejedor Aguilera ([linkedin](https://es.linkedin.com/in/virilo-tejedor-aguilera-7aa19753), [kaggle](https://www.kaggle.com/virilo))

### Questions? Do you want to Contribute? ###

* info | at | boosterama | dot | io