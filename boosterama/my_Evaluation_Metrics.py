# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 18:50:53 2015

@author: DataCousins
"""


############################################
### IMPORTS - Respetar ORDEN ALFABÉTICO  ###
############################################

from colorama import Fore, Back, Style
import numpy as np
import scipy as sp
from sklearn import metrics
import sys


#######################################################
#      Definiciones matemáticas de las métricas       #
#######################################################   
 
 

 
# https://www.kaggle.com/wiki/Metrics       
        
def Gini(y_true, y_predicted):
# Simple implementation of the (normalized) gini score in numpy
# Fully vectorized, no python loops, zips, etc.
# Significantly (>30x) faster than previous implementions

    # check and get number of samples
    assert y_true.shape == y_predicted.shape
    n_samples = y_true.shape[0]
    
    # sort rows on prediction column 
    # (from largest to smallest)
    arr = np.array([y_true, y_predicted]).transpose()
    true_order = arr[arr[:,0].argsort()][::-1,0]
    pred_order = arr[arr[:,1].argsort()][::-1,0]
    
    # get Lorenz curves
    L_true = np.cumsum(true_order) / np.sum(true_order)
    L_pred = np.cumsum(pred_order) / np.sum(pred_order)
    L_ones = np.linspace(1/n_samples, 1, n_samples)
    
    # get Gini coefficients (area between curves)
    G_true = np.sum(L_ones - L_true)
    G_pred = np.sum(L_ones - L_pred)
    
    # normalize to true Gini coefficient
    return G_pred/G_true


def LOGLOSS(y_true, y_predicted):
    epsilon = 1e-15
    y_predicted = sp.maximum(epsilon, y_predicted)
    y_predicted = sp.minimum(1-epsilon, y_predicted)
    ll = sum(y_true*sp.log(y_predicted) + sp.subtract(1,y_true)*sp.log(sp.subtract(1,y_predicted)))
    ll = ll * -1.0/len(y_true)
    return ll


def MAE(y_true, y_predicted):
    return np.mean(np.abs(y_true - y_predicted))


def quadratic_weighted_kappa(rater_a, rater_b, min_rating=None, max_rating=None):

    def confusion_matrix(rater_a, rater_b, min_rating=None, max_rating=None):
        """
        Returns the confusion matrix between rater's ratings
        """
        assert(len(rater_a) == len(rater_b))
        if min_rating is None:
            min_rating = min(rater_a + rater_b)
        if max_rating is None:
            max_rating = max(rater_a + rater_b)
        num_ratings = int(max_rating - min_rating + 1)
        conf_mat = [[0 for i in range(num_ratings)]
                    for j in range(num_ratings)]
        for a, b in zip(rater_a, rater_b):
            conf_mat[a - min_rating][b - min_rating] += 1
        return conf_mat

    def histogram(ratings, min_rating=None, max_rating=None):
        """
        Returns the counts of each type of rating that a rater made
        """
        if min_rating is None:
            min_rating = min(ratings)
        if max_rating is None:
            max_rating = max(ratings)
        num_ratings = int(max_rating - min_rating + 1)
        hist_ratings = [0 for x in range(num_ratings)]
        for r in ratings:
            hist_ratings[r - min_rating] += 1
        return hist_ratings
    
    
    """
    Calculates the quadratic weighted kappa
    quadratic_weighted_kappa calculates the quadratic weighted kappa
    value, which is a measure of inter-rater agreement between two raters
    that provide discrete numeric ratings.  Potential values range from -1
    (representing complete disagreement) to 1 (representing complete
    agreement).  A kappa value of 0 is expected if all agreement is due to
    chance.
    quadratic_weighted_kappa(rater_a, rater_b), where rater_a and rater_b
    each correspond to a list of integer ratings.  These lists must have the
    same length.
    The ratings should be integers, and it is assumed that they contain
    the complete range of possible ratings.
    quadratic_weighted_kappa(X, min_rating, max_rating), where min_rating
    is the minimum possible rating, and max_rating is the maximum possible
    rating
    """
    rater_a = np.array(rater_a, dtype=int)
    rater_b = np.array(rater_b, dtype=int)
    assert(len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(min(rater_a), min(rater_b))
    if max_rating is None:
        max_rating = max(max(rater_a), max(rater_b))
    conf_mat = confusion_matrix(rater_a, rater_b,
                                min_rating, max_rating)
    num_ratings = len(conf_mat)
    num_scored_items = float(len(rater_a))

    hist_rater_a = histogram(rater_a, min_rating, max_rating)
    hist_rater_b = histogram(rater_b, min_rating, max_rating)

    numerator = 0.0
    denominator = 0.0

    for i in range(num_ratings):
        for j in range(num_ratings):
            expected_count = (hist_rater_a[i] * hist_rater_b[j]
                              / num_scored_items)
            d = pow(i - j, 2.0) / pow(num_ratings - 1, 2.0)
            numerator += d * conf_mat[i][j] / num_scored_items
            denominator += d * expected_count / num_scored_items

    return 1.0 - numerator / denominator


def quadratic_weighted_kappa_xgb(rater_a, rater_b, min_rating=None, max_rating=None):
    
    if type(rater_b).__name__ == 'DMatrix':
        rater_b=rater_b.get_label()

    def confusion_matrix(rater_a, rater_b, min_rating=None, max_rating=None):
        """
        Returns the confusion matrix between rater's ratings
        """
        assert(len(rater_a) == len(rater_b))
        if min_rating is None:
            min_rating = min(rater_a + rater_b)
        if max_rating is None:
            max_rating = max(rater_a + rater_b)
        num_ratings = int(max_rating - min_rating + 1)
        conf_mat = [[0 for i in range(num_ratings)]
                    for j in range(num_ratings)]
        for a, b in zip(rater_a, rater_b):
            conf_mat[a - min_rating][b - min_rating] += 1
        return conf_mat

    def histogram(ratings, min_rating=None, max_rating=None):
        """
        Returns the counts of each type of rating that a rater made
        """
        if min_rating is None:
            min_rating = min(ratings)
        if max_rating is None:
            max_rating = max(ratings)
        num_ratings = int(max_rating - min_rating + 1)
        hist_ratings = [0 for x in range(num_ratings)]
        for r in ratings:
            hist_ratings[r - min_rating] += 1
        return hist_ratings
    
    
    """
    Calculates the quadratic weighted kappa
    quadratic_weighted_kappa calculates the quadratic weighted kappa
    value, which is a measure of inter-rater agreement between two raters
    that provide discrete numeric ratings.  Potential values range from -1
    (representing complete disagreement) to 1 (representing complete
    agreement).  A kappa value of 0 is expected if all agreement is due to
    chance.
    quadratic_weighted_kappa(rater_a, rater_b), where rater_a and rater_b
    each correspond to a list of integer ratings.  These lists must have the
    same length.
    The ratings should be integers, and it is assumed that they contain
    the complete range of possible ratings.
    quadratic_weighted_kappa(X, min_rating, max_rating), where min_rating
    is the minimum possible rating, and max_rating is the maximum possible
    rating
    """
    rater_a = np.array(rater_a, dtype=int)
    rater_b = np.array(rater_b, dtype=int)
    assert(len(rater_a) == len(rater_b))
    if min_rating is None:
        min_rating = min(min(rater_a), min(rater_b))
    if max_rating is None:
        max_rating = max(max(rater_a), max(rater_b))
    conf_mat = confusion_matrix(rater_a, rater_b,
                                min_rating, max_rating)
    num_ratings = len(conf_mat)
    num_scored_items = float(len(rater_a))

    hist_rater_a = histogram(rater_a, min_rating, max_rating)
    hist_rater_b = histogram(rater_b, min_rating, max_rating)

    numerator = 0.0
    denominator = 0.0

    for i in range(num_ratings):
        for j in range(num_ratings):
            expected_count = (hist_rater_a[i] * hist_rater_b[j]
                              / num_scored_items)
            d = pow(i - j, 2.0) / pow(num_ratings - 1, 2.0)
            numerator += d * conf_mat[i][j] / num_scored_items
            denominator += d * expected_count / num_scored_items

    return 'error',1.0 - numerator / denominator


def R2(y_true, y_predicted):
    return metrics.r2_score(y_true, y_predicted)

# https://www.kaggle.com/wiki/RootMeanSquaredError
def RMSE(y_true, y_predicted):
    return np.sqrt(np.mean(np.power( y_true - y_predicted , 2)))


def RMSPE(y_true, y_predicted):    
    return np.sqrt(np.mean(np.power( (y_true - y_predicted)/y_true , 2)))
    

# https://www.kaggle.com/wiki/RootMeanSquaredLogarithmicError

def RMSLE(y_true, y_predicted):
    rmsle=np.sqrt(np.mean(np.power( np.log(y_predicted+1)-np.log(y_true+1) , 2)))
    return rmsle

def SAFE_RMSLE(y, y_predicted):
    if np.any(y_predicted<0):
        print("safe_rmsle received negative predictions.  clipping")
        y_predicted=np.max(y_predicted,0)
        
    rmsle=np.sqrt(
        np.mean((np.log1p(y) - np.log1p(y_predicted)) ** 2)
    )
    if np.isnan(rmsle):
        print("hala!")
    return rmsle


def ROC_AUC(y_true, y_predicted):
    return metrics.roc_auc_score(y_true, y_predicted)
    


#######################################################
#                 Función my_score                    #
#######################################################

def my_score(y_true, y_predicted,EVALUATION_METRIC,COMPETITION_NAME,score_name,name_y_true,name_y_predicted,doPrint,ML_APPLY_RESTRICTIONS=None):
        
        
    #######################################################
    #   Reshape de y_true e y_predicted                   #
    #######################################################
    '''
    print()
    print('y_true',y_true)
    print('y_true type',type(y_true))
    print('y_true shape',y_true.shape)
    print('y_predicted',y_predicted)
    print('y_predicted type',type(y_predicted))
    print('y_predicted shape',y_predicted.shape)
    print()
    '''
    y_true = y_true.reshape(y_true.shape[0],)
    y_predicted = y_predicted.reshape(y_predicted.shape[0],)
    
    
    #######################################################
    # Caso particular en el que my_score se ejecuta en    # 
    # CrossValidation de GridSearchCV o XGB:              #
    #######################################################
    
    # En este caso, aplicamos restricciones, si las hay, a y_predicted 
    if score_name == "Score TrainCV" and len(ML_APPLY_RESTRICTIONS) > 0:
        y_predicted, rarp_y_predicted = apply_restrictions(y_predicted,ML_APPLY_RESTRICTIONS,"y_trainCV_transf_preds_Fold_N")

    # En este caso, aplicamos restricciones, si las hay, a y_predicted 
    elif score_name == "Score Train XGB" and len(ML_APPLY_RESTRICTIONS) > 0:
        y_predicted, rarp_y_predicted = apply_restrictions(y_predicted,ML_APPLY_RESTRICTIONS,"y_train_transf_preds_XGB")


    #######################################################
    #    Print information about y_true and y_predicted   #
    #######################################################

    if doPrint and ('int' in str(y_true.dtype) or 'float' in str(y_true.dtype)) and ('int' in str(y_predicted.dtype) or 'float' in str(y_predicted.dtype)):
        print_score_info(y_true, y_predicted,score_name,name_y_true,name_y_predicted)


    #######################################################
    #   Procesado previo especifico de cada competicion   # 
    #######################################################

    if COMPETITION_NAME == 'rossmann':
        y_predicted = y_predicted[y_true!=0]
        y_true = y_true[y_true!=0]


    ######################################################
    #   Metrica a aplicar                                #
    ######################################################
        
        
    if EVALUATION_METRIC == 'Acc':
        return metrics.accuracy_score(y_true, y_predicted)
        
    elif EVALUATION_METRIC == 'Gini':
        return Gini(y_true, y_predicted)
    
    elif EVALUATION_METRIC == 'LogLoss':
        return LOGLOSS(y_true, y_predicted)

    elif EVALUATION_METRIC == 'MAE':
        return MAE(y_true, y_predicted)

    elif EVALUATION_METRIC == 'R2':
        return R2(y_true, y_predicted)

    elif EVALUATION_METRIC == 'RMSE':
        return RMSE(y_true, y_predicted)
        
    elif EVALUATION_METRIC == 'RMSLE':
        return RMSLE(y_true, y_predicted)
        
    elif EVALUATION_METRIC == 'RMSPE':
        return RMSPE(y_true, y_predicted)
                
    elif EVALUATION_METRIC == 'ROC_AUC':
        return ROC_AUC(y_true, y_predicted)

    elif EVALUATION_METRIC == 'QWKappa':
        return quadratic_weighted_kappa(y_true, y_predicted)
    else:
        print()
        print(Fore.RED + "--------------------\n EVAL METRIC ERROR:" + Style.RESET_ALL)
        print()
        print()
        print(Fore.RED + "Evaluation metric '" + EVALUATION_METRIC + "' is currently NOT supported: \n\tEdit my_score function on my_Evaluation_Metrics.py to add it." + Style.RESET_ALL)
        print()
        
        print(Fore.RED + "----------------------------------------------------------------------" + Style.RESET_ALL)
        print()
        sys.exit() 
    
        






def print_score_info(y_true, y_predicted,score_name,name_y_true,name_y_predicted):  
    print('')
    print(score_name)
    print(' ',name_y_true,'        values:',len(y_true)," neg:",np.sum(y_true<0)," zero:",np.sum(y_true==0)," pos:",np.sum(y_true>0))
    print(' ',name_y_predicted,'  values:',len(y_predicted)," neg:",np.sum(y_predicted<0)," zero:",np.sum(y_predicted==0)," pos:",np.sum(y_predicted>0))
    print('')


def apply_restrictions(y_preds,ML_APPLY_RESTRICTIONS,name_element):
    
    for RESTRICTION in ML_APPLY_RESTRICTIONS:

        if RESTRICTION.lower()=='negativetozero':
            restriction_afected_rows = np.sum(y_preds<0)
            restriction_afected_rows_percentage = np.round(100*np.sum(y_preds<0)/len(y_preds),decimals=4)
            y_preds[y_preds<0] = 0.0
    
        if RESTRICTION.lower()=='round':
            restriction_afected_rows = len(y_preds)
            restriction_afected_rows_percentage = 100
            y_preds = np.round(y_preds)
                
    #print("Restriction  " + str(ML_APPLY_RESTRICTIONS) + " has been applied to " + str(restriction_afected_rows) + " rows, " + str(restriction_afected_rows_percentage) + "% of " + name_element + " rows")
    
    return y_preds, restriction_afected_rows_percentage
    
    
