# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 12:02:47 2016

@author: DataCousins
"""

import glob
import numpy as np
import pandas as pd
import os
import shutil
import subprocess

from colorama import Fore, Back, Style
from my_Input_Output import hdf5_dataset_name
from my_ExternalScripts_fx import *
from my_Flow_Control import *

'''
Wrapper to use an external execution like a ml model
'''
class ExternalScriptExecutor:

    def __init__(self, d, results, fold):
        (self.idx_test, self.y_test_preds, self.N_test, self.idx_val, self.y_val, self.y_val_preds, self.N_val, self.idx_dtype, self.y_dtype) = external_script_execute(d, results, fold)
        self.dashboard, self.results, self.fold = d, results, fold
    
    def execution_results(self):
        return [], [], []
    
    def predict_proba(self, X):
        return self.predict(X)
        
    def predict(self, X):
        predictions=None
        print("self.y_val_preds", len(self.y_val_preds))
        print("self.y_val: ", len(self.y_val))
        print("self.y_test_preds: ", len(self.y_test_preds))
        
        
        
        print("1234 1234")
        print("1234 1234")
        print("1234 1234")
        
        if len(X)==len(self.y_val_preds):
            print("ExternalScriptExecutor, preguntan por Y_preds validacion")
            predictions=self.y_val_preds
        elif len(X)==len(self.y_test_preds):
            print("ExternalScriptExecutor, preguntan por Y_preds test set")
            predictions=self.y_test_preds
        else:
            print("ExternalScriptExecutor, preguntan por Y_preds train set: devuelvo CEROS")
            predictions = np.zeros(len(X))
        return predictions
        

def get_files(path):
    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            yield file


def external_script_execute(d, results, fold): 

    id_execution = d['ID']       
    
    inPath = os.path.normpath(d['DIR_BASE']+'/'+'external_submissions')
    # Fichero esperado
    if d['EXTERNAL_SCRIPTS_IMPORT'][-3:] == '.py':
        file_name_expected = d['EXTERNAL_SCRIPTS_IMPORT'][:-3]+'.csv'
    elif d['EXTERNAL_SCRIPTS_IMPORT'][-2:] == '.R':
        file_name_expected = d['EXTERNAL_SCRIPTS_IMPORT'][:-2]+'.csv'
    else:
        print('Error: fichero no soportado. Solo preparado para ejecutar external_scripts R o Python')       
    file_name_expected_path = inPath+os.path.normpath("/") + file_name_expected
    
    print("A la salida del script, esperamos haya creado: " + file_name_expected_path)
        
    # Configuramos directorios entrada según si hay o no custom_import
    if d['SYS_DO_CUSTOM_IMPORT']==1:
        input_path = d['DIR_BASE']+'/input_custom/'
    else:
        input_path = d['DIR_BASE']+'/input/'
    output_path = d['DIR_BASE']+'/external_scripts/input/'
     
     
    # Diferenciamos según sea una ejecución Padre o Hijo        
    if d['ID_FAMILY']==0:
        print("External Script: Ejecucion padre")
        
        # Configuramos numero de runs y nombre del y_dataset_preds en cada run
        tipo_ejecucion = "padre"       
        num_runs = 1
        y_dataset_preds_name = ['y_test_preds_padre']
    else:
        print("External Script: Ejecucion hijo")
        
        # Configuramos numero de runs y nombre del y_dataset_preds en cada run
        tipo_ejecucion = "hijo"
        num_runs = 2
        y_dataset_preds_name = ['y_test_preds_hijo', 'y_val_preds_hijo']
        campos_comunes, campos_train_no_test, campos_test_no_train, campos_train_ordenados, campos_test_ordenados = analisis_train_test(d,input_path)
    
 
    for run in range(num_runs):
        
        print("External Script: Run ", run)
        
        # Colocamos en /external_scripts/input los ficheros adecuados
        if tipo_ejecucion == "padre":
            files = get_files(input_path)
            for file in files:
                shutil.copy2(input_path + file, output_path)
            

        elif tipo_ejecucion == "hijo":

            # Inicialmente copiamos todos los ficheros, como si fuera ejec. padre
            # Después machacaremos los necesarios
            files = get_files(input_path)
            for file in files:
                shutil.copy2(input_path + file, output_path)
                
                
            # Leemos indices idx_train_so y idx_val_so
            idx_train_name = hdf5_dataset_name(d,"idx_train",fold,"ExternalScriptsSubseting")+'.csv'
            idx_val_name = hdf5_dataset_name(d,"idx_val",fold,"ExternalScriptsSubseting")+'.csv'
            
            idx_train_so = pd.read_csv(d['DIR_EXTERNAL_SCRIPTS']+idx_train_name,header=None)
            idx_train_so = list(idx_train_so[0])
            
            idx_val_so = pd.read_csv(d['DIR_EXTERNAL_SCRIPTS']+idx_val_name,header=None)
            idx_val_so = list(idx_val_so[0])
            
            idx_TRAIN_so = sorted(idx_train_so + idx_val_so)
            
            
            # Generamos Train.csv (con Train pequeño) en External_Scripts/input
          
            y_train = csv_subsetting_train(input_path + d['COMP_TRAIN_FILES'][0], output_path + d['COMP_TRAIN_FILES'][0], d['COMP_COLUMN_ID'], idx_TRAIN_so, idx_train_so, d['COMP_COLUMN_CLASS'])
                       
            if run == 0:   
                # Copiamos Test.csv (Test) a External_Scripts/input
                shutil.copy2(input_path + d['COMP_TEST_FILES'][0], output_path + d['COMP_TEST_FILES'][0])
                          
            elif run == 1:
                # Generamos Test.csv (con Val) en External_Scripts/input
                y_val = csv_subsetting_val_as_test(input_path + d['COMP_TRAIN_FILES'][0], output_path + d['COMP_TEST_FILES'][0], d['COMP_COLUMN_ID'], idx_TRAIN_so, idx_val_so, d['COMP_COLUMN_CLASS'], campos_train_no_test, campos_test_no_train, campos_test_ordenados)
   
            else: sys.exit("Error en ExternalScripts colocacion de ficheros en input")
            
        else: sys.exit("Error en ExternalScripts colocacion de ficheros en input")
        
        
        # Tipo de datos de idx e y. Nos servirán para el almacenamiento HDF5
        idx_dtype = np.int64
        if d['COMP_PROBLEM_TYPE'] == 'Regr' or d['COMP_PROBLEM_TYPE'] == 'Class_B':
            y_dtype = np.float64
            if run == 1:
                y_val = y_val.astype(float)
        else:
            y_dtype = pd.read_csv(input_path+d['COMP_TRAIN_FILES'][0],header=0,usecols=np.array([d['COMP_COLUMN_CLASS']])).dtypes[0]           

        
        ########################################################################################
        ########################################################################################
        #
        # Ejecutamos el script de external_scripts/src
        #
        ########################################################################################
        ########################################################################################
        
        wd_original = os.getcwd()
        os.chdir(d['DIR_BASE']+'/external_scripts/src')
            
        print (Fore.BLACK + Back.GREEN + Style.DIM+ "Starting a new python process (say bye to Spyder debug!)" +  Style.RESET_ALL)

        print('checkingggg:',d['EXTERNAL_SCRIPTS_IMPORT'][-3:])

        if d['EXTERNAL_SCRIPTS_IMPORT'][-3:] == '.py':
            cmd = ["python", d['EXTERNAL_SCRIPTS_IMPORT']]
            
        elif d['EXTERNAL_SCRIPTS_IMPORT'][-2:] == '.R':
            cmd = ["/usr/bin/R", "-f", d['EXTERNAL_SCRIPTS_IMPORT']]
            
        else:
            sys.exit('Error: fichero EXTERNAL_SCRIPTS_IMPORT no esperado')
            
        
        
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        pid = str(p.pid)
        print (Fore.BLACK + Back.GREEN + Style.DIM+ "isolated process " + pid + Style.RESET_ALL)
        sys.stdout.flush()
        
        subprocess_in_execution= True        
        while subprocess_in_execution:
            line = p.stdout.readline()
            if line:
                print (Fore.BLACK + Back.GREEN + Style.DIM+ "isolated process " + pid + ": " +  Style.RESET_ALL + str(line))
            else:
                subprocess_in_execution=False            

        os.chdir(wd_original)
        
        ########################################################################################
        ########################################################################################

        
        # Leemos los ficheros que tenemos en el directorio external_submissions
        
        filesPaths=sorted(glob.glob(inPath+os.path.normpath("/")+'*.csv'))
        
        
        
        
        

        
        # Si está el fichero esperado, lo renombramos según nomenclatura acordada
        if file_name_expected_path in filesPaths:
            print("fichero encontrado",file_name_expected)

            file_name = y_dataset_preds_name[run] + '_' + file_name_expected
            file_name_path = inPath + os.path.normpath("/") + file_name
            
            if os.path.exists(file_name_path):
                os.remove(file_name_path)
                
            os.rename(file_name_expected_path,file_name_path)
        else:
            print()
            print()
            print('Error en ejecucion external script  ', d['EXTERNAL_SCRIPTS_IMPORT'])
            print()
            print('file_name_expected_path',file_name_expected_path)
            print()
            print('filesPaths',filesPaths)
            print()
            print()
            end_this_execution(id_execution, d, results, 'script other execution error')
            return
            
        
           
           
        # Extraemos información del nombre del fichero
        file_name_info = file_name.split("_") 

        results['so_alg']=file_name_info[5]
        results['so_titulo']=file_name_info[6]
        results['so_idScript']=file_name_info[7]
        results['so_autor']=file_name_info[8].replace('.csv','')
        results['so_puntuacion']=file_name_info[4]

        
              
        # Importamos el fichero
        idx_dataset_preds, y_dataset_preds = import_external_submissions(file_name_path,d['COMP_COLUMN_ID'],d['COMP_COLUMN_CLASS'])

        
        if d['ID_FAMILY']==0:
            idx_test = idx_dataset_preds.copy()
            y_test_preds = y_dataset_preds.copy()
            N_test = len(y_test_preds)
                         
        else:
            if run == 0:
                idx_test = idx_dataset_preds.copy()
                y_test_preds = y_dataset_preds.copy()
                N_test = len(y_test_preds)
            else:
                idx_val = idx_dataset_preds.copy()
                y_val_preds = y_dataset_preds.copy()
                N_val = len(y_val_preds)
            

    if d['ID_FAMILY']==0:
        return(idx_test, y_test_preds, N_test, idx_dtype, y_dtype)                           
    else:        
        return(idx_test, y_test_preds, N_test, idx_val, y_val, y_val_preds, N_val, idx_dtype, y_dtype)