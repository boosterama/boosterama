# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 21:16:37 2017

@author: virilo.tejedor
"""

import os 

from framework.metaclasses import singleton

from configparser import ConfigParser
import chardet
from pathlib import Path
from types import SimpleNamespace

def get_config_file_content(file_path):
    HEADER="[DEFAULT]"
    rawdata = open(file_path, 'rb').read()
    result = chardet.detect(rawdata)
    encoding = result['encoding']
    content = Path(file_path).read_text(encoding=encoding)
    if not HEADER in content:
        content = HEADER + "\n" + content
    return (content)

@singleton
class ConfigurationLoader(): 
    def __init__(self):
        DEFAULT_CONFIGURATION_FILE="default_config.ini"
        USER_CONFIGURATION_FILE="boosterama.ini"
        
        
        print("Loading configuration from ", DEFAULT_CONFIGURATION_FILE +" + custom " +USER_CONFIGURATION_FILE if os.path.isfile(USER_CONFIGURATION_FILE) else DEFAULT_CONFIGURATION_FILE)
        default_config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), DEFAULT_CONFIGURATION_FILE)
        if not os.path.isfile(default_config_path):
            raise RuntimeError("BAD CONFIGURATION - '" + str(default_config_path) + "' not found - ¿pip package error? See 'bonus' at https://stackoverflow.com/a/35563606")
        
        config_parser = ConfigParser()
        config_parser.read_string(get_config_file_content(default_config_path))
        
        if os.path.isfile(USER_CONFIGURATION_FILE):
            config_parser.read_string(get_config_file_content(USER_CONFIGURATION_FILE))
        
        container = SimpleNamespace()
        for k in config_parser['DEFAULT']:
            setattr(container, k, config_parser['DEFAULT'][k])      
        self.config=container
    
    def get_config(self):
        return self.config

boosterama_config = ConfigurationLoader().get_config()