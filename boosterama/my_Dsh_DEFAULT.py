# -*- coding: utf-8 -*-
"""
Created on Fri Jul 17 12:54:33 2015

@author: DataCousins
"""

import os

default_dashboard={

######################
#  META
######################

'FRIENDLY_ID':'noname', # Name here your dashboard
#                         It's an human-friendly unique identifier alternative to ID_EXECUTION (internal id used by boosterama)
#                         Boosterama ensures that this human-friendly name is unique
#                         If there already exists another dashboard on your executions database with the same FRIENDLY_ID, a secuential number is added at the end

'HASHTAGS':['starter'], # hashtags to filter your executions.  Useful for second level models

'AUTHOR':'x', # your name / nickname ;-)

######################
#  MACHINE LEARNING
######################
'LEVEL' : '1_ML',         #  {string} ('1_ML', '1_EXTERNAL_SCRIPTS', '1_EXTERNAL_PREDICTIONS', '2_ML', '2_CARUANA', '2_ENSEMBLE', '3_CARUANA', '3_BASIC_ENSEMBLE')
'ML_GRIDSEARCHCV_OR_ALGORITHM' : 'Algorithm',     # {string} ('GridSearchCV','Algorithm')
 
'ITERATOR' : 'fastloop', # 'grid', 'fastloop'

'ML_ALGO' : ["GBR"], # NO ITERABLE {list of strings} Elegir Algoritmos (["LR"],["XGB"],["GBR"],["RFR"],["ELN"],["SGDR"],["RIDG"])
# CLASSIFICATION: Ada=Adaboost  DT=DecissionTree   RF=RandomForest  SGD=Stochastic Gradient Descent  SVM=Support Vector Machine    
# REGRESSION:     AdaR=AdaBoostRegressor  ELN=ElasticNet  GBR=GradientBoostingRegressor  LR=LinearRegression   RFR=RandomForestRegressor  RIDG=Ridge  SGDR=SGDRegressor

'RANDOM_SEED' : 1000,

'META_TEST':False, #Internal.  To identify a test execution

'ML_CATEGORICAL_FEATURES': 'auto', # accepts: list of feature names, LightGBM accepts 'auto'.  future, accept: expression "y_str"
'ML_CATEGORICAL_FEATURES_THRESHOLD': 150, # max distinct values in column to be considered as categorical

################################################################################################
# CrossValidation
###################
'SPLIT_CV_TYPE'              : 'Random', #'Random','Stratified', 'TimeSeries', 'Index'

'SPLIT_CV_KFOLDS'            :  1, # 0  ===>  Train = 100%, Val = 0% \\\  1  ===> ejecución normal con Val \\\   >= 2  ===> CrossValidation
'SPLIT_CV_KFOLDS_EARLY_STOP' :  0,  # {int}  0  ===>  Does all folds normally \\\  1  ===> stops on first fold if score is not the best. otherwise continues with rest of folds.

'SPLIT_VALIDATION_SET_PERC'  :  0.3,      #  {float16} (between 0.00 and 1.00)  
# Only when ('SPLIT_CV_KFOLDS' = 1) AND ('SPLIT_CV_TYPE' = 'Random' or 'Stratified')
'SPLIT_IMPORT_INDEXES'       : 0, # id to export the folds split. i.e.:'my_split' 



# CrossValidation for TimeSeries    
# # # # # # # # # # # # # # # # #    (section ignored unless 'SPLIT_CV_TYPE' = 'TimeSeries') 
'SPLIT_TS_TYPE'               : 'TS_CV_TRAIN-ORIGIN_VAL-FIX',  #  {string}   'TS_CV_ACADEMIC', 'TS_CV_TRAIN-ORIGIN_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX', 'TS_CV_TRAIN-FIX_VAL-FIX_NO-OVERLAP'
# Num_folds taken from    'SPLIT_CV_KFOLDS'  value
'SPLIT_TS_train_FIX_NUM'      : 0,                 #  {int} number of observations (starting from last one) of the train set
'SPLIT_TS_VALIDATION_FIX_NUM' : 0,                 #  {int} number of observations (starting from last one) of the validation set
# 'SPLIT_VALIDATION_SET_PERC' is ignored => we use 'SPLIT_TS_VALIDATION_FIX_NUM'


################################################################################################
#Transformaciones X
#####################
'PRP_DATE_ENCODE'             : (),    # [ITER]  [([list],[list],int,int,int)]  ([()]=Don't do,['year','month','yearmonth','dayofweek','dayofyear','day','weekofyear','quarter','hour','isweekend','owen_Day_-1','owen_Month_-1', 'owen_dayofweek', 'isofficehours','daysfromfirst']) datetime64 features converted to these Integers
        #([date_cols],['year','month'], do_Owen, DROP_New_after)    # Siempre borramos la variable original
'PRP_OWEN_2_DATE_2_FEATURE'  : (),    #[([list],str,[list of lists])],   # ([()]=Don't do)  ejemplo1:  [(['Car_Type'],'Date_',[[-1,-1],[-2,-2],[-7,-7],[-1,-7]])],  ejemplo2:  [(['y_str_orig'],'Date_',[[-7,-7],[-1,-7]])],  Both dates included. The order for period end/start does NOT matter.
                   # [([scope], 'date column', [[start_period, end_period]])]

'PRP_BOOLEAN_ENCODE'      : 1,        # {uint8} (0=No, 1=Yes)  Convierte booleans a integers. Importante. Si NO se selecciona, algunas competiciones petarán.

'PRP_FLOAT_ENCODE'        : (),      # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)] convierte a categórica floats y luego hace Owen y Dropea Orig/Encoded
'PRP_INTEGER_ENCODE'      : (),     # [ITER] [([list],int,int,int)]    [()]=Don't do  ejemplo: [(['y_int'],1,0,1)] convierte a categórica integers y luego hace Owen y Dropea Orig/Encoded)
          # ([scope], do_owen, DROP_orig_after, DROP_New_after)

'PRP_LABEL_ENCODE_LEX'    : (),          # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_Obj'],1,0,1)]  Convierte categóricas a integers usando lógica lexicográfica. Importante. Si NO se selecciona, el ML no funcionará si hay categoricas.  
       # ([scope], do_owen, DROP_orig_after, DROP_New_after)
'PRP_LABEL_ENCODE'        : 1,          # {uint8} (0=No, 1=Yes)  Convierte categóricas a integers. Importante. Si NO se selecciona, el ML no funcionará si hay categoricas.  

'PRP_FLOAT_QUANTILE'      : (),  # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)]   convierte a cuantiles y hace owen.  ejemplos: [['y_float_LMen17']]   Siempre empieza por y_  o por o_
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)

'PRP_MONOTONIC_INT'       : [],     # {[[string]]} ([[]]=Don't do)  convierte a counts los campos integers (mantiene features originales) ejemplos: [['y_num_LMen17']] [['y_obj_LMen17']]  [['y_str_LMas17_LMen50']]  [['o_obj_LMen17']]   Siempre empieza por y_  o por o_
'PRP_MONOTONIC_STR'       : [],     # {[[string]]} ([[]]=Don't do)  convierte a counts los campos strings (mantiene features originales) ejemplos: [['y_num_LMen17']] [['y_obj_LMen17']]  [['y_str_LMas17_LMen50']]  [['o_obj_LMen17']]   Siempre empieza por y_  o por o_

'PRP_NORMALIZE_NUMERIC'   : (),# [ITER] [(list,string)]  [()]=Don't do   ("Rescaling", "Standarizing", "Other")  Ejemplo:  [(['y_float'],'Standarizing')]
   # ([scope], method)
'PRP_X_TRANSFORM'         : (),      # [ITER] [(list,string)]  [()]=Don't do ('boxcox0.25', 'log','square','exp','sqrt')
    # ([scope], method)


'PRP_ONE_HOT_ENCODE'      : (),     # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_Obj'],1,0,1)] 
   # ([scope], do_owen, DROP_orig_after, DROP_New_after)
'PRP_ONE_HOT_ENCODE_THRESHOLD': 4,     # [ITER] [int]   []=Don't do  ejemplo: [4]  Minimum frequency of a value to apply OneHotEncode (below it, it will be grouped under the value 'other')
'PRP_ONE_HOT_ENCODE_DROP_ONE_DUMMY': 1, # 1 to drop extra column and avoid multicolinearity



'PRP_OWEN_TRICK'        : (),        # {[[string]]} ([[]]=Don't do)  ejemplos: [['y_num_LMen17']] [['y_obj_LMen17']]  [['y_str_LMas17_LMen50']]  [['o_obj_LMen17']]   Siempre empieza por y_  o por o_
'PRP_OWEN_TRICK_METRICS': ['default'],  #default metric: 'default','mean','mode','std','max','min','sum'.  
                                    # using 'default' as metric, boosterama uses:  - 'mode' for multiclass problems, also to aggregate objects          - 'mean' otherwise
'PRP_OWEN_TRANSFORM'    : 'No',  # 'No', 'log' to apply log after the agg/metric
'PRP_OWEN_RANDOM_RANGE' : 0.05,  #  random factor to multiply owen values on train to avoid overfitting. Range will be  VALUE * random(-RANDOM_RANGE , +RANDOM_RANGE)


'PRP_OWEN_2_FEATURE'    : (),   # ([()]=Don't do)  ejemplos:  [(['y_all'],['mean','std'],'Species')]
                    # [([scope], [metrics], 'variable_name_2_reference' )]



'PRP_REPLACER'             : (), #  [()]=Don't do,   Ejemplo: [[(['y_num'],1,111),(['y_str'],'Blue','MILL')]]
                  # [[([SCOPE], REPLACE_THIS, PUT_THIS)]]
'PRP_REPLACE_RARE_STRINGS' : (), # [ITER]   [(list, int, int, uint16)] [()]=Don't do,   Ejemplo: [(['y_string'],1,0,39)]   (0=No, 1 to 65535 = Threshold) con MENOS de threshold 
   # ([SCOPE], DO_OWEN, DROP_Orig_after, LMen Threshold)

   
'PRP_WINSORING'            : (),  # [ITER] [(list,float,float)] #   [()]=Don't do,     float ratios percentile from 0.0 to 1.0  ejemplo: [(['y_float_int'],0.03,0.97)]
                 #([scope], lower_limit, upper_limit)

'PRP_NA_BINARIZE'          : [],         #  {[[string]]} ([[]]=Don't do)  ejemplo: [['y_int_LMen17']] 
'PRP_NA_FILL'             : (),   # [ITER] {string} ([()]=Don't do, '0'= rellenar a '0', 'mean','median','mode' any integer value IN QUOTES: '0'  '-9999' ...)
               # [([['y_num'],-1],[['y_str_obj'],'aftfdgddgGgffdghfgd'])]


# NLP
'PRP_NLP_TFIDF_SVM'      : (),  # [ITER] [([list],int,int,int)]   [()]=Don't do  ejemplo: [(['y_float'],1,0,1)]   convierte a cuantiles y hace owen.  ejemplos: [['y_float_LMen17']]   Siempre empieza por y_  o por o_
               # ([scope], DROP_orig_after)


'PRP_PCA'                   : 'No',
'PRP_PCA_STANDARIZE'        : 'Yes', # Standarize columns before PCA
'PRP_PCA_N_COMPONENTS'      : '2',
'PRP_PCA_COLUMNS'           : 'all',  #accepts query_col
'PRP_PCA_RANDOM_SEED'       : 'dashboard', # int or 'dashboard' to use dashboard['RANDOM_SEED'] value
'PRP_PCA_SVD_SOLVER'        : 'auto',
'PRP_PCA_TOL'               : 0.0,
'PRP_PCA_ITERATED_POWER'    : 'auto',
'PRP_PCA_WHITEN'            : 'No',


# TEXT PREPROCESSING
####################

'PRP_TF_IDF'                : [], # [] Don't do.  List of tuples (text_column_name, params_dictionary).  params_dictionary could have 'prefix' for the new columns, and params for class TfidfVectorizer constructor
#                                   Example: [('question_text', {'max_features':10, 'token_pattern':"\w+", 'prefix':'q_'}), ...]


'PRP_REGEX_COUNT'           : [], # [] Don't do. List of dictionaries.  Each dict contains all the regex to apply to one colum.  Boosterama creates a new colum per field/regex
#                                   Example {   'field':'answer_text',
#                                     'regex_list':[
#                                         'youtube\\.com|youtu\\.be',
#                                         'thank|thanx'}],

#Transformaciones Y
#####################

'ML_PRP_Y_TRANSFORM'          : 'No',      # [ITER] {string} ('No'=Don't do,'log','square','exp','sqrt')
'ML_Y_POSPROCESSING_RANK'     : 'No',   # 'Yes' to rank predictions between [0,1] for both validation and test predictions. (try it with ROC_AUC and other ranking metrics).  It's executed just before ML_Y_POSPROCESSING functions. rank is performed only in 'y_preds' columns, this function doesn't transform 'y'
'ML_Y_POSPROCESSING'          : [],       #[]=Don't do,  else use a list of strings with the functions you like to apply, using 'preds' as variable
#                                             Example: ['preds*0.8', 'preds.clip(0, np.inf)']
'PRP_Y_WINSORING_REMOVE'      : (0,0),# [ITER] {(float,float)} # ((0,0)=Don't do, (lower_limit,upper_limit)) float ratios percentile from 0.0 to 1.0 #Remove Y outlier rows


#Transformaciones Y_test_preds
###############################
'ML_PRP_Y_APPLY_RESTRICTIONS' : (),#[ITER] {string}    [()]=Don't do, Ejemplo: [('NegativeToZero','Round','ToInt')]


#############################################################################################################
##          FEATURE ENGINEERING            
#############################################

# Join/Diff/Multiply these columns in Test and Train
'PRP_COLS_DIFF'     : (),   #  Don't do = [()] 
           # [([["CoverageField1B","PropertyField21B"],["GeographicField6A","GeographicField8A"]],0,0,'One')],   #  Don't do = [()] 
'PRP_COLS_JOIN'     : (),   #  Don't do = [()]       
'PRP_COLS_MULTIPLY' : (),   #  Don't do = [()] 
       # [([(COMBINACION_COLS)], do_Owen, DROP_New_after, One/Many_2/Many_3 )]
       # Ejemplo1:  [([['y_all']],1,0,'One')]        Ejemplo_2:   [([['SepalLength','SepalWidth'],['PetalLength','PetalWidth']],0,0,'One')] 


'PRP_COUNT_BY_ROWS'       : ([]), # {([values1], [values2] ...)}  [[None, 0, "np.nan", -1, "-1", pd.NaT], [0], [-1]] For each list in values tupel, add an extra column counting the ocurrences of values on each row


#############################################################################################################
##           FEATURE SELECTION             ##
#############################################

# DROP
'PRP_DROP_NEAR_ZERO_VARIANCE'               : 'No',        # (['No']=Don't do)  ejemplo: [0.1]  drops features with variance lower than 0.1
'PRP_DROP_CONSTANTS'                        :  0,        # {uint8} (0=No, 1=Yes)
'PRP_DROP_FEATURES_ALL_NA_IN_TRAIN_OR_TEST' :  0, # {uint8} (0=No, 1=Yes) Aparentemente mejor resultado 0

#Drop these columns from Test and Train
'PRP_DROP_THESE_COLUMNS'                    : [],# 
'PRP_DROP_THESE_COLUMNS_b4_ML'              : [],# 

'PRP_DROP_ALL_BUT_THESE' : [],# [ITER]         (  [[]]=Don't do)   Ejemplo: [['PetalWidth'],['SepalLength'],['IsPretty'],['SepalWidth'],['Color'],['PetalLength']]

'PRP_IMPORT_FEATURES':[],   # accepted formats ['stacked_feature_train_test.csv', ('feat1_train.csv', 'feat1_test.csv'), 'feat1_{}.csv']
                            # features stacked in a single file must have first train rows, and then test rows. it is important when we declare PRP_IMPORT_FEATURES_TRAIN_TEST_ROWS

'PRP_IMPORT_FEATURES_TRAIN_TEST_ROWS':(0,0), 
'PRP_IMPORT_FEATURES_USING_SAME_ROW_ORDER':0, # 1 -Concat operation;  0- merge operation (merge ensures no row order problems)

'PRP_IMPORT_DELETE_DUPLICATED_COLUMNS':'auto', # 'auto', 'Yes', 'No'.  'auto' will only look for duplicates when additional imports are made using PRP_IMPORT_FEATURES (error prone)

#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 




#############################################################################################################
##               ML LEVEL 2                ##
#############################################
'L2_y_test_preds_PADRE_o_HIJO' : 'hijo',  # 'padre' o 'hijo'

'L2_QUERY_WHERE'             : "score_validation > 0.0", #ID_EXECUTION=1612282146476 or ID_EXECUTION=1612281741184   #"score_validation<0.362858467 and score_train>0.06"
'L2_QUERY_ORDER_BY'          : ('ID_EXECUTION','ASC'),

'L2_CV_OR_DATASET'           : 'CV',  # 'CV' will use only Level1 & Level2 with CV (ignoring d[L2_LEVEL1_DATASETS_TRAIN]).  'DATASET' will take into account d['L2_LEVEL1_DATASETS_TRAIN' and TEST]

'L2_LEVEL1_DATASETS_TRAIN'   : 'level1_Train50%pRS_WR(0,0)_Val40%', # Ignored when 'L2_CV_OR_DATASET'=='CV'
'L2_LEVEL1_DATASETS_TEST'    : 'level1_test100%',                   # Ignored when 'L2_CV_OR_DATASET'=='CV'


# # # # # # # # # # # # # # # # # # # # # # #
##               2_ENSEMBLE                ##
# # # # # # # # # # # # # # # # # # # # # # #

# Tomamos en cuenta config de ML Level 2 
# Ignoramos folds, algoritmo, etc...
'L2_ENSEMBLE_NUM_ROUNDS'         : 2,  

# # # # # # # # # # # # # # # # # # # # # # #
##                 CARUANA                 ##
# # # # # # # # # # # # # # # # # # # # # # #

'L2_CARUANA_BY_BLOCKS'           : 0,                        # {uint8} (0=No, 1=Yes)
'L2_CARUANA_BY_BLOCKS_COLUMN_ID' : 'VAR_0005',     # {string}
'L2_CARUANA_MAX_ROUNDS'          : 200,                     # {uint16}
'L2_CARUANA_MAX_ROUNDS_WITHOUT_IMPROVEMENTS' : 10, # {uint16}
'L2_CARUANA_SORT'                : 'none', # {string} ("none", "score", "pearson+score")
'L2_CARUANA_NUM_PREDICTIONS'     : 0,   #{int} (0 = use all, otherwise = ensemble the first N predictions)
'L2_CARUANA_INITIALIZATION'      : [],  # Initial weights


#############################################################################################################
##               ML LEVEL 3                ##
#############################################
'L3_DO_CONVERT_2_RANKING'      : 1,



#############################################
##         EXTERNAL_PREDICTIONS              ##
#############################################
'EXTERNAL_PREDICTIONS_IMPORT' : (), # [ITER] {lista de tuplas} [()]=Don't do  [(cvPredsFile1, testPredsFile1), (cvPredsFile2, testPredsFile2), ..., (cvPredsFileN, testPredsFileN)]

#############################################
##             EXTERNAL_SCRIPTS              ##
#############################################
'EXTERNAL_SCRIPTS_IMPORT' : '', #{string list} ([]=Don't do, 'puntuacion_algoritmo_tituloScript_idScript_autor_NNN')


                                  
#############################################
##           DELETE / OVERFIT              ##
#############################################
# Requires 'LEVEL' == '0_DELETE'
'DELETE_OR_OVERFIT'        : "Delete",   # [ITER] {string} ('Delete' / 'Overfit')
# 'Delete' will totally delete execution from DDBB and everywhere
# 'Overfit' will mark the execution as 'overfit' so it won't be used in Level2, Ensemble...   It still will be there, so it won't run again when tried to be run with same dashboard.
'DELETE_OR_OVERFIT_QUERY_WHERE'  : "ID_EXECUTION=11111111111111111", #ID_EXECUTION=1612282146476 or ID_EXECUTION=1612281741184   #"score_validation<0.362858467 and score_train>0.06"


#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|- 



################################################################################################################################
#      PRINTS & CHARTS
#############################

# prints
'PRINT_ANALYSIS_ON_TRAIN_TEST' : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_CUSTOM_IMPORT'          : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_CUSTOM_IMPORT_DIFFS'    : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_DASHBOARD'              : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_DROP_INFO'              : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_EVAL_INFO'              : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_NA_INFO'                : 1,       # {uint8} (0=No, 1=Yes)
'PRINT_NA_INFO_SHOW_FIELDS'    : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_QUERY_COLS'             : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_TIME'                   : 0,       # {uint8} (0=No, 1=Yes)  final time will always be printed
'PRINT_X_INFO'                 : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_Y_INFO'                 : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_TRANSFORM_Y_INFO'       : 0,       # {uint8} (0=No, 1=Yes)
'PRINT_FEATURE_IMPORTANCE'     : 0,

# charts
'CHART_CONFUSION_MATRIX'       : 0,       # {uint8} (0=No, 1=Yes) 
'CHART_CONFUSION_MATRIX'       : 0,       # {uint8} (0=No, 1=Yes) 
'CHART_CORRELATIONS'           : 0,       # {uint8} (0=No, 1=Yes)  
'CHART_CORRELATIONS_LONG'      : 0,       # {uint8} (0=No, 1=Yes)  This chart takes a looooong time to finish.
'CHART_HISTOGRAMS'             : 0,       # {uint8} (0=No, 1=Yes)
'CHART_BOXPLOT'                : 0,       # {uint8} (0=No, 1=Yes)


#############################################
##            HDF5,SQLITE SAVE             ##
#############################################
'SAVE_HDF5_EXECUTIONS'           : 1,          # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_CORR_MATRIX'          : 1,         # {uint8} (0=No, 1=Yes)
'SAVE_HDF5_FEATURES_IMPORTANCE'  : 1, # {uint8} (0=No, 1=Yes)

'SAVE_SQLITE_EXECUTIONS'         : 1,          # {uint8} (0=No, 1=Yes)

'SAVE_CSV_PREPROCESSED_DATASETS' : 0,     # Exports preprocessed datasets to CSV

'SAVE_FOLDS_SPLIT': 0, # id to export the folds split. i.e.:'my_split' 

#   -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  


#############################################
##                ITERABLE                 ##
#############################################

'ITER_DO_DOUBLE_EXECUTION' : 1, #  Caruana... : If dashboard is trained with a subset, ITERATOR_DO_DOUBLE_EXECUTION performs one extra execution trained with the complete trainset predicting the complete test set

'REITERATE'                : False,
'REITERATE_MAX_LEVELS'     : 5,  # 0 - Infinite 
'REITERATE_GLOBAL_MIN_ENHACEMENT_THRESHOLD'  : -1, # -1 to ignore this threshold
#'REITERATE_FEATURE_MIN_ENHACEMENT_THRESHOLD' : 0.001,

'REITERATE_LEVEL'          : 0, # Internal - USUALLY defined as 0.  Indicates in wich level of reiteration this dashboard has been executed

'ITER_ITERABLE_DASHBOARD'  : True, # USUALLY True.  True - iterable variables are list of values, # False - iterable variables contains values


#############################################
##                BAGGING                  ##
#############################################

'BAGGING': 0,
'BAGGING_ALGORITHM': [],
'BAGGING_ALGORITHM_PARAMS': {},


##########################################
##       NA Imputation based on ML      ##
##########################################
'NA_ML_IMPUTATION'            : False,

'NA_ML_FIELDS'                : 'auto',
'NA_ML_EXTRA_COLUMNS_TO_DROP' :  [],

#'NA_ML_FIELDS' : ['quantity_1', 'quantity_2' ], # Para imputar todas las columnas con NA usar 'auto'.  En otro caso, poner una lista con los nombres de las columnas a imputar
#'NA_ML_EXTRA_COLUMNS_TO_DROP' :  ['material_id'  ,'component_id_2','component_id_1' ,'id', 'quantity_3' ,'component_id_3' ,'quantity_4' ,'component_id_4' ,'component_id_5' ,'quantity_5' ,'component_id_6' ,'quantity_6' ,'component_id_7' ,'quantity_7' ,'component_id_8' ,'quantity_8'], # Columnas que hay que dropear para la imputación, aparte de las ya declaradas en DROP_COLUMS que también se "dropean"

#################################################
##  Parámetros configuración de las funciones  ##
#################################################

#import_data
'IMP_TRAIN_SUBSET_TYPE'         : 'allSamples',   #Para todo el dataset poner 'allSamples'. Otras opciones son: 'booleanExpressions', 'interPercentileRange',  'pPercentageStratifiedRandomSamples','pPercentageRandomSamples', 'nConsecutiveSamples', 'nRandomSamples'
'IMP_TRAIN_PERCENTAGE'          : 50,                            #Sólo se usa con TRAIN_SUBSET_TYPE:  'pPercentageRandomSamples'
'IMP_TRAIN_NUM_SAMPLES'         : 1000,                        #Sólo se usa con TRAIN_SUBSET_TYPE:  'nConsecutiveSamples' o TRAIN_SUBSET_TYPE:  'nRandomSamples'
'IMP_TRAIN_FIRST_ROW'           : 1,                          #Sólo se usa con TRAIN_SUBSET_TYPE:  'nConsecutiveSamples'
'IMP_TRAIN_INTER_PERCENTILE_RANGE': (0.0, 1.0),                #Sólo se usa con TRAIN_SUBSET_TYPE:  'interPercentileRange'
'IMP_TRAIN_SUBSET_EXPRESSION_LIST': [],         # Sólo se usa con TRAIN_SUBSET_TYPE:  'booleanExpressions'  Example: ["df['YEAR']<2018", 'df.DAY.isin(1,2,3)']
'IMP_TRAIN_STRATIFIED'          : [], ### NO RELLENAR ###, se rellena automáticamente en función de IMP_TRAIN_SUBSET_TYPE y SPLIT_CV_KFOLDS

'IMP_TEST_SUBSET_TYPE'          : 'allSamples',    #Para todo el dataset poner 'allSamples'. Otras opciones son: 'pPercentageStratifiedRandomSamples','pPercentageRandomSamples', 'nConsecutiveSamples', 'nRandomSamples'
'IMP_TEST_PERCENTAGE'           : 10,                             #Sólo se usa con TEST_SUBSET_TYPE:  'pPercetageRandomSamples'
'IMP_TEST_NUM_SAMPLES'          : 1000,                         #Sólo se usa con TEST_SUBSET_TYPE:  'nConsecutiveSamples' o TEST_SUBSET_TYPE:  'nRandomSamples'
'IMP_TEST_FIRST_ROW'            : 1,                           #Sólo se usa con TEST_SUBSET_TYPE:  'nConsecutiveSamples'

'IMP_TRAIN_ENCODING_TYPE'       : 'utf-8',
'IMP_TRAIN_DELIMITER'           : ',',
'IMP_TEST_ENCODING_TYPE'        : 'utf-8',
'IMP_TEST_DELIMITER'            : ',',

'IMP_LOAD_GROUND_TRUTH_ONLY'    : 0,  # 0 - Normal data load,  1 - Custom import will ignore all the columns except COMP_COLUMN_ID and COMP_COLUMN_CLASS

'META_TRAIN_SPLIT_DESC'         : 'level1_Train100%_WR(0,0)_CV', # Internal, don't modify

'SEPARATOR_COLS_JOIN'           : "|+|",
'SEPARATOR_COLS_DIFF'           : "|-|",
'SEPARATOR_COLS_MULTIPLY'       : "|*|",
'SEPARATOR_DATE_ENCODE'         : "|Enc-Dat|",
'SEPARATOR_DATE_OWEN2FEAT'      : "|Enc-Dat-OW2FEAT|",
'SEPARATOR_HOT_ENCODE'          : "|OHE|",
'SEPARATOR_LABEL_ENCODE_LEX'    : "|Enc-Lex|",
'SEPARATOR_MONOTONIC'           : "|Monot|",
'SEPARATOR_NUMBER_ENCODE'       : "|Enc-Num|",
'SEPARATOR_NA_BINARIZE'         : "|NA-bin|",
'SEPARATOR_NA_COUNT'            : "|NA-count_{}|",
'SEPARATOR_OWEN'                : "|OW|",
'SEPARATOR_QUANTILES'           : "|Quant|",
'SEPARATOR_TFIDF'               : "|TFIDF|",

#split_train_test_components
'SPLIT_VALIDATION_RANDOM_STATE' : 0,
'ML_ALGO_PARAMETROS'            : [],  ### NO RELLENAR ### [ITER] {list of dictionaries} ### NO RELLENAR ###, se rellena automáticamente en función de la configuración de algoritmos




#########################################
##                STEPS                ##
#########################################

'SYS_DO_IMPORT'            : 1,

'SYS_DO_CUSTOM_IMPORT'     : 0,          # requiere definir el CUSTOM_IMPORT() más abajo
'SYS_DO_CUSTOM_ML'         : 0,          # requiere definir el CUSTOM_ML() más abajo
'SYS_DO_CUSTOM_SUBMISSION_PRE' : 0,          # requiere definir el CUSTOM_SUBMISSION_PRE() más abajo
'SYS_DO_CUSTOM_SUBMISSION_CSV' : 0,          # requiere definir el CUSTOM_SUBMISSION_CSV() más abajo

'SYS_DO_SUBMISSION'        : 1,            #            {uint8} (0=No, 1=Yes)
'ML_FEATURE_IMPORTANCE'    : 0,           # {uint8} (0=No, 1=Yes) 
'ML_MODEL_TO_PICKLE'       : 0,            #            {uint8} (0=No, 1=Yes)
'SYS_SEND_EMAIL'           : 0,            #            {uint8} (0=No, 1=Yes)


#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  


#############################
#  COMPETITION CONFIGURATION
#############################

#Special Column Names (useful for submissions)
'COMP_NAME'                      : "rossmann",                 # {string}
'COMP_PROBLEM_TYPE'              : 'Regr',              # {string} ('Regr', 'Class_B', 'Class_M')

'COMP_COLUMN_ID'                 : "Id",                  # {string}
'COMP_COLUMN_CLASS'              : "Sales",            # {string}
'COMP_COLUMN_CLASS_4_SUBMISSION' : "",    # {string}

'COMP_TRAIN_FILES'               : ['train.csv'],      # {string} list of strings
'COMP_TEST_FILES'                : ['test.csv'],       # {string} list of strings
'COMP_Y_TEST_FILE'               : 'y_test.csv',     # {string} list of strings

  #CLASSIFICATION     #Acc - Accuracy Score    #F1 - F1 Score    #Log  - Log Loss
  #REGRESSION         #Gini - Gini Score       #MSE  - Mean Squared Error
'COMP_EVALUATION_METRIC'         : "RMSPE",       # {string} ("RMSLE","Gini")
'COMP_EVALUATION_METRIC_GREATER_IS_BETTER' : 0, # {uint8} (0=No, 1=Yes)

'ML_PREDICT_PROBA'               : 0, # Para Clasificación ¿binaria?, si calcula la clase o la prob de cda clase
'ML_CLASS_PROBA'                 : 0, # Para ¿multiclase?





#Convert Column to DATE in Train & Test
'PRP_COLUMN_2_DATE'              : [],        # {[string]} 

#Change Column Names in Train & Test
'PRP_CHANGE_COL_NAMES_OLD'       : [],        # {[string]} 
'PRP_CHANGE_COL_NAMES_NEW'       : [] ,       # {[string]}

#  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  

# -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  -|-  
}


if 'CONDA_DEFAULT_ENV' in os.environ:
    default_dashboard['ANACONDA_ENVIRONMENT']=os.environ['CONDA_DEFAULT_ENV']
else:
    default_dashboard['ANACONDA_ENVIRONMENT']='--default--'
