# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 18:16:04 2016

@author: DataCousins
"""
############################################
### IMPORTS - Respetar ORDEN ALFABÉTICO  ###
############################################

from colorama import Fore, Back, Style
import sys

from my_Input_Output import *


############################################
###               FUNCIONES              ###
############################################

def read_level1_metafeatures(d, results):
    
    # Comprobamos que hay ejecuciones previas de level 1
    if not os.path.exists(d['DIR_DATA']+'executions_data.hdf5') or not os.path.exists(d['DIR_DATA']+'executions_info.db'):
        sys.exit("Error: Estas intentando ejecutar LEVEL2 sin ejecuciones previas de LEVEL1")


    ############################################
    ###  Select desired HDF5 Level1 datasets ###
    ############################################ 

    level1_dataset_idx_val_selected = d['L2_LEVEL1_DATASETS_TRAIN']+'_idx_val'
    level1_dataset_y_val_selected = d['L2_LEVEL1_DATASETS_TRAIN']+'_y_val'
    level1_dataset_y_val_preds_selected = d['L2_LEVEL1_DATASETS_TRAIN']+'_y_val_preds'
    level1_dataset_idx_test_selected = d['L2_LEVEL1_DATASETS_TEST']+'_idx_test'
    level1_dataset_y_test_preds_selected = d['L2_LEVEL1_DATASETS_TEST']+'_y_test_preds'


    
    ############################################
    ##  TRAIN:Selecting desired id_executions ##
    ############################################  
    
    # Añadimos a la query la condicion del dataset level1_dataset_y_val_preds_selected
    y_val_preds_selection_query = "hdf5_dataset_y_val_preds_name = '"+level1_dataset_y_val_preds_selected+"'"     
    
    query_orderby_field = d['L2_QUERY_ORDER_BY'][0]
    query_orderby_direction = d['L2_QUERY_ORDER_BY'][1]
    
    select_list = ['ID_FAMILY', 'ID_EXECUTION'] + [query_orderby_field]

    
    if d['L2_QUERY_WHERE'] == "":
        query_where = y_val_preds_selection_query + " AND overfit=0"
    elif d['LEVEL'][0:2] == '2_' and d['L2_CV_OR_DATASET'] == 'CV':
        query_where = d['L2_QUERY_WHERE'] + " AND overfit=0"
    else:
        query_where = y_val_preds_selection_query + " AND " + d['L2_QUERY_WHERE'] + " AND overfit=0"
    
    #query_where+=" AND IMP_TRAIN_SUBSET_TYPE='{}'".format(d['IMP_TRAIN_SUBSET_TYPE'])
    query_where+=" AND META_TRAIN_SPLIT_DESC='{}'".format(d['L2_LEVEL1_DATASETS_TRAIN'])
    

    
    # Ejecutamos query para obtener los id_executions (en esta ejecución ordenamos por id_execution)
    family_ids_requested_list,ids_executions_requested_list,orderby_field_values = sqlite_query_select_id_executions(select_list,query_where,'ID_EXECUTION','ASC',d['DIR_DATA'])

    
    # Ejecutamos query para obtener los id_executions (en esta ejecución ordenamos por d['L2_QUERY_ORDER_BY'])
    family_ids_requested_list_ordered,ids_executions_requested_list_ordered,orderby_field_values_ordered = sqlite_query_select_id_executions(select_list,query_where,query_orderby_field,query_orderby_direction,d['DIR_DATA'])


    #############################################################
    ##  Selecting common HDF5 id_executions in TRAIN and Test  ##
    ############################################################# 
    
    TRAIN_ids_executions_found, TRAIN_ids_executions_not_found = hdf5_find_id_executions(d, ids_executions_requested_list,level1_dataset_y_val_preds_selected, d['DIR_DATA'], d['L2_CV_OR_DATASET'])
    
    
    if d['L2_y_test_preds_PADRE_o_HIJO'].lower()=='padre':
        tomar_ejecuciones_padre = True
    elif d['L2_y_test_preds_PADRE_o_HIJO'].lower()=='hijo':
        tomar_ejecuciones_padre = False
    else:
        sys.exit("Error: La variable de dashboard L2_y_test_preds_PADRE_o_HIJO puede tomar solamente los valores padre o hijo")
        
    if tomar_ejecuciones_padre == True:      
        Test_ids_executions_found,  Test_ids_executions_not_found  = hdf5_find_id_executions(d, family_ids_requested_list,level1_dataset_y_test_preds_selected,d['DIR_DATA'], d['L2_CV_OR_DATASET'])
        indexes_executions_TRAIN_found = [i for i, item in enumerate(ids_executions_requested_list) if item in set(TRAIN_ids_executions_found)]
        indexes_executions_Test_found = [i for i, item in enumerate(family_ids_requested_list) if item in set(Test_ids_executions_found)]
        common_indexes = sorted(list(set(indexes_executions_TRAIN_found) & set(indexes_executions_Test_found)))
        TRAIN_ids_executions = [ids_executions_requested_list[index] for index in common_indexes]
        Test_ids_executions =      [family_ids_requested_list[index] for index in common_indexes]
    else:
        Test_ids_executions_found,  Test_ids_executions_not_found  = hdf5_find_id_executions(d, ids_executions_requested_list,level1_dataset_y_test_preds_selected,d['DIR_DATA'], d['L2_CV_OR_DATASET'])
        common_ids_executions = sorted(list(set(TRAIN_ids_executions_found) & set(Test_ids_executions_found)))
        TRAIN_ids_executions = common_ids_executions.copy()          
        Test_ids_executions  = common_ids_executions.copy()
    
    # Ojo, en el nombre de las columnas estoy utilizando las del Train, que son las de y_val_preds
    # Las del test serían las de las ejecuciones padre, pero como deben llevar el mismo nombre tomamos las del Train
    columns_label = [str(x) for x in TRAIN_ids_executions]         
    

    # Guardamos información para Resultados
    results['levelN_TRAIN_ids_executions'] = TRAIN_ids_executions
    results['levelN_Test_ids_executions'] = Test_ids_executions


    
    if len(TRAIN_ids_executions) ==0:
        if d['LEVEL'][0:2]=='2_':
            the_level = 'LEVEL_2'
        else:
            the_level = ''
        print()
        print(Fore.RED + "---------------------------\n " + the_level + " EXECUTIONS ERROR:" + Style.RESET_ALL)
        print()
        print()
        print(Fore.RED + "Your query '" + d['L2_QUERY_WHERE'] + "' is NOT returning any valid Execution_IDs."  + Style.RESET_ALL)
        
        if d['LEVEL'][0:2]=='2_' and d['L2_CV_OR_DATASET'] == 'CV':
            print(Fore.RED + "\tThe variable d['L2_CV_OR_DATASET'] is set to 'CV', so we are trying to take all executions from your query that have a FULL out-of-fold train set."  + Style.RESET_ALL)
        else:
            print(Fore.RED + "\tThe variable d['L2_CV_OR_DATASET'] is set to 'DATASET', so we are only taking Train executions from your query AND the following dataset: " + str(d['L2_LEVEL1_DATASETS_TRAIN'])  + Style.RESET_ALL)
        print()
        
        print(Fore.RED + "----------------------------------------------------------------------" + Style.RESET_ALL)
        print()
        sys.exit()
    
    ############################################
    ###   TRAIN: Read executions_data.hdf5   ###
    ############################################ 

    
    # Construimos TRAIN__pd
    X_TRAIN_read = hdf5_read_execution_data (d, TRAIN_ids_executions, level1_dataset_y_val_preds_selected, d['DIR_DATA'])
    
    X_TRAIN_read__pd = pd.DataFrame(data=X_TRAIN_read, columns=columns_label)
    
   
    idx_TRAIN_read__pd = hdf5_read_element(level1_dataset_idx_val_selected, d['DIR_DATA'], [d['COMP_COLUMN_ID']])
    y_TRAIN_read__pd = hdf5_read_element(level1_dataset_y_val_selected, d['DIR_DATA'], [d['COMP_COLUMN_CLASS']])
    



    ############################################
    ###   Test: Read executions_data.hdf5    ###
    ############################################ 

    # Analogamente construimos test__pd
    X_test_read = hdf5_read_execution_data (d, Test_ids_executions, level1_dataset_y_test_preds_selected, d['DIR_DATA'])       
    X_test_read__pd = pd.DataFrame(data=X_test_read,columns=columns_label)
    idx_test_read__pd = hdf5_read_element(level1_dataset_idx_test_selected, d['DIR_DATA'], [d['COMP_COLUMN_ID']])




    ############################################
    ###   Ordenamos las columnas de          ###
    ###   X_TRAIN_read__pd y X_test_read__pd ###
    ###   según d['L2_QUERY_ORDER_BY']       ###
    ############################################ 

    # las columnas de X_TRAIN_read__pd y X_test_read__pd, llevan como nombre
    # los id_execution de las ejecuciones hijo y_val_preds, según se indicaba más arriba

    # convertimos a string las id_executions ordenadas
    str_ids_executions_requested_list_ordered = [str(x) for x in ids_executions_requested_list_ordered]
    
    # ordenamos los nombres de las columnas según str_ids_executions_requested_list_ordered
    columns_label_ordered = [item for i, item in enumerate(str_ids_executions_requested_list_ordered) if item in set(columns_label)]

    # Guardamos información para Resultados
    results['levelN_columns_label_ordered'] = columns_label_ordered

    # Imprimimos columns_label_ordered
    print('Executions ordered by ',d['L2_QUERY_ORDER_BY'][0],d['L2_QUERY_ORDER_BY'][1])
    print(columns_label_ordered)
    
    # aplicamos ese orden de columnas a X_TRAIN_read__pd y X_test_read__pd
    X_TRAIN_read__pd = X_TRAIN_read__pd[columns_label_ordered]
    X_test_read__pd = X_test_read__pd[columns_label_ordered]

    return idx_TRAIN_read__pd, X_TRAIN_read__pd, y_TRAIN_read__pd, idx_test_read__pd, X_test_read__pd, columns_label_ordered