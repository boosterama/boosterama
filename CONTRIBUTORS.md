Contributors of boosterama/boosterama
=====================================

boosterama has been developed and used by, in alphabetical order:

* Fernando Constantino Aguilera
* Javier Tejedor Aguilera
* Virilo Tejedor Aguilera

They call themselves DataCousins, DataPrimos in Spanish, and have participated as a Kaggle team on several competitions using this software
 
Everyone is more than welcomed, it is a great way to make the project better and more accessible to more users.

Comitters
---------
Committers are people who have made substantial contribution to the project and granted write access to the project. In alphabetical order:

* [Fernando Constantino Aguilera](), 
  - He created this project with Javier and Virilo (DataCousins).
* [Javier Tejedor Aguilera](), 
  - He created this project with Virilo and Fernando (DataCousins).
* [Virilo Tejedor Aguilera](https://bitbucket.org/virilo/), 
  - He created this project with Fernando and Javier (DataCousins).

Become a Comitter
-----------------
boosterama is a opensource project and we are actively looking for new comitters who are willing to help maintaining and lead the project.
Committers comes from contributors who:
* Made substantial contribution to the project.
* Willing to spent time on maintaining and lead the project.

New committers will be proposed by current comitter memembers, with support from more than two of current comitters.

List of Contributors
--------------------

Come on guys and girls!  It's time for real automated Machine Learning software!
